# This directory contains STM32 Firmware projects

The DPG Project does not use any STM32, but Microcontrollers are used in the development process to server as Test Data Sources or Devices under Test.

Each project should be in a subdirectory, where the Name contains the used Microcontroller (e.g. F401_SPI_Slave).

# Overview

This project configures the SPI2 in Slave mode, for testing the Aardvark SPI Master.

Each received byte will be sent back negated on the next transfer.

## Pinning

SPI pins are configured as follows:

| SPI-Pin | Nucleo-Pin | Aardvark Pin |
|---------|------------|--------------|
|   MISO  |   PC2      |      5       |
|   MOSI  |   PC3      |      8       |
|   SCKL  |   PB10     |      7       |
|    CS   |   PB12     |      9       |

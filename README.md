# DPG Digitaler Protokollgenerator

<!-- vim-markdown-toc GFM -->

- [Quickstart Guide](#quickstart-guide)
  - [Hardware Setup](#hardware-setup)
    - [creating the Linux image](#creating-the-linux-image)
    - [configuring the board](#configuring-the-board)
    - [Booting Linux](#booting-linux)
    - [Starting the application](#starting-the-application)
  - [Connecting the SPI](#connecting-the-spi)
  - [Software user manuel](#software-user-manuel)
    - [main.cpp](#maincpp)
    - [Intanziation and initialization](#intanziation-and-initialization)
    - [Sending of a configuration](#sending-of-a-configuration)
    - [Sending and receiving data](#sending-and-receiving-data)
    - [Transfer data](#transfer-data)
    - [Analyse data](#analyse-data)

<!-- vim-markdown-toc -->

For more Documentation, see the Latex document [here](doc/system_documentation)

# Quickstart Guide

## Hardware Setup

### creating the Linux image

A Linux image containing all required components can be built using [buildroot](buildroot.org).

For a local build, follow the instructions in [buildroot/README.md](buildroot/README.md).

Alternatively a version built by the CI can be used. For that visit the [package registry](https://gitlab.fh-ooe.at/hsd/dpg/-/packages), select the dpg_image package and download the latest image.
Note, that the file is compressed using gzip, and must be unpacked using `gunzip` or 7Zip (on Windows) before using it further.

Then flash the image to the SD Card and plug it into the DE1-SoC board.

### configuring the board

On the DE1-SoC, the MSEL Switches have to be set correctly.
The MSEL (small SMD switches on the bottom of the board) mode must be set to "00010" (Fast Passive Parallel x16, optional encryption).

For correct operation the board must be connected to the local network with Ethernet.

### Booting Linux

To get a Serial Connection to the Linux System and Bootloader, connect a USB Mini B cable to the port directly over the SD Card slot.
The serial is configured to 115200 baud, 8 bit data, no parity.

Power on the board now, after a few seconds all 7-Segment displays should read '0'.

### Starting the application 

The hps_webserver will try to start automatically, if you know the ip address of the board you can skip this step.

To get the IP Address of the system, connect the Serial console and log in. The Login root with password root is enabled by default for all alpha releases.

In the shell, run `ip address` to print all known IP Addresses. Use the address for ethernet (usually called enpXsY or enoX).

If there is no address for the ethernet, try running `udhcpc` which tries to
get a new address from DHCP. Normally udhcpc is started on boot, but if it took
to long to get the network up, it sometimes doesn't get a lease.
If the local network does not provide a DHCP server, the IP Address must be
manually configured using the `ip` utility.

## Connecting the SPI

Currently the SPI Pins are located on the GPIO0 header on the following Pins:

| Functionality | Pin      |
| ---           | ---      |
| MOSI          | GPIO0_D35 |
| MISO          | GPIO0_D33 |
| SCK           | GPIO0_D31 |
| CS            | GPIO0_D29 |

The Pinout of the relevant Part of the GPIO0 Header is Summarised here. Direction: Ethernet Port points up

| ...       | ...        |
| ---       | ---
| VCC 3.3V  | GND        |
| D26       | D27        |
| D28 (GND) | D29 (CS)   |
| D30 (GND) | D31 (SCK)  |
| D32 (GND) | D33 (MISO) |
| D34 (GND) | D35 (MOSI) |

When connecting the SPI pins, care must be taken when choosing the cable.
To get a good signal integrity, use a ribbon cable, where signal pins and ground pins alternate.
For example the following Cable layout works well with the current SPI pinout:

    ---------------------------------
                 GND
    ---------------------------------
                 MOSI
    ---------------------------------
                 GND
    ---------------------------------
                 MISO
    ---------------------------------
                 GND
    ---------------------------------
                 SCK
    ---------------------------------
                 GND
    ---------------------------------
                 CS
    ---------------------------------

The GND lines between data reduce crosstalk between the data lines. Without those pins, crosstalk will be a problem for cables longer than about 10cm.

If you do not have a ribbon cable, that can be used, use single cables for all
data lines. If you have crosstalk problems, try to route the cables more
"chaotic", so they don't run parallel for long distances.

## Software user manuel
In this section, a sample programme is created step by step to illustrate the use of the Dpg-Api.
For further informations and/or detailed descriptions of the functions and objects please open
system_documentation.pdf.

### main.cpp
After the project has been created, a new C++ file must be added to source files. Here the file
was named "main.cpp". Once the file has been created, the following lines of code can be inserted:
```cpp
#include "DPG_Api.h"
#include "SpiConfiguration.h"
#include "SpiData.h"
#include "json.hpp"
using namespace std;
using namespace dpg;

int main() {
    try
    {
        // enter your code here
    }
    // catch error messages from DPG-Api
    catch (string const& error){
        cerr << error << endl;
    }
    // catch error messages from JSON-lib.
    catch (exception const& errJSON) {
        cerr << errJSON.what() << endl;
    }
    // catch every other messages
    catch (...) {
        cerr << "Unexpected error!" << endl;
    }

    return 0;
}
```
Within the try block, the code segments below are inserted.

### Intanziation and initialization
In order to use the Dpg-Api, a DpgInterface object must first be created. The object must be given a Uniform
Resource Identifier (URI) consisting of the IP address of the server and a suitable port. Then the DpgInterface
object must be initialised. To do this, the Init() function is called and the desired communication type is transferred.
```cpp
// object for communication between PC and FPGA
DpgInterface dpg("10.24.99.105:8080");  // example 1 - with ip-address
//DpgInterface dpg("localhost:8080");   // example 2 - with "localhost"
if (!dpg.Init(IfTypes::SPI)) {
    cout << "ERROR in main.c: 'dpg.Init()' was unable to initialize DPG-member." << endl;
    return -1;
}
```

### Sending of a configuration
After the initialisation has been successful, a configuration can be created and sent to the server. The SendConfig()
function is used for this purpose.
!!ATTENTION!!:
Since SendConfig() requires a unique_ptr, the created configuration object must be passed to the function via move and
is not available for further use afterwards.
```cpp
// object for configuration of the wanted interface
unique_ptr<SpiConfiguration> SpiConfig = make_unique<SpiConfiguration>(1000000, SpiConfiguration::SpiMode::cpolLow_shiftedRising);
if (!dpg.SendConfig(move(SpiConfig))) {
    cout << "ERROR in main.c: 'dpg.SendConfig()' was unable to send configuration to server." << endl;
    return -2;
}
```

### Sending and receiving data
The two functions SendData() and ReceiveData() are available for sending and receiving data. To be able to send data, they must first be created and filled. The data can then be sent using the SendData() function (!!ATTENTION!! unique_ptr again). A TransferHdl object is returned. This object must be given to the ReceiveData() function. !!ATTENTION!! - this function works blocking without time-out handling!
Both functions can be used to control how and when the data is sent or received. They can be called and used as often as desired, also independently of the function TransferData().
```cpp
// object for data collection of the wanted interface
unique_ptr<SpiData> SpiDataColl = make_unique<SpiData>();
for (int i = 0; i < 50; i++) {
    SpiDataColl->AddData(char(i));
}
// send data to FPGA
shared_ptr<TransferHdl> SpiHdl = dpg.SendData(move(SpiDataColl));
if (SpiHdl == nullptr) {
    cout << "ERROR in main.c: 'dpg.SendData()' was unable to send data to server." << endl;
    return -3;
}
// receive data from FPGA
Transfer recvData = { TransferState::TRANSFER_ONGOING, nullptr };
recvData = dpg.ReceiveData(SpiHdl);
if (recvData.state == TransferState::TRANSFER_UNDEF) {
    cout << "ERROR in main.c: 'dpg.ReceiveData()' was unable to receive data from server." << endl;
    return -3;
}
```

### Transfer data
For a secure and simple transfer, the TransferData() function can be used. This function is passed an entire STL container (e.g.: a vector).
The vector must be filled with data before. The function then takes over the sending and receiving.
First, all data stored in the vector is sent. Secondly, an attempt is made to retrieve the first data package sent, from the server.
If it has not yet been received by the master, the receiving is attempted again. It is important to know that the TransferData() function has a
build in time-out handling!
```cpp
// object for data collection of the wanted interface
vector<Data::UPtr> SpiDataCollVec;
for (int i = 0; i < 10; i++) {
    unique_ptr<SpiData> SpiDataColl = make_unique<SpiData>();
    for (int j = 0; j < 50; j++) {
        SpiDataColl->AddData(char(j));
    }
    SpiDataCollVec.push_back(move(SpiDataColl));
}
// transfer data to FPGA
vector<Data::UPtr> recvDataVec = dpg.TransferData(SpiDataCollVec);
```

### Analyse data
In order to be able to evaluate the received data, a simple function is available. With AnalyzeData(), the received data can be compared with expected data and displayed in different ways.
Firstly, there is a console output that compares the data and also outputs a visual confirmation (text output with "OK" or "NOK").
Secondly, there is a pure check whether the data are identical. If there is a discrepancy, it is aborted at this point and the position inside the vector is returned via the return value.
```cpp
// --> e.g.: print on the console
unique_ptr<SpiData> SpiDataCollExp = make_unique<SpiData>();
for (int i = 0; i < 50; i++) {
    SpiDataCollExp->AddData(char(255-i));
}
dpg.AnalyzeData(move(recvData.data), move(SpiDataCollExp), OutSel::CONSOLE);

// --> e.g.: compare received and expected data in the programm
vector<Data::UPtr> SpiDataCollVecExp;
for (int i = 0; i < 10; i++) {
    unique_ptr<SpiData> SpiDataCollExp = make_unique<SpiData>();
    for (int j = 0; j < 50; j++) {
        SpiDataCollExp->AddData(char(255-j));
    }
    SpiDataCollVecExp.push_back(move(SpiDataCollExp));
}
for (int i = 9; i >= 0; i--) {
    int result = dpg.AnalyzeData(move(recvDataVec[i]), move(SpiDataCollVecExp[i]), OutSel::NONE);
    if (result != 0) {
        cout << "ERROR in main.c: transfered data don't match expected data!" << endl;
        return -5;
    }
    else {
        cout << "main.c: transfered data at vector index [" << i << "] match expected data!" << endl;
    }
}
cout << endl << "main.c: EXITING!" << endl;
```

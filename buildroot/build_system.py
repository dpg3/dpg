#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from pathlib import Path
import subprocess
import shutil
import stat
import os

# Constants

PRJ_PATH = Path(__file__).parents[1].absolute()
WEBSERVER_PATH = PRJ_PATH / Path('hps/code/hps_webserver')
DE1_SOC_EXTERNAL = PRJ_PATH / 'buildroot/external/board/de1-soc'
ROOTFS_OVERLAY = DE1_SOC_EXTERNAL / 'rootfs-overlay'

# Functions
def print_header(title: str):
    print("============================================================")
    print(title.center(60))
    print("============================================================")


# Configuration

parser = argparse.ArgumentParser()
parser.add_argument(
    "--hw",
    help="Synthesize a VHDL Testbed, provide path to quartus folder. If not provided skip vhdl synthesis",
)
parser.add_argument(
    "--clean-toolchain",
    action="store_true",
    help="Always build a clean toolchain, even if a toolchain is present",
)
parser.add_argument(
    "--clean-buildroot",
    action="store_true",
    help="Cleanbuild the buildroot system",
)
args = parser.parse_args()


print("Working in Project ", PRJ_PATH)

hw: str | None = args.hw

if hw is not None:
    hw_path = Path(hw).absolute().resolve()
    out_path = hw_path / "output_files"
    if out_path.exists():
        shutil.rmtree(out_path)
    out_path.mkdir()
    os.chmod(
        out_path, stat.S_ISGID | 0o0774
    )  # Set GID bit to keep permissions of generated files by docker

    print("Synthesizing hw testbed ", hw_path)
    docker_hw_path = str(hw_path.relative_to(PRJ_PATH))
    QUARTUS_DOCKER_BASE = f'docker run --rm --network host -v "{PRJ_PATH.absolute()}:/project" alexanderdaum/quartus-lite:18.1.1 /bin/bash -c "{{cmd}}"'

    subprocess.call(
        QUARTUS_DOCKER_BASE.format(
            cmd=f"/project/ci/synth_vhdl_testbed.sh /project/{docker_hw_path} /project/buildroot/external/board/de1-soc/soc_system.rbf",
            flags="",
        ),
        shell=True,
    )

# hps webserver
print_header("Compiling HPS Webserver")

CROSS_DOCKER_BASE = f'docker run --rm -v "{PRJ_PATH.absolute()}:/project" -v "/var/run/docker.sock:/var/run/docker.sock" alexanderdaum/cargo-cross:1.66 /bin/bash -c "{{cmd}}"'
subprocess.call(
    CROSS_DOCKER_BASE.format(
        cmd=f"cd /project/{WEBSERVER_PATH.relative_to(PRJ_PATH)} && CROSS_DOCKER_IN_DOCKER=true cross b --release --target=armv7-unknown-linux-gnueabihf"
    ),
    shell=True,
)

built_server = WEBSERVER_PATH / 'target/armv7-unknown-linux-gnueabihf/release/hps_webserver'
rootfs_usr_bin = ROOTFS_OVERLAY / 'usr/bin'
if not rootfs_usr_bin.exists():
    rootfs_usr_bin.mkdir()
shutil.copyfile(built_server, rootfs_usr_bin.resolve() / 'hps_webserver')

# Toolchain
BUILDROOT_DOCKER_BASE = f'docker run --rm -v "{PRJ_PATH.absolute()}:/project" alexanderdaum/buildroot:2022-08 /bin/bash -c "{{cmd}}"'

toolchain_path = (
    PRJ_PATH / "buildroot" / "external" / "arm-dpg-linux-gnueabihf_sdk-buildroot.tar.gz"
)
toolchain_builddir = PRJ_PATH / "toolchain"

if args.clean_toolchain:
    print_header("Cleaning Toolchain artifacts")
    if toolchain_path.exists():
        toolchain_path.unlink()
    if toolchain_builddir.exists():
        shutil.rmtree(toolchain_builddir)

if not toolchain_path.exists():
    print_header("Building the toolchain")
    # build the toolchain
    if not toolchain_builddir.exists():
        toolchain_builddir.mkdir(mode=stat.S_ISGID | 0o0774)
    sdk_file_relative = toolchain_path.resolve().relative_to(PRJ_PATH)

    subprocess.call(
        BUILDROOT_DOCKER_BASE.format(
            cmd=f"cd /project && bash /project/ci/build_toolchain.sh {sdk_file_relative}"
        ),
        shell=True,
    )

    os.chmod(toolchain_path, 0o666)

# Buildroot

buildroot_path = PRJ_PATH / "output"

if args.clean_buildroot:
    print_header("Cleaning Buildroot artifacts")
    if buildroot_path.exists():
        shutil.rmtree(buildroot_path)

if not buildroot_path.exists():
    buildroot_path.mkdir(mode=stat.S_ISGID | 0o0774)

subprocess.call(
    BUILDROOT_DOCKER_BASE.format(
        cmd=f"cd /project/output && make -C /buildroot-2022.08.3/ O=/project/output BR2_EXTERNAL=/project/buildroot/external dpg_defconfig && make -s"
    ),
    shell=True,
)

generated_image = PRJ_PATH / "output" / "images" / "sdcard.img"
print("Image should be generated in ", generated_image.absolute())


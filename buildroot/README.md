# Buildroot System

This directory contains a buildroot project to create a full Linux system for the DPG board.

To build the system, you need a Linux System, running the python script on Windows does not work, 
but you can use WSL2.

## Overview

Not all parts of the project are built by buildroot. The VHDL synthesis and rust webserver compilation
are not done with buildroot.

There are two ways to use buildroot with this project. You can either compile all parts separately
and run the buildroot command yourself, or you can use the automated python build script.

## Automated Build

To use the automated build, you must first install the necessary dependencies:

- Docker
- Python3
- Pipenv for Python3

To run the script use `pipenv run ./build_system.py --hw {path_to_vhdl_dir}`

If the --hw argument is not specified, no vhdl testbed will be synthesized.
For other supported arguments check `pipenv run ./build_system.py -h`

If the script fails, make sure the docker daemon is running.

### Cleanup

The build processes in the docker containers run as root and generate build artifacts owned by root.
For most build dirs the python script sets the SETGID bit, so the users group should have access to the files.

But it can still happen, that artifacts without wrong access rights are created. The easiest way to remove then is running `sudo git clean -fdx`.
Please be aware, that this command deletes ALL untracked and ignored files from the git repo, not only build artifacts.
So make sure all files you want to keep are committed first.

## Manual Build

To launch buildroot, the official Vagrantfile is used, so a consistent environment can be created.
Alternatively the buildroot Docker image from this repo located [here](../docker/buildroot) can be used.

### Building the Toolchain

The toolchain is built separately, so it can be reused over multiple builds.

To compile the toolchain, start a buildroot vagrant or docker environment and start a new out-of-tree build using

    mkdir toolchain && cd toolchain
    make -C PATH_TO_BUILDROOT O=$(pwd) BR2_EXTERNAL=/project/buildroot/external dpg_toolchain_defconfig
    make sdk
    cp images/arm-dpg-linux-gnueabihf_sdk-buildroot.tar.gz /project/buildroot/external/arm-dpg-linux-gnueabihf_sdk-buildroot.tar.gz

### Building the System

After building the toolchain (or getting an existing build), the system can be built.

We do this in a separate build dir

    mkdir dpg_build && cd dpg_build
    make -C PATH_TO_BUILDROOT O=$(pwd) BR2_EXTERNAL=/project/buildroot/external dpg_defconfig
    make
    # Result is now in images/sdcard.img

### Update the soc_system

To update the synthesized hardware, choose the correct Testbed and synthesize it, then copy the generated .rbf file to `external/board/de1-soc/soc_system.rbf`.

### Including the hps_webserver

The HPS Webserver Rust application is currently not integrated in buildroot.
Cross compile the hps_webserver application beforehand using cross-rs and add it to the rootfs overlay in external/board/de1-soc/rootfs-overlay.

The webserver executable should be `external/board/de1-soc/rootfs-overlay/usr/bin/hps_webserver` for autostart to work correctly.


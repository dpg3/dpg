#!/bin/sh
set -xe

# To pack the FPGA configuration .rbf into the image
# we need to put it in the output directory first.
cp ${BR2_EXTERNAL_DE1SOC_CUSTOM_PATH}/board/de1-soc/soc_system.rbf ${BINARIES_DIR}/soc_system.rbf

# Generate the image using the default buildroot tool
support/scripts/genimage.sh -c ${BR2_EXTERNAL_DE1SOC_CUSTOM_PATH}/board/de1-soc/genimage.cfg

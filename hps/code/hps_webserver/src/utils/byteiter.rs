/// An Adapter, that can iterate over some bytes of a u32 value.
///
/// Construct with: U32ByteIter::new(data, length)
/// length must never be more than 4!
///
/// The iterator will iterate over the first length bytes of the Word,
/// assuming little endian order
/// So first byte will be 7:0, then 15:8, ...
/// # Examples
/// ```
/// use hps_webserver_libs::utils::byteiter::U32ByteIter;
///
/// let mut buf: Vec<u8> = vec!();
/// buf.extend(U32ByteIter::new(0x12345678, 4));
/// ```

pub struct U32ByteIter {
    data: u32,
    left: u32,
}

impl U32ByteIter {
    /// Creates a new [`U32ByteIter`].
    ///
    /// Params
    /// word: A 32-bit word
    /// num_bytes: how many bytes should be extracted
    pub fn new(word: u32, num_bytes: u32) -> Self {
        // Limit to 4 bytes!
        debug_assert!(num_bytes <= 4);
        let num_bytes = if num_bytes > 4 { 4 } else { num_bytes };
        Self {
            data: word,
            left: num_bytes,
        }
    }
}

impl Iterator for U32ByteIter {
    type Item = u8;

    fn next(&mut self) -> Option<Self::Item> {
        // If some bytes are left, get the current byte, shift right and have one fewer left
        if self.left > 0 {
            let b = (self.data & 0xFF) as u8;
            self.data >>= 8;
            self.left -= 1;
            Some(b)
        } else {
            None
        }
    }
}

pub struct ByteToU32Iter<'a> {
    data: &'a Vec<u8>,
    idx: usize,
}

impl<'a> ByteToU32Iter<'a> {
    pub fn new(data: &'a Vec<u8>) -> ByteToU32Iter<'a> {
        ByteToU32Iter { data, idx: 0 }
    }
}

impl<'a> Iterator for ByteToU32Iter<'a> {
    type Item = u32;

    fn next(&mut self) -> Option<Self::Item> {
        if self.idx == self.data.len() {
            return None;
        } else {
            let byte_left = self.data.len() - self.idx;
            let bytes_done = byte_left.min(4);
            let mut val: u32 = 0;
            for i in 0..bytes_done {
                val = val | (self.data[self.idx + i] as u32) << 8 * i;
            }
            self.idx += bytes_done;
            Some(val)
        }
    }
}

#[cfg(test)]
mod test {
    use super::{ByteToU32Iter, U32ByteIter};

    #[test]
    fn test_byte_iter() {
        let mut itr = U32ByteIter::new(0x11223344, 4);
        assert_eq!(itr.next(), Some(0x44u8));
        assert_eq!(itr.next(), Some(0x33u8));
        assert_eq!(itr.next(), Some(0x22u8));
        assert_eq!(itr.next(), Some(0x11u8));
        assert_eq!(itr.next(), None);

        let mut itr = U32ByteIter::new(0x11223344, 2);
        assert_eq!(itr.next(), Some(0x44u8));
        assert_eq!(itr.next(), Some(0x33u8));
        assert_eq!(itr.next(), None);
    }

    #[test]
    fn test_u32_iter() {
        let data = vec![0x44u8, 0x33u8, 0x22u8, 0x11u8];
        let mut itr = ByteToU32Iter::new(&data);
        assert_eq!(itr.next(), Some(0x11223344u32));
        assert_eq!(itr.next(), None);

        let data = vec![0x44u8, 0x33u8, 0x22u8, 0x11u8, 0x05u8];
        let mut itr = ByteToU32Iter::new(&data);
        assert_eq!(itr.next(), Some(0x11223344u32));
        assert_eq!(itr.next(), Some(0x00000005u32));
        assert_eq!(itr.next(), None);
    }
}

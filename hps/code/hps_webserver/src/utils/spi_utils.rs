#[derive(Debug, Clone, PartialEq, Eq)]
pub struct PrescalerResult {
    pub best_clk_per_halfbit: u32,
    pub exact: bool,
}

impl PrescalerResult {
    pub fn new(best_clk_per_halfbit: u32, exact: bool) -> Self {
        Self {
            best_clk_per_halfbit,
            exact,
        }
    }
}

pub fn calc_clock_prescaler(clk_freq: u32, spi_freq: u32) -> PrescalerResult {
    // calc 2x prescaler first
    let presc = clk_freq / spi_freq;
    // then add 1 and divide by two to round the result
    let per_halfbit = (presc + 1) / 2;

    let exact = spi_freq * per_halfbit * 2 == clk_freq;

    PrescalerResult::new(per_halfbit, exact)
}

pub fn calc_bitrate(clk_freq: u32, clk_per_halfbit: u32) -> u32 {
    // Add 1 before halfing to round correctly
    ((clk_freq / clk_per_halfbit) + 1)/2
}

#[cfg(test)]
mod test {

    use super::*;

    #[test]
    fn test_calc_prescaler() {
        assert_eq!(calc_clock_prescaler(100, 50), PrescalerResult::new(1, true));
        assert_eq!(
            calc_clock_prescaler(100, 60),
            PrescalerResult::new(1, false)
        );
        assert_eq!(
            calc_clock_prescaler(100, 40),
            PrescalerResult::new(1, false)
        );

        assert_eq!(calc_clock_prescaler(100, 10), PrescalerResult::new(5, true));
        assert_eq!(calc_clock_prescaler(100, 6), PrescalerResult::new(8, false));
    }

    #[test]
    fn test_calc_freq() {
        assert_eq!(calc_bitrate(1000, 5), 100);
        assert_eq!(calc_bitrate(1000, 3), 167);
    }
}

use std::collections::HashMap;

use tokio::sync::Mutex;

use crate::{
    data_buffer::DataBuffer,
    webapi::types::{IFType, InterfaceCapabilities},
};

pub mod interface_constants;

pub struct InterfaceData {
    /// The Buffer. Needs to be boxed, so it can be mocked for Tests easily
    pub buffer: Box<dyn DataBuffer + Send + Sync>,
    /// What type of Interface this is
    pub kind: IFType,
    /// The capabilities of this Interface
    pub capabilities: InterfaceCapabilities,
}

/// A collection of multiple interfaces
pub struct Interfaces {
    pub bufs: Mutex<HashMap<String, InterfaceData>>,
}

//========================================
//             Constants
//========================================

use crate::webapi::types::InterfaceCapabilities;

pub const SPI0_CAPABILITIES: InterfaceCapabilities = InterfaceCapabilities::SPI {
    max_bitrate: 50_000_000,
    flexible_frequency: false,
};

use std::{collections::HashMap, env::args, sync::Arc, time::Duration};

use actix_web::{web, App, HttpServer};
use fpga_interface::mock::MockDriverData;
use tokio::sync::Mutex;
use webapi::types::IFType;

mod data_buffer;
mod interfaces;
mod webapi;

use interfaces::interface_constants::*;

use crate::{
    data_buffer::fpga_buffer::FpgaBuffer,
    fpga_interface::{linux_driver::LinuxDriver, mock::MockSimpleDriver},
    interfaces::{InterfaceData, Interfaces},
};

mod fpga_interface;

enum ServerMode {
    Real,
    Demo,
}

#[tokio::main]
async fn main() -> std::io::Result<()> {
    let mode = if let Some(s) = args().skip(1).next() {
        if s == "demo" {
            ServerMode::Demo
        } else {
            panic!("Unrecognised argument {:?}", s);
        }
    } else {
        ServerMode::Real
    };

    match mode {
        ServerMode::Real => real_mode().await,
        ServerMode::Demo => demo_mode().await,
    }
}

async fn real_mode() -> Result<(), std::io::Error> {
    let mut interfaces = HashMap::new();
    let driver = Arc::new(Mutex::new(
        LinuxDriver::new("ff200000.DPG_SPI0", &SPI0_CAPABILITIES).expect("Could not open FPGA Device"),
    ));
    interfaces.insert(
        "SPI0".to_string(),
        InterfaceData {
            buffer: Box::new(FpgaBuffer::new(driver)),
            kind: IFType::SPI,
            capabilities: SPI0_CAPABILITIES,
        },
    );
    let state = web::Data::new(Interfaces {
        bufs: Mutex::new(interfaces),
    });

    println!("Starting server now!");

    HttpServer::new(move || App::new().app_data(state.clone()).configure(webapi::config))
        .bind("0.0.0.0:8080")?
        .run()
        .await
}

struct DemoMockResponder {
    incr: u8,
}

impl DemoMockResponder {
    fn new(incr: u8) -> Self {
        Self { incr }
    }
}

impl MockDriverData for DemoMockResponder {
    fn respond(&mut self, data: u8) -> Option<u8> {
        std::thread::sleep(Duration::from_millis(10));
        Some(data + self.incr)
    }
}

async fn demo_mode() -> Result<(), std::io::Error> {
    let mut interfaces = HashMap::new();
    interfaces.insert(
        "SPI0".to_string(),
        InterfaceData {
            buffer: Box::new(FpgaBuffer::new(Arc::new(Mutex::new(
                MockSimpleDriver::new(Box::new(DemoMockResponder::new(1))),
            )))),
            kind: IFType::SPI,
            capabilities: SPI0_CAPABILITIES,
        },
    );
    interfaces.insert(
        "SPI1".to_string(),
        InterfaceData {
            buffer: Box::new(FpgaBuffer::new(Arc::new(Mutex::new(
                MockSimpleDriver::new(Box::new(DemoMockResponder::new(0x10))),
            )))),
            kind: IFType::SPI,
            capabilities: SPI0_CAPABILITIES,
        },
    );
    let state = web::Data::new(Interfaces {
        bufs: Mutex::new(interfaces),
    });

    println!("Starting server now!");

    HttpServer::new(move || App::new().app_data(state.clone()).configure(webapi::config))
        .bind("0.0.0.0:8080")?
        .run()
        .await
}

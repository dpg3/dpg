use std::io;

use async_trait::async_trait;

use crate::webapi::types::InterfaceConfig;

// pub mod simple_driver;
pub mod buffer_bypass;
pub mod linux_driver;
pub mod mock;

// internal error enum for simple driver
#[derive(derive_error::Error, Debug)]
pub enum DriverError {
    /// Indicates, that the Operation or Configuration is not applicable on this device
    #[error(msg_embedded, no_from, non_std)]
    WrongDevice(String),

    /// If an IO-Error occured when accessing a system resource
    Io(io::Error),
}

/// Represents an Interface to a Communication Interface.
/// This trait is intended for synchronous interfaces, that send and receive data at the same time
/// (SPI, I2C, etc.)
#[async_trait]
pub trait InterfaceSimpleDriver {
    /// Sends an Receives Some Data.
    ///
    /// The `recv_buffer` Argument is used to provide a Buffer for received Data. If it is None,
    /// received Data is discarded.
    ///
    /// # Arguments
    ///
    /// * `send_buffer` - A `Vec<u8>` containing the bytes to be sent
    /// * `recv_buffer` - If a Some is passed, received Data is appended to the reference.
    ///
    /// # Returns
    ///
    /// Ok if the Operation succeeded, an Error Otherwise
    async fn transfer(
        &mut self,
        send_buffer: Vec<u8>,
        recv_buffer: Option<&mut Vec<u8>>,
    ) -> Result<(), DriverError>;

    /// Sets the Configuration. Returns a [DriverError::WrongDevice] if the Configuration does not
    /// match the Device
    async fn configure(&mut self, interface_configure: InterfaceConfig) -> Result<(), DriverError>;

    /// Reads the current Configuration from the Device and returns it.
    async fn read_config(&mut self) -> Result<InterfaceConfig, DriverError>;
}

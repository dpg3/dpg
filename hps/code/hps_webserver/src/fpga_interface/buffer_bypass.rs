use std::{
    fs::OpenOptions,
    mem::{self, size_of},
    time::Duration,
};

use async_trait::async_trait;
use memmap::{MmapMut, MmapOptions};

use hps_webserver_libs::utils::spi_utils;
use crate::webapi::types::{InterfaceCapabilities, InterfaceConfig, SpiCPHA, SpiCPOL};

use super::{DriverError, InterfaceSimpleDriver};

const REG_BASE: u32 = 0xff200000;

const SIMPLE_BUFFER_WRITE: u32 = REG_BASE + 0x20;
const SIMPLE_BUFFER_LEN: u32 = 8;

const SPI0_BASE: u32 = REG_BASE;

pub struct BufferBypassDriver {
    send_data: MmapMut,
    config: MmapMut,
    clock_rate: u32,
}

#[repr(C)]
struct SPIConfigRegs {
    cpol: u32,
    cpha: u32,
    pre_delay: u32,
    post_delay: u32,
    clk_per_halfbit: u32,
}

impl BufferBypassDriver {
    pub fn new(capabilities: &InterfaceCapabilities) -> Result<Self, DriverError> {
        let file = OpenOptions::new().read(true).write(true).open("/dev/mem")?;
        unsafe {
            let send_data = MmapOptions::new()
                .offset(SIMPLE_BUFFER_WRITE as u64)
                .len(SIMPLE_BUFFER_LEN as usize)
                .map_mut(&file)?;
            let config = MmapOptions::new()
                .offset(SPI0_BASE as u64)
                .len(size_of::<SPIConfigRegs>())
                .map_mut(&file)?;

            let clock_rate = match capabilities {
                InterfaceCapabilities::SPI {
                    max_bitrate,
                    flexible_frequency: false,
                } => *max_bitrate,
                InterfaceCapabilities::SPI {
                    max_bitrate: _,
                    flexible_frequency: true,
                } => {
                    return Err(DriverError::WrongDevice(
                        "Flexible Frequency not yet supported".to_string(),
                    ))
                }
            };
            Ok(Self {
                send_data,
                config,
                clock_rate,
            })
        }
    }
}

#[async_trait]
impl InterfaceSimpleDriver for BufferBypassDriver {
    async fn transfer(
        &mut self,
        send_buffer: Vec<u8>,
        recv_buffer: Option<&mut Vec<u8>>,
    ) -> Result<(), super::DriverError> {
        if let Some(buf) = recv_buffer {
            for byte in send_buffer {
                self.send_data[0] = byte;
                println!("Sending {}", byte);
                tokio::time::sleep(Duration::from_micros(1000)).await;
                let read = self.send_data[4];
                println!("Read {}", read);
                buf.push(read);
            }
        } else {
            for byte in send_buffer {
                self.send_data[0] = byte;
                println!("Sending {}", byte);
                tokio::time::sleep(Duration::from_micros(1000)).await;
            }
        }
        Ok(())
    }

    async fn configure(
        &mut self,
        interface_configure: InterfaceConfig,
    ) -> Result<(), super::DriverError> {
        println!("Configuring...");
        match interface_configure {
            InterfaceConfig::SPI {
                bitrate,
                cpha,
                cpol,
                pre_delay,
                post_delay,
            } => {
                unsafe {
                    let config_ptr: *mut SPIConfigRegs = mem::transmute(self.config.as_mut_ptr());
                    (*config_ptr).cpha = if cpha == SpiCPHA::SmplRising { 0 } else { 1 };
                    (*config_ptr).cpol = if cpol == SpiCPOL::Low { 0 } else { 1 };
                    (*config_ptr).pre_delay = pre_delay.unwrap_or(8) as u32;
                    (*config_ptr).post_delay = post_delay.unwrap_or(8) as u32;
                    (*config_ptr).clk_per_halfbit =
                        spi_utils::calc_clock_prescaler(self.clock_rate, bitrate)
                            .best_clk_per_halfbit;
                }
                Ok(())
            }
        }
    }

    async fn read_config(&mut self) -> Result<InterfaceConfig, DriverError> {
        unsafe {
            let config_ptr: *mut SPIConfigRegs = mem::transmute(self.config.as_mut_ptr());
            let cfg = InterfaceConfig::SPI {
                cpha: if (*config_ptr).cpha == 0 {
                    SpiCPHA::SmplRising
                } else {
                    SpiCPHA::SmplFalling
                },
                cpol: if (*config_ptr).cpol == 0 {
                    SpiCPOL::Low
                } else {
                    SpiCPOL::High
                },
                pre_delay: Some((*config_ptr).pre_delay as u8),
                post_delay: Some((*config_ptr).post_delay as u8),
                bitrate: spi_utils::calc_bitrate(self.clock_rate, (*config_ptr).clk_per_halfbit),
            };
            Ok(cfg)
        }
    }
}

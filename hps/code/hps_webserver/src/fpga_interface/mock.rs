use async_trait::async_trait;

use crate::webapi::types::{InterfaceConfig, SpiCPHA, SpiCPOL};

use super::{DriverError, InterfaceSimpleDriver};

type BoxedMockData = Box<dyn MockDriverData + Send + Sync>;

pub trait MockDriverData {
    fn respond(&mut self, data: u8) -> Option<u8>;
}

#[cfg(test)]
mod mock_iter {
    use super::MockDriverData;

    pub struct MockIter<TIter>
    where
        TIter: Iterator<Item = u8>,
    {
        iter: TIter,
    }

    impl<TIter> MockIter<TIter>
    where
        TIter: Iterator<Item = u8>,
    {
        pub fn new(iter: TIter) -> Self {
            Self { iter }
        }
    }

    impl<TIter> MockDriverData for MockIter<TIter>
    where
        TIter: Iterator<Item = u8>,
    {
        fn respond(&mut self, _data: u8) -> Option<u8> {
            self.iter.next()
        }
    }
}

pub struct MockSimpleDriver {
    send_copy: Vec<u8>,
    // Irgend ein Iter, gibt u8 zurück, dyn = Wie ptr auf Basisklasse
    // Box = unique ptr
    receive_mock: BoxedMockData,
    fpga_config: InterfaceConfig,
}

impl MockSimpleDriver {
    /// C-Tor
    pub fn new(rec_mock: BoxedMockData) -> Self {
        // mit let = Zuweisung, ohne let und ; einfach statemant das returned wird
        MockSimpleDriver {
            // init vec
            send_copy: vec![],
            receive_mock: rec_mock,
            fpga_config: InterfaceConfig::SPI {
                bitrate: 999999,
                cpha: SpiCPHA::SmplRising,
                cpol: SpiCPOL::Low,
                pre_delay: Some(4),
                post_delay: Some(4),
            },
        }
    }

    #[cfg(test)]
    pub fn with_iter(rec_mock: impl Iterator<Item = u8> + Send + Sync + 'static) -> Self {
        use self::mock_iter::MockIter;

        Self::new(Box::new(MockIter::new(rec_mock)))
    }

    #[cfg(test)]
    pub fn get_last_config(&mut self) -> InterfaceConfig {
        self.fpga_config
    }
}

#[async_trait]
impl InterfaceSimpleDriver for MockSimpleDriver {
    async fn transfer(
        &mut self,
        send_buffer: Vec<u8>,
        recv_buffer: Option<&mut Vec<u8>>,
    ) -> Result<(), DriverError> {
        self.send_copy = send_buffer;

        // Check if rec buff there
        if let Some(rbuf) = recv_buffer {
            // Vektor durchgehen und checken ob byte vorhanden oder nicht
            // wenn byte vorhanden in Vektor geben sonst error
            for b in &self.send_copy {
                if let Some(res) = self.receive_mock.respond(*b) {
                    rbuf.push(res);
                }
            }
        }
        Ok(())
    }

    /// saves the config as member
    async fn configure(&mut self, interface_configure: InterfaceConfig) -> Result<(), DriverError> {
        self.fpga_config = interface_configure;
        Ok(())
    }

    async fn read_config(&mut self) -> Result<InterfaceConfig, DriverError> {
        Ok(self.fpga_config)
    }
}

#[cfg(test)]
mod test {

    use super::*;

    #[tokio::test]
    async fn test_mock() {
        let mut driver = MockSimpleDriver::with_iter(0..100);
        let send = vec![5, 6, 7, 8];
        let mut rec = vec![];
        driver.transfer(send, Some(&mut rec)).await.unwrap();
        assert_eq!(rec, vec![0, 1, 2, 3]);

        let cfg = InterfaceConfig::SPI {
            bitrate: 1000,
            cpol: SpiCPOL::Low,
            cpha: SpiCPHA::SmplRising,
            pre_delay: Some(10),
            post_delay: Some(1),
        };

        driver
            .configure(cfg)
            .await
            .expect("Interface configure failed");
        assert_eq!(driver.get_last_config(), cfg);
    }
}

use std::{
    fs::{File, OpenOptions},
    io,
};

use memmap::{MmapMut, MmapOptions};

use crate::{utils::byteiter::ByteToU32Iter, webapi::types::InterfaceConfig};

use super::{DriverError, InterfaceSimpleDriver};

const WORD: u32 = 0x00000004;
const READY: u32 = 0x00000001;

// var for calculating clk divider
const CLK_FREQUENCY: u32 = 50_000_000; // TODO adjust clk frequency

// base addresses for memmap
const HW_REGS_BASE: u32 = 0xff200000;
const HW_REGS_SPAN: u32 = 0x00200000;
const HW_REGS_MASK: u32 = HW_REGS_SPAN - 1;

struct FpgaGitRevision {
    revision: [u32; 8],
}

// base addresses for fpga data buffer
const FPGA_BUF_BASE: u32 = 0x0;

const FPGA_GIT_REV_BASE: u32 = 0;
const OFF_FPGA_TX_START: u32 = 8;
const OFF_FPGA_TX_TRANSFER: u32 = 9;
const OFF_FPGA_TX_DATA: u32 = 10;
const OFF_FPGA_TX_COUNT: u32 = 11;
const OFF_FPGA_RX_DATA: u32 = 12;
const OFF_FPGA_RX_PTR: u32 = 13;

// base addresses for FPGA Master
const FPGA_MASTER_BASE: u32 = 0x00000020;

const OFF_SPI_MASTER_CPOL: u32 = 0; // clock polarity
const OFF_SPI_MASTER_CPHA: u32 = 1; // clock phase
const OFF_SPI_MASTER_PRE_DELAY: u32 = 2; // clock divider
const OFF_SPI_MASTER_POST_DELAY: u32 = 3; // clock divider
const OFF_SPI_MASTER_CLK_PER_HALFBIT: u32 = 4; // clock divider

pub struct SimpleDriver {
    // mutable memmap for fpga access
    mmap: Option<MmapMut>,
}

/// provides the driver function
impl SimpleDriver {
    /// initializes the mmap
    /// returns OK when alright, io::error otherwise
    fn init_simpledriver(&mut self) -> io::Result<()> {
        // open file
        let file: File = OpenOptions::new().read(true).write(true).open("/dev/mem")?;

        // map file
        self.mmap = Some(unsafe {
            MmapOptions::new()
                .offset(HW_REGS_BASE as u64)
                .len(HW_REGS_SPAN as usize)
                .map_mut(&file)?
        });

        Ok(())
    }

    /// C-Tor for SimpleDriver
    pub fn new() -> io::Result<SimpleDriver> {
        let mut me = SimpleDriver { mmap: None };
        me.init_simpledriver()?;

        Ok(me)
    }

    // returns pointer to fpga ram address
    fn get_mmap_address(&self, fpga_addr: u32) -> Result<*mut u32, DriverError> {
        // TODO check memmap fail -> fpga online?
        match &self.mmap {
            None => Err(DriverError::NotInitialized),

            Some(ptr) => Ok((ptr.as_ptr() as u32 + (fpga_addr & HW_REGS_MASK)) as *mut u32),
        }
    }

    // checks if fpga is ready
    fn fpga_ready(&mut self) -> Result<bool, DriverError> {
        let fpga_processing = self.get_mmap_address(FPGA_TX_TRANSFER)?;

        if unsafe { *fpga_processing != READY } {
            Ok(false)
        } else {
            Ok(true)
        }
    }

    // TODO error handling
    // get git revision
    // return git revision as u32 when OK else driver error
    fn get_git_rev(&mut self) -> Result<Vec<u32>, DriverError> {
        let mut fpga_git_rev = self.get_mmap_address(FPGA_GIT_REV_BASE)?;
        let mut git_rev = Vec::new();

        for _git_rev_len in 0..(FPGA_GIT_REV_SPAN - FPGA_GIT_REV_BASE) / WORD {
            unsafe {
                git_rev.push(*fpga_git_rev);

                // offset(1) increments by word
                fpga_git_rev = fpga_git_rev.offset(1);
            }
        }
        Ok(git_rev)
    }

    fn get_tx_next_frame(&mut self) -> Result<bool, DriverError> {
        let tx_next_frame: bool;
        let fpga_tx_next_frame = self.get_mmap_address(FPGA_TX_NEXT_FRAME)?;

        unsafe {
            tx_next_frame = *fpga_tx_next_frame != 0;
        }

        Ok(tx_next_frame)
    }

    fn get_tx_len(&mut self) -> Result<u32, DriverError> {
        let mut tx_len: u32 = 0;
        let fpga_tx_len = self.get_mmap_address(FPGA_TX_LENGTH)?;

        unsafe {
            tx_len = *fpga_tx_len;
        }

        Ok(tx_len)
    }

    fn get_tx_start(&mut self) -> Result<bool, DriverError> {
        let tx_start: bool;
        let fpga_tx_start = self.get_mmap_address(FPGA_TX_START)?;

        unsafe {
            tx_start = *fpga_tx_start != 0;
        }

        Ok(tx_start)
    }

    fn get_rx_finish(&mut self) -> Result<bool, DriverError> {
        let rx_finish: bool;
        let fpga_rx_finish = self.get_mmap_address(FPGA_RX_FINISH)?;

        unsafe {
            rx_finish = *fpga_rx_finish != 0;
        }

        Ok(rx_finish)
    }

    fn get_tx_transfer(&mut self) -> Result<bool, DriverError> {
        let tx_transfer: bool;
        let fpga_tx_transfer = self.get_mmap_address(FPGA_TX_TRANSFER)?;

        unsafe {
            tx_transfer = *fpga_tx_transfer != 0;
        }

        Ok(tx_transfer)
    }

    fn get_rx_len(&mut self) -> Result<u32, DriverError> {
        let mut rx_len: u32 = 0;
        let fpga_rx_len = self.get_mmap_address(FPGA_RX_LENGTH)?;

        unsafe {
            rx_len = *fpga_rx_len;
        }

        Ok(rx_len)
    }
}

/// implements the simple driver interface
impl InterfaceSimpleDriver for SimpleDriver {
    fn transfer(
        &mut self,
        send_buffer: Vec<u8>,
        recv_buffer: Option<&mut Vec<u8>>,
    ) -> Result<(), DriverError> {
        let fpga_data_addr = self.get_mmap_address(FPGA_TX_DATA_BASE)?;

        let fpga_data_len = self.get_mmap_address(FPGA_TX_LENGTH)?;

        // TODO check for out of bounce -> check fpga ready/ processing/ new frame
        if self.fpga_ready()? {
            let itr = ByteToU32Iter::new(&send_buffer);
            // set data length in fpga
            unsafe {
                *fpga_data_len = send_buffer.len() as u32;
            }

            // send data
            // Access to fpga addresses in words
            // eg.: 0x00000000, 0x00000004, 0x00000008,...

            // Change! Write always to same address

            for data in itr {
                unsafe {
                    *fpga_data_addr = data;
                    // offset(1) increments by word
                    // fpga_data_addr = fpga_data_addr.offset(1);
                }
            }

            // Check if rec buff there
            // if there fill else skip
            if let Some(_rbuf) = recv_buffer {

                // TODO read buffer
            }
        }
        // like return Ok(()); --> Ok without data
        Ok(())
    }

    /// configures the fpga master
    fn configure(&mut self, interface_configure: InterfaceConfig) -> Result<(), DriverError> {
        match interface_configure {
            InterfaceConfig::SPI {
                bitrate,
                cpha,
                cpol,
            } => {
                // address clk polarity
                let fpga_master_cpol_addr = self.get_mmap_address(FPGA_MASTER_CPOL)?;
                unsafe {
                    *fpga_master_cpol_addr = cpol as u32;
                }
                // address fpga master clk phase
                let fpga_master_cpha_addr = self.get_mmap_address(FPGA_MASTER_CPHA)?;
                unsafe {
                    *fpga_master_cpha_addr = cpha as u32;
                }
                // address clk divider
                let fpga_data_clkdiv_addr = self.get_mmap_address(FPGA_MASTER_CDIV)?;
                // Clk divider = Clk frequency / bit rate;
                unsafe {
                    *fpga_data_clkdiv_addr = CLK_FREQUENCY / bitrate;
                }
            }
        }
        Ok(())
    }
}

use std::{
    fs::File,
    io::{self, Read, Write},
};

use async_trait::async_trait;
use hps_webserver_libs::utils::spi_utils;

use crate::{
    webapi::types::{InterfaceCapabilities, InterfaceConfig, SpiCPHA, SpiCPOL},
};

use super::{DriverError, InterfaceSimpleDriver};

pub struct LinuxDriver {
    cpha: File,
    cpol: File,
    pre_delay: File,
    post_delay: File,
    clk_per_halfbit: File,

    chardev: File,

    clock_rate: u32,
}

const SYSFS_DEV: &'static str = "/sys/bus/platform/devices";

const BLOCK_SIZE: usize = 1024;

impl LinuxDriver {
    fn open_file_rw(path: String) -> io::Result<File> {
        File::options().read(true).write(true).open(path)
    }

    ///Create a new Interface for an SPI using a Linux device driver
    ///
    ///This will use sysfs files to configure the SPI master. These files must be located
    ///in /sys/bus/platform/devices/`dev_name`/.
    ///
    ///Data transfers happen over a character device named /dev/dpg_spi0 for now. A way to auto
    ///configure this is being worked on
    ///
    ///# Arguments
    ///
    ///* `dev_name` - Name of the device, specifies the path of special files
    ///* `capabilities` - Capabilities of the device, used to check configuration.
    pub fn new(
        dev_name: &str,
        capabilities: &InterfaceCapabilities,
    ) -> Result<LinuxDriver, DriverError> {
        let cpha = Self::open_file_rw(format!("{}/{}/cpha", SYSFS_DEV, dev_name))?;
        let cpol = Self::open_file_rw(format!("{}/{}/cpol", SYSFS_DEV, dev_name))?;
        let pre_delay = Self::open_file_rw(format!("{}/{}/pre_delay", SYSFS_DEV, dev_name))?;
        let post_delay = Self::open_file_rw(format!("{}/{}/post_delay", SYSFS_DEV, dev_name))?;
        let clk_per_halfbit =
            Self::open_file_rw(format!("{}/{}/clk_per_half_bit", SYSFS_DEV, dev_name))?;
        let chardev = Self::open_file_rw(format!("/dev/dpg_spi0"))?; // TODO configurable chardev

        let clock_rate = match capabilities {
            InterfaceCapabilities::SPI {
                max_bitrate,
                flexible_frequency: false,
            } => *max_bitrate,
            InterfaceCapabilities::SPI {
                max_bitrate: _,
                flexible_frequency: true,
            } => {
                return Err(DriverError::WrongDevice(
                    "Flexible Frequency not yet supported".to_string(),
                ))
            }
        };

        Ok(Self {
            cpha,
            cpol,
            pre_delay,
            post_delay,
            clk_per_halfbit,

            chardev,
            clock_rate,
        })
    }
}

#[async_trait]
impl InterfaceSimpleDriver for LinuxDriver {
    async fn transfer(
        &mut self,
        send_buffer: Vec<u8>,
        recv_buffer: Option<&mut Vec<u8>>,
    ) -> Result<(), super::DriverError> {
        let mut buf: &[u8] = &send_buffer;
        match recv_buffer {
            Some(read) => {
                read.resize(buf.len(), 0);
                let mut rdbuf: &mut [u8] = read;
                while buf.len() > BLOCK_SIZE {
                    self.chardev.write_all(&buf[0..BLOCK_SIZE])?;
                    self.chardev.read_exact(&mut rdbuf[0..BLOCK_SIZE])?;
                    buf = &buf[BLOCK_SIZE..];
                    rdbuf = &mut rdbuf[BLOCK_SIZE..];
                }
                self.chardev.write_all(buf)?;
                self.chardev.read_exact(&mut rdbuf[0..buf.len()])?;
            }
            None => {
                while buf.len() > BLOCK_SIZE {
                    self.chardev.write_all(&buf[0..BLOCK_SIZE])?;
                    buf = &buf[BLOCK_SIZE..];
                }
                self.chardev.write_all(buf)?;
                let mut buf = [0; BLOCK_SIZE];
                self.chardev.read(&mut buf)?;
            }
        }
        Ok(())
    }

    async fn configure(
        &mut self,
        interface_configure: crate::webapi::types::InterfaceConfig,
    ) -> Result<(), super::DriverError> {
        match interface_configure {
            InterfaceConfig::SPI {
                bitrate,
                cpha,
                cpol,
                pre_delay,
                post_delay,
            } => {
                cpha.write_to_sysfs(&mut self.cpha)?;
                cpol.write_to_sysfs(&mut self.cpol)?;
                let clocks =
                    spi_utils::calc_clock_prescaler(self.clock_rate, bitrate).best_clk_per_halfbit;
                clocks.write_to_sysfs(&mut self.clk_per_halfbit)?;
                if let Some(pd) = pre_delay {
                    pd.write_to_sysfs(&mut self.pre_delay)?;
                }
                if let Some(pd) = post_delay {
                    pd.write_to_sysfs(&mut self.post_delay)?;
                }
                Ok(())
            }
        }
    }

    async fn read_config(&mut self) -> Result<InterfaceConfig, DriverError> {
        todo!()
    }
}

trait SysfsWrite {
    fn write_to_sysfs(&self, file: &mut File) -> io::Result<()>;
}

impl SysfsWrite for SpiCPHA {
    fn write_to_sysfs(&self, file: &mut File) -> io::Result<()> {
        match self {
            SpiCPHA::SmplRising => file.write_all(&mut [b'0']),
            SpiCPHA::SmplFalling => file.write_all(&mut [b'1']),
        }
    }
}

impl SysfsWrite for SpiCPOL {
    fn write_to_sysfs(&self, file: &mut File) -> io::Result<()> {
        match self {
            SpiCPOL::Low => file.write_all(&mut [b'0']),
            SpiCPOL::High => file.write_all(&mut [b'1']),
        }
    }
}

impl SysfsWrite for u32 {
    fn write_to_sysfs(&self, file: &mut File) -> io::Result<()> {
        file.write_all(format!("{}", self).as_bytes())
    }
}
impl SysfsWrite for u8 {
    fn write_to_sysfs(&self, file: &mut File) -> io::Result<()> {
        file.write_all(format!("{}", self).as_bytes())
    }
}

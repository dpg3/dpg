pub mod fpga_buffer;

mod command_queue;
#[cfg(test)]
pub mod mock_buffer;

use async_trait::async_trait;
use derive_error::Error;

use crate::{fpga_interface::DriverError, webapi::types::InterfaceConfig};

/// Status of a ReceiveBuffer
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum ReceiveBufferStatus {
    /// Transfer was finished, received Data is contained here
    Ready(Vec<u8>),
    /// Transfer was finished, but received Data was not kept
    SendOnlyFinished,
    /// The Transfer is still in Progress or has not started yet
    NotReady,
    /// The Transfer was not created yet
    NotExisting,

    /// An error occured. Contains only a String message due to typing Constraints in async fns
    Error(String),
}

#[derive(Debug, Error)]
pub enum DataBufferError {
    /// No Transfer with the specified ID exists
    NoSuchTransfer,
    /// An Error occured in the tokio runtime. Not recoverable
    TokioError,
    /// An Error occured in communication with the Driver
    DriverError(DriverError),

    /// An Internal Error, that indicates a programming error instead of a usage error
    #[error(msg_embedded, no_from, non_std)]
    InternalError(String),
}

/// Central Type for all Transfer IDs
/// 32 bit should be enough if we can handle overflows
/// more than 2^32 transfers at the same time would not fit into memory anyway
pub type TransferID = u32;

/// A trait for the DataBuffer. Manages multiple concurrent transfers and configuration changes.
///
/// All functions of this
///
/// # Operation Ordering
///
/// The [DataBuffer::put_send_data] and [DataBuffer::set_new_config] operations are executed in the same order as they are
/// created.
///
/// # ID Management
#[async_trait]
pub trait DataBuffer {
    /// Add a new Transfer to the DataBuffer and return its ID.
    ///
    /// If keep_recv is true, then a new receive buffer will also be allocated, which can be queried later.
    ///
    /// Transfers are guaranteed to be executed in the order they are created.
    /// Configuration changes with [DataBuffer::set_new_config] are also ordered the same way.
    ///
    /// # Arguments
    ///
    /// * `data` - A ByteBuffer of data to be sent
    /// * `keep_recv` - If the result of the transfer should be kept
    async fn put_send_data(
        &mut self,
        data: Vec<u8>,
        keep_recv: bool,
    ) -> Result<TransferID, DataBufferError>;

    /// Get Status and Data of a Receive Transfer
    /// If the transfer was finished Ready will be returned with the data and the Transfer will be removed.
    /// Otherwise NotReady or NotExisiting will be returned and the Transfer will still exist
    ///
    /// # Arguments
    ///
    /// * `id` - The id of the transfer
    ///
    /// # Returns
    ///
    /// The status and optional Data of the Transfer
    async fn get_recv_data<'a>(&'a mut self, id: TransferID) -> ReceiveBufferStatus;

    /// Returns a list of TransferIDs that currently exist for this Buffer.
    async fn list_transfers(&self) -> Vec<TransferID>;

    /// Remove a transfer data from memory.
    /// This should be called when a Transfer is no longer used, so the ID can be reused.
    async fn delete_recv_data(&mut self, id: TransferID) -> Result<(), DataBufferError>;

    /// Set a new Configuration.
    ///
    /// Transfers created before a call to this function use the old config.
    /// Transfers created after a call to this function use the new config.
    async fn set_new_config(&mut self, cfg: InterfaceConfig) -> Result<(), DataBufferError>;

    /// Returns the current config.
    ///
    /// If a call to set_new_config was issued, then this might still return the old config, if the
    /// configuration Update was not completed before this call
    async fn get_curr_config(&mut self) -> Result<InterfaceConfig, DataBufferError>;
}

/// Compute the next free ID using a linear search
///
/// # Arguments
///
/// * `old_id` - The current ID
/// * `id_free` - A Function to check if a given [TransferID] is still free
fn compute_next_id<F>(old_id: TransferID, id_free: F) -> TransferID
where
    F: Fn(TransferID) -> bool,
{
    let mut next_id = old_id.wrapping_add(1);
    while !id_free(next_id) {
        next_id = next_id.wrapping_add(1);
    }
    next_id
}

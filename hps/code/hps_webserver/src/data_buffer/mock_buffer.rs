use std::collections::{HashMap, VecDeque};

use async_trait::async_trait;

use crate::{webapi::types::InterfaceConfig};

use super::{compute_next_id, DataBuffer, DataBufferError, ReceiveBufferStatus, TransferID};

pub trait DataBufferDataSide {
    /// Put the received Data for some Transfer. This marks the transfer as finished, and no new data will be accepted by the Buffer for this tranfer.
    /// a get_recv_data call with the same id will return a Ready Status some time in the future.
    /// @param id The id of the transfer
    /// @data The data, that was received
    /// @return true if the data was inserted, false if there either is no such transfer, or it was already completed.
    fn put_recv_data(&mut self, id: TransferID, data: Vec<u8>) -> Result<(), DataBufferError>;

    /// Returns the next Data Element in the send queue.
    /// This data was put there by put_send_data and has already been assigned an id.
    /// The return type contains an option for the id, if that is None, it means,
    /// that the response of the Device should not be kept
    /// @return a Some if there is Data, a None if no new Data was queued
    fn get_next_send_data(&mut self) -> Option<BufferEntry>;
}

pub struct BufferEntry {
    /// The transfer of the send Transfer, if it is None, then the Read result should not be kept
    pub transfer_id: TransferID,
    pub keep_recv: bool,
    pub buffer: Vec<u8>,
}

#[derive(PartialEq, Eq, Debug)]
pub enum ReceiveBuffer {
    NotAllocated,
    Finished(Vec<u8>),
}

pub struct MockDataBuffer {
    to_send: VecDeque<BufferEntry>,
    receive: HashMap<TransferID, ReceiveBuffer>,
    next_id: TransferID,
    config: Option<InterfaceConfig>,
}

impl MockDataBuffer {
    pub fn new() -> Self {
        MockDataBuffer {
            to_send: VecDeque::new(),
            receive: HashMap::new(),
            next_id: 0,
            config: None,
        }
    }
    pub fn with_config(cfg: InterfaceConfig) -> Self {
        MockDataBuffer {
            to_send: VecDeque::new(),
            receive: HashMap::new(),
            next_id: 0,
            config: Some(cfg),
        }
    }
}

#[async_trait]
impl DataBuffer for MockDataBuffer {
    async fn put_send_data(
        &mut self,
        data: Vec<u8>,
        keep_recv: bool,
    ) -> Result<TransferID, DataBufferError> {
        let tid = self.next_id;
        self.next_id = compute_next_id(self.next_id, |id| !self.receive.contains_key(&id));
        self.receive.insert(tid, ReceiveBuffer::NotAllocated);
        self.to_send.push_back(BufferEntry {
            buffer: data,
            keep_recv,
            transfer_id: tid,
        });
        Ok(tid)
    }

    async fn get_recv_data(&mut self, id: TransferID) -> super::ReceiveBufferStatus {
        let dat = self.receive.get(&id);
        match dat {
            None => ReceiveBufferStatus::NotExisting,
            Some(ReceiveBuffer::Finished(v)) => ReceiveBufferStatus::Ready(v.clone()),
            Some(_) => ReceiveBufferStatus::NotReady,
        }
    }

    async fn list_transfers(&self) -> Vec<TransferID> {
        Vec::from_iter(self.receive.keys().copied())
    }

    async fn delete_recv_data(&mut self, id: TransferID) -> Result<(), DataBufferError> {
        let rem = self.receive.remove(&id);
        match rem {
            None => Err(DataBufferError::NoSuchTransfer),
            Some(_) => Ok(()),
        }
    }

    async fn set_new_config(&mut self, cfg: InterfaceConfig) -> Result<(), DataBufferError> {
        self.config = Some(cfg);
        Ok(())
    }

    async fn get_curr_config(&mut self) -> Result<InterfaceConfig, DataBufferError> {
        self.config
            .ok_or(DataBufferError::InternalError("Never Configured".to_string()))
    }
}

impl DataBufferDataSide for MockDataBuffer {
    fn put_recv_data(&mut self, id: TransferID, data: Vec<u8>) -> Result<(), DataBufferError> {
        let existing = self.receive.get_mut(&id);
        match existing {
            None => Err(DataBufferError::NoSuchTransfer),
            Some(rb) => match rb {
                ReceiveBuffer::NotAllocated => {
                    self.receive.insert(id, ReceiveBuffer::Finished(data));
                    Ok(())
                }
                ReceiveBuffer::Finished(_) => Err(DataBufferError::InternalError(
                    "Transfer already finished".to_string(),
                )),
            },
        }
    }

    fn get_next_send_data(&mut self) -> Option<BufferEntry> {
        self.to_send.pop_front()
    }
}

#[cfg(test)]
mod tests {
    use crate::data_buffer::{
        mock_buffer::{DataBufferDataSide, ReceiveBuffer},
        DataBuffer, ReceiveBufferStatus,
    };

    use super::MockDataBuffer;

    #[tokio::test]
    async fn test_put() {
        let mut db = MockDataBuffer::new();
        let buf = vec![0u8, 1u8, 2u8];
        db.put_send_data(buf, true).await.unwrap();
        assert!(db.to_send.len() == 1);
        assert!(db.to_send[0].keep_recv);
        db.put_send_data(vec![3u8, 4u8], false).await.unwrap();
        assert!(db.to_send.len() == 2);
        assert!(!db.to_send[1].keep_recv);
    }

    #[tokio::test]
    async fn test_put_then_get() {
        let mut db = MockDataBuffer::new();
        db.put_send_data(vec![3u8, 4u8], true).await.unwrap();
        assert!(db.to_send.len() == 1);
        let next_send = db.get_next_send_data();
        assert!(next_send.is_some());
        let next_send = next_send.unwrap();
        assert!(db.to_send.len() == 0);
        assert_eq!(next_send.buffer, vec!(3u8, 4u8));
    }

    #[tokio::test]
    async fn test_get_none() {
        let mut db = MockDataBuffer::new();
        assert!(db.get_next_send_data().is_none());
    }

    #[tokio::test]
    async fn put_recv() {
        let mut db = MockDataBuffer::new();
        let tid = db.put_send_data(vec![3u8], true).await.unwrap();
        let inserted = db.put_recv_data(tid, vec![1u8]);
        assert!(inserted.is_ok());
        assert_eq!(db.receive.len(), 1);
        assert_eq!(db.receive[&tid], ReceiveBuffer::Finished(vec![1u8]));
    }

    #[tokio::test]
    async fn get_recv() {
        let mut db = MockDataBuffer::new();
        assert_eq!(db.get_recv_data(0).await, ReceiveBufferStatus::NotExisting);
        let tid = db.put_send_data(vec![3u8], true).await.unwrap();
        assert_eq!(db.receive.len(), 1);
        assert_eq!(db.get_recv_data(tid).await, ReceiveBufferStatus::NotReady);
        db.put_recv_data(tid, vec![1u8]).unwrap();

        let rcv = db.get_recv_data(tid).await;
        match rcv {
            ReceiveBufferStatus::Ready(dat) => assert_eq!(dat, vec!(1u8)),
            _ => assert!(false),
        }
    }
}

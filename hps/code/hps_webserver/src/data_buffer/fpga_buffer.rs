use std::{collections::HashMap, sync::Arc};

use async_trait::async_trait;

use tokio::sync::{oneshot, Mutex};

use crate::{fpga_interface::InterfaceSimpleDriver, webapi::types::InterfaceConfig};

use super::{compute_next_id, DataBuffer, DataBufferError, ReceiveBufferStatus, TransferID};

use super::command_queue::CommandQueue;

type DriverArc = Arc<Mutex<dyn InterfaceSimpleDriver + Send + Sync>>;

pub struct FpgaBuffer {
    cmd_queue: CommandQueue,
    next_tf_id: TransferID,
    receive_buf: Arc<Mutex<HashMap<TransferID, ReceiveBuffer>>>,
    interface: DriverArc,
}

#[derive(Debug)]
pub enum ReceiveBuffer {
    NotAllocated,
    SendOnlyFinished,
    Finished(Vec<u8>),
    Error(String),
}

const BUFFER_WARNING_THRESHOLD: usize = 10000;

#[async_trait]
impl DataBuffer for FpgaBuffer {
    async fn put_send_data(
        &mut self,
        data: Vec<u8>,
        keep_recv: bool,
    ) -> Result<TransferID, super::DataBufferError> {
        let curr_tid = self.get_and_inc_id().await;
        self.provision_rec_buf(curr_tid).await?;

        let recv_data = self.start_transfer(data, keep_recv);

        let rbuf_c = self.receive_buf.clone();
        tokio::spawn(async move {
            let rx = recv_data.await.expect("Sending Thread Crashed");
            rbuf_c.lock().await.insert(curr_tid, rx);
        });

        Ok(curr_tid)
    }

    async fn get_recv_data<'a>(&'a mut self, id: TransferID) -> super::ReceiveBufferStatus {
        self.receive_buf
            .lock()
            .await
            .get(&id)
            .map(|r| r.into())
            .unwrap_or(ReceiveBufferStatus::NotExisting)
    }

    async fn list_transfers(&self) -> Vec<TransferID> {
        self.receive_buf.lock().await.keys().cloned().collect()
    }

    async fn delete_recv_data(&mut self, id: TransferID) -> Result<(), super::DataBufferError> {
        self.receive_buf
            .lock()
            .await
            .remove(&id)
            .map(|_| ())
            .ok_or(DataBufferError::NoSuchTransfer)
    }

    async fn set_new_config(&mut self, cfg: InterfaceConfig) -> Result<(), DataBufferError> {
        let interf = self.interface.clone();
        let rec = self
            .cmd_queue
            .enqueue(async move { interf.lock().await.configure(cfg).await });
        match rec.await {
            Ok(driver_res) => Ok(driver_res?),
            Err(_) => Err(DataBufferError::TokioError),
        }
    }

    async fn get_curr_config(&mut self) -> Result<InterfaceConfig, DataBufferError> {
        Ok(self.interface.lock().await.read_config().await?)
    }
}

impl<'a> From<&'a ReceiveBuffer> for ReceiveBufferStatus {
    fn from(r: &'a ReceiveBuffer) -> Self {
        match r {
            ReceiveBuffer::NotAllocated => ReceiveBufferStatus::NotReady,
            ReceiveBuffer::SendOnlyFinished => ReceiveBufferStatus::SendOnlyFinished,
            ReceiveBuffer::Finished(data) => ReceiveBufferStatus::Ready(data.clone()),
            ReceiveBuffer::Error(e) => ReceiveBufferStatus::Error(e.clone()),
        }
    }
}

impl FpgaBuffer {
    pub fn new(fpga_if: Arc<Mutex<dyn InterfaceSimpleDriver + Send + Sync>>) -> Self {
        Self {
            cmd_queue: CommandQueue::new(),
            next_tf_id: 0,
            receive_buf: Arc::new(Mutex::new(HashMap::new())),
            interface: fpga_if,
        }
    }

    /// Put a new transfer into the Command Queue
    fn start_transfer(
        &mut self,
        data: Vec<u8>,
        keep_recv: bool,
    ) -> oneshot::Receiver<ReceiveBuffer> {
        let driver = self.interface.clone();

        self.cmd_queue.enqueue(async move {
            let mut driver = driver.lock().await;
            if keep_recv {
                let mut buf = Vec::with_capacity(data.len());
                match driver.transfer(data, Some(&mut buf)).await {
                    Ok(_) => ReceiveBuffer::Finished(buf),
                    Err(e) => ReceiveBuffer::Error(format!("{}", e)),
                }
            } else {
                match driver.transfer(data, None).await {
                    Ok(_) => ReceiveBuffer::SendOnlyFinished,
                    Err(e) => ReceiveBuffer::Error(format!("{}", e)),
                }
            }
        })
    }

    /// Return ID for the current transfer and update internal state
    ///
    /// This will simply return Numbers 0 to u32::max() at first, then overrun back to 0
    /// An ID that is still in use will be skipped
    async fn get_and_inc_id(&mut self) -> TransferID {
        let curr_id = self.next_tf_id;
        let rbuf = self.receive_buf.lock().await;
        self.next_tf_id = compute_next_id(self.next_tf_id, |id| !rbuf.contains_key(&id));
        if rbuf.len() > BUFFER_WARNING_THRESHOLD {
            println!("WARNING: There are currently {} Transfers in memory... Make sure unused Transfers get deleted", rbuf.len());
        }
        curr_id
    }

    async fn provision_rec_buf(&mut self, id: TransferID) -> Result<(), DataBufferError> {
        let mut rbuf = self.receive_buf.lock().await;
        if rbuf.contains_key(&id) {
            return Err(DataBufferError::InternalError(format!("Key {} already exists", id)))
        }
        rbuf.insert(id, ReceiveBuffer::NotAllocated);
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use std::{sync::Arc, time::Duration};

    use tokio::sync::Mutex;

    use crate::{
        data_buffer::{DataBuffer, ReceiveBufferStatus},
        fpga_interface::mock::MockSimpleDriver,
    };

    use super::FpgaBuffer;

    fn create_test_data(driver: MockSimpleDriver) -> FpgaBuffer {
        FpgaBuffer::new(Arc::new(Mutex::new(driver)))
    }

    #[tokio::test]
    async fn test_simple_read_write() {
        let mut buf = create_test_data(MockSimpleDriver::with_iter(0..100));
        let mut cnt = 0;
        let tid = buf.put_send_data(vec![100, 100, 100], true).await.unwrap();
        loop {
            let recdat = buf.get_recv_data(tid).await;
            match recdat {
                ReceiveBufferStatus::Ready(d) => {
                    assert_eq!(d, vec![0, 1, 2]);
                    break;
                }
                ReceiveBufferStatus::NotReady => {
                    tokio::time::sleep(Duration::from_millis(10)).await;
                    cnt += 1;
                    if cnt == 1000 {
                        assert!(false, "Timed out");
                    }
                }
                a => {
                    panic!("Wrong Status {:?}", a)
                }
            }
        }
    }
}

use std::future::Future;

use tokio::{sync::oneshot, task::JoinHandle};

/// Queue for executing tasks in an ordered way.
/// Tasks are created as Tokio "Green Thread" Tasks
pub struct CommandQueue {
    /// The last created task or None if this is a new CommandQueue.
    /// Used to serialize Execution order
    last_task: Option<JoinHandle<()>>,
}

impl CommandQueue {
    /// Create a new CommandQueue
    pub fn new() -> Self {
        Self { last_task: None }
    }

    /// Enqueue a new Task to this Queue.
    ///
    /// This will spawn a new Task, that waits until all previous commands were executed,
    /// then awaits the future.
    /// The return value of the Future is sent over a oneshot channel.
    ///
    /// # Arguments
    ///
    /// * `command` - A future representing a Command. Can simply be an async block
    ///
    /// # Examples
    ///
    /// ```
    /// use data_buffer::command_queue::CommandQueue;
    /// use std::time::Duration;
    /// use tokio::sync::oneshot::Receiver;
    ///
    /// let mut cmd_queue = CommandQueue::new();
    /// let res1: Receiver<()> = cmd_queue.enqueue(async move {
    ///     tokio::time::sleep(Duration::from_millis(1000));
    ///     println!("Hello 1");
    /// }).await;
    ///
    /// let res2: Receiver<i32> = cmd_queue.enqueue(async move {
    ///     tokio::time::sleep(Duration::from_millis(1000));
    ///     println!("Hello 2");
    ///     2i32
    /// }).await;
    ///
    /// // Hello 1 will be printed before Hello 2.
    /// assert_eq!(2, res2.await.unwrap());
    ///
    /// ```
    pub fn enqueue<'a, Command, Res>(&'a mut self, command: Command) -> oneshot::Receiver<Res>
    where
        Command: Future<Output = Res> + Send + 'static,
        Res: Send + 'static,
    {
        let (tx, rx) = oneshot::channel();

        let last_task = self.last_task.take();

        self.last_task = Some(tokio::spawn(async move {
            if let Some(t) = last_task {
                t.await.unwrap();
            }
            let res = command.await;
            // Cannot use expect/unwrap because error type is the data itself and it does not impl
            // Debug
            if let Err(_) = tx.send(res) {
                panic!("Could not send Result of queue")
            }
        }));

        rx
    }
}

// When the CommandQueue gets dropped, all enqueued commands should be abortet
impl Drop for CommandQueue {
    fn drop(&mut self) {
        if let Some(t) = self.last_task.take() {
            t.abort();
        }
    }
}

#[cfg(test)]
mod test {
    use std::{sync::Arc, time::Duration};

    use crate::data_buffer::command_queue::CommandQueue;
    use tokio::sync::{oneshot::Receiver, Mutex};

    #[tokio::test]
    async fn test() {
        let mut cmd_queue = CommandQueue::new();

        let test_data = Arc::new(Mutex::new(vec![]));

        let td1 = test_data.clone();
        let td2 = test_data.clone();

        let _res1: Receiver<()> = cmd_queue.enqueue(async move {
            tokio::time::sleep(Duration::from_millis(10)).await;
            td1.lock().await.push(1);
        });

        let res2: Receiver<i32> = cmd_queue.enqueue(async move {
            td2.lock().await.push(2);
            2i32
        });

        assert_eq!(2, res2.await.unwrap());
        // Task 1 is called before Task 2
        // Task 2 is finished after the await in the line above
        assert_eq!(vec![1, 2], *test_data.lock().await);
    }
}

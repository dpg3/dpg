use actix_web::delete;
use actix_web::get;

use actix_web::post;
use actix_web::put;

use actix_web::web;
use actix_web::HttpRequest;
use actix_web::HttpResponse;
use actix_web::Responder;

use crate::data_buffer::ReceiveBufferStatus;

use crate::data_buffer::TransferID;
use crate::webapi::services::serve_delete_transfer;
use crate::webapi::services::serve_get_config;
use crate::webapi::services::serve_update_config;
use crate::InterfaceData;
use crate::Interfaces;

mod services;
pub mod types;

use types::*;

//============================================================
//                 Helper Functions
//============================================================

async fn get_part_of_interface<T, R>(
    data: &web::Data<Interfaces>,
    name: &String,
    extract: T,
) -> Option<R>
where
    T: Fn(&InterfaceData) -> R,
{
    let all_interfaces = data.bufs.lock().await;
    let interface = all_interfaces.get(name);
    interface.map(extract)
}

fn transfer_url(interf: &str, tid: TransferID) -> String {
    format!("/interfaces/{interf}/transfers/{tid}")
}

//============================================================
//                 Handler Functions
//============================================================

#[get("/interfaces")]
async fn get_interface_list(
    data: web::Data<Interfaces>,
    query: web::Query<InterfaceTypeQuery>,
) -> impl Responder {
    let mut bufs = data.bufs.lock().await;
    InterfaceList {
        ifs: Vec::from_iter(services::serv_get_interface_list(&mut bufs, query.if_type)),
    }
}

#[get("/interfaces/{IFName}")]
async fn get_interface_descr(
    req: HttpRequest,
    data: web::Data<Interfaces>,
    path: web::Path<String>,
) -> impl Responder {
    let interface_name = path.into_inner();
    let kind = get_part_of_interface(&data, &interface_name, |data| data.kind).await;
    match kind {
        None => HttpResponse::NotFound().respond_to(&req),
        Some(kind) => InterfaceDescription::new(kind, &interface_name).respond_to(&req),
    }
}

#[get("/interfaces/{IFName}/capabilities")]
async fn get_interface_capabilities(
    data: web::Data<Interfaces>,
    path: web::Path<String>,
) -> impl Responder {
    get_part_of_interface(&data, &path, |data| data.capabilities).await
}

#[get("/interfaces/{IFName}/config")]
async fn get_interface_config(
    data: web::Data<Interfaces>,
    path: web::Path<String>,
) -> impl Responder {
    let path = path.into_inner();
    let mut bufs = data.bufs.lock().await;
    match bufs.get_mut(&path) {
        Some(buf) => Some(serve_get_config(buf).await),
        None => None,
    }
}

#[put("/interfaces/{IFName}/config")]
async fn set_interface_config(
    data: web::Data<Interfaces>,
    path: web::Path<String>,
    body: web::Json<InterfaceConfig>,
) -> impl Responder {
    let path = path.into_inner();
    let mut bufs = data.bufs.lock().await;
    match bufs.get_mut(&path) {
        Some(buf) => Some(serve_update_config(buf, body.into_inner()).await),
        None => None,
    }
}

#[get("/interfaces/{IFName}/transfers")]
async fn list_interface_transfers(
    data: web::Data<Interfaces>,
    path: web::Path<String>,
) -> impl Responder {
    // we need to convert the path to a string explicitly, so we can index the map correctly
    let name: &String = &path;
    let buffers = &data.bufs.lock().await;
    let ifd = buffers.get(name);
    match ifd {
        None => None,
        Some(ifd) => {
            let ids = ifd.buffer.list_transfers().await;
            let tv = ids
                .into_iter()
                .map(|id| format!("/interfaces/{}/transfers/{}", name, id))
                .collect();
            Some(TransferList { transfers: tv })
        }
    }
}

#[post("/interfaces/{IFName}/transfers")]
async fn create_new_transfer(
    data: web::Data<Interfaces>,
    path: web::Path<String>,
    body: web::Json<NewTransfer>,
) -> impl Responder {
    let strpath: &str = &path;
    let mut interfs = data.bufs.lock().await;
    let int_data = interfs.get_mut(strpath);
    match int_data {
        Some(data) => Some(
            services::serv_create_interface(data, body.into_inner())
                .await
                .map(|tid| CreatedTransfer {
                    transfer: transfer_url(strpath, tid),
                }),
        ),
        None => None,
    }
}

fn create_transfer_response(transfer_data: ReceiveBufferStatus) -> impl Responder {
    match transfer_data {
        ReceiveBufferStatus::Ready(d) => Transfer::Finished { data: d.clone() },
        ReceiveBufferStatus::NotReady => Transfer::InProgress,
        ReceiveBufferStatus::NotExisting => Transfer::Error {
            error_msg: "Transfer does not exist".to_string(),
        },
        ReceiveBufferStatus::SendOnlyFinished => Transfer::Finished { data: vec![] },
        ReceiveBufferStatus::Error(e) => Transfer::Error {
            error_msg: format!("{:?}", e),
        },
    }
}

#[get("/interfaces/{IFName}/transfers/{TransferID}")]
async fn query_transfer(
    _req: HttpRequest,
    data: web::Data<Interfaces>,
    path: web::Path<(String, u32)>,
) -> impl Responder {
    let (interface, transfer) = path.into_inner();
    let mut buf = data.bufs.lock().await;
    let bufs = buf.get_mut(&interface);
    match bufs {
        Some(ifd) => Some(create_transfer_response(
            ifd.buffer.get_recv_data(transfer).await,
        )),
        None => None,
    }
}

#[delete("/interfaces/{IFName}/transfers/{TransferID}")]
async fn delete_transfer(
    data: web::Data<Interfaces>,
    path: web::Path<(String, TransferID)>,
) -> impl Responder {
    let (interface, transfer) = path.into_inner();
    let mut bufs = data.bufs.lock().await;
    match bufs.get_mut(&interface) {
        Some(interf) => Some(serve_delete_transfer(interf, transfer).await),
        None => None,
    }
}

pub fn config(cfg: &mut web::ServiceConfig) {
    cfg.service(get_interface_list);
    cfg.service(get_interface_descr);
    cfg.service(get_interface_capabilities);
    cfg.service(get_interface_config);
    cfg.service(set_interface_config);
    cfg.service(list_interface_transfers);
    cfg.service(create_new_transfer);
    cfg.service(query_transfer);
    cfg.service(delete_transfer);
}

#[cfg(test)]
mod tests;

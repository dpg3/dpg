use std::collections::HashMap;

use crate::data_buffer::{
    mock_buffer::{DataBufferDataSide, MockDataBuffer},
    DataBuffer, DataBufferError,
};
use actix_web::{test, web, App, http::StatusCode};
use serde::Deserialize;
use tokio::sync::Mutex;

use super::*;

type MockWebData = web::Data<Interfaces>;

const TEST_SPI0_DEFAULT_CONFIG: InterfaceConfig = InterfaceConfig::SPI {
    bitrate: 5_000_000,
    cpha: SpiCPHA::SmplRising,
    cpol: SpiCPOL::Low,
    pre_delay: Some(8),
    post_delay: Some(8),
};
const TEST_SPI0_CAPABILITIES: InterfaceCapabilities = InterfaceCapabilities::SPI {
    max_bitrate: 50_000_000,
    flexible_frequency: false,
};

fn transfer_url(interf: &str, transfer: u32) -> String {
    format!("/interfaces/{}/transfers/{}", interf, transfer)
}

/// Create the State for the Test Application
fn create_state() -> MockWebData {
    let mut interfaces = HashMap::new();
    interfaces.insert(
        "SPI0".to_string(),
        InterfaceData {
            buffer: Box::new(MockDataBuffer::with_config(TEST_SPI0_DEFAULT_CONFIG)),
            kind: IFType::SPI,
            capabilities: TEST_SPI0_CAPABILITIES,
        },
    );
    web::Data::new(Interfaces {
        bufs: Mutex::new(interfaces),
    })
}

async fn add_transfer(dat: &MockWebData, trans: Vec<u8>) -> u32 {
    dat.bufs
        .lock()
        .await
        .get_mut("SPI0")
        .unwrap()
        .buffer
        .put_send_data(trans, true)
        .await
        .unwrap()
}

///@ return true if data was inserted
async fn complete_transfer(
    dat: &MockWebData,
    trans_id: u32,
    resp: Vec<u8>,
) -> Result<(), DataBufferError> {
    let mut guard = dat.bufs.lock().await;

    let b = &mut guard.get_mut("SPI0").unwrap().buffer;
    unsafe {
        let ptr = b.as_mut() as *mut (dyn DataBuffer);
        let ptr = ptr as *mut MockDataBuffer;
        (*ptr).put_recv_data(trans_id, resp)
    }
}

/// Checks if a GET request to the given URI using the Test App results in a 404 Error
async fn check_404(endp: &str) {
    let app = test::init_service(App::new().configure(config).app_data(create_state())).await;
    let req = test::TestRequest::default().uri(endp).to_request();
    let resp = test::call_service(&app, req).await;
    assert_eq!(resp.status().as_u16(), 404);
}

async fn json_get_with_state<T: for<'a> Deserialize<'a>>(endp: &str, state: MockWebData) -> T {
    let app = test::init_service(App::new().configure(config).app_data(state)).await;
    let req = test::TestRequest::default().uri(endp).to_request();
    let resp = test::call_service(&app, req).await;
    assert_eq!(resp.status().as_u16(), 200);
    let req = test::TestRequest::default().uri(endp).to_request();
    let resp: T = test::call_and_read_body_json(&app, req).await;
    resp
}

/// Does a get request and deserializes the Body Json to T.
/// Also checks if the resulting transfer returned 200 OK. For this it calls the handler twice.
async fn json_get<T: for<'a> Deserialize<'a>>(endp: &str) -> T {
    json_get_with_state(endp, create_state()).await
}

#[actix_web::test]
async fn test_interface_list() {
    let resp: Vec<InterfaceListEntry> = json_get("/interfaces").await;
    assert_eq!(resp.len(), 1);
    assert_eq!(resp[0].interface_type, IFType::SPI);
    assert_eq!(resp[0].link, "/interfaces/SPI0");
}

#[actix_web::test]
async fn test_get_wrong_interface() {
    check_404("/interfaces/SPI1").await;
}

#[actix_web::test]
async fn test_get_interface() {
    let resp: InterfaceDescription = json_get("/interfaces/SPI0").await;
    assert_eq!(resp.typ, IFType::SPI);
    assert_eq!(resp.capabilities, "/interfaces/SPI0/capabilities");
    assert_eq!(resp.config, "/interfaces/SPI0/config");
    assert_eq!(resp.transfers, "/interfaces/SPI0/transfers");
}

#[actix_web::test]
async fn test_get_config() {
    let resp: InterfaceConfig = json_get("/interfaces/SPI0/config").await;
    assert_eq!(resp, TEST_SPI0_DEFAULT_CONFIG);
}

#[actix_web::test]
async fn test_get_config_no_if() {
    check_404("/interfaces/SPI7/config").await;
}

#[actix_web::test]
async fn test_get_capabilities() {
    let resp: InterfaceCapabilities = json_get("/interfaces/SPI0/capabilities").await;
    assert_eq!(resp, TEST_SPI0_CAPABILITIES);
}

#[actix_web::test]
async fn test_get_capabilities_no_if() {
    check_404("/interfaces/SPI7/capabilities").await;
}

#[actix_web::test]
async fn test_get_transfers() {
    let resp: TransferList = json_get("/interfaces/SPI0/transfers").await;
    assert_eq!(0, resp.transfers.len());
    let state = create_state();
    let tid = add_transfer(&state, vec![]).await;
    let resp: TransferList = json_get_with_state("/interfaces/SPI0/transfers", state).await;
    assert_eq!(1, resp.transfers.len());
    assert_eq!(
        format!("/interfaces/SPI0/transfers/{}", tid),
        resp.transfers[0]
    );
}

#[actix_web::test]
async fn test_get_transfers_no_if() {
    check_404("/interfaces/SPI7/transfers").await;
}

#[actix_web::test]
async fn test_get_a_transfer_no_if() {
    check_404(&transfer_url("SPI3", 0)).await;
}

#[actix_web::test]
async fn test_get_a_transfer_no_transfer() {
    let resp: Transfer = json_get("/interfaces/SPI0/transfers/10").await;
    assert!(
        matches!(resp, Transfer::Error { .. }),
        "Expected an Error when getting a Transfer, that does not exist"
    )
}

#[actix_web::test]
async fn test_get_a_transfer() {
    let state = create_state();
    let tid = add_transfer(&state, vec![]).await;
    let resp: Transfer = json_get_with_state(&transfer_url("SPI0", tid), state.clone()).await;

    assert!(matches!(resp, Transfer::InProgress));
    assert!(
        complete_transfer(&state, tid, vec![1, 2, 3]).await.is_ok(),
        "Could not add recv data for transfer"
    );

    let compl_resp: Transfer = json_get_with_state(&transfer_url("SPI0", tid), state.clone()).await;
    assert!(
        compl_resp
            == Transfer::Finished {
                data: vec![1, 2, 3]
            }
    )
}

#[actix_web::test]
async fn test_post_transfer() {
    let state = create_state();
    let endp = "/interfaces/SPI0/transfers";
    let app = test::init_service(App::new().configure(config).app_data(state)).await;

    let body = NewTransfer {
        wordsize: Some(8),
        data: vec![5u32, 10u32],
        keep_received: Some(true),
    };
    let req = test::TestRequest::post()
        .uri(endp)
        .set_json(body)
        .to_request();
    let res: CreatedTransfer = test::call_and_read_body_json(&app, req).await;

    println!("Response to Post: {:?}", res);

    let req2 = test::TestRequest::get().uri(&res.transfer).to_request();
    let resp: Transfer = test::call_and_read_body_json(&app, req2).await;
    assert!(matches!(resp, Transfer::InProgress));
}

#[actix_web::test]
async fn test_delete_transfer() {
    let state = create_state();
    let tid = add_transfer(&state, vec![]).await;
    let endp = &transfer_url("SPI0", tid);
    let app = test::init_service(App::new().configure(config).app_data(state)).await;

    let req = test::TestRequest::default().uri(endp).to_request();
    let resp: Transfer = test::call_and_read_body_json(&app, req).await;
    assert!(matches!(resp, Transfer::InProgress));

    let delrq = test::TestRequest::delete().uri(endp).to_request();
    let resp: DeletedTransfer = test::call_and_read_body_json(&app, delrq).await;
    assert_eq!(resp.transfer, tid);

    let req = test::TestRequest::default().uri(endp).to_request();
    let resp: Transfer = test::call_and_read_body_json(&app, req).await;
    assert!(matches!(resp, Transfer::Error { .. }));
}

#[actix_web::test]
async fn test_put_config() {
    let state = create_state();
    let app = test::init_service(App::new().configure(config).app_data(state.clone())).await;
    let new_cfg = InterfaceConfig::SPI {
        bitrate: 100,
        cpha: SpiCPHA::SmplFalling,
        cpol: SpiCPOL::High,
        pre_delay: Some(0),
        post_delay: Some(16),
    };
    let req = test::TestRequest::put()
        .uri("/interfaces/SPI0/config")
        .set_json(new_cfg)
        .to_request();
    let resp = test::call_service(&app, req).await;
    assert_eq!(resp.status(), StatusCode::ACCEPTED);

    let resp: InterfaceConfig = json_get_with_state("/interfaces/SPI0/config", state).await;
    assert_eq!(resp, new_cfg);
}

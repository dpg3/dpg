use actix_web::body::BoxBody;
use actix_web::http::header::ContentType;
use actix_web::http::StatusCode;
use actix_web::HttpResponse;
use actix_web::HttpResponseBuilder;
use actix_web::Responder;
use actix_web::ResponseError;
use serde::Deserialize;
use serde::Serialize;

use derive_error::Error;

use crate::data_buffer::DataBufferError;
use crate::data_buffer::TransferID;

use super::services::ParameterError;

//============================================================
//                 Json Body Types
//============================================================

#[derive(Copy, Clone, Debug, Serialize, Deserialize, PartialEq, Eq)]
pub enum IFType {
    SPI,
}

/// Macro to implement the Responder Trait for a struct that is Serializeable with serde
/// This will just send an OK response with the JSON serialized Data.
macro_rules! json_responder {
    ($t: ident, $code: expr) => {
        impl Responder for $t {
            type Body = BoxBody;

            fn respond_to(self, _req: &actix_web::HttpRequest) -> HttpResponse<Self::Body> {
                let body = serde_json::to_string(&self).unwrap();
                HttpResponseBuilder::new(StatusCode::from_u16($code).unwrap())
                    .content_type(ContentType::json())
                    .body(body)
            }
        }
    };
}

#[derive(Debug, Serialize, Deserialize, PartialEq, Eq)]
pub struct InterfaceListEntry {
    pub interface_type: IFType,
    pub link: String,
}

impl InterfaceListEntry {
    pub fn new(typ: IFType, name: &str) -> Self {
        Self {
            interface_type: typ,
            link: format!("/interfaces/{}", name),
        }
    }
}

#[derive(Serialize, Deserialize)]
pub struct InterfaceList {
    pub ifs: Vec<InterfaceListEntry>,
}

impl Responder for InterfaceList {
    type Body = BoxBody;

    fn respond_to(self, _req: &actix_web::HttpRequest) -> HttpResponse<Self::Body> {
        let body = serde_json::to_string(&self.ifs).unwrap();
        HttpResponse::Ok()
            .content_type(ContentType::json())
            .body(body)
    }
}

#[derive(Serialize, Deserialize)]
pub struct InterfaceDescription {
    pub typ: IFType,
    pub config: String,
    pub capabilities: String,
    pub transfers: String,
}

impl InterfaceDescription {
    pub fn new(typ: IFType, interface_name: &str) -> Self {
        InterfaceDescription {
            typ,
            config: format!("/interfaces/{}/config", interface_name),
            capabilities: format!("/interfaces/{}/capabilities", interface_name),
            transfers: format!("/interfaces/{}/transfers", interface_name),
        }
    }
}

json_responder!(InterfaceDescription, 200u16);

#[derive(Serialize, Deserialize)]
/// Struct for Error Responses over the REST API
pub struct DPGError {
    pub msg: String,
}

json_responder!(DPGError, 200u16);

/// Defines the capabilities of an Interface.
#[derive(Debug, Serialize, Deserialize, Clone, Copy, PartialEq, Eq)]
#[serde(tag = "interface_type")]
pub enum InterfaceCapabilities {
    SPI {
        /// The maximum bitrate, that the SPI supports
        max_bitrate: u32,
        /// if true => the SPI supports any frequency below max_bitrate.
        /// if false => the SPI only supports integer divisions of the max_bitrate
        flexible_frequency: bool,
    },
}

json_responder!(InterfaceCapabilities, 200u16);

#[allow(non_camel_case_types)]
#[derive(Copy, Clone, Debug, Serialize, Deserialize, PartialEq, Eq)]
pub enum SpiCPHA {
    /// Sample Data on the Rising Edge, write on the falling
    SmplRising,
    /// Sample Data on the Falling Edge, write on the rising
    SmplFalling,
}

#[derive(Copy, Clone, Debug, Serialize, Deserialize, PartialEq, Eq)]
pub enum SpiCPOL {
    /// Clock idle polarity LOW
    Low,
    /// Clock idle polarity HIGH
    High,
}

#[derive(Serialize, Deserialize, Debug, Clone, Copy, PartialEq, Eq)]
#[serde(tag = "interface_type")]
pub enum InterfaceConfig {
    SPI {
        bitrate: u32,
        cpha: SpiCPHA,
        cpol: SpiCPOL,
        pre_delay: Option<u8>,
        post_delay: Option<u8>,
    },
}

json_responder!(InterfaceConfig, 200u16);

#[derive(Debug, Serialize, Deserialize)]
pub struct TransferList {
    pub transfers: Vec<String>,
}

json_responder!(TransferList, 200u16);

#[derive(Serialize, Deserialize, PartialEq, Eq)]
#[serde(tag = "status")]
pub enum Transfer {
    Error { error_msg: String },
    InProgress,
    Finished { data: Vec<u8> },
}

json_responder!(Transfer, 200u16);

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct NewTransfer {
    pub wordsize: Option<u32>,
    pub data: Vec<u32>,
    pub keep_received: Option<bool>,
}

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct CreatedTransfer {
    pub transfer: String,
}

json_responder!(CreatedTransfer, 200u16);

#[derive(Clone, Serialize, Deserialize)]
pub struct DeletedTransfer {
    pub transfer: TransferID,
}

json_responder!(DeletedTransfer, 200u16);

//============================================================
//                 Query Types
//============================================================

#[derive(Deserialize, Clone, Debug, Copy)]
pub struct InterfaceTypeQuery {
    pub if_type: Option<IFType>,
}

//============================================================
//                 Error Types
//============================================================

#[derive(Debug, Serialize)]
pub struct JsonError {
    pub msg: String,
}

json_responder!(JsonError, 400);

#[derive(Debug, Error)]
pub enum DPGAPIError {
    DataBufferError(DataBufferError),
    ParameterError(ParameterError),
}

impl From<&DPGAPIError> for JsonError {
    fn from(e: &DPGAPIError) -> Self {
        return JsonError {
            msg: format!("{}", e),
        };
    }
}

impl ResponseError for DPGAPIError {
    fn status_code(&self) -> StatusCode {
        StatusCode::BAD_REQUEST
    }

    fn error_response(&self) -> HttpResponse<BoxBody> {
        let json: JsonError = self.into();
        let body = serde_json::to_string(&json).unwrap();
        HttpResponseBuilder::new(self.status_code())
            .content_type(ContentType::json())
            .body(body)
    }
}

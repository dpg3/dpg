// Module containing service functions, that operate on the correct types and
// and can be used by REST API Functions.

use std::collections::HashMap;

use actix_web::HttpResponse;

use hps_webserver_libs::utils::{byteiter::U32ByteIter};
use crate::{data_buffer::TransferID,  InterfaceData};

use super::types::{
    DPGAPIError, DeletedTransfer, IFType, InterfaceConfig, InterfaceListEntry, NewTransfer,
};

pub type AllInterfaces = HashMap<String, InterfaceData>;

#[derive(Debug, derive_error::Error)]
pub enum ParameterError {
    #[error(no_from, non_std)]
    /// Word size must be a multiple of 8, but was not
    WrongWordsize(u32),
}

/// Get all interfaces matching the type filter and returns an iterator over them.
/// @param type_filter If None, then all Interfaces are returned. Otherwise only interfaces with the same type
pub fn serv_get_interface_list<'mutex>(
    interfaces: &'mutex mut AllInterfaces,
    type_filter: Option<IFType>,
) -> impl Iterator<Item = InterfaceListEntry> + 'mutex {
    interfaces
        .iter()
        .filter(move |(_name, data)| type_filter.map_or(true, |typ| typ == data.kind))
        .map(|(name, data)| InterfaceListEntry::new(data.kind, name))
}

pub async fn serv_create_interface(
    interf: &mut InterfaceData,
    new_transfer: NewTransfer,
) -> Result<u32, DPGAPIError> {
    let wordsize = new_transfer.wordsize.unwrap_or(8);
    if wordsize % 8 != 0 {
        return Err(ParameterError::WrongWordsize(wordsize).into());
    }
    let bytes_per_word = wordsize / 8;
    let keep_received = new_transfer.keep_received.unwrap_or(true);
    let data = new_transfer
        .data
        .iter()
        .flat_map(|w| U32ByteIter::new(*w, bytes_per_word))
        .collect();
    Ok(interf.buffer.put_send_data(data, keep_received).await?)
}

pub async fn serve_delete_transfer(
    interf: &mut InterfaceData,
    to_delete: TransferID,
) -> Result<DeletedTransfer, DPGAPIError> {
    interf.buffer.delete_recv_data(to_delete).await?;
    Ok(DeletedTransfer {
        transfer: to_delete,
    })
}

pub async fn serve_update_config(
    interf: &mut InterfaceData,
    config: InterfaceConfig,
) -> Result<HttpResponse, DPGAPIError> {
    interf.buffer.set_new_config(config).await?;
    Ok(HttpResponse::Accepted().finish())
}

pub async fn serve_get_config(interf: &mut InterfaceData) -> Result<InterfaceConfig, DPGAPIError> {
    Ok(interf.buffer.get_curr_config().await?)
}

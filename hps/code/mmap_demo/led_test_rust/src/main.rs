
use std::fs::{File, OpenOptions};
use memmap::MmapOptions;

const HW_REGS_BASE: u32 = 0xff200000;
const HW_REGS_SPAN: u32 = 0x00200000;
const HW_REGS_MASK: u32 = HW_REGS_SPAN - 1;
const LED_PIO_BASE: u32 = 0x0;

/*
fn do_stuff() -> io::Result<()>{

    let file:File = OpenOptions::new()
        .read(true)
        .write(true)
        .open("/dev/mem")?;

}
*/

fn main() {

    /* open file */
    let file:File = OpenOptions::new()
        .read(true)
        .write(true)
        .open("/dev/mem").expect("Error open");

    /* map file */
    let mmap = unsafe {
        MmapOptions::new()
            .offset(HW_REGS_BASE as u64)
            .len(HW_REGS_SPAN as usize)
            .map_mut(&file).expect("Error map")
    };

    /* Get the address that maps to the LEDs */
    let h2p_lw_led_addr = (mmap.as_ptr() as u32 + (LED_PIO_BASE & HW_REGS_MASK)) as *mut u32;

    /* Add 1 to the PIO register */
    unsafe { *h2p_lw_led_addr = *h2p_lw_led_addr + 1; }
}

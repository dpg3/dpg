#!/usr/bin/env python3

from pathlib import Path
from vunit import VUnit

VU = VUnit.from_argv()


#############################################
# add UVVM
#
# to add a VIP library simply add it to the uvvm_libraries list.
#############################################
uvvm_libraries = [
    "uvvm_util",
    "uvvm_vvc_framework",
    "bitvis_vip_scoreboard",
    "bitvis_vip_clock_generator",
    "bitvis_vip_spi",
    "bitvis_vip_avalon_st",
    "bitvis_vip_avalon_mm",
]

UNITS_PATH = Path(__file__).resolve().parents[1]
VHDL_BASEDIR = Path(__file__).resolve().parents[2]
uvvm_root_path = VHDL_BASEDIR / "uvvm"

for libname in uvvm_libraries:
    location = uvvm_root_path / libname / "sim" / libname
    print('adding library "', libname, '" from directory:', location)
    VU.add_external_library(libname, location)


lib = VU.add_library("lib")


#############################################
# Adding source files
#############################################

# Packages

# rtl files
lib.add_source_files(
    VHDL_BASEDIR / "databuffer" / "src" / "grp_databuffer" / "*" / "src" / "*.vhd"
)
lib.add_source_files(VHDL_BASEDIR / "spi_master" / "src" / "*.vhd")

# testbench(es)
lib.add_source_files(UNITS_PATH / "tb" / "*.vhd")

VU.add_osvvm()


#############################################
# automatically load wave.do and execute run -all
#############################################
for tb in lib.get_test_benches():
    gui_do_path = Path(__file__).resolve().parent / "gui.do"
    tb.set_sim_option("modelsim.init_file.gui", str(gui_do_path))


VU.main()

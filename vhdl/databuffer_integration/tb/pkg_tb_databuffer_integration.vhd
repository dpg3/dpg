----------------------------------------------------------------------------------------------------------------
-- Workfile:    pkg_databuffer_integration.vhd
-- Author:      Tobias Kerbl, Alexander Daum
----------------------------------------------------------------------------------------------------------------
-- Description: This package contains all constants, functions and definitions for the testbench of the'Databuffer'. 
--              It also contains constants for the Simulation via UVVM (BFMs, etc.)
----------------------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------
-- Libraries
--------------------------------------------------------------------------

-- Databuffer library
library work; 
use work.pkg_databuffer.all;

library ieee;
use ieee.std_logic_1164.all;


-- UVVM librarys
library uvvm_util;
context uvvm_util.uvvm_util_context;
library uvvm_vvc_framework;
use uvvm_vvc_framework.ti_vvc_framework_support_pkg.all;

-- -- Clock Generator librarys
library bitvis_vip_clock_generator;
context bitvis_vip_clock_generator.vvc_context;

-- -- Avalon MM librarys
library bitvis_vip_avalon_mm;
use bitvis_vip_avalon_mm.avalon_mm_bfm_pkg.all;

-- SPI-Slave librarys
library bitvis_vip_spi;
use bitvis_vip_spi.spi_bfm_pkg.all;

--------------------------------------------------------------------------
-- Package
--------------------------------------------------------------------------
package pkg_tb_databuffer_integration is

    ---------------------------------------
    -- Constants
    ---------------------------------------
    constant cAvalonStMasterIdx    : natural := 0;
    constant cAvalonStSlaveIdx     : natural := 1;
    constant cAvalonMmMasterIdx    : natural := 0;
    constant cClockGeneratorIdx    : natural := 1;

    constant cAvalonStChannelWidth : natural := 1;
    constant cAvalonStErrorWidth   : natural := 1;
    constant cAvalonStEmptyWidth   : natural := 1;

    constant cMaxTimeToWait        : time := 1 ms;

    ---------------------------------------
    -- Avalon MM Master - BFM Config
    ---------------------------------------
    constant C_AVALON_MM_BFM_CONFIG : t_avalon_mm_bfm_config := (
        max_wait_cycles          => 10,
        max_wait_cycles_severity => TB_FAILURE,
        clock_period             => cClkPeriod,
        clock_period_margin      => 0 ns,
        clock_margin_severity    => TB_ERROR,
        setup_time               => 0 ns,
        hold_time                => 0 ns,
        bfm_sync                 => SYNC_ON_CLOCK_ONLY,
        match_strictness         => MATCH_EXACT,
        num_wait_states_read     => 0,
        num_wait_states_write    => 0,
        use_waitrequest          => true,
        use_readdatavalid        => true,
        use_response_signal      => false,
        use_begintransfer        => false,
        id_for_bfm               => ID_BFM,
        id_for_bfm_wait          => ID_BFM_WAIT,
        id_for_bfm_poll          => ID_BFM_POLL
    );
    ----------------------------------------
    -- SPI Slave - BFM Config
    ----------------------------------------
    constant C_SPI_BFM_CONFIG : t_spi_bfm_config := (
        CPOL             => '0',
        CPHA             => '0',
        spi_bit_time     => 2 * cClkPeriod,
        ss_n_to_sclk     => 0 ns, --4 * cClkPeriod,  This has to be 0 ns because UVVM doesn't use this delay as it should 
        sclk_to_ss_n     => 4 * cClkPeriod,    
        inter_word_delay => 0 ns,
        match_strictness => MATCH_EXACT,
        id_for_bfm       => ID_BFM,
        id_for_bfm_wait  => ID_BFM_WAIT,
        id_for_bfm_poll  => ID_BFM_POLL
    );
end pkg_tb_databuffer_integration;

-- package body
package body pkg_tb_databuffer_integration is
-- currently nothing to declare here
end pkg_tb_databuffer_integration;

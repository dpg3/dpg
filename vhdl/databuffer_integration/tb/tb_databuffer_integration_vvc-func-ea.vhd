----------------------------------------------------------------------------------------------------------------
-- Workfile:    tb_databuffer_integration_vvc-func-ea.vhd
-- Author:      Tobias Kerbl, Alexander Daum
----------------------------------------------------------------------------------------------------------------
-- Description: This file contains the testcases of the UVVM tests.
----------------------------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library vunit_lib;
context vunit_lib.vunit_context;

library uvvm_util;
context uvvm_util.uvvm_util_context;

library uvvm_vvc_framework;
use uvvm_vvc_framework.ti_vvc_framework_support_pkg.all;

library bitvis_vip_clock_generator;
context bitvis_vip_clock_generator.vvc_context;

-- UVVM VIP Avalon MM
library bitvis_vip_avalon_mm;
use bitvis_vip_avalon_mm.vvc_methods_pkg.all;
use bitvis_vip_avalon_mm.td_vvc_framework_common_methods_pkg.all;

library bitvis_vip_spi;
use bitvis_vip_spi.vvc_methods_pkg.all;
use bitvis_vip_spi.td_vvc_framework_common_methods_pkg.all;

-- Databuffer libraries
use work.pkg_databuffer.all;
use work.pkg_tb_databuffer_integration.all;

-- Test bench entity
entity tb_DataBufferIntegration is
    generic (runner_cfg : string); -- used by VUnit
end entity tb_DataBufferIntegration;

architecture Bhv of tb_DataBufferIntegration is

-----------------------------------------------------------------------------
-- General Types
-----------------------------------------------------------------------------
-- Avalon Memory Mapped Write

    type aDataset is array(natural range <>) of std_ulogic_vector(7 downto 0);
    type aSendData is array(natural range <>) of std_ulogic_vector(31 downto 0);

    function genCountingDataset(size_ref: aDataset) return aDataset is
        variable result : aDataset(size_ref'range);
    begin
        for i in result'range loop 
            result(i) := std_ulogic_vector(to_unsigned(i, 8));
        end loop;
        return result;
    end;


    function sendDataFromDataset(ds: aDataset; size_ref: aSendData) return aSendData is
        variable result : aSendData(size_ref'range);
    begin
        assert ds'length = 4*result'length report "Invalid Dataset length" severity failure;
        for i in result'range loop
            for j in 0 to 3 loop 
                result(i)((8*(j+1)-1) downto (8*j)) := ds(ds'low + 4*i + j);
            end loop;
        end loop;
        return result;
    end;

    constant cTinyDataset : aDataset(0 to 3) := (x"55", x"AA", x"17", x"33");


-----------------------------------------------------------------------------
-- Signals
-----------------------------------------------------------------------------
signal ResetAsync    : std_ulogic := '0';
signal DataAvailable : std_ulogic := '0';

begin

  -----------------------------------------------------------------------------
  -- Instantiate DUT
  -----------------------------------------------------------------------------
    i_test_harness : entity work.th_databuffer_integration_vvc
    generic map (
        gLengthOfRam => 15
    )
    port map (
        reset_n_i        => ResetAsync,
        data_available_o => DataAvailable
    );

  -----------------------------------------------------------------------------
  -- Main test process
  -----------------------------------------------------------------------------
    p_main : process

    -- Test datasets
    -----------------------------------------------------------------------------
    variable seed1 : positive := 17;
    variable seed2 : positive := 1;
    impure function genRandomDataset(size_ref: aDataset) return aDataset is
        variable result : aDataset(size_ref'range);
        variable rand : real;
        variable randInt : integer;
    begin
        for i in result'range loop 
            uniform(seed1, seed2, rand);
            randInt := integer(round(rand * 256.0 - 0.5));
            result(i) := std_ulogic_vector(to_unsigned(randInt, 8));
        end loop;
        return result;
    end;

    -- Test helper functions/procedures
    -----------------------------------------------------------------------------
    procedure ReleaseReset is
    begin
        wait for 20 * cClkPeriod;
        ResetAsync <= not('0');
        wait for 20 * cClkPeriod;
    end procedure ReleaseReset;

    -- Activates the interrupt
    procedure ActivateIRQ is
    begin
        -- activate irq
        avalon_mm_write(AVALON_MM_VVCT, cAvalonMmMasterIdx, to_unsigned(cNrIRQMask, cAddressWidth), x"00000001", "Activate IRQ");
        await_completion(AVALON_MM_VVCT, cAvalonMmMasterIdx, cMaxTimeToWait, "");

        wait for 100 ns;

    end procedure ActivateIRQ;
    
    -- Releases the interrupt
    procedure ReleaseIRQ is
    begin
        -- release irq
        avalon_mm_write(AVALON_MM_VVCT, cAvalonMmMasterIdx, to_unsigned(cNrIRQStatus, cAddressWidth), x"00000000", "Release IRQ");
        await_completion(AVALON_MM_VVCT, cAvalonMmMasterIdx, cMaxTimeToWait, "");
    end procedure ReleaseIRQ;

    -- Start a transfer by setting the TxStart bit to 0
    -- Check if the transfer-bit ist set correctly (depends on the boolean-parameter)
    procedure StartTransferAndCheckTransferBit(
        CheckForTransferBit : in boolean
    ) is
    begin
        -- start buffer
        avalon_mm_write(AVALON_MM_VVCT, cAvalonMmMasterIdx, to_unsigned(cNrTxStart, cAddressWidth), x"00000001", "Set start bit");
        await_completion(AVALON_MM_VVCT, cAvalonMmMasterIdx, cMaxTimeToWait, "");

        -- Check transfer bit
        if(CheckForTransferBit = true) then
            avalon_mm_check(AVALON_MM_VVCT, cAvalonMmMasterIdx, to_unsigned(cNrTxTransfer, cAddressWidth), x"00000001", "Check if transfer bit is 1 ");
            await_completion(AVALON_MM_VVCT, cAvalonMmMasterIdx, cMaxTimeToWait, "");
        end if;
    end procedure StartTransferAndCheckTransferBit;

    -- Fills the buffer with avalon mm writes
    procedure FillBuffer (
        InputData : in aDataset;
        WaitForCompletion : in boolean
    ) is
        variable send_data : aSendData(InputData'low/4 to InputData'high/4);
    begin
        send_data := sendDataFromDataset(InputData, send_data);
        -- fill buffer
        for i in send_data'range loop
            -- write to avalon mm
            avalon_mm_write(AVALON_MM_VVCT, cAvalonMmMasterIdx, to_unsigned(cNrTxData, cAddressWidth), send_data(i), "Write to databuffer: " & integer'image(i));
            -- wait till write is finished
            if(WaitForCompletion = true) then
                await_completion(AVALON_MM_VVCT, cAvalonMmMasterIdx, cMaxTimeToWait, "");
            end if;
        end loop;
    end procedure;

    procedure DoSpiTransfer(expected_data: aDataset; send_data: aDataset; do_await_completion: boolean) is
    begin
        assert expected_data'low = send_data'low and expected_data'high = send_data'high report "SendData and Expected Data must have same length" severity failure;
        spi_slave_transmit_and_check(SPI_VVCT, 0, send_data(send_data'low), expected_data(send_data'low), "", ERROR, START_TRANSFER_ON_NEXT_SS);
        for i in send_data'low+1 to send_data'high loop
            spi_slave_transmit_and_check(SPI_VVCT, 0, send_data(i), expected_data(i), "", ERROR, START_TRANSFER_IMMEDIATE);
        end loop;

        if do_await_completion then
            await_completion(SPI_VVCT, 0, cMaxTimeToWait, "");
        end if;
    end procedure;

    procedure DoReadDataBack(expected_data: aDataset; wait_time : time) is
        variable exp_data_words : aSendData(expected_data'low/4 to expected_data'high/4);
    begin
        exp_data_words := sendDataFromDataset(expected_data, exp_data_words);
        await_value(DataAvailable, '1', 0 ns, wait_time, "Waiting for Interrupt");
        ReleaseIRQ;

        for i in exp_data_words'range loop
            avalon_mm_check(AVALON_MM_VVCT, cAvalonMmMasterIdx, to_unsigned(cNrRxData, cAddressWidth), exp_data_words(i), "Check if transfer bits are read correctly");
            await_completion(AVALON_MM_VVCT, cAvalonMmMasterIdx, cMaxTimeToWait, "");
        end loop;
    end;

        variable vDataset4 : aDataset(0 to 3);
        variable vDataset4_2 : aDataset(0 to 3);
        variable vDataset16 : aDataset(0 to 15);
        variable vDataset16_2 : aDataset(0 to 15);
        variable vDataset16_3 : aDataset(0 to 15);
        variable vDataset16_4 : aDataset(0 to 15);
        variable vDataset64 : aDataset(0 to 63);

    begin
        test_runner_setup(runner, runner_cfg);         -- VUnit setup

        report("###################################") severity note;

        -- Wait for UVVM to finish initialization
        await_uvvm_initialization(VOID);

        while test_suite loop -- loop used to run several VUnit testcases
            --------------------------------------------------------------------------------------------------    
            -- Test 1:
            -- This test is for checking, that values are correctly written into the write-register
            -- The value of the write-register, will be written into the ram block afterwards
            --------------------------------------------------------------------------------------------------
            if run("Test 1: Single Transfer, four byte") then
                start_clock(CLOCK_GENERATOR_VVCT, cClockGeneratorIdx, "Start clock generator");
                ReleaseReset;
                ActivateIRQ;

                vDataset4 := genRandomDataset(vDataset4);
                vDataset4_2 := genRandomDataset(vDataset4);

                -- fill buffer
                FillBuffer(vDataset4, true);
                
                -- Start transfer
                StartTransferAndCheckTransferBit(false);
                DoSpiTransfer(vDataset4, vDataset4_2, true);
                DoReadDataBack(vDataset4_2, 30 ns);
            --------------------------------------------------------------------------------------------------    
            -- Test 3:
            -- This test is for checking, that written values are correctly outputed via the avalon streaming
            -- interface (--> Input for SPI-Master)
            -- In this test, there will be no buffer swap. This means, that one wave of bytes will be inserted
            -- and transmitted
            --------------------------------------------------------------------------------------------------
            elsif run("Test 2: Single Transfer, multiple bytes") then
                start_clock(CLOCK_GENERATOR_VVCT, cClockGeneratorIdx, "Start clock generator");
                ReleaseReset;
                ActivateIRQ;

                vDataset16 := genRandomDataset(vDataset16);
                vDataset16_2 := genRandomDataset(vDataset16);

                -- fill buffer
                FillBuffer(vDataset16, true);
                
                -- Start transfer
                StartTransferAndCheckTransferBit(false);
                DoSpiTransfer(vDataset16, vDataset16_2, true);
                DoReadDataBack(vDataset16_2, 30 ns);
            elsif run("Test 3: Write-Read Repeat") then
                start_clock(CLOCK_GENERATOR_VVCT, cClockGeneratorIdx, "Start clock generator");
                ReleaseReset;
                ActivateIRQ;

                for i in 1 to 4 loop
                    vDataset16 := genRandomDataset(vDataset16);
                    vDataset16_2 := genRandomDataset(vDataset16);

                    -- fill buffer
                    FillBuffer(vDataset16, true);
                    
                    -- Start transfer
                    StartTransferAndCheckTransferBit(false);
                    DoSpiTransfer(vDataset16, vDataset16_2, true);
                    DoReadDataBack(vDataset16_2, 30 ns);
                end loop;
            elsif run("Test 4: Keep Buffer Busy") then
                start_clock(CLOCK_GENERATOR_VVCT, cClockGeneratorIdx, "Start clock generator");
                ReleaseReset;
                ActivateIRQ;

                vDataset16 := genRandomDataset(vDataset16);
                vDataset16_2 := genRandomDataset(vDataset16);

                    -- fill buffer
                FillBuffer(vDataset16, false);
                StartTransferAndCheckTransferBit(false);
                DoSpiTransfer(vDataset16, vDataset16_2, false);

                for i in 1 to 4 loop
                    vDataset16_3 := genRandomDataset(vDataset16);
                    vDataset16_4 := genRandomDataset(vDataset16);

                    -- fill buffer
                    FillBuffer(vDataset16_3, false);
                    
                    -- Start transfer
                    StartTransferAndCheckTransferBit(false);
                    DoSpiTransfer(vDataset16_3, vDataset16_4, false);
                    DoReadDataBack(vDataset16_2, 10 us);

                    vDataset16_2 := vDataset16_4;
                end loop;

                DoReadDataBack(vDataset16_2, 10 us);
            end if;
        end loop;

        test_runner_cleanup(runner);

    end process; 
end architecture;

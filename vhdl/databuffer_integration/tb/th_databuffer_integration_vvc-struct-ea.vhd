----------------------------------------------------------------------------------------------------------------
-- Workfile:    th_databuffer_integration_vvc-struct-ea.vhd
-- Author:      Tobias Kerbl, Alexander Daum
----------------------------------------------------------------------------------------------------------------
-- Description: This file contains the test-harness for the UVVM tests.
----------------------------------------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

-- Databuffer library
use work.pkg_databuffer.all;
use work.pkg_tb_databuffer_integration.all;

-- Avalon ST librarys
library bitvis_vip_avalon_st;
use bitvis_vip_avalon_st.avalon_st_bfm_pkg.all;

-- Avalon MM librarys
library bitvis_vip_avalon_mm;
use bitvis_vip_avalon_mm.avalon_mm_bfm_pkg.all;

-- UVVM librarys
library uvvm_util;
context uvvm_util.uvvm_util_context;
library uvvm_vvc_framework;
use uvvm_vvc_framework.ti_vvc_framework_support_pkg.all;

-- Clock Generator librarys
library bitvis_vip_clock_generator;
context bitvis_vip_clock_generator.vvc_context;

-- SPI-Slave librarys
library bitvis_vip_spi;
use bitvis_vip_spi.spi_bfm_pkg.all;

-- Test harness entity
entity th_databuffer_integration_vvc is
    generic (
                gLengthOfRam : natural := cLengthOfRAM
            );
    port (
             reset_n_i        : in std_ulogic;
             data_available_o : out std_ulogic 
         );
begin
end entity th_databuffer_integration_vvc;

----------------------------------------------------------------------
-- Test harness architecture
----------------------------------------------------------------------
architecture Struct of th_databuffer_integration_vvc is

    constant cSPIAddressWidth          : natural := 32;
    constant cClkPerHalfSclkWidth      : natural := 16;
    constant cPrePostDelayCounterWidth : natural := 8;
    constant cBytesPerPacket : natural := 8; 

  -- DSP interface and general control signals
    signal clk            : std_logic := '0';
    signal data_available : std_logic := '0';
  --signal rst_async : std_logic := '0';

    constant cAvalonSPIMmMasterIdx : natural := 1;

  ----------------------------------------
  -- Avalon Streaming Slave - Interface
  ----------------------------------------
    signal avalon_st_if_sink : t_avalon_st_if(
        channel(cAvalonStChannelWidth-1 downto 0),
        data(cAvalonSTDataWidth-1 downto 0),
        data_error(cAvalonStErrorWidth-1 downto 0),
        empty(cAvalonStEmptyWidth-1 downto 0)
    ) := init_avalon_st_if_signals(false, cAvalonStChannelWidth, cAvalonSTDataWidth, cAvalonStErrorWidth, cAvalonStEmptyWidth);

  ----------------------------------------
  -- Avalon Streaming Master - Interface
  ----------------------------------------
signal avalon_st_if_source : t_avalon_st_if(
channel(cAvalonStChannelWidth-1 downto 0),
data(cAvalonSTDataWidth-1 downto 0),
data_error(cAvalonStErrorWidth-1 downto 0),
empty(cAvalonStEmptyWidth-1 downto 0)
  ) := init_avalon_st_if_signals(true, cAvalonStChannelWidth, cAvalonSTDataWidth, cAvalonStErrorWidth, cAvalonStEmptyWidth);

  ----------------------------------------
  -- Avalon MM Slave - Interface
  ----------------------------------------
  signal avalon_mm_if : t_avalon_mm_if(
  address(cAddressWidth - 1 downto 0),
  writedata(cAvalonMMDataWidth - 1 downto 0),
  readdata(cAvalonMMDataWidth - 1 downto 0),
  byte_enable((cAvalonMMDataWidth / 8) - 1 downto 0)
  ) := init_avalon_mm_if_signals(cAddressWidth, cAvalonMMDataWidth);

  
  ----------------------------------------
  -- Avalon MM Slave for SPI - Interface
  ----------------------------------------
  signal avalon_mm_if_spi : t_avalon_mm_if(
  address(cSPIAddressWidth - 1 downto 0),
  writedata(cAvalonMMDataWidth - 1 downto 0),
  readdata(cAvalonMMDataWidth - 1 downto 0),
  byte_enable((cAvalonMMDataWidth / 8) - 1 downto 0)
  ) := init_avalon_mm_if_signals(cSPIAddressWidth, cAvalonMMDataWidth);


  signal spi_if : t_spi_if := init_spi_if_signals(C_SPI_BFM_CONFIG, false);

begin
  -----------------------------------------------------------------------------
  -- Instantiate the concurrent procedure that initializes UVVM
  -----------------------------------------------------------------------------
    i_ti_uvvm_engine : entity uvvm_vvc_framework.ti_uvvm_engine;

  -----------------------------------------------------------------------------
  -- Instantiate DUT
  -----------------------------------------------------------------------------
    i_databuffer : entity work.databuffer
    generic map (
                    gAddressWidth       => cAddressWidth,
                    gAvalonMMDataWidth  => cAvalonMMDataWidth,
                    gAvalonSTDataWidth  => cAvalonSTDataWidth,
                    gLengthOfRAM        => gLengthOfRam
                )
    port map (
-- General
                 clk              => clk,
                 reset_n          => reset_n_i, --not rst_async,

    -- Avalon MM Slave
                 address          => avalon_mm_if.address,
                 write            => avalon_mm_if.write,
                 writedata        => avalon_mm_if.writedata,
                 read             => avalon_mm_if.read,
                 waitrequest      => avalon_mm_if.waitrequest,
                 readdata         => avalon_mm_if.readdata,
                 readdatavalid    => avalon_mm_if.readdatavalid,

    -- Avalon ST Source
                 readySrc         => avalon_st_if_sink.ready, --'0',
                 dataSrc          => avalon_st_if_sink.data,
                 validSrc         => avalon_st_if_sink.valid,
                 startofpacketSrc => avalon_st_if_sink.start_of_packet,
                 endofpacketSrc   => avalon_st_if_sink.end_of_packet,

    -- Avalon ST Sink
                 dataSink         => avalon_st_if_source.data,
                 validSink        => avalon_st_if_source.valid,
                 readySink        => avalon_st_if_source.ready,

    -- Avalon Interrupt
                 dataAvailable    => data_available
             );

    i_spi_master: entity work.spi_master
    generic map (
                    gDataWidthST              => cAvalonSTDataWidth,
                    gDataWidthMM              => cAvalonMMDataWidth,
                    gAddressWidth             => cSPIAddressWidth,
                    gClkPerHalfSclkWidth      => cClkPerHalfSclkWidth,
                    gPrePostDelayCounterWidth => cPrePostDelayCounterWidth)
    port map (
                 inResetAsync        => reset_n_i,
                 iClk                => clk,
                 oReady              => avalon_st_if_sink.ready,
                 iValid              => avalon_st_if_sink.valid,
                 iData               => avalon_st_if_sink.data,
                 iEndOfPackageSink   => avalon_st_if_sink.end_of_packet,
                 iStartOfPackageSink => avalon_st_if_sink.start_of_packet,
                 oValid              => avalon_st_if_source.valid,
                 oData               => avalon_st_if_source.data,
                 iAddress            => avalon_mm_if_spi.address,
                 iRead               => avalon_mm_if_spi.read,
                 oReadData           => avalon_mm_if_spi.readdata,
                 iWrite              => avalon_mm_if_spi.write,
                 iWriteData          => avalon_mm_if_spi.writedata,
                 oSCLK               => spi_if.sclk,
                 oCS                 => spi_if.ss_n,
                 oMOSI               => spi_if.mosi,
                 iMISO               => spi_if.miso);

    -----------------------------------------------------------------------------
    -- Avalon MM Master
    -----------------------------------------------------------------------------
    i_avalon_mm_master : entity bitvis_vip_avalon_mm.avalon_mm_vvc
    generic map(
                   GC_ADDR_WIDTH       => cAddressWidth,
                   GC_DATA_WIDTH       => cAvalonMMDataWidth,
                   GC_INSTANCE_IDX     => cAvalonMmMasterIdx,
                   GC_AVALON_MM_CONFIG => C_AVALON_MM_BFM_CONFIG
               )
    port map(
                clk                     => clk,
                avalon_mm_vvc_master_if => avalon_mm_if
            );

    i_avalon_mm_master_spi : entity bitvis_vip_avalon_mm.avalon_mm_vvc
    generic map(
                   GC_ADDR_WIDTH       => cSPIAddressWidth,
                   GC_DATA_WIDTH       => cAvalonMMDataWidth,
                   GC_INSTANCE_IDX     => cAvalonSPIMmMasterIdx,
                   GC_AVALON_MM_CONFIG => C_AVALON_MM_BFM_CONFIG
               )
    port map(
                clk                     => clk,
                avalon_mm_vvc_master_if => avalon_mm_if_spi
            );

  -----------------------------------------------------------------------------
  -- SPI Slave 
  -----------------------------------------------------------------------------
  spi_vcc_slave : entity bitvis_vip_spi.spi_vvc
    generic map(
      GC_DATA_WIDTH       => cAvalonSTDataWidth,
      GC_DATA_ARRAY_WIDTH => cBytesPerPacket,
      GC_INSTANCE_IDX     => 0,
      GC_MASTER_MODE      => false,
      GC_SPI_CONFIG       => C_SPI_BFM_CONFIG
    )
    port map(
      spi_vvc_if => spi_if
    );

    -----------------------------------------------------------------------------
    -- Async Reset (low active)
    -----------------------------------------------------------------------------
    -- Toggle the reset after 5 clock periods
    --p_RstAsync : rst_async <= '1', '0' after 5 * cClkPeriod;

    -----------------------------------------------------------------------------
    -- Clock Generator VVC
    -----------------------------------------------------------------------------
    i_clock_generator_vvc : entity bitvis_vip_clock_generator.clock_generator_vvc
    generic map(
                   GC_INSTANCE_IDX    => cClkGen,
                   GC_CLOCK_NAME      => "Databuffer-Clock",
                   GC_CLOCK_PERIOD    => cClkPeriod,
                   GC_CLOCK_HIGH_TIME => cClkPeriod / 2
               )
    port map(
                clk => clk
            );

    -----------------------------------------------------------------------------
    -- IRQ Output
    -----------------------------------------------------------------------------
    data_available_o <= data_available;

end Struct;

----------------------------------------------------------------------------------------------------------------
-- Workfile:    tb_SpiMaster-Bhv-ea.vhd
-- Author:      Matthias Possenig
----------------------------------------------------------------------------------------------------------------
-- Description: This file contains the test-harness for the UVVM tests.
----------------------------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library vunit_lib;
context vunit_lib.vunit_context;
library uvvm_util;
context uvvm_util.uvvm_util_context;
library uvvm_vvc_framework;
use uvvm_vvc_framework.ti_vvc_framework_support_pkg.all;
library bitvis_vip_clock_generator;
context bitvis_vip_clock_generator.vvc_context;
-- UVVM VIP Avalon MM
library bitvis_vip_avalon_mm;
use bitvis_vip_avalon_mm.vvc_methods_pkg.all;
use bitvis_vip_avalon_mm.td_vvc_framework_common_methods_pkg.all;
-- UVVM VIP Avalon ST 
library bitvis_vip_avalon_st;
use bitvis_vip_avalon_st.vvc_methods_pkg.all;
use bitvis_vip_avalon_st.td_vvc_framework_common_methods_pkg.all;
-- UVVM VIP Spi Master
library bitvis_vip_spi;
use bitvis_vip_spi.vvc_methods_pkg.all;
use bitvis_vip_spi.td_vvc_framework_common_methods_pkg.all;

-- Test bench entity
entity tbSpiMaster is
    generic (runner_cfg : string); -- used by VUnit
end entity tbSpiMaster;

architecture Bhv of tbSpiMaster is
    constant CIdxAvalonMMmaster : natural := 0;
    constant cClkPeriod : time := 10 ns;
begin

    -----------------------------------------------------------------------------
    -- Instantiate DUT
    -----------------------------------------------------------------------------
    i_test_harness : entity work.th_spi_master_vvc;

    p_main : process
        constant cTestSingleByte_1 : t_slv_array(0 to 0)(8 - 1 downto 0) := (0 => x"A5");
        constant cTestSingleByte_2 : t_slv_array(0 to 0)(8 - 1 downto 0) := (0 => x"FF");
        constant cTestVector_1 : t_slv_array(0 to 3)(8 - 1 downto 0) := (0 => x"FF", 1 => x"FF", 2 => x"00", 3 => x"AA");
        constant cTestVector_2 : t_slv_array(0 to 3)(8 - 1 downto 0) := (0 => x"AB", 1 => x"11", 2 => x"02", 3 => x"01");
        constant cTestVector_3 : t_slv_array(0 to 1)(8 - 1 downto 0) := (0 => x"00", 1 => x"FF");

        variable v_cmd_idx : natural; -- Command index for the last read
    begin
        test_runner_setup(runner, runner_cfg);         -- VUnit setup

        -- Wait for UVVM to finish initialization
        await_uvvm_initialization(VOID);

        
        while test_suite loop                          -- loop used to run several VUnit testcases
            if run("testcase 1: Send single Byte") then
                start_clock(CLOCK_GENERATOR_VVCT, 1, "Start clock generator");

                -----------------------------------------------------------------------------
                -- test streaming interface Databuffer -> SPI-Master
                -----------------------------------------------------------------------------

                wait for 20 * cClkPeriod;
                --RstAsync <= not('0');
                wait for 20 * cClkPeriod;

                avalon_st_transmit(AVALON_ST_VVCT, 0, cTestSingleByte_1, "Send 1 byte to SPI Master");
                spi_slave_check_only(SPI_VVCT, 0, x"A5", "Expect 1 byte data from SPI-Master");
                await_completion(SPI_VVCT, 0, 1 ms, "wait until SPI sent data");

                wait for 20 * cClkPeriod;
                
            elsif run("testcase 2: Send multiple Bytes") then -- define one VUnit testcase
                start_clock(CLOCK_GENERATOR_VVCT, 1, "Start clock generator");

                wait for 20 * cClkPeriod;
                --RstAsync <= not('0');
                wait for 20 * cClkPeriod;

                -----------------------------------------------------------------------------
                -- test streaming interface Databuffer -> SPI-Master
                -----------------------------------------------------------------------------

                avalon_st_transmit(AVALON_ST_VVCT, 0, (x"AB", x"11", x"02", x"01"), "Send 4 bytes to SPI Master");
                --spi_slave_check_only(SPI_VVCT, 0, cTestVector_2, "Expect data from SPI-Master", ERROR, START_TRANSFER_IMMEDIATE, C_SCOPE);
                spi_slave_check_only(SPI_VVCT, 0, cTestVector_2(0), "", ERROR, START_TRANSFER_ON_NEXT_SS);
                spi_slave_check_only(SPI_VVCT, 0, cTestVector_2(1), "", ERROR, START_TRANSFER_IMMEDIATE, C_SCOPE);
                spi_slave_check_only(SPI_VVCT, 0, cTestVector_2(2), "", ERROR, START_TRANSFER_IMMEDIATE, C_SCOPE);
                spi_slave_check_only(SPI_VVCT, 0, cTestVector_2(3), "", ERROR, START_TRANSFER_IMMEDIATE, C_SCOPE);

                await_completion(SPI_VVCT, 0, 1 ms, "wait until SPI sent data");

                --wait for 20 ms;
            elsif run("testcase 3: Send different chuncks of data") then 
                start_clock(CLOCK_GENERATOR_VVCT, 1, "Start clock generator");

                wait for 20 * cClkPeriod;
                --RstAsync <= not('0');
                wait for 20 * cClkPeriod;

                avalon_st_transmit(AVALON_ST_VVCT, 0, (x"00", x"FF"), "Send 2 bytes to SPI Master");
                spi_slave_check_only(SPI_VVCT, 0, cTestVector_3(0), "", ERROR, START_TRANSFER_ON_NEXT_SS);--START_TRANSFER_IMMEDIATE, C_SCOPE);
                spi_slave_check_only(SPI_VVCT, 0, cTestVector_3(1), "", ERROR, START_TRANSFER_IMMEDIATE, C_SCOPE);

                await_completion(SPI_VVCT, 0, 1 ms, "wait until SPI sent data");

                wait for 10 * cClkPeriod;

                -- start new transaction
                avalon_st_transmit(AVALON_ST_VVCT, 0, (x"00", x"00", x"00"), "Send 3 bytes to SPI Master");

                spi_slave_check_only(SPI_VVCT, 0, x"00", "Expect first byte from SPI-Master", ERROR, START_TRANSFER_ON_NEXT_SS);
                spi_slave_check_only(SPI_VVCT, 0, x"00", "Expect second byte from SPI-Master", ERROR, START_TRANSFER_IMMEDIATE, C_SCOPE);
                spi_slave_check_only(SPI_VVCT, 0, x"00", "Expect third byte from SPI-Master", ERROR, START_TRANSFER_IMMEDIATE, C_SCOPE);

                await_completion(SPI_VVCT, 0, 1 ms, "wait until SPI sent data");

                wait for 10 * cClkPeriod;

                -- start new transaction
                avalon_st_transmit(AVALON_ST_VVCT, 0, (x"FF", x"FF", x"FF"), "Send 3 bytes to SPI Master");

                spi_slave_check_only(SPI_VVCT, 0, x"FF", "Expect first byte from SPI-Master", ERROR, START_TRANSFER_ON_NEXT_SS);
                spi_slave_check_only(SPI_VVCT, 0, x"FF", "Expect second byte from SPI-Master", ERROR, START_TRANSFER_IMMEDIATE, C_SCOPE);
                spi_slave_check_only(SPI_VVCT, 0, x"FF", "Expect third byte from SPI-Master", ERROR, START_TRANSFER_IMMEDIATE, C_SCOPE);

                await_completion(SPI_VVCT, 0, 1 ms, "wait until SPI sent data");

                wait for 10 * cClkPeriod;

                -- start new transaction
                avalon_st_transmit(AVALON_ST_VVCT, 0, (x"AA", x"55", x"AA"), "Send 3 bytes to SPI Master");

                spi_slave_check_only(SPI_VVCT, 0, x"AA", "Expect first byte from SPI-Master", ERROR, START_TRANSFER_ON_NEXT_SS);
                spi_slave_check_only(SPI_VVCT, 0, x"55", "Expect second byte from SPI-Master", ERROR, START_TRANSFER_IMMEDIATE, C_SCOPE);
                spi_slave_check_only(SPI_VVCT, 0, x"AA", "Expect third byte from SPI-Master", ERROR, START_TRANSFER_IMMEDIATE, C_SCOPE);

                await_completion(SPI_VVCT, 0, 1 ms, "wait until SPI sent data");

                wait for 10 * cClkPeriod;

                -- start new transaction
                avalon_st_transmit(AVALON_ST_VVCT, 0, (x"55", x"AA", x"55"), "Send 3 bytes to SPI Master");

                spi_slave_check_only(SPI_VVCT, 0, x"55", "Expect first byte from SPI-Master", ERROR, START_TRANSFER_ON_NEXT_SS);
                spi_slave_check_only(SPI_VVCT, 0, x"AA", "Expect second byte from SPI-Master", ERROR, START_TRANSFER_IMMEDIATE, C_SCOPE);
                spi_slave_check_only(SPI_VVCT, 0, x"55", "Expect third byte from SPI-Master", ERROR, START_TRANSFER_IMMEDIATE, C_SCOPE);

                await_completion(SPI_VVCT, 0, 1 ms, "wait until SPI sent data");

                wait for 10 * cClkPeriod;
            elsif run("testcase 4: Configure spi master using memory mapping") then 
                start_clock(CLOCK_GENERATOR_VVCT, 1, "Start clock generator");

                wait for 20 * cClkPeriod;
                --RstAsync <= not('0');
                wait for 20 * cClkPeriod;

                -------------------------------------------------
                -- Check default values
                -------------------------------------------------

                avalon_mm_check(AVALON_MM_VVCT, 0, x"00000000", x"00000000", "Check CPOL");
                await_completion(AVALON_MM_VVCT, 0, 1 ms, "Wait until CPOL checked");
                
                avalon_mm_check(AVALON_MM_VVCT, 0, x"00000001", x"00000000", "Check CPHA");
                await_completion(AVALON_MM_VVCT, 0, 1 ms, "Wait until CPHA checked");

                avalon_mm_check(AVALON_MM_VVCT, 0, x"00000002", x"00000002", "Check pre_delay");
                await_completion(AVALON_MM_VVCT, 0, 1 ms, "Wait until pre_delay checked");

                avalon_mm_check(AVALON_MM_VVCT, 0, x"00000003", x"00000002", "Check post_delay");
                await_completion(AVALON_MM_VVCT, 0, 1 ms, "Wait until post_delay checked");

                wait for 20 * cClkPeriod;

                avalon_mm_write(AVALON_MM_VVCT, 0, x"00000000", x"00000001", "Write to CPOL-register");
                await_completion(AVALON_MM_VVCT, 0, 1 ms, "");
                avalon_mm_check(AVALON_MM_VVCT, 0, x"00000000", x"00000001", "Check if value is correct registerd");
                await_completion(AVALON_MM_VVCT, 0, 1 ms, "");

                wait for 20 * cClkPeriod;

                avalon_mm_write(AVALON_MM_VVCT, 0, x"00000002", x"0000000F", "Write to pre_delay-register");
                await_completion(AVALON_MM_VVCT, 0, 1 ms, "");
                avalon_mm_check(AVALON_MM_VVCT, 0, x"00000002", x"0000000F", "Check if value is correct registerd");
                await_completion(AVALON_MM_VVCT, 0, 1 ms, "");

                wait for 20 * cClkPeriod;

            elsif run("testcase 5: Receive data on MISO line") then 
                start_clock(CLOCK_GENERATOR_VVCT, 1, "Start clock generator");

                wait for 20 * cClkPeriod;
                --RstAsync <= not('0');
                wait for 20 * cClkPeriod;

                avalon_st_transmit(AVALON_ST_VVCT, 0, cTestSingleByte_2, "Send single byte to SPI Master");
                spi_slave_transmit_and_receive(SPI_VVCT, 0, x"AA", "Transmit single byte and receive a byte");

                await_completion(SPI_VVCT, 0, 1 ms, "wait until data has been transfered");

                avalon_st_expect(AVALON_ST_VVCT, 1, cTestSingleByte_1, "Receive single byte from Spi-Master", ERROR, C_SCOPE);

                await_completion(AVALON_ST_VVCT, 0, 1 ms, "wait until SPI-Master sent data to ST-Sink");

            elsif run("testcase 6: Receive multiple bytes on MISO line") then 
                start_clock(CLOCK_GENERATOR_VVCT, 1, "Start clock generator");

                wait for 20 * cClkPeriod;
                --RstAsync <= not('0');
                wait for 20 * cClkPeriod;

                avalon_st_transmit(AVALON_ST_VVCT, 0, (x"AA", x"55", x"AA", x"55"), "Send multiple bytes to SPI Master");
                spi_slave_transmit_and_receive(SPI_VVCT, 0, cTestVector_1(0), "", START_TRANSFER_ON_NEXT_SS, C_SCOPE);
                spi_slave_transmit_and_receive(SPI_VVCT, 0, cTestVector_1(1), "", START_TRANSFER_IMMEDIATE, C_SCOPE);
                spi_slave_transmit_and_receive(SPI_VVCT, 0, cTestVector_1(2), "", START_TRANSFER_IMMEDIATE, C_SCOPE);
                spi_slave_transmit_and_receive(SPI_VVCT, 0, cTestVector_1(3), "", START_TRANSFER_IMMEDIATE, C_SCOPE);

                v_cmd_idx := get_last_received_cmd_idx(SPI_VVCT, 0);
                await_completion(SPI_VVCT, 0, v_cmd_idx, 1 ms, "wait until data has been transfered");

                avalon_st_expect(AVALON_ST_VVCT, 1, (x"AA", x"55", x"AA"), "Receive byte from Spi-Master", ERROR, C_SCOPE);

                await_completion(AVALON_ST_VVCT, 0, 1 ms, "wait until SPI-Master sent data to ST-Sink");
            end if;
        end loop;
        test_runner_cleanup(runner);
    end process;
end architecture;
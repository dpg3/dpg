onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tbspimaster/i_test_harness/RstAsync
add wave -noupdate /tbspimaster/i_test_harness/i_spi/iClk
add wave -noupdate /tbspimaster/i_test_harness/i_spi/oSCLK
add wave -noupdate /tbspimaster/i_test_harness/i_spi/iMISO
add wave -noupdate /tbspimaster/i_test_harness/i_spi/oMOSI
add wave -noupdate -expand /tbspimaster/i_test_harness/i_spi/R
add wave -noupdate -childformat {{/tbspimaster/i_test_harness/avalon_st_vvc_master/avalon_st_vvc_if.data -radix hexadecimal}} -expand -subitemconfig {/tbspimaster/i_test_harness/avalon_st_vvc_master/avalon_st_vvc_if.data {-radix hexadecimal}} /tbspimaster/i_test_harness/avalon_st_vvc_master/avalon_st_vvc_if
add wave -noupdate -expand /tbspimaster/i_test_harness/avalon_st_vvc_slave/avalon_st_vvc_if
add wave -noupdate -expand /tbspimaster/i_test_harness/spi_vcc_slave/spi_vvc_if
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {183454 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 250
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {44752 ps} {976592 ps}

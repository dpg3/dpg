----------------------------------------------------------------------------------------------------------------
-- Workfile:    th_SpiMaster-Struct-ea.vhd
-- Author:      Matthias Possenig
----------------------------------------------------------------------------------------------------------------
-- Description: This file contains the test-harness for the UVVM tests.
----------------------------------------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

-- SPI-Master library
library work;
use work.pkgSpiMaster.all;

-- Avalon ST librarys
library bitvis_vip_avalon_st;
use bitvis_vip_avalon_st.avalon_st_bfm_pkg.all;

-- Avalon MM librarys
library bitvis_vip_avalon_mm;
use bitvis_vip_avalon_mm.avalon_mm_bfm_pkg.all;

-- UVVM librarys
library uvvm_util;
context uvvm_util.uvvm_util_context;
library uvvm_vvc_framework;
use uvvm_vvc_framework.ti_vvc_framework_support_pkg.all;

-- Clock Generator librarys
library bitvis_vip_clock_generator;
context bitvis_vip_clock_generator.vvc_context;

-- SPI-Slave librarys
library bitvis_vip_spi;
use bitvis_vip_spi.td_vvc_framework_common_methods_pkg.all;
use bitvis_vip_spi.spi_bfm_pkg.all;

-- Test harness entity
entity th_spi_master_vvc is
end entity th_spi_master_vvc;

----------------------------------------------------------------------
-- Test harness architecture
----------------------------------------------------------------------
architecture Struct of th_spi_master_vvc is

  -- DSP interface and general control signals
  signal clk       : std_logic := '0';
  signal RstAsync : std_logic := '0';

  ----------------------------------------
  -- Avalon Streaming Slave Interface => SPI Master to VVC
  ----------------------------------------
  signal avalon_st_if_sink : t_avalon_st_if(
  channel(0 downto 0),
  data(cAvalonSTDataWidth-1 downto 0),
  data_error(0 downto 0),
  empty(0 downto 0)
  ) := init_avalon_st_if_signals(false, 1, cAvalonSTDataWidth, 1, 1);

  ----------------------------------------
  -- Avalon Streaming Master Interface => VVC to SPI Master
  ----------------------------------------
  signal avalon_st_if_source : t_avalon_st_if(
  channel(0 downto 0),
  data(cAvalonSTDataWidth-1 downto 0),
  data_error(0 downto 0),
  empty(0 downto 0)
  ) := init_avalon_st_if_signals(true, 1, cAvalonSTDataWidth, 1, 1); 

  ----------------------------------------
  -- Avalon MM Slave - Interface
  ----------------------------------------
  signal avalon_mm_if : t_avalon_mm_if(
  address(cAddressWidth - 1 downto 0),
  byte_enable((cAvalonMMDataWidth / 8) - 1 downto 0),
  writedata(cAvalonMMDataWidth - 1 downto 0),
  readdata(cAvalonMMDataWidth - 1 downto 0)
  ) := init_avalon_mm_if_signals(cAddressWidth, cAvalonMMDataWidth);

  ----------------------------------------
  -- SPI Master Slave - Interface
  ----------------------------------------
  signal spi_if : t_spi_if := init_spi_if_signals(C_SPI_BFM_CONFIG, false);

begin
  -----------------------------------------------------------------------------
  -- Instantiate the concurrent procedure that initializes UVVM
  -----------------------------------------------------------------------------
  i_ti_uvvm_engine : entity uvvm_vvc_framework.ti_uvvm_engine;

  -----------------------------------------------------------------------------
  -- Instantiate DUT
  -----------------------------------------------------------------------------
  i_spi : entity work.spi_master
    generic map(
      gDataWidthMM => 32,
      gDataWidthST => 8
    )
    port map(
      -- clock and reset
      inResetAsync => not RstAsync,
      iClk         => clk,

      -- Avalon ST Sink
      oReady => avalon_st_if_source.ready,
      iValid => avalon_st_if_source.valid,
      iData  => avalon_st_if_source.data,
      iEndOfPackageSink   => avalon_st_if_source.end_of_packet,
      iStartOfPackageSink => avalon_st_if_source.start_of_packet,

      -- Avalon ST Source
      oValid => avalon_st_if_sink.valid,
      oData  => avalon_st_if_sink.data,
      --oEndOfPackageSource   => avalon_st_if_sink.end_of_packet,
      --oStartOfPackageSource => avalon_st_if_sink.start_of_packet,

      -- Avalon MM Slave
      iAddress        => avalon_mm_if.address,
      iRead           => avalon_mm_if.read,
      oReadData       => avalon_mm_if.readdata,
      iWrite          => avalon_mm_if.write,
      iWriteData      => avalon_mm_if.writedata,

      -- SPI-Interface
      oSCLK => spi_if.sclk,
      oCS   => spi_if.ss_n,
      oMOSI => spi_if.mosi,
      iMISO => spi_if.miso
    );

  -----------------------------------------------------------------------------
  -- Avalon ST master VVC
  -----------------------------------------------------------------------------
  avalon_st_vvc_master : entity bitvis_vip_avalon_st.avalon_st_vvc
    generic map(
      GC_VVC_IS_MASTER        => true,
      GC_DATA_WIDTH           => avalon_st_if_source.data'length,
      GC_DATA_ERROR_WIDTH     => avalon_st_if_source.data_error'length,
      GC_EMPTY_WIDTH          => avalon_st_if_source.empty'length,
      GC_INSTANCE_IDX         => 0,
      GC_AVALON_ST_BFM_CONFIG => C_AVALON_ST_MASTER_BFM_CONFIG)
    port map(
      clk              => Clk,
      avalon_st_vvc_if => avalon_st_if_source);

  -----------------------------------------------------------------------------
  -- Avalon ST slave VVC
  -----------------------------------------------------------------------------
  avalon_st_vvc_slave : entity bitvis_vip_avalon_st.avalon_st_vvc
    generic map(
      GC_VVC_IS_MASTER        => false,
      GC_DATA_WIDTH           => avalon_st_if_sink.data'length,
      GC_DATA_ERROR_WIDTH     => avalon_st_if_sink.data_error'length,
      GC_EMPTY_WIDTH          => avalon_st_if_sink.empty'length,
      GC_INSTANCE_IDX         => 1,
      GC_AVALON_ST_BFM_CONFIG => C_AVALON_ST_SLAVE_BFM_CONFIG)

    port map(
      clk              => Clk,
      avalon_st_vvc_if => avalon_st_if_sink
    );

  -----------------------------------------------------------------------------
  -- SPI Slave 
  -----------------------------------------------------------------------------
  spi_vcc_slave : entity bitvis_vip_spi.spi_vvc
    generic map(
      GC_DATA_WIDTH       => cAvalonSTDataWidth,
      GC_DATA_ARRAY_WIDTH => cBytesPerPacket,
      GC_INSTANCE_IDX     => 0,
      GC_MASTER_MODE      => false,
      GC_SPI_CONFIG       => C_SPI_BFM_CONFIG
    )
    port map(
      spi_vvc_if => spi_if
    );

  -----------------------------------------------------------------------------
  -- Avalon MM Master
  -----------------------------------------------------------------------------
  i_avalon_mm_master : entity bitvis_vip_avalon_mm.avalon_mm_vvc
    generic map(
      GC_ADDR_WIDTH       => cAddressWidth,
      GC_DATA_WIDTH       => cAvalonMMDataWidth,
      GC_INSTANCE_IDX     => 0,
      GC_AVALON_MM_CONFIG => C_AVALON_MM_BFM_CONFIG
    )
    port map(
      clk                     => clk,
      avalon_mm_vvc_master_if => avalon_mm_if
    );

  -- Toggle the reset after 5 clock periods
  p_RstAsync : RstAsync <= '1', '0' after 5 * cClkPeriod;

  -----------------------------------------------------------------------------
  -- Clock Generator VVC
  -----------------------------------------------------------------------------
  i_clock_generator_vvc : entity bitvis_vip_clock_generator.clock_generator_vvc
    generic map(
      GC_INSTANCE_IDX    => cClkGen,
      GC_CLOCK_NAME      => "SPI-Master-Clock",
      GC_CLOCK_PERIOD    => cClkPeriod,
      GC_CLOCK_HIGH_TIME => cClkPeriod / 2
    )
    port map(
      clk => clk
    );
end Struct;
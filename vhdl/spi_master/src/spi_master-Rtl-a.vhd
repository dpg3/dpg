----------------------------------------------------------------------------------------------------------------
-- Workfile:    spi_master-Rtl-a.vhd
-- Author:      Matthias Possenig
----------------------------------------------------------------------------------------------------------------
-- Description: This file contains the architecture of the Spi-Master. The Spi-Master has multiple interfaces to 
--              transfer data: 
--               - Spi Interface -> To transfer Data using the Spi-Protocol
--                 - MISO
--                 - MOSI
--                 - SCLK
--                 - CS
--               - Avalon MM -> Configuration of Spi-Master
--                 | Address  |    Description   |
--                 | -------- | ---------------- |
--                 |   0x00   |      CPOL        |
--                 |   0x01   |      CPHA        |
--                 |   0x02   |   pre_delay      |
--                 |   0x03   |   post_delay     |
--                 |   0x04   | clk_per_half_bit |
--                 | -------- | ---------------- |
--               - Avalon ST Sink -> Receive Bytewise Data from a data buffer (for example simple_data_buffer)
--               - Avalon ST Source -> Transfer received data on MISO line back to a data buffer  
--
--              Configuration SPI-Mode: 
--                - Mode 0 (CPOL = 0, CPHA = 0) 
--                - Mode 1 (CPOL = 0, CPHA = 1)
--                - Mode 2 (CPOL = 1, CPHA = 0)
--                - Mode 3 (CPOL = 1, CPHA = 1)
----------------------------------------------------------------------------------------------------------------

architecture Rtl of spi_master is

    -- States for each phase of a transmission
    type spi_state_t is (IDLE, PRE_DELAY, TRANSFER, POST_DELAY);
    -- State of sclk -> when NX_BIT => prepare next bit, when SAMPLE => MISO/MOSI line is read
    type spi_clk_state_t is (NX_BIT, SAMPLE);

    -- contains all registers
    type reg_set_t is record
        -- config CPOL and CPHA
        CPOL : std_ulogic;
        CPHA : std_ulogic;

        -- Chip select 
        CS_n : std_ulogic;

        -- delays from CS to SCLK
        pre_delay       : unsigned(gPrePostDelayCounterWidth - 1 downto 0);
        reset_pre_delay : unsigned(gPrePostDelayCounterWidth - 1 downto 0);
        -- delays from SCLK to CS
        post_delay       : unsigned(gPrePostDelayCounterWidth - 1 downto 0);
        reset_post_delay : unsigned(gPrePostDelayCounterWidth - 1 downto 0);

        -- SPI Master operation state
        spi_state : spi_state_t;
        -- SPI Clock state
        spi_clk_state : spi_clk_state_t;

        -- SPI Internal clock state and helper signals
        SCLK                    : std_ulogic;
        clk_count_per_half_sclk : unsigned(gClkPerHalfSclkWidth - 1 downto 0);

        -- Avalon Sink ready (internal signal)
        Ready : std_ulogic;

        -- Avalon Source valid (internal signal)
        rx_valid : std_ulogic;

        -- Current bit index
        bit_idx : integer range 0 to gDataWidthST - 1;

        -- Tx Data (buffer for current byte to send)
        tx_data : std_ulogic_vector(gDataWidthST - 1 downto 0);
        -- Rx Data (buffer for received byte)
        rx_data : std_ulogic_vector(gDataWidthST - 1 downto 0);

        -- flag if current byte is last byte
        last_byte       : std_ulogic;
        end_of_transfer : std_ulogic;

        -- Clock count per half bit (Minimum = 2)
        clk_per_half_bit : unsigned(gClkPerHalfSclkWidth - 1 downto 0); --integer range 1 to gMaxClockPerHalfSclk;

        -- Spi data out (internal signal)
        MOSI : std_ulogic;

        -- avalon mm read data register
        readdata_mm : std_ulogic_vector(gDataWidthMM - 1 downto 0);
    end record;

    -- Reset value for registers
    constant c_reg_init : reg_set_t := (
        CPOL             => '0',
        CPHA             => '0',
        CS_n             => not('0'),
        pre_delay        => to_unsigned(2, gPrePostDelayCounterWidth),
        post_delay       => to_unsigned(2, gPrePostDelayCounterWidth),
        reset_pre_delay  => to_unsigned(2, gPrePostDelayCounterWidth),
        reset_post_delay => to_unsigned(2, gPrePostDelayCounterWidth),
        spi_state        => IDLE,
        spi_clk_state    => SAMPLE,
        SCLK             => '0',
        clk_count_per_half_sclk => (others => '0'),
        Ready            => '1',
        rx_valid         => '0',
        bit_idx          => gDataWidthST - 1,
        tx_data => (others => '0'),
        rx_data => (others => '0'),
        last_byte        => '0',
        end_of_transfer  => '0',
        clk_per_half_bit => to_unsigned(3, gClkPerHalfSclkWidth),
        MOSI             => '0',
        readdata_mm => (others => '0')
    );

    -- Register signals for current and next value
    signal R, NxR : reg_set_t;
begin

    ---------------------------------------------------------------------------------------------
    -- Register process
    ---------------------------------------------------------------------------------------------
    Registers : process (iClk, inResetAsync) is
    begin
        if (inResetAsync = not('1')) then
            -- set defaults
            R <= c_reg_init;
        elsif rising_edge(iClk) then
            -- take new values
            R <= NxR;
        end if;
    end process;

    ---------------------------------------------------------------------------------------------
    -- Combinatorial process
    ---------------------------------------------------------------------------------------------
    Comb : process (R, iValid, iData, iMISO, iStartOfPackageSink, iEndOfPackageSink, iAddress, iRead, iWrite, iWriteData) is
    begin
        -- set initial values
        NxR <= R;

        ---------------------------------------------------------------------------------------------
        -- Configuration - Avalon MM
        ---------------------------------------------------------------------------------------------
        if iWrite = '1' then
            if iAddress = (iAddress'range => '0') then
                NxR.CPOL <= iWriteData(0); -- only LSB relevant
            elsif unsigned(iAddress) = to_unsigned(1, iAddress'length) then
                NxR.CPHA <= iWriteData(0); -- only LSB relevant
            elsif unsigned(iAddress) = to_unsigned(2, iAddress'length) then
                NxR.reset_pre_delay <= resize(unsigned(iWriteData), gPrePostDelayCounterWidth);
            elsif unsigned(iAddress) = to_unsigned(3, iAddress'length) then
                NxR.reset_post_delay <= resize(unsigned(iWriteData), gPrePostDelayCounterWidth);
            elsif unsigned(iAddress) = to_unsigned(4, iAddress'length) then
                NxR.clk_per_half_bit <= resize(unsigned(iWriteData), gClkPerHalfSclkWidth);
            end if;
        elsif iRead = '1' then
            if iAddress = (iAddress'range => '0') then
                NxR.readdata_mm <= (0 => R.CPOL, others => '0'); -- only LSB relevant
            elsif unsigned(iAddress) = to_unsigned(1, iAddress'length) then
                NxR.readdata_mm <= (0 => R.CPHA, others => '0'); -- only LSB relevant
            elsif unsigned(iAddress) = to_unsigned(2, iAddress'length) then
                NxR.readdata_mm <= std_ulogic_vector(resize(R.reset_pre_delay, gDataWidthMM));
            elsif unsigned(iAddress) = to_unsigned(3, iAddress'length) then
                NxR.readdata_mm <= std_ulogic_vector(resize(R.reset_post_delay, gDataWidthMM));
            elsif unsigned(iAddress) = to_unsigned(4, iAddress'length) then
                NxR.readdata_mm <= std_ulogic_vector(resize(R.clk_per_half_bit, gDataWidthMM));
            end if;
        end if;

        ---------------------------------------------------------------------------------------------
        -- FSM - Send/Receive data, Spi Clock generation and Data buffering
        ---------------------------------------------------------------------------------------------
        case R.spi_state is
            when IDLE =>
                -- reset values
                NxR.CS_n                    <= not('0');           -- chip select disabled
                NxR.bit_idx                 <= gDataWidthST - 1;   -- reset bit index
                NxR.SCLK                    <= R.CPOL;             -- reset SCLK to idle state
                NxR.Ready                   <= '1';                -- we are now ready to receive new data
                NxR.clk_count_per_half_sclk <= R.clk_per_half_bit - 1; -- reset clk per half sclk counter
                NxR.end_of_transfer         <= '0';                -- reset end transfer flag

                -- reset delay counter
                NxR.pre_delay  <= R.reset_pre_delay;
                NxR.post_delay <= R.reset_post_delay;
                NxR.last_byte  <= '0';

                -- change to pre-delay state if new bytes are arriving 
                if iStartOfPackageSink = '1' and iValid = '1' then
                    NxR.spi_state <= PRE_DELAY;
                    NxR.tx_data   <= iData;
                    NxR.Ready     <= '0';

                    if iEndOfPackageSink = '1' then 
                        NxR.last_byte <= '1';
                    end if;
                end if;
            when PRE_DELAY =>
                NxR.CS_n <= not('1'); -- chip select enable
                NxR.SCLK <= R.CPOL;   -- reset SCLK

                -- define initial Clock state
                if (R.CPHA = '0') then
                    NxR.spi_clk_state <= SAMPLE;
                else
                    NxR.spi_clk_state <= NX_BIT;
                end if;

                -- handle pre-delay and go to transfer state if delay over
                if R.pre_delay > 0 then
                    NxR.pre_delay <= R.pre_delay - 1;
                else
                    NxR.spi_state <= TRANSFER;
                end if;

            when TRANSFER =>

                ---------------------------------------------------------------------------------------------
                -- FSM for SPI-Clock Generation
                ---------------------------------------------------------------------------------------------
                case R.spi_clk_state is
                    when SAMPLE =>
                        -- set sclk to user defined state
                        NxR.SCLK <= (R.CPOL xor R.CPHA);

                        if R.clk_count_per_half_sclk = 0 then
                            -- got to next sclk state and reset clk counter
                            NxR.spi_clk_state           <= NX_BIT;
                            NxR.clk_count_per_half_sclk <= R.clk_per_half_bit - 1;

                            -- depending on Clock phase we reset/decrement bit index here
                            if (R.CPHA = '1') then
                                if R.bit_idx = 0 then
                                    NxR.bit_idx <= gDataWidthST - 1; -- reset bit index
                                else
                                    NxR.bit_idx <= R.bit_idx - 1; -- next bit
                                end if;
                            end if;
                        else
                            -- count down once
                            NxR.clk_count_per_half_sclk <= R.clk_count_per_half_sclk - 1;
                        end if;
                    when NX_BIT =>
                        -- set sclk to user defined state
                        NxR.SCLK <= not(R.CPOL xor R.CPHA);

                        if R.clk_count_per_half_sclk = 0 then
                            -- got to next sclk state and reset clk counter
                            NxR.spi_clk_state           <= SAMPLE;
                            NxR.clk_count_per_half_sclk <= R.clk_per_half_bit - 1;

                            -- depending on Clock phase we reset/decrement bit index here
                            if (R.CPHA = '0') then
                                if R.bit_idx = 0 then
                                    NxR.bit_idx <= gDataWidthST - 1; -- reset bit index
                                else
                                    NxR.bit_idx <= R.bit_idx - 1; -- next bit
                                end if;
                            end if;
                        else
                            -- count down once
                            NxR.clk_count_per_half_sclk <= R.clk_count_per_half_sclk - 1;
                        end if;
                end case;

                ---------------------------------------------------------------------------------------------
                -- MOSI - send data
                ---------------------------------------------------------------------------------------------

                -- set MOSI line to next bit
                NxR.MOSI <= R.tx_data(R.tx_data'left);

                -- shift tx data to left so we can get the next bit in next cycle
                if (R.spi_clk_state = NX_BIT and R.clk_count_per_half_sclk = 0 and (R.CPHA = '0' or R.bit_idx /= gDataWidthST - 1)) then
                    NxR.tx_data <= R.tx_data(R.tx_data'left - 1 downto 0) & R.tx_data(0); -- upper bit is thrown away and 0 is attached at LSB
                end if;

                -- Ready to read new byte
                if R.bit_idx = 0 and R.last_byte = '0' and R.spi_clk_state = NX_BIT and R.clk_count_per_half_sclk = 0 then
                    NxR.Ready <= '1';
                end if;

                ---------------------------------------------------------------------------------------------
                -- MISO - receive data
                ---------------------------------------------------------------------------------------------   

                -- sample the MISO line
                NxR.rx_valid <= '0';
                if (R.spi_clk_state = SAMPLE and R.clk_count_per_half_sclk = 0) then
                    -- attach new bit to rx_data upper bit gets thrown away
                    NxR.rx_data  <= R.rx_data(R.rx_data'left - 1 downto 0) & iMISO;
                    -- rx_data is valid when last bit is received
                    if R.bit_idx = 0 then
                        NxR.rx_valid <= '1';
                    end if;
                end if;

                ---------------------------------------------------------------------------------------------
                -- Next State for FSM
                --------------------------------------------------------------------------------------------- 
                -- go to post delay state if all bytes have been sent
                if (R.bit_idx = 0 and R.last_byte = '1') then
                    NxR.end_of_transfer <= '1';
                elsif R.end_of_transfer = '1' then
                    NxR.SCLK      <= R.CPOL;
                    NxR.spi_state <= POST_DELAY;
                    NxR.MOSI      <= '0';
                end if;

                ---------------------------------------------------------------------------------------------
                -- Register next data
                ---------------------------------------------------------------------------------------------

                -- if new data is available and we are ready to receive it
                if iValid = '1' and R.Ready = '1' then
                    NxR.tx_data   <= iData;    -- buffer next byte
                    NxR.spi_state <= TRANSFER; -- stay in transfer state
                    NxR.Ready     <= '0';      -- signal that we are busy

                    -- set last byte flag if necessary
                    if iEndOfPackageSink = '1' then
                        NxR.last_byte <= '1';
                    end if;
                end if;

            when POST_DELAY =>

                -- count down post_delay counter
                if R.post_delay > 0 then
                    NxR.post_delay <= R.post_delay - 1;
                else
                    -- deselect slave and go back to IDLE state
                    NxR.spi_state <= IDLE;
                    NxR.CS_n      <= not('0');
                end if;

        end case;
    end process;

    ---------------------------------------------------------------------------------------------
    -- connect internal signals with external signals
    ---------------------------------------------------------------------------------------------

    oReady    <= R.Ready;
    oSCLK     <= R.SCLK;
    oMOSI     <= R.MOSI;
    oCS       <= R.CS_n;
    oValid    <= R.rx_valid;
    oData     <= R.rx_data;
    oReadData <= R.readdata_mm;

end architecture;

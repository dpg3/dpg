----------------------------------------------------------------------------------------------------------------
-- Workfile:    pkgSpiMaster.vhd
-- Author:      Matthias Possenig
----------------------------------------------------------------------------------------------------------------
-- Description: This package contains all constants, functions and definitions for the SPI-Master. 
--              It also contains constants for the Simulation via UVVM.
----------------------------------------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

-- Avalon ST librarys
library bitvis_vip_avalon_st;
use bitvis_vip_avalon_st.avalon_st_bfm_pkg.all;

-- Avalon MM librarys
library bitvis_vip_avalon_mm;
use bitvis_vip_avalon_mm.avalon_mm_bfm_pkg.all;

-- UVVM librarys
library uvvm_util;
context uvvm_util.uvvm_util_context;
library uvvm_vvc_framework;
use uvvm_vvc_framework.ti_vvc_framework_support_pkg.all;

-- Clock Generator librarys
library bitvis_vip_clock_generator;
context bitvis_vip_clock_generator.vvc_context;

-- SPI-Slave librarys
library bitvis_vip_spi;
use bitvis_vip_spi.td_vvc_framework_common_methods_pkg.all;
use bitvis_vip_spi.spi_bfm_pkg.all;

package pkgSpiMaster is

    -- Constants for clock
    constant cClkPeriod : time    := 10 ns; -- 100 MHz
    constant cClkGen    : natural := 1;

    constant cSpiClkPeriod   : time    := 20 ns;  --1 sec / 40e6;
    constant cBytesPerPacket : natural := 8; 

    constant cAddressWidth      : natural := 32;
    constant cAvalonMMDataWidth : natural := 32;
    constant cAvalonSTDataWidth : natural := 8;

    ----------------------------------------
    -- Avalon Streaming Slave - BFM Config 
    ----------------------------------------
    constant C_AVALON_ST_SLAVE_BFM_CONFIG : t_avalon_st_bfm_config := (
        max_wait_cycles          => 1000,
        max_wait_cycles_severity => ERROR,
        clock_period             => cClkPeriod,
        clock_period_margin      => 0 ns,
        clock_margin_severity    => TB_ERROR,
        setup_time               => cClkPeriod/4,
        hold_time                => cClkPeriod/4,
        bfm_sync                 => SYNC_ON_CLOCK_ONLY,
        match_strictness         => MATCH_EXACT,
        symbol_width             => 8,
        first_symbol_in_msb      => true,
        max_channel              => 0,
        use_packet_transfer      => true,
        id_for_bfm               => ID_BFM
    );

    ----------------------------------------
    -- Avalon Streaming Master - BFM Config 
    ----------------------------------------
    constant C_AVALON_ST_MASTER_BFM_CONFIG : t_avalon_st_bfm_config := (
        max_wait_cycles          => 1000,
        max_wait_cycles_severity => ERROR,
        clock_period             => cClkPeriod,
        clock_period_margin      => 0 ns,
        clock_margin_severity    => TB_ERROR,
        setup_time               => cClkPeriod/4,
        hold_time                => cClkPeriod/4,
        bfm_sync                 => SYNC_ON_CLOCK_ONLY,
        match_strictness         => MATCH_EXACT,
        symbol_width             => 8,
        first_symbol_in_msb      => true,
        max_channel              => 0,
        use_packet_transfer      => true,
        id_for_bfm               => ID_BFM
    );

    ----------------------------------------
    -- Avalon MM Master - BFM Config
    ----------------------------------------

    constant C_AVALON_MM_BFM_CONFIG : t_avalon_mm_bfm_config := (
        max_wait_cycles          => 10,
        max_wait_cycles_severity => TB_FAILURE,
        clock_period             => cClkPeriod,
        clock_period_margin      => 0 ns,
        clock_margin_severity    => TB_ERROR,
        setup_time               => 0 ns,
        hold_time                => 0 ns,
        bfm_sync                 => SYNC_ON_CLOCK_ONLY,
        match_strictness         => MATCH_EXACT,
        num_wait_states_read     => 1,
        num_wait_states_write    => 0,
        use_waitrequest          => false,
        use_readdatavalid        => false,
        use_response_signal      => false,
        use_begintransfer        => false,
        id_for_bfm               => ID_BFM,
        id_for_bfm_wait          => ID_BFM_WAIT,
        id_for_bfm_poll          => ID_BFM_POLL
    );

    ----------------------------------------
    -- SPI Slave - BFM Config
    ----------------------------------------
    constant C_SPI_BFM_CONFIG : t_spi_bfm_config := (
        CPOL             => '0',
        CPHA             => '0',
        spi_bit_time     => 2 * cClkPeriod,
        ss_n_to_sclk     => 0 ns, --4 * cClkPeriod,  This has to be 0 ns because UVVM doesn't use this delay as it should 
        sclk_to_ss_n     => 4 * cClkPeriod,    
        inter_word_delay => 0 ns,
        match_strictness => MATCH_EXACT,
        id_for_bfm       => ID_BFM,
        id_for_bfm_wait  => ID_BFM_WAIT,
        id_for_bfm_poll  => ID_BFM_POLL
    );

end package pkgSpiMaster;

-- package body
package body pkgSpiMaster is

end package body pkgSpiMaster;

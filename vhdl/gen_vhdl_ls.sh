#!/bin/env sh

cat vhdl_ls.toml.base
fd -u '\.vhd' | grep -v "structure-ea.vhd" | grep -iv "UVVM" | grep -v "vunit_out" | grep -v "Example" | grep -v "quartus" | grep -v "synthesis" | awk '{print "\t\""$1"\","}'
echo ']'


# UVVM
UVVM_BASEDIR=uvvm

for i in $(ls "$UVVM_BASEDIR")
do
    if [ -f "$UVVM_BASEDIR/$i/script/compile_order.txt" ]
    then
        echo "$i.files = ["
        cat $UVVM_BASEDIR/$i/script/compile_order.txt | awk -v dir="$UVVM_BASEDIR/$i/script/" 'NF && FNR != 1 {print "\t\""dir$1"\","}'
        echo "]"
    fi
done

# VUNIT

# Currently use a local copy, maybe add a vunit submodule later...
VUNIT_BASEDIR=~/Software/VHDL/vunit

echo "vunit_lib.files = ["
echo -e "\t\"$VUNIT_BASEDIR/vunit/vhdl/*.vhd\","
echo -e "\t\"$VUNIT_BASEDIR/vunit/vhdl/**/src/*.vhd\","
echo -e "\t\"$VUNIT_BASEDIR/vunit/vhdl/**/test/*.vhd\""
echo "]"

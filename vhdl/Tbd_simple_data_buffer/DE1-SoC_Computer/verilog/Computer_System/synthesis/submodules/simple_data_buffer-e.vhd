-------------------------------------------------------------------------------
-- Title      : Simple Data Buffer
-- Project    : DPG
-------------------------------------------------------------------------------
-- File       : simple_data_buffer-e.vhd
-- Author     : Matthias Possenig
-------------------------------------------------------------------------------
-- Description: Gets a byte via avalon-mm and sends it to the spi-master. 
--              Receiving bytes are currently ignored.
-------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity simple_data_buffer is
    generic (
        gDataWidthST  : natural := 8;
        gDataWidthMM  : natural := 32;
        gAddressWidth : natural := 32
    );

    port(
        -- clock and reset
        inResetAsync : in std_ulogic;
        iClk         : in std_ulogic;

        -- Avalon ST Source
        iReady : in std_ulogic;
        oValid : out std_ulogic;
        oData  : out std_ulogic_vector(gDataWidthST - 1 downto 0);
        oEndOfPackageSource   : out std_ulogic;
        oStartOfPackageSource : out std_ulogic;

        -- Avalon ST Sink
        iValid : in std_ulogic;
        iData  : in std_ulogic_vector(gDataWidthST - 1 downto 0);

        -- Avalon MM
        iAddress        : in std_ulogic_vector(gAddressWidth - 1 downto 0);
        iRead           : in std_ulogic;
        oReadData       : out std_ulogic_vector(gDataWidthMM - 1 downto 0);
        iWrite          : in std_ulogic;
        iWriteData      : in std_ulogic_vector(gDataWidthMM - 1 downto 0)
    );
end simple_data_buffer;
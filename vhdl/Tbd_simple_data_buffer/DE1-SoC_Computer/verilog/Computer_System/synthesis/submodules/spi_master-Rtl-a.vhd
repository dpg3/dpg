----------------------------------------------------------------------------------------------------------------
-- Workfile:    spi_master-Rtl-a.vhd
-- Author:      Matthias Possenig
----------------------------------------------------------------------------------------------------------------
-- Description: This file contains the Rtl logic for the SPI-Master. The SPI-Master is configured using a Avalon 
--              Memory Mapped interface. 
--              Configuration SPI-Mode: 
--                - Mode 0 (CPOL = 0, CPHA = 0)
--                - Mode 1 (CPOL = 0, CPHA = 1)
--                - Mode 2 (CPOL = 1, CPHA = 0)
--                - Mode 3 (CPOL = 1, CPHA = 1)
-- 
--              The data to send is received from the Buffer using an Avalon Streaming Interface. One data 
--              transfer contains 1 Byte of data to send. 
--              The data received on the MISO port is sent to the buffer using an Avalon Streaming Interface. 
--              The data transfer also contains 1 Byte per transfer.
----------------------------------------------------------------------------------------------------------------

architecture Rtl of spi_master is

    type t_SpiState is (IDLE, PRE_DELAY, TRANSFER, POST_DELAY);
    type t_SpiClkState is (SHIFT, SAMPLE);

    type t_Reg is record
        -- config CPOL and CPHA
        CPOL : std_ulogic;
        CPHA : std_ulogic;

        -- Chip select 
        nCS : std_ulogic;

        -- delay's
        pre_delay  : unsigned(gPrePostDelayCounterWidth - 1 downto 0);
        post_delay : unsigned(gPrePostDelayCounterWidth - 1 downto 0);
        reset_pre_delay  : unsigned(gPrePostDelayCounterWidth - 1 downto 0);
        reset_post_delay : unsigned(gPrePostDelayCounterWidth - 1 downto 0);

        -- SPI Master operation state
        SpiState : t_SpiState;

        -- SPI Clock state
        SpiClkState : t_SpiClkState;

        -- SPI Internal clock state and helper signals
        SpiClk              : std_ulogic;
        clkCountPerHalfSclk : unsigned(gClkPerHalfSclkWidth-1 downto 0); --integer range 0 to gMaxClockPerHalfSclk;

        -- Avalon Sink ready (internal signal)
        Ready : std_ulogic;

        -- Avalon Source valid (internal signal)
        RxValid : std_ulogic;

        -- Current bit index
        BitIdx : integer range 0 to gDataWidthST - 1;
        --RxBitIdx : integer range 0 to gDataWidthST - 1;
        --TxBitIdx : integer range 0 to gDataWidthST - 1;

        -- Tx Data (buffer for current byte to send)
        txData : std_ulogic_vector(gDataWidthST - 1 downto 0);

        -- Rx Data 
        rxData : std_ulogic_vector(gDataWidthST - 1 downto 0); 

        -- flag if current byte is last byte
        lastByte : std_ulogic;
        endTransfer : std_ulogic;

        -- Clock count per half bit (Minimum = 2)
        clkPerHalfBit : unsigned(gClkPerHalfSclkWidth-1 downto 0); --integer range 1 to gMaxClockPerHalfSclk;

        -- Spi data out (internal signal)
        MOSI : std_ulogic;

        -- avalon mm read data register
        ReadDataMM : std_ulogic_vector(gDataWidthMM - 1 downto 0);
    end record;

    -- Reset value for registers
    constant cRegInit : t_Reg := (
        CPOL             => '0',
        CPHA             => '0',
        nCS              => not('0'),
        pre_delay        => to_unsigned(2, gPrePostDelayCounterWidth),
        post_delay       => to_unsigned(2, gPrePostDelayCounterWidth),
        reset_pre_delay  => to_unsigned(2, gPrePostDelayCounterWidth),
        reset_post_delay => to_unsigned(2, gPrePostDelayCounterWidth),
        SpiState         => IDLE,
        SpiClkState      => SAMPLE,
        SpiClk           => '0',
        clkCountPerHalfSclk => (others => '0'),   
        Ready            => '0',
        RxValid          => '0',
        BitIdx           => gDataWidthST - 1,
        txData           => (others => '0'),
        rxData           => (others => '0'),
        lastByte         => '0',
        endTransfer      => '0',
        clkPerHalfBit    => to_unsigned(3, gClkPerHalfSclkWidth),
        MOSI             => '0',
        ReadDataMM       => (others => '0')
    );

    -- Register signals for current and next value
    signal R, NxR : t_Reg;
begin

    -- Register process
    Registers : process (iClk, inResetAsync) is
    begin
        if (inResetAsync = not('1')) then
            -- set defaults
            R <= cRegInit;
        elsif rising_edge(iClk) then
            R <= NxR;
        end if;
    end process;

    -- Combinatorial process
    Comb : process (R, iValid, iData, iMISO, iStartOfPackageSink, iEndOfPackageSink, iAddress, iRead, iWrite, iWriteData) is
    begin

        -- set initial values
        NxR <= R;

---------------------------------------------------------------------------------------------
-- Configuration - Avalon MM
---------------------------------------------------------------------------------------------
        if iWrite = '1' then
            if iAddress = (iAddress'range => '0') then
                NxR.CPOL <= iWriteData(0);
            elsif unsigned(iAddress) = to_unsigned(1, iAddress'length) then
                NxR.CPHA <= iWriteData(0);
            elsif unsigned(iAddress) = to_unsigned(2, iAddress'length) then
                NxR.reset_pre_delay <= resize(unsigned(iWriteData), gPrePostDelayCounterWidth);
            elsif unsigned(iAddress) = to_unsigned(3, iAddress'length) then 
                NxR.reset_post_delay <= resize(unsigned(iWriteData), gPrePostDelayCounterWidth);
            elsif unsigned(iAddress) = to_unsigned(4, iAddress'length) then
                NxR.clkPerHalfBit <= resize(unsigned(iWriteData), gClkPerHalfSclkWidth);
            end if;
        elsif iRead = '1' then
            if iAddress = (iAddress'range => '0') then
                NxR.ReadDataMM <= (0 => R.CPOL, others => '0');
            elsif unsigned(iAddress) = to_unsigned(1, iAddress'length) then
                NxR.ReadDataMM <= (0 => R.CPHA, others => '0');
            elsif unsigned(iAddress) = to_unsigned(2, iAddress'length) then
                NxR.ReadDataMM <= std_ulogic_vector(resize(R.reset_pre_delay, gDataWidthMM)); 
            elsif unsigned(iAddress) = to_unsigned(3, iAddress'length) then
                NxR.ReadDataMM <= std_ulogic_vector(resize(R.reset_post_delay, gDataWidthMM));
            elsif unsigned(iAddress) = to_unsigned(4, iAddress'length) then 
                NxR.clkPerHalfBit <= resize(unsigned(iWriteData), gClkPerHalfSclkWidth);
            end if;
        end if;

---------------------------------------------------------------------------------------------
-- FSM - Send/Receive data, Spi Clock generation and Data buffering
---------------------------------------------------------------------------------------------
        case R.SpiState is
            when IDLE =>
                NxR.nCS <= not('0');                            -- chip select disabled
                NxR.BitIdx <= gDataWidthST - 1;                 -- reset bit index
                NxR.SpiClk <= R.CPOL;                           -- reset SpiClk to idle state
                NxR.Ready <= '0';                               -- we are now ready to receive new data
                NxR.clkCountPerHalfSclk <= R.clkPerHalfBit;--(others=> '0');      -- reset clk per half sclk counter
                NxR.endTransfer <= '0';                         -- reset end transfer flag

                -- reset delay counter
                NxR.pre_delay <= R.reset_pre_delay;
                NxR.post_delay <= R.reset_post_delay;
                NxR.lastByte <= '0';

                -- change to pre-delay state if new bytes are arriving 
                if iStartOfPackageSink = '1' and iValid = '1' then 
                    NxR.SpiState <= PRE_DELAY;
                    NxR.Ready <= '1';
                end if;
            when PRE_DELAY =>
                NxR.nCS <= not('1');  -- chip select enable
                NxR.SpiClk <= R.CPOL; -- reset SpiClk

                -- register new data after first delay clock
                if R.nCS = not('0') then 
                    NxR.txData <= iData;
                    NxR.Ready <= '0';

                    -- if current byte is also the last byte set according flag
                    if iEndOfPackageSink = '1' then 
                        NxR.lastByte <= '1';
                    end if;
                end if;

                -- define initial Clock state
                if (R.CPHA = '0') then 
                    NxR.SpiClkState <= SAMPLE;
                else 
                    NxR.SpiClkState <= SHIFT;
                end if;

                -- handle pre-delay and go to transfer state if delay over
                if R.pre_delay > 0 then 
                    NxR.pre_delay <= R.pre_delay - 1;
                else 
                    NxR.SpiState <= TRANSFER;
                end if;

            when TRANSFER =>
                
---------------------------------------------------------------------------------------------
-- SPI-Clock Generation
---------------------------------------------------------------------------------------------
                case R.SpiClkState is
                    when SAMPLE => 
                        -- set sclk to user defined state
                        NxR.SpiClk <= (R.CPOL xor R.CPHA);
                        
                        if R.clkCountPerHalfSclk = 0 then 
                            NxR.SpiClkState <= SHIFT;
                            NxR.clkCountPerHalfSclk <= R.clkPerHalfBit - 1; 
                            if (R.CPHA = '1') then 
                                if R.BitIdx = 0 then 
                                    NxR.BitIdx <= gDataWidthST - 1;
                                else 
                                    NxR.BitIdx <= R.BitIdx - 1;
                                end if;
                            end if;
                        else 
                            NxR.clkCountPerHalfSclk <= R.clkCountPerHalfSclk - 1;
                        end if;
                    when SHIFT =>
                        NxR.SpiClk <= not(R.CPOL xor R.CPHA);
                            
                        if R.clkCountPerHalfSclk = 0 then 
                            NxR.SpiClkState <= SAMPLE;
                            NxR.clkCountPerHalfSclk <= R.clkPerHalfBit - 1;
                            if (R.CPHA = '0') then 
                                if R.BitIdx = 0 then 
                                    NxR.BitIdx <= gDataWidthST - 1;
                                else 
                                    NxR.BitIdx <= R.BitIdx - 1;
                                end if;
                            end if;
                        else 
                            NxR.clkCountPerHalfSclk <= R.clkCountPerHalfSclk - 1;
                        end if;
                end case;

---------------------------------------------------------------------------------------------
-- MOSI - send data
---------------------------------------------------------------------------------------------
                NxR.MOSI <= R.txData(R.txData'left);
                if (R.SpiClkState = SHIFT and R.clkCountPerHalfSclk = 0 and (R.CPHA='0' or R.BitIdx /= gDataWidthST - 1)) then
                    NxR.txData <= R.txData(R.txData'left - 1 downto 0) & R.txData(0);
                end if;

                -- Ready to read new byte
                if R.BitIdx = 0 and R.lastByte = '0' and R.SpiClkState = SHIFT and R.clkCountPerHalfSclk = 0 then 
                    NxR.Ready <= '1';
                end if;

---------------------------------------------------------------------------------------------
-- MISO - receive data
---------------------------------------------------------------------------------------------   
                if (R.SpiClkState = SAMPLE and R.clkCountPerHalfSclk = 0) then --and R.BitIdx /= 0) then
                    NxR.RxValid <= '0';
                    NxR.rxData <= R.rxData(R.rxData'left - 1 downto 0) & iMISO;
                    if R.BitIdx = 0 then 
                        NxR.RxValid <= '1';
                    end if;
                end if;

                -- go to post delay state if all bytes have been sent
                if (R.BitIdx = 0 and R.lastByte = '1') then 
                    NxR.endTransfer <= '1';
                elsif R.endTransfer = '1' then 
                    NxR.SpiClk <= R.CPOL;
                    NxR.SpiState <= POST_DELAY; 
                    NxR.MOSI <= '0';
                end if; 

---------------------------------------------------------------------------------------------
-- Register next data
---------------------------------------------------------------------------------------------
                -- if new data is available and we are ready to receive it
                if iValid = '1' and R.Ready = '1' then
                    NxR.txData     <= iData;                          -- buffer next byte
                    NxR.SpiState   <= TRANSFER;                       -- stay in transfer state
                    NxR.Ready      <= '0';                            -- signal that we are busy

                    -- set last byte flag if necessary
                    if iEndOfPackageSink = '1' then 
                        NxR.lastByte <= '1';
                    end if;
                end if;
            
            when POST_DELAY =>
                
                if R.post_delay > 0 then 
                    NxR.post_delay <= R.post_delay - 1;
                else 
                    NxR.SpiState <= IDLE;
                    NxR.nCS <= not('0');
                end if;

        end case;
    end process;


    -- connect internal signals with external signals
    oReady <= R.Ready;
    oSCLK  <= R.SpiClk;
    oMOSI  <= R.MOSI;
    oCS    <= R.nCS;
    oValid <= R.RxValid;
    oData  <= R.rxData;
    oReadData <= R.ReadDataMM;

end architecture;
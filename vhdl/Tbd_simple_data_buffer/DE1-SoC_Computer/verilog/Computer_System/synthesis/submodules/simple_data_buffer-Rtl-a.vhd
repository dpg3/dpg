-------------------------------------------------------------------------------
-- Title      : Simple Data Buffer
-- Project    : DPG
-------------------------------------------------------------------------------
-- File       : simple_data_buffer-Rtl-a.vhd
-- Author     : Matthias Possenig
-------------------------------------------------------------------------------
-- Description: Gets a byte via avalon-mm and sends it to the spi-master. 
--              Receiving bytes are currently ignored.
-------------------------------------------------------------------------------


architecture Rtl of simple_data_buffer is

    type t_Reg is record
        txData : std_ulogic_vector(gDataWidthST-1 downto 0);
        txDataValid : std_ulogic;
        txStartOfPacket : std_ulogic;
        txEndOfPacket : std_ulogic;
        txLastReadyState : std_ulogic;
        rxData : std_ulogic_vector(gDataWidthST-1 downto 0);
        -- txLastByteRead : std_ulogic;
        ReadDataMM : std_ulogic_vector(gDataWidthMM - 1 downto 0);
    end record;

    constant cRegInit : t_Reg := (
        txData => (others => '0'),
        txDataValid => '0',
        txStartOfPacket => '0',
        txEndOfPacket => '0',
        txLastReadyState => '0',
        rxData => (others => '0'),
        -- txLastByteRead => '0',
        ReadDataMM => (others => '0')
    );

    signal R, NxR : t_Reg;

begin

    -- Register process
    Registers : process (iClk, inResetAsync) is
    begin
        if (inResetAsync = not('1')) then
            -- set defaults
            R <= cRegInit;
        elsif rising_edge(iClk) then
            R <= NxR;
        end if;
    end process;

    -- combinatorial process
    CombProcess : process (R, iReady, iValid, iData, iWrite, iWriteData, iAddress, iRead) is
    begin
        -- set defaults
        NxR <= R;

        -- save ready state
        NxR.txLastReadyState <= iReady;

        ------------------------------------------------------
        -- Get data from mm interface if new data is available
        ------------------------------------------------------

        if (iWrite ='1') then 
            if iAddress = (iAddress'range => '0') then 
                -- send data to spi master
                NxR.txData <= std_ulogic_vector(resize(unsigned(iWriteData), gDataWidthST));
                NxR.txDataValid <= '1';
            end if;
            -- write to address 1 (readdata) has no effect
        elsif (iRead = '1') then 
            if iAddress = (iAddress'range => '0') then 
                -- return the sending data to spi master
                NxR.ReadDataMM <= std_ulogic_vector(resize(unsigned(R.txData), gDataWidthMM));
            elsif unsigned(iAddress) = to_unsigned(1, gAddressWidth) then 
                -- return the received data to spi master 
                NxR.ReadDataMM <= std_ulogic_vector(resize(unsigned(R.rxData),gDataWidthMM));
            end if;
        end if;

        ------------------------------------------------------
        -- Send data to spi-master via streaming interface
        ------------------------------------------------------

        -- check if the last byte was read
        if R.txEndOfPacket = '1' and R.txLastReadyState = '1' and iReady = '0' then 
            --NxR.txLastByteRead <= '1';
            NxR.txEndOfPacket  <= '0';
            NxR.txDataValid    <= '0';
        else
            -- handle start of packet
            --if R.txCurrByte = 0 then 
                NxR.txStartOfPacket <= '1';
            --else
                --NxR.txStartOfPacket <= '0';
            --end if;

            -- handle end of packet
            --if R.txCurrByte = dataSize-1 then 
                NxR.txEndOfPacket <= '1';
            --else
                --NxR.txEndOfPacket <= '0';
            --end if;
        end if;

        --------------------------------------
        -- recv data
        --------------------------------------
        if iValid = '1' then 
            NxR.rxData <= iData;
        end if;
    end process;

    -- connect outputs to internal signals
    oValid <= R.txDataValid;
    oData <= R.txData;
    oStartOfPackageSource  <= R.txStartOfPacket;
    oEndOfPackageSource <= R.txEndOfPacket;
    oReadData <= R.ReadDataMM;

end architecture;
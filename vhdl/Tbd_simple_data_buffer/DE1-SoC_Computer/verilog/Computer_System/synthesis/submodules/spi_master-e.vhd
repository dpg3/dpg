-------------------------------------------------------------------------------
-- Title      : SPI-Master
-- Project    : DPG
-------------------------------------------------------------------------------
-- File       : spi_master-e.vhd
-- Author     : Matthias Possenig
-------------------------------------------------------------------------------
-- Description:
-------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity spi_master is
    generic (
        gDataWidthST  : natural := 8;
        gDataWidthMM  : natural := 32;
        gAddressWidth : natural := 32;
        gClkPerHalfSclkWidth : natural := 16;
        gPrePostDelayCounterWidth : natural := 8
    );
    port (
        -- clock and reset
        inResetAsync : in std_ulogic;
        iClk         : in std_ulogic;

        -- Avalon ST Sink
        oReady : out std_ulogic;
        iValid : in std_ulogic;
        iData  : in std_ulogic_vector(gDataWidthST - 1 downto 0);
        iEndOfPackageSink   : in std_ulogic;
        iStartOfPackageSink : in std_ulogic;

        -- Avalon ST Source
        oValid : out std_ulogic;
        oData  : out std_ulogic_vector(gDataWidthST - 1 downto 0);

        -- Avalon MM Slave
        iAddress        : in std_ulogic_vector(gAddressWidth - 1 downto 0);
        iRead           : in std_ulogic;
        oReadData       : out std_ulogic_vector(gDataWidthMM - 1 downto 0);
        iWrite          : in std_ulogic;
        iWriteData      : in std_ulogic_vector(gDataWidthMM - 1 downto 0);

        -- SPI-Interface
        oSCLK : out std_ulogic;
        oCS   : out std_ulogic;
        oMOSI : out std_ulogic;
        iMISO : in std_ulogic
    );
end spi_master;
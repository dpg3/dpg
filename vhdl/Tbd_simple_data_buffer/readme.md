## Description

This Testbed contains following components: 
- HPS
- Spi-Master
- Simple-Data-Buffer
- PIO

The HPS job is to control the Simple-Data-Buffer and the SPI-Master. The Simple-Data-Buffer is connected to the SPI-Master using Avalon Streaming. The SPI-Interface is connected to Pins of the GPIO0 Pin-Header. The PIO Element is only here so we can control if everything is setup correctly. The PIO controls the LEDR0 on De1-SoC Board. For the clock we are using 50 MHz.

## Memory Map

|    Offset   |     End     | Description |
| ----------- | ----------- | ----------- |
| 0x0000_0000 | 0x0000_001f | SPI-Master Config |
| 0x0000_0020 | 0x0000_0027 | Simple-Data-Buffer |
| 0x0000_0030 | 0x0000_003f | PIO for LEDR0 |

## Pin Assignment

| SPI-Signal | Pin on Board |
| ---------- | ---------- |
| MISO | GPIO0_35 |
| MOSI | GPIO0_34 |
| CS | GPIO0_33 |
| SCLK | GPIO0_32 |

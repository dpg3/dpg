library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all; 

entity Tbd_simple_data_buffer is

    port (
        CLOCK_50 : in std_logic;
        DPG_SPI_MISO : in std_logic;
        DPG_SPI_MOSI : out std_logic;
        DPG_SPI_CS : out std_logic;
        DPG_SPI_SCLK : out std_logic;

        LEDR           : out std_logic;
        
        HPS_DDR3_ADDR  : out std_logic_vector(14 downto 0);
        HPS_DDR3_BA    : out std_logic_vector(2 downto 0);
        HPS_DDR3_CK_P  : out std_logic;
        HPS_DDR3_CK_N  : out std_logic;
        HPS_DDR3_CKE   : out std_logic;
        HPS_DDR3_CS_N  : out std_logic;
        HPS_DDR3_RAS_N : out std_logic;
        HPS_DDR3_CAS_N : out std_logic;

        HPS_DDR3_WE_N    : out std_logic;
        HPS_DDR3_RESET_N : out std_logic;
        HPS_DDR3_DQ      : inout std_logic_vector(31 downto 0) := (others => 'X');
        HPS_DDR3_DQS_P   : inout std_logic_vector(3 downto 0)  := (others => 'X');
        HPS_DDR3_DQS_N   : inout std_logic_vector(3 downto 0)  := (others => 'X');
        HPS_DDR3_ODT     : out std_logic;
        HPS_DDR3_DM      : out std_logic_vector(3 downto 0);
        HPS_DDR3_RZQ     : in std_logic := 'X'
    );

end entity Tbd_simple_data_buffer;
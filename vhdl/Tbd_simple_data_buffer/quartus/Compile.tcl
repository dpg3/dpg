# Quartus spreads TCL statements over packages which we have to load
# before the statements can be used
load_package project
load_package flow


# Create a new Quartus project (and delete any existing at the same time)
project_open -force Tbd_simple_data_buffer

execute_flow -compile

# Before the script ends close the Quartus project.
project_close

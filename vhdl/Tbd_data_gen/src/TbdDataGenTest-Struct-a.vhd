----------------------------------------------------------------------------------------------------------------
-- Workfile:    TbdDataGenTest-Struct-a.vhd
-- Author:      Matthias Possenig
----------------------------------------------------------------------------------------------------------------
-- Description: Testbed for Data Generator with SPI master
----------------------------------------------------------------------------------------------------------------

architecture Struct of TbdDataGenTest is
    signal MasterReady : std_ulogic;
    signal GenDataValid : std_ulogic;
    signal GenData : std_ulogic_vector(7 downto 0);
    signal GenEOP, GenSOP : std_ulogic;
    signal MasterDataValid : std_ulogic;
    signal MasterData : std_ulogic_vector(7 downto 0);

    signal Counter : natural := 0;
    signal DebouncedReset : std_ulogic := '0';
begin

    DataGen : entity work.data_gen
        port map(
            inResetAsync => DebouncedReset,
            iClk => iClk,

            iReady => MasterReady,
            oValid => GenDataValid,
            oData => GenData,
            oEndOfPackageSource => GenEOP,
            oStartOfPackageSource => GenSOP,

            iValid => MasterDataValid,
            iData => MasterData
        );

    process (iClk, inRstAsync) is 
    begin
        if inRstAsync = not('1') then 
            DebouncedReset <= '0';
            Counter <= 0;
        elsif rising_edge(iClk) then 
            if Counter = 512 then 
                DebouncedReset <= '1';
            else 
                DebouncedReset <= '0';
                Counter <= Counter + 1;
            end if;
        end if;
    end process;



    SPI_Master : entity work.spi_master
        port map(
            inResetAsync => DebouncedReset,
            iClk => iClk,

            oReady => MasterReady,
            iValid => GenDataValid,
            iData  => GenData,
            iEndOfPackageSink => GenEOP,
            iStartOfPackageSink => GenSOP,

            oValid => MasterDataValid,
            oData => MasterData,
            
            iAddress => (others => '0'),
            iRead => '0',
            oReadData => open,
            iWrite => '0',
            iWriteData => (others => '0'),

            oSCLK => oSclk,
            oCS => onCS,
            oMOSI => oMOSI,
            iMISO => iMISO
        );

end architecture;
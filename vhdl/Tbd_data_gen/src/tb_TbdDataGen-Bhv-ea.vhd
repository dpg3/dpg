----------------------------------------------------------------------------------------------------------------
-- Workfile:    tb_TbdDataGenTest-Bhv-ea.vhd
-- Author:      Matthias Possenig
----------------------------------------------------------------------------------------------------------------
-- Description: Testbench for the Testbed of the Data Generator
----------------------------------------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity tb_TbdDataGenTest is
end entity;

architecture Bhv of tb_TbdDataGenTest is

    signal Clk : std_ulogic := '0';
    signal nRstAsync : std_ulogic := '0';

    signal MISO : std_ulogic := '0';
    signal Sclk, MOSI, nCS : std_ulogic;

    constant cClkPeriod : time := 10 ns; -- 100 MHz
    constant cHalfClkPeriod : time := 5 ns;

begin

    -- clock gen
    Clk <= not Clk after cHalfClkPeriod;

    TbdDataGen : entity work.TbdDataGenTest 
        port map(
            iClk => Clk,
            inRstAsync => nRstAsync,

            oSclk => Sclk,
            oMOSI => MOSI,
            iMISO => MISO,
            onCS  => nCS
        );

    Stimuli : process is 
    begin
        nRstAsync <= '0';
        wait for 50 * cClkPeriod;
        nRstAsync <= '1';

        wait;

    end process;


end architecture;
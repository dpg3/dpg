-------------------------------------------------------------------------------
-- Title      : Testbed for Data Generator
-- Project    : DPG
-------------------------------------------------------------------------------
-- File       : TbdDataGenTest-e.vhd
-- Author     : Matthias Possenig
-------------------------------------------------------------------------------
-- Description:
-------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity TbdDataGenTest is
    port(
        -- clock and reset
        iClk         : in  std_ulogic; -- DE1-SoC: CLOCK_50
        inRstAsync   : in  std_ulogic; -- DE1-Soc: SW[9]

        -- SPI signals
        oSclk : out std_ulogic;
        oMOSI : out std_ulogic;
        iMISO : in std_ulogic;
        onCS  : out std_ulogic
    );
end TbdDataGenTest;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all; 

entity Tbd_databuffer is

    port (
        -- clock input 
        CLOCK_50 : in std_logic;

        -- spi master
        DPG_SPI_MISO : in std_logic;
        DPG_SPI_MOSI : out std_logic;
        DPG_SPI_CS : out std_logic;
        DPG_SPI_SCLK : out std_logic;

        DPG_SPI_GND : out std_ulogic_vector(3 downto 0);

        -- Visualizer
        LEDR : out std_logic_vector(10-1 downto 0);
        HEX0 : out std_logic_vector(7-1 downto 0);
        HEX1 : out std_logic_vector(7-1 downto 0);
        HEX2 : out std_logic_vector(7-1 downto 0);
        HEX3 : out std_logic_vector(7-1 downto 0);
        HEX4 : out std_logic_vector(7-1 downto 0);
        HEX5 : out std_logic_vector(7-1 downto 0);

        -- hps memory
        HPS_DDR3_ADDR  : out std_logic_vector(14 downto 0);
        HPS_DDR3_BA    : out std_logic_vector(2 downto 0);
        HPS_DDR3_CK_P  : out std_logic;
        HPS_DDR3_CK_N  : out std_logic;
        HPS_DDR3_CKE   : out std_logic;
        HPS_DDR3_CS_N  : out std_logic;
        HPS_DDR3_RAS_N : out std_logic;
        HPS_DDR3_CAS_N : out std_logic;

        HPS_DDR3_WE_N    : out std_logic;
        HPS_DDR3_RESET_N : out std_logic;
        HPS_DDR3_DQ      : inout std_logic_vector(31 downto 0) := (others => 'X');
        HPS_DDR3_DQS_P   : inout std_logic_vector(3 downto 0)  := (others => 'X');
        HPS_DDR3_DQS_N   : inout std_logic_vector(3 downto 0)  := (others => 'X');
        HPS_DDR3_ODT     : out std_logic;
        HPS_DDR3_DM      : out std_logic_vector(3 downto 0);
        HPS_DDR3_RZQ     : in std_logic := 'X'
    );

end entity Tbd_databuffer;

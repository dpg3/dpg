
architecture Struct of Tbd_databuffer is

    component HpsAndSpiMaster is
		port (
			clk_clk                  : in    std_logic                     := 'X';             -- clk
			memory_mem_a             : out   std_logic_vector(14 downto 0);                    -- mem_a
			memory_mem_ba            : out   std_logic_vector(2 downto 0);                     -- mem_ba
			memory_mem_ck            : out   std_logic;                                        -- mem_ck
			memory_mem_ck_n          : out   std_logic;                                        -- mem_ck_n
			memory_mem_cke           : out   std_logic;                                        -- mem_cke
			memory_mem_cs_n          : out   std_logic;                                        -- mem_cs_n
			memory_mem_ras_n         : out   std_logic;                                        -- mem_ras_n
			memory_mem_cas_n         : out   std_logic;                                        -- mem_cas_n
			memory_mem_we_n          : out   std_logic;                                        -- mem_we_n
			memory_mem_reset_n       : out   std_logic;                                        -- mem_reset_n
			memory_mem_dq            : inout std_logic_vector(31 downto 0) := (others => 'X'); -- mem_dq
			memory_mem_dqs           : inout std_logic_vector(3 downto 0)  := (others => 'X'); -- mem_dqs
			memory_mem_dqs_n         : inout std_logic_vector(3 downto 0)  := (others => 'X'); -- mem_dqs_n
			memory_mem_odt           : out   std_logic;                                        -- mem_odt
			memory_mem_dm            : out   std_logic_vector(3 downto 0);                     -- mem_dm
			memory_oct_rzqin         : in    std_logic                     := 'X';             -- oct_rzqin
			spi_interface_miso       : in    std_logic                     := 'X';             -- miso
			spi_interface_mosi       : out   std_logic;                                        -- mosi
			spi_interface_sclk       : out   std_logic;                                        -- sclk
			spi_interface_cs         : out   std_logic;                                        -- cs
			visualizer_ledr_output   : out   std_logic_vector(9 downto 0);                     -- output
			visualizer_sevenseg_hex0 : out   std_logic_vector(6 downto 0);                     -- hex0
			visualizer_sevenseg_hex1 : out   std_logic_vector(6 downto 0);                     -- hex1
			visualizer_sevenseg_hex2 : out   std_logic_vector(6 downto 0);                     -- hex2
			visualizer_sevenseg_hex3 : out   std_logic_vector(6 downto 0);                     -- hex3
			visualizer_sevenseg_hex4 : out   std_logic_vector(6 downto 0);                     -- hex4
			visualizer_sevenseg_hex5 : out   std_logic_vector(6 downto 0)                      -- hex5
		);
	end component HpsAndSpiMaster;

    signal tmpHex0 : std_logic_vector(6 downto 0);
    signal tmpHex1 : std_logic_vector(6 downto 0);
    signal tmpHex2 : std_logic_vector(6 downto 0);
    signal tmpHex3 : std_logic_vector(6 downto 0);
    signal tmpHex4 : std_logic_vector(6 downto 0);
    signal tmpHex5 : std_logic_vector(6 downto 0);

begin

    u0 : component HpsAndSpiMaster
		port map (
			clk_clk                        => CLOCK_50,            --           clk.clk
			memory_mem_a                   => HPS_DDR3_ADDR,
            memory_mem_ba                  => HPS_DDR3_BA,
            memory_mem_ck                  => HPS_DDR3_CK_P,
            memory_mem_ck_n                => HPS_DDR3_CK_N,
            memory_mem_cke                 => HPS_DDR3_CKE,
            memory_mem_cs_n                => HPS_DDR3_CS_N,
            memory_mem_ras_n               => HPS_DDR3_RAS_N,
            memory_mem_cas_n               => HPS_DDR3_CAS_N,
            memory_mem_we_n                => HPS_DDR3_WE_N,
            memory_mem_reset_n             => HPS_DDR3_RESET_N,
            memory_mem_dq                  => HPS_DDR3_DQ,
            memory_mem_dqs                 => HPS_DDR3_DQS_P,
            memory_mem_dqs_n               => HPS_DDR3_DQS_N,
            memory_mem_odt                 => HPS_DDR3_ODT,
            memory_mem_dm                  => HPS_DDR3_DM,
            memory_oct_rzqin               => HPS_DDR3_RZQ,
			spi_interface_miso => DPG_SPI_MISO, -- spi_interface.miso
			spi_interface_mosi => DPG_SPI_MOSI, --              .mosi
			spi_interface_sclk => DPG_SPI_SCLK, --              .sclk
			spi_interface_cs   => DPG_SPI_CS,   --              .cs
            visualizer_ledr_output   => LEDR,   --     visualizer_ledr.output
			visualizer_sevenseg_hex0 => tmpHex0, -- visualizer_sevenseg.hex0
			visualizer_sevenseg_hex1 => tmpHex1, --                    .hex1
			visualizer_sevenseg_hex2 => tmpHex2, --                    .hex2
			visualizer_sevenseg_hex3 => tmpHex3, --                    .hex3
			visualizer_sevenseg_hex4 => tmpHex4, --                    .hex4
			visualizer_sevenseg_hex5 => tmpHex5  --                    .hex5
		);

        -- invert hex signals
        HEX0 <= not(tmpHex0);
        HEX1 <= not(tmpHex1);
        HEX2 <= not(tmpHex2);
        HEX3 <= not(tmpHex3);
        HEX4 <= not(tmpHex4);
        HEX5 <= not(tmpHex5);

        DPG_SPI_GND <= (others => '0');

end architecture;

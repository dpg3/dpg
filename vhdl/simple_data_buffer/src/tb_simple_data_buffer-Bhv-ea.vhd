
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity tb_data_gen is
end entity;

architecture Bhv of tb_data_gen is

    signal ReadyFromMaster, ValidFromGen, EndOfPacketFromGen, StartOfPacketSourceFromGen, ValidFromMaster : std_ulogic                    := '0';
    signal DataFromGen, DataFromMaster                                                                    : std_ulogic_vector(7 downto 0) := (others => '0');

    -- avalon mm for spi
    signal ReadSPI, WriteSPI         : std_ulogic                     := '0';
    signal AddressSPI                : std_ulogic_vector(31 downto 0) := (others => '0');
    signal ReadDataSPI, WriteDataSPI : std_ulogic_vector(31 downto 0) := (others => '0');

    -- avalon mm for simple data buffer
    signal ReadBuf, WriteBuf         : std_ulogic                     := '0';
    signal AddressBuf                : std_ulogic_vector(31 downto 0) := (others => '0');
    signal ReadDataBuf, WriteDataBuf : std_ulogic_vector(31 downto 0) := (others => '0');

    signal clk       : std_ulogic := '0';
    signal nRstAsync : std_ulogic := '1';

    signal MISO, MOSI, SCLK, nCS : std_ulogic := '0';

    constant cClkPeriod : time := 10 ns; -- 100 MHz
    constant cHalfClkPeriod : time := 5 ns;

    type aTestvalue is record
        TxAddress   : std_ulogic_vector(31 downto 0);
        RxAddress   : std_ulogic_vector(31 downto 0);
        WriteData : std_ulogic_vector(31 downto 0);
    end record;

    type aTestvalueColl is array (natural range <>) of aTestvalue;

    constant Testvalues : aTestvalueColl(1 to 4) := (
        (
            TxAddress => (others => '0'),
            RxAddress => (0 => '1', others => '0'),
            WriteData => x"000000AA"
        ),
        (
            TxAddress => (others => '0'),
            RxAddress => (0 => '1', others => '0'),
            WriteData => x"000000BB"
        ),
        (
            TxAddress => (others => '0'),
            RxAddress => (0 => '1', others => '0'),
            WriteData => x"000000FF"
        ),
        (
            TxAddress => (others => '0'),
            RxAddress => (0 => '1', others => '0'),
            WriteData => x"0000000A"
        )
    );
begin

    -----------------------------------------------------------------------------
    -- Instantiate DUT
    -----------------------------------------------------------------------------
    simple_data_buffer : entity work.simple_data_buffer
        port map(
            -- clock and reset
            inResetAsync => nRstAsync,
            iClk         => clk,

            -- Avalon ST Sink
            iReady                => ReadyFromMaster,
            oValid                => ValidFromGen,
            oData                 => DataFromGen,
            oEndOfPackageSource   => EndOfPacketFromGen,
            oStartOfPackageSource => StartOfPacketSourceFromGen,

            -- Avalon ST Source
            iValid => ValidFromMaster,
            iData  => DataFromMaster,

            -- Avalon MM 
            iAddress => AddressBuf,
            iRead => ReadBuf,
            oReadData  => ReadDataBuf,
            iWrite => WriteBuf,
            iWriteData => WriteDataBuf
        );

    spi_master : entity work.spi_master
        generic map(
            gDataWidthMM => 32,
            gDataWidthST => 8
        )
        port map(
            -- clock and reset
            inResetAsync => nRstAsync,
            iClk         => clk,

            -- Avalon ST Sink
            oReady              => ReadyFromMaster,
            iValid              => ValidFromGen,
            iData               => DataFromGen,
            iEndOfPackageSink   => EndOfPacketFromGen,
            iStartOfPackageSink => StartOfPacketSourceFromGen,

            -- Avalon ST Source
            oValid => ValidFromMaster,
            oData  => DataFromMaster,

            -- Avalon MM Slave
            iAddress   => AddressSPI,
            iRead      => ReadSPI,
            oReadData  => ReadDataSPI,
            iWrite     => WriteSPI,
            iWriteData => WriteDataSPI,

            -- SPI-Interface
            oSCLK => SCLK,
            oCS   => nCS,
            oMOSI => MOSI,
            iMISO => MISO
        );

    -- loopback data
    MISO <= MOSI;

    -- clock gen
    clk <= not clk after cHalfClkPeriod;

    Stimuli : process is
    begin
        nRstAsync <= '0';
        wait for 20 * cClkPeriod;
        nRstAsync <= '1';

        wait for 10 * cClkPeriod;

        for i in Testvalues'range loop
            AddressBuf <= Testvalues(i).TxAddress;
            WriteBuf <= '1';
            WriteDataBuf <= Testvalues(i).WriteData;
            wait for 2 * cClkPeriod;
            WriteBuf <= '0';
            wait for 100 * cClkPeriod;
            AddressBuf <= Testvalues(i).RxAddress;
            ReadBuf <= '1';
            wait for 2*cClkPeriod;
            assert ReadDataBuf = Testvalues(i).WriteData report "Invalid Byte received" 
            severity error;
            wait for 2*cClkPeriod;
            ReadBuf <= '0';
            wait for 2*cClkPeriod;

        end loop;

        wait;
    end process;

end architecture;
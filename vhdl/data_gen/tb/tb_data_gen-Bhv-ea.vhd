
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity tb_data_gen is
end entity;

architecture Bhv of tb_data_gen is

    signal ReadyFromMaster, ValidFromGen, EndOfPacketFromGen, StartOfPacketSourceFromGen, ValidFromMaster : std_ulogic                    := '0';
    signal DataFromGen, DataFromMaster                                                                    : std_ulogic_vector(7 downto 0) := (others => '0');

    signal Read, Write         : std_ulogic                     := '0';
    signal Address             : std_ulogic_vector(31 downto 0) := (others => '0');
    signal ReadData, WriteData : std_ulogic_vector(31 downto 0) := (others => '0');

    signal clk       : std_ulogic := '0';
    signal nRstAsync : std_ulogic := '1';

    signal MISO, MOSI, SCLK, nCS : std_ulogic := '0';

    constant cClkPeriod : time := 10 ns; -- 100 MHz
    constant cHalfClkPeriod : time := 5 ns;

begin

    -----------------------------------------------------------------------------
    -- Instantiate DUT
    -----------------------------------------------------------------------------
    data_generator : entity work.data_gen
        port map(
            -- clock and reset
            inResetAsync => nRstAsync,
            iClk         => clk,

            -- Avalon ST Sink
            iReady                => ReadyFromMaster,
            oValid                => ValidFromGen,
            oData                 => DataFromGen,
            oEndOfPackageSource   => EndOfPacketFromGen,
            oStartOfPackageSource => StartOfPacketSourceFromGen,

            -- Avalon ST Source
            iValid => ValidFromMaster,
            iData  => DataFromMaster
        );

    spi_master : entity work.spi_master
        generic map(
            gDataWidthMM => 32,
            gDataWidthST => 8
        )
        port map(
            -- clock and reset
            inResetAsync => nRstAsync,
            iClk         => clk,

            -- Avalon ST Sink
            oReady              => ReadyFromMaster,
            iValid              => ValidFromGen,
            iData               => DataFromGen,
            iEndOfPackageSink   => EndOfPacketFromGen,
            iStartOfPackageSink => StartOfPacketSourceFromGen,

            -- Avalon ST Source
            oValid => ValidFromMaster,
            oData  => DataFromMaster,

            -- Avalon MM Slave
            iAddress   => Address,
            iRead      => Read,
            oReadData  => ReadData,
            iWrite     => Write,
            iWriteData => WriteData,

            -- SPI-Interface
            oSCLK => SCLK,
            oCS   => nCS,
            oMOSI => MOSI,
            iMISO => MISO
        );
    
    -- clock gen
    clk <= not clk after cHalfClkPeriod;

    Stimuli : process is
    begin
        nRstAsync <= '0';
        wait for 20 * cClkPeriod;
        nRstAsync <= '1';

        wait for 1000 * cClkPeriod;
        wait;
    end process;

end architecture;
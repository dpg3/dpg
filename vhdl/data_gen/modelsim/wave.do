onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_data_gen/clk
add wave -noupdate /tb_data_gen/nRstAsync
add wave -noupdate /tb_data_gen/MOSI
add wave -noupdate /tb_data_gen/MISO
add wave -noupdate /tb_data_gen/SCLK
add wave -noupdate /tb_data_gen/ReadyFromMaster
add wave -noupdate /tb_data_gen/ValidFromGen
add wave -noupdate /tb_data_gen/DataFromGen
add wave -noupdate /tb_data_gen/data_generator/oStartOfPackageSource
add wave -noupdate /tb_data_gen/data_generator/oEndOfPackageSource
add wave -noupdate -expand /tb_data_gen/data_generator/R
add wave -noupdate -expand /tb_data_gen/spi_master/R
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {683902 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 325
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {1723656 ps}

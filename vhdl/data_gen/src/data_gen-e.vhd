-------------------------------------------------------------------------------
-- Title      : Data Generator
-- Project    : DPG
-------------------------------------------------------------------------------
-- File       : data_gen-e.vhd
-- Author     : Matthias Possenig
-------------------------------------------------------------------------------
-- Description:
-------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity data_gen is
    generic (
        gDataWidthST  : natural := 8;
        gDataWidthMM  : natural := 32;
        gAddressWidth : natural := 32
    );

    port(
        -- clock and reset
        inResetAsync : in std_ulogic;
        iClk         : in std_ulogic;

        -- Avalon ST Source
        iReady : in std_ulogic;
        oValid : out std_ulogic;
        oData  : out std_ulogic_vector(gDataWidthST - 1 downto 0);
        oEndOfPackageSource   : out std_ulogic;
        oStartOfPackageSource : out std_ulogic;

        -- Avalon ST Sink
        iValid : in std_ulogic;
        iData  : in std_ulogic_vector(gDataWidthST - 1 downto 0)
    );
end data_gen;
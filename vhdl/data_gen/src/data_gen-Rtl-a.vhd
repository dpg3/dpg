----------------------------------------------------------------------------------------------------------------
-- Workfile:    data_gen-Rtl-a.vhd
-- Author:      Matthias Possenig
----------------------------------------------------------------------------------------------------------------
-- Description: This file contains the hdl for a Avalon-ST data generator to test our SPI-Master on real 
--              Hardware without the other components. 
----------------------------------------------------------------------------------------------------------------

architecture Rtl of data_gen is

    constant dataSize : natural := 4; -- 4 bytes
    type dataArr is array(dataSize-1 downto 0) of std_ulogic_vector(gDataWidthST - 1 downto 0);
    constant data : dataArr := (
        0 => x"AA",
        1 => x"FF", 
        2 => x"55",
        3 => x"0F"
    );

    type t_Reg is record
        txData : std_ulogic_vector(gDataWidthST-1 downto 0);
        txCurrByte : natural;
        txDataValid : std_ulogic;
        txStartOfPacket : std_ulogic;
        txEndOfPacket : std_ulogic;
        txLastReadyState : std_ulogic;
        rxData : std_ulogic_vector(gDataWidthST-1 downto 0);
        txIdleCounter : unsigned(7 downto 0);
        txLastByteRead : std_ulogic;
    end record;

    constant cRegInit : t_Reg := (
        txData => (others => '0'),
        txCurrByte => 0,
        txDataValid => '0',
        txStartOfPacket => '0',
        txEndOfPacket => '0',
        txLastReadyState => '0',
        rxData => (others => '0'),
        txIdleCounter => (others => '0'),
        txLastByteRead => '0'
    );

    signal R, NxR : t_Reg;

begin

    -- Register process
    Registers : process (iClk, inResetAsync) is
    begin
        if (inResetAsync = not('1')) then
            -- set defaults
            R <= cRegInit;
        elsif rising_edge(iClk) then
            R <= NxR;
        end if;
    end process;

    CombProcess : process (R, iReady, iValid, iData) is
    begin
        NxR <= R;

        -- save ready state
        NxR.txLastReadyState <= iReady;

        -- wait for a couple of clock cycles 
        if R.txLastByteRead = '1' and R.txIdleCounter < 255 then 
            NxR.txIdleCounter <= R.txIdleCounter + 1;
        else 
            -- reset idlecounter und last byte flag
            NxR.txIdleCounter <= (others => '0');
            NxR.txLastByteRead <= '0';

            if R.txCurrByte < dataSize then 
                NxR.txData <= data(R.txCurrByte);
                NxR.txDataValid <= '1';
            end if;

            -- check if the last byte was read
            if R.txEndOfPacket = '1' and R.txLastReadyState = '1' and iReady = '0' then 
                NxR.txLastByteRead <= '1';
                NxR.txEndOfPacket  <= '0';
                NxR.txDataValid    <= '0';
            else
                -- handle start of packet
                if R.txCurrByte = 0 then 
                    NxR.txStartOfPacket <= '1';
                else
                    NxR.txStartOfPacket <= '0';
                end if;

                -- handle end of packet
                if R.txCurrByte = dataSize-1 then 
                    NxR.txEndOfPacket <= '1';
                else
                    NxR.txEndOfPacket <= '0';
                end if;
            end if;

            --------------------------------------
            -- send data
            --------------------------------------
            --if (iReady = '1' and R.txCurrByte = 0) or (iReady = '0' and R.txCurrByte /= 0 and R.txCurrByte < dataSize and R.txLastReadyState = '1') then 
            if (R.txCurrByte < dataSize and R.txLastReadyState = '1' and iReady = '0') then 
                NxR.txCurrByte <= R.txCurrByte + 1;
            elsif R.txCurrByte = dataSize then                   
                -- reset index and put valid line to low
                NxR.txCurrByte <= 0;
            end if;
        end if;

        --------------------------------------
        -- recv data
        --------------------------------------
        if iValid = '1' then 
            NxR.rxData <= iData;
        end if;
    end process;

    -- connect outputs to internal signals
    oValid <= R.txDataValid;
    oData <= R.txData;
    oStartOfPackageSource  <= R.txStartOfPacket;
    oEndOfPackageSource <= R.txEndOfPacket;

end architecture;
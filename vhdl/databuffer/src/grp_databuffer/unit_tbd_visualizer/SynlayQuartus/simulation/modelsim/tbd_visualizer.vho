-- Copyright (C) 2018  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 18.1.0 Build 625 09/12/2018 SJ Lite Edition"

-- DATE "10/16/2022 16:59:28"

-- 
-- Device: Altera 5CSEMA5F31C6 Package FBGA896
-- 

-- 
-- This VHDL file should be used for ModelSim (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY ALTERA_LNSIM;
LIBRARY CYCLONEV;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE ALTERA_LNSIM.ALTERA_LNSIM_COMPONENTS.ALL;
USE CYCLONEV.CYCLONEV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	tbd_visualizer IS
    PORT (
	iClk : IN std_logic;
	inResetAsync : IN std_logic;
	SW : IN IEEE.STD_LOGIC_1164.std_ulogic_vector(3 DOWNTO 0);
	LEDR : OUT IEEE.STD_LOGIC_1164.std_ulogic_vector(9 DOWNTO 0);
	HEX0 : OUT IEEE.STD_LOGIC_1164.std_ulogic_vector(6 DOWNTO 0);
	HEX1 : OUT IEEE.STD_LOGIC_1164.std_ulogic_vector(6 DOWNTO 0);
	HEX2 : OUT IEEE.STD_LOGIC_1164.std_ulogic_vector(6 DOWNTO 0);
	HEX3 : OUT IEEE.STD_LOGIC_1164.std_ulogic_vector(6 DOWNTO 0);
	HEX4 : OUT IEEE.STD_LOGIC_1164.std_ulogic_vector(6 DOWNTO 0);
	HEX5 : OUT IEEE.STD_LOGIC_1164.std_ulogic_vector(6 DOWNTO 0)
	);
END tbd_visualizer;

-- Design Ports Information
-- LEDR[0]	=>  Location: PIN_V16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- LEDR[1]	=>  Location: PIN_W16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- LEDR[2]	=>  Location: PIN_V17,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- LEDR[3]	=>  Location: PIN_V18,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- LEDR[4]	=>  Location: PIN_W17,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- LEDR[5]	=>  Location: PIN_W19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- LEDR[6]	=>  Location: PIN_Y19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- LEDR[7]	=>  Location: PIN_W20,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- LEDR[8]	=>  Location: PIN_W21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- LEDR[9]	=>  Location: PIN_Y21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- HEX0[0]	=>  Location: PIN_AE26,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- HEX0[1]	=>  Location: PIN_AE27,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- HEX0[2]	=>  Location: PIN_AE28,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- HEX0[3]	=>  Location: PIN_AG27,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- HEX0[4]	=>  Location: PIN_AF28,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- HEX0[5]	=>  Location: PIN_AG28,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- HEX0[6]	=>  Location: PIN_AH28,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- HEX1[0]	=>  Location: PIN_AJ29,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- HEX1[1]	=>  Location: PIN_AH29,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- HEX1[2]	=>  Location: PIN_AH30,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- HEX1[3]	=>  Location: PIN_AG30,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- HEX1[4]	=>  Location: PIN_AF29,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- HEX1[5]	=>  Location: PIN_AF30,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- HEX1[6]	=>  Location: PIN_AD27,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- HEX2[0]	=>  Location: PIN_AB23,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- HEX2[1]	=>  Location: PIN_AE29,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- HEX2[2]	=>  Location: PIN_AD29,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- HEX2[3]	=>  Location: PIN_AC28,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- HEX2[4]	=>  Location: PIN_AD30,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- HEX2[5]	=>  Location: PIN_AC29,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- HEX2[6]	=>  Location: PIN_AC30,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- HEX3[0]	=>  Location: PIN_AD26,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- HEX3[1]	=>  Location: PIN_AC27,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- HEX3[2]	=>  Location: PIN_AD25,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- HEX3[3]	=>  Location: PIN_AC25,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- HEX3[4]	=>  Location: PIN_AB28,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- HEX3[5]	=>  Location: PIN_AB25,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- HEX3[6]	=>  Location: PIN_AB22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- HEX4[0]	=>  Location: PIN_AA24,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- HEX4[1]	=>  Location: PIN_Y23,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- HEX4[2]	=>  Location: PIN_Y24,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- HEX4[3]	=>  Location: PIN_W22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- HEX4[4]	=>  Location: PIN_W24,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- HEX4[5]	=>  Location: PIN_V23,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- HEX4[6]	=>  Location: PIN_W25,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- HEX5[0]	=>  Location: PIN_V25,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- HEX5[1]	=>  Location: PIN_AA28,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- HEX5[2]	=>  Location: PIN_Y27,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- HEX5[3]	=>  Location: PIN_AB27,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- HEX5[4]	=>  Location: PIN_AB26,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- HEX5[5]	=>  Location: PIN_AA26,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- HEX5[6]	=>  Location: PIN_AA25,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- iClk	=>  Location: PIN_AF14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- inResetAsync	=>  Location: PIN_AE12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[1]	=>  Location: PIN_AC12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[2]	=>  Location: PIN_AF9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[3]	=>  Location: PIN_AF10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[0]	=>  Location: PIN_AB12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default


ARCHITECTURE structure OF tbd_visualizer IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_iClk : std_logic;
SIGNAL ww_inResetAsync : std_logic;
SIGNAL ww_SW : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_LEDR : std_logic_vector(9 DOWNTO 0);
SIGNAL ww_HEX0 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_HEX1 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_HEX2 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_HEX3 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_HEX4 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_HEX5 : std_logic_vector(6 DOWNTO 0);
SIGNAL \~QUARTUS_CREATED_GND~I_combout\ : std_logic;
SIGNAL \iClk~input_o\ : std_logic;
SIGNAL \iClk~inputCLKENA0_outclk\ : std_logic;
SIGNAL \DUT|Add0~1_sumout\ : std_logic;
SIGNAL \inResetAsync~input_o\ : std_logic;
SIGNAL \DUT|Add0~2\ : std_logic;
SIGNAL \DUT|Add0~49_sumout\ : std_logic;
SIGNAL \DUT|Add0~50\ : std_logic;
SIGNAL \DUT|Add0~45_sumout\ : std_logic;
SIGNAL \DUT|Add0~46\ : std_logic;
SIGNAL \DUT|Add0~41_sumout\ : std_logic;
SIGNAL \DUT|Add0~42\ : std_logic;
SIGNAL \DUT|Add0~37_sumout\ : std_logic;
SIGNAL \DUT|Add0~38\ : std_logic;
SIGNAL \DUT|Add0~33_sumout\ : std_logic;
SIGNAL \DUT|Add0~34\ : std_logic;
SIGNAL \DUT|Add0~29_sumout\ : std_logic;
SIGNAL \DUT|Add0~30\ : std_logic;
SIGNAL \DUT|Add0~25_sumout\ : std_logic;
SIGNAL \DUT|Add0~26\ : std_logic;
SIGNAL \DUT|Add0~5_sumout\ : std_logic;
SIGNAL \DUT|Add0~6\ : std_logic;
SIGNAL \DUT|Add0~97_sumout\ : std_logic;
SIGNAL \DUT|Add0~98\ : std_logic;
SIGNAL \DUT|Add0~93_sumout\ : std_logic;
SIGNAL \DUT|Add0~94\ : std_logic;
SIGNAL \DUT|Add0~89_sumout\ : std_logic;
SIGNAL \DUT|Add0~90\ : std_logic;
SIGNAL \DUT|Add0~85_sumout\ : std_logic;
SIGNAL \DUT|R.StrobeCount[12]~DUPLICATE_q\ : std_logic;
SIGNAL \DUT|Add0~86\ : std_logic;
SIGNAL \DUT|Add0~81_sumout\ : std_logic;
SIGNAL \DUT|Add0~82\ : std_logic;
SIGNAL \DUT|Add0~13_sumout\ : std_logic;
SIGNAL \DUT|Add0~14\ : std_logic;
SIGNAL \DUT|Add0~17_sumout\ : std_logic;
SIGNAL \DUT|R.StrobeCount[15]~DUPLICATE_q\ : std_logic;
SIGNAL \DUT|Add0~18\ : std_logic;
SIGNAL \DUT|Add0~21_sumout\ : std_logic;
SIGNAL \DUT|R.StrobeCount[16]~feeder_combout\ : std_logic;
SIGNAL \DUT|Add0~22\ : std_logic;
SIGNAL \DUT|Add0~9_sumout\ : std_logic;
SIGNAL \DUT|Equal0~0_combout\ : std_logic;
SIGNAL \DUT|R.StrobeCount[2]~DUPLICATE_q\ : std_logic;
SIGNAL \DUT|Equal0~1_combout\ : std_logic;
SIGNAL \DUT|R.StrobeCount[0]~DUPLICATE_q\ : std_logic;
SIGNAL \DUT|Add0~10\ : std_logic;
SIGNAL \DUT|Add0~77_sumout\ : std_logic;
SIGNAL \DUT|Add0~78\ : std_logic;
SIGNAL \DUT|Add0~73_sumout\ : std_logic;
SIGNAL \DUT|Add0~74\ : std_logic;
SIGNAL \DUT|Add0~69_sumout\ : std_logic;
SIGNAL \DUT|Add0~70\ : std_logic;
SIGNAL \DUT|Add0~65_sumout\ : std_logic;
SIGNAL \DUT|Add0~66\ : std_logic;
SIGNAL \DUT|Add0~61_sumout\ : std_logic;
SIGNAL \DUT|Add0~62\ : std_logic;
SIGNAL \DUT|Add0~57_sumout\ : std_logic;
SIGNAL \DUT|Add0~58\ : std_logic;
SIGNAL \DUT|Add0~53_sumout\ : std_logic;
SIGNAL \DUT|Equal0~2_combout\ : std_logic;
SIGNAL \DUT|Equal0~3_combout\ : std_logic;
SIGNAL \DUT|Equal0~4_combout\ : std_logic;
SIGNAL \DUT|R.Strobe~feeder_combout\ : std_logic;
SIGNAL \DUT|R.Strobe~q\ : std_logic;
SIGNAL \SW[3]~input_o\ : std_logic;
SIGNAL \DUT|R.LightChaserOn~feeder_combout\ : std_logic;
SIGNAL \DUT|R.LightChaserOn~q\ : std_logic;
SIGNAL \DUT|R.LightChaserOnDelayed~q\ : std_logic;
SIGNAL \DUT|R.LEDStates.LightChaserOn~q\ : std_logic;
SIGNAL \SW[1]~input_o\ : std_logic;
SIGNAL \DUT|R.LedsOn~q\ : std_logic;
SIGNAL \SW[2]~input_o\ : std_logic;
SIGNAL \DUT|R.LedsOff~q\ : std_logic;
SIGNAL \DUT|R.LedsOffDelayed~q\ : std_logic;
SIGNAL \DUT|R.LedsOnDelayed~q\ : std_logic;
SIGNAL \DUT|NextR.LEDStates.LightChaserOn~0_combout\ : std_logic;
SIGNAL \DUT|NextR.LEDStates.LightChaserOn~1_combout\ : std_logic;
SIGNAL \DUT|R.LEDStates.LightChaserOn~DUPLICATE_q\ : std_logic;
SIGNAL \DUT|NextR.LEDStates.AllLedsOn~0_combout\ : std_logic;
SIGNAL \DUT|NextR.LEDStates.AllLedsOn~1_combout\ : std_logic;
SIGNAL \DUT|R.LEDStates.LightChaserRunning~q\ : std_logic;
SIGNAL \DUT|NextR.LEDStates.AllLedsOn~2_combout\ : std_logic;
SIGNAL \DUT|R.LEDStates.AllLedsOn~q\ : std_logic;
SIGNAL \DUT|Selector0~0_combout\ : std_logic;
SIGNAL \DUT|Selector1~0_combout\ : std_logic;
SIGNAL \DUT|Selector2~0_combout\ : std_logic;
SIGNAL \DUT|Selector3~0_combout\ : std_logic;
SIGNAL \DUT|Selector4~0_combout\ : std_logic;
SIGNAL \DUT|Selector5~0_combout\ : std_logic;
SIGNAL \DUT|Selector6~0_combout\ : std_logic;
SIGNAL \DUT|Selector7~0_combout\ : std_logic;
SIGNAL \DUT|Selector8~0_combout\ : std_logic;
SIGNAL \DUT|Selector9~0_combout\ : std_logic;
SIGNAL \SW[0]~input_o\ : std_logic;
SIGNAL \DUT|R.IncCounter~feeder_combout\ : std_logic;
SIGNAL \DUT|R.IncCounter~q\ : std_logic;
SIGNAL \DUT|R.IncCounterDelayed~q\ : std_logic;
SIGNAL \DUT|NextR.Carries[0]~0_combout\ : std_logic;
SIGNAL \DUT|R.BCDCounter[0][0]~6_combout\ : std_logic;
SIGNAL \DUT|R.BCDCounter[0][3]~0_combout\ : std_logic;
SIGNAL \DUT|R.BCDCounter[0][0]~q\ : std_logic;
SIGNAL \DUT|NextR~4_combout\ : std_logic;
SIGNAL \DUT|R.BCDCounter[0][1]~q\ : std_logic;
SIGNAL \DUT|R.BCDCounter[0][2]~1_combout\ : std_logic;
SIGNAL \DUT|R.BCDCounter[0][2]~q\ : std_logic;
SIGNAL \DUT|NextR~5_combout\ : std_logic;
SIGNAL \DUT|R.BCDCounter[0][3]~q\ : std_logic;
SIGNAL \DUT|R.BCDCounter[0][1]~DUPLICATE_q\ : std_logic;
SIGNAL \DUT|Mux6~0_combout\ : std_logic;
SIGNAL \DUT|R.Hex[0][0]~q\ : std_logic;
SIGNAL \DUT|Mux5~0_combout\ : std_logic;
SIGNAL \DUT|R.Hex[0][1]~q\ : std_logic;
SIGNAL \DUT|Mux4~0_combout\ : std_logic;
SIGNAL \DUT|R.Hex[0][2]~q\ : std_logic;
SIGNAL \DUT|Mux3~0_combout\ : std_logic;
SIGNAL \DUT|R.Hex[0][3]~q\ : std_logic;
SIGNAL \DUT|Mux2~0_combout\ : std_logic;
SIGNAL \DUT|R.Hex[0][4]~q\ : std_logic;
SIGNAL \DUT|R.BCDCounter[0][3]~DUPLICATE_q\ : std_logic;
SIGNAL \DUT|Mux1~0_combout\ : std_logic;
SIGNAL \DUT|R.Hex[0][5]~q\ : std_logic;
SIGNAL \DUT|Mux0~0_combout\ : std_logic;
SIGNAL \DUT|R.Hex[0][6]~q\ : std_logic;
SIGNAL \DUT|R.BCDCounter[1][0]~q\ : std_logic;
SIGNAL \DUT|Equal1~0_combout\ : std_logic;
SIGNAL \DUT|R.BCDCounter[1][0]~2_combout\ : std_logic;
SIGNAL \DUT|R.BCDCounter[1][0]~DUPLICATE_q\ : std_logic;
SIGNAL \DUT|R.BCDCounter[1][1]~q\ : std_logic;
SIGNAL \DUT|R.BCDCounter[1][1]~3_combout\ : std_logic;
SIGNAL \DUT|R.BCDCounter[1][1]~DUPLICATE_q\ : std_logic;
SIGNAL \DUT|R.BCDCounter[1][2]~4_combout\ : std_logic;
SIGNAL \DUT|R.BCDCounter[1][2]~q\ : std_logic;
SIGNAL \DUT|R.BCDCounter[1][3]~q\ : std_logic;
SIGNAL \DUT|R.BCDCounter[1][3]~5_combout\ : std_logic;
SIGNAL \DUT|R.BCDCounter[1][3]~DUPLICATE_q\ : std_logic;
SIGNAL \DUT|Mux13~0_combout\ : std_logic;
SIGNAL \DUT|R.Hex[1][0]~q\ : std_logic;
SIGNAL \DUT|Mux12~0_combout\ : std_logic;
SIGNAL \DUT|R.Hex[1][1]~q\ : std_logic;
SIGNAL \DUT|Mux11~0_combout\ : std_logic;
SIGNAL \DUT|R.Hex[1][2]~q\ : std_logic;
SIGNAL \DUT|Mux10~0_combout\ : std_logic;
SIGNAL \DUT|R.Hex[1][3]~q\ : std_logic;
SIGNAL \DUT|Mux9~0_combout\ : std_logic;
SIGNAL \DUT|R.Hex[1][4]~q\ : std_logic;
SIGNAL \DUT|Mux8~0_combout\ : std_logic;
SIGNAL \DUT|R.Hex[1][5]~q\ : std_logic;
SIGNAL \DUT|Mux7~0_combout\ : std_logic;
SIGNAL \DUT|R.Hex[1][6]~q\ : std_logic;
SIGNAL \DUT|R.Hex[2][0]~feeder_combout\ : std_logic;
SIGNAL \DUT|R.Hex[2][0]~q\ : std_logic;
SIGNAL \DUT|R.Hex[2][1]~feeder_combout\ : std_logic;
SIGNAL \DUT|R.Hex[2][1]~q\ : std_logic;
SIGNAL \DUT|R.Hex[2][2]~feeder_combout\ : std_logic;
SIGNAL \DUT|R.Hex[2][2]~q\ : std_logic;
SIGNAL \DUT|R.Hex[2][3]~feeder_combout\ : std_logic;
SIGNAL \DUT|R.Hex[2][3]~q\ : std_logic;
SIGNAL \DUT|R.Hex[2][4]~feeder_combout\ : std_logic;
SIGNAL \DUT|R.Hex[2][4]~q\ : std_logic;
SIGNAL \DUT|R.Hex[2][5]~feeder_combout\ : std_logic;
SIGNAL \DUT|R.Hex[2][5]~q\ : std_logic;
SIGNAL \DUT|R.Hex[3][0]~feeder_combout\ : std_logic;
SIGNAL \DUT|R.Hex[3][0]~q\ : std_logic;
SIGNAL \DUT|R.Hex[3][1]~feeder_combout\ : std_logic;
SIGNAL \DUT|R.Hex[3][1]~q\ : std_logic;
SIGNAL \DUT|R.Hex[3][2]~feeder_combout\ : std_logic;
SIGNAL \DUT|R.Hex[3][2]~q\ : std_logic;
SIGNAL \DUT|R.Hex[3][3]~feeder_combout\ : std_logic;
SIGNAL \DUT|R.Hex[3][3]~q\ : std_logic;
SIGNAL \DUT|R.Hex[3][4]~feeder_combout\ : std_logic;
SIGNAL \DUT|R.Hex[3][4]~q\ : std_logic;
SIGNAL \DUT|R.Hex[3][5]~feeder_combout\ : std_logic;
SIGNAL \DUT|R.Hex[3][5]~q\ : std_logic;
SIGNAL \DUT|R.Hex[4][0]~feeder_combout\ : std_logic;
SIGNAL \DUT|R.Hex[4][0]~q\ : std_logic;
SIGNAL \DUT|R.Hex[4][1]~feeder_combout\ : std_logic;
SIGNAL \DUT|R.Hex[4][1]~q\ : std_logic;
SIGNAL \DUT|R.Hex[4][2]~feeder_combout\ : std_logic;
SIGNAL \DUT|R.Hex[4][2]~q\ : std_logic;
SIGNAL \DUT|R.Hex[4][3]~feeder_combout\ : std_logic;
SIGNAL \DUT|R.Hex[4][3]~q\ : std_logic;
SIGNAL \DUT|R.Hex[4][4]~feeder_combout\ : std_logic;
SIGNAL \DUT|R.Hex[4][4]~q\ : std_logic;
SIGNAL \DUT|R.Hex[4][5]~feeder_combout\ : std_logic;
SIGNAL \DUT|R.Hex[4][5]~q\ : std_logic;
SIGNAL \DUT|R.Hex[5][0]~feeder_combout\ : std_logic;
SIGNAL \DUT|R.Hex[5][0]~q\ : std_logic;
SIGNAL \DUT|R.Hex[5][1]~feeder_combout\ : std_logic;
SIGNAL \DUT|R.Hex[5][1]~q\ : std_logic;
SIGNAL \DUT|R.Hex[5][2]~feeder_combout\ : std_logic;
SIGNAL \DUT|R.Hex[5][2]~q\ : std_logic;
SIGNAL \DUT|R.Hex[5][3]~feeder_combout\ : std_logic;
SIGNAL \DUT|R.Hex[5][3]~q\ : std_logic;
SIGNAL \DUT|R.Hex[5][4]~feeder_combout\ : std_logic;
SIGNAL \DUT|R.Hex[5][4]~q\ : std_logic;
SIGNAL \DUT|R.Hex[5][5]~feeder_combout\ : std_logic;
SIGNAL \DUT|R.Hex[5][5]~q\ : std_logic;
SIGNAL \DUT|R.StrobeCount\ : std_logic_vector(24 DOWNTO 0);
SIGNAL \DUT|R.Leds\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \DUT|R.Carries\ : std_logic_vector(6 DOWNTO 0);
SIGNAL \DUT|ALT_INV_R.BCDCounter[1][3]~DUPLICATE_q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.BCDCounter[1][1]~DUPLICATE_q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.BCDCounter[1][0]~DUPLICATE_q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.BCDCounter[0][3]~DUPLICATE_q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.BCDCounter[0][1]~DUPLICATE_q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.LEDStates.LightChaserOn~DUPLICATE_q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.StrobeCount[12]~DUPLICATE_q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.StrobeCount[2]~DUPLICATE_q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.StrobeCount[15]~DUPLICATE_q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.StrobeCount[0]~DUPLICATE_q\ : std_logic;
SIGNAL \ALT_INV_SW[0]~input_o\ : std_logic;
SIGNAL \ALT_INV_SW[3]~input_o\ : std_logic;
SIGNAL \DUT|ALT_INV_R.IncCounterDelayed~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.IncCounter~q\ : std_logic;
SIGNAL \DUT|ALT_INV_Equal1~0_combout\ : std_logic;
SIGNAL \DUT|ALT_INV_R.BCDCounter[0][3]~0_combout\ : std_logic;
SIGNAL \DUT|ALT_INV_R.Carries\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \DUT|ALT_INV_Equal0~4_combout\ : std_logic;
SIGNAL \DUT|ALT_INV_Equal0~3_combout\ : std_logic;
SIGNAL \DUT|ALT_INV_Equal0~2_combout\ : std_logic;
SIGNAL \DUT|ALT_INV_Equal0~1_combout\ : std_logic;
SIGNAL \DUT|ALT_INV_Equal0~0_combout\ : std_logic;
SIGNAL \DUT|ALT_INV_R.LightChaserOnDelayed~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.LightChaserOn~q\ : std_logic;
SIGNAL \DUT|ALT_INV_NextR.LEDStates.LightChaserOn~0_combout\ : std_logic;
SIGNAL \DUT|ALT_INV_R.LedsOffDelayed~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.LedsOff~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.LedsOnDelayed~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.LedsOn~q\ : std_logic;
SIGNAL \DUT|ALT_INV_NextR.LEDStates.AllLedsOn~0_combout\ : std_logic;
SIGNAL \DUT|ALT_INV_R.BCDCounter[1][3]~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.BCDCounter[1][2]~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.BCDCounter[1][1]~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.BCDCounter[1][0]~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.BCDCounter[0][3]~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.BCDCounter[0][2]~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.BCDCounter[0][1]~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.BCDCounter[0][0]~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.LEDStates.LightChaserOn~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.Strobe~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.LEDStates.AllLedsOn~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.LEDStates.LightChaserRunning~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.Hex[5][5]~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.Hex[5][4]~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.Hex[5][3]~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.Hex[5][2]~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.Hex[5][1]~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.Hex[5][0]~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.Hex[4][5]~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.Hex[4][4]~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.Hex[4][3]~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.Hex[4][2]~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.Hex[4][1]~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.Hex[4][0]~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.Hex[3][5]~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.Hex[3][4]~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.Hex[3][3]~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.Hex[3][2]~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.Hex[3][1]~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.Hex[3][0]~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.Hex[2][5]~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.Hex[2][4]~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.Hex[2][3]~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.Hex[2][2]~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.Hex[2][1]~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.Hex[2][0]~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.Hex[1][6]~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.Hex[1][5]~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.Hex[1][4]~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.Hex[1][3]~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.Hex[1][2]~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.Hex[1][1]~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.Hex[1][0]~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.Hex[0][6]~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.Hex[0][5]~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.Hex[0][4]~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.Hex[0][3]~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.Hex[0][2]~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.Hex[0][1]~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.Hex[0][0]~q\ : std_logic;
SIGNAL \DUT|ALT_INV_R.Leds\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \DUT|ALT_INV_Add0~21_sumout\ : std_logic;
SIGNAL \DUT|ALT_INV_R.StrobeCount\ : std_logic_vector(24 DOWNTO 0);

BEGIN

ww_iClk <= iClk;
ww_inResetAsync <= inResetAsync;
ww_SW <= IEEE.STD_LOGIC_1164.TO_STDLOGICVECTOR(SW);
LEDR <= IEEE.STD_LOGIC_1164.TO_STDULOGICVECTOR(ww_LEDR);
HEX0 <= IEEE.STD_LOGIC_1164.TO_STDULOGICVECTOR(ww_HEX0);
HEX1 <= IEEE.STD_LOGIC_1164.TO_STDULOGICVECTOR(ww_HEX1);
HEX2 <= IEEE.STD_LOGIC_1164.TO_STDULOGICVECTOR(ww_HEX2);
HEX3 <= IEEE.STD_LOGIC_1164.TO_STDULOGICVECTOR(ww_HEX3);
HEX4 <= IEEE.STD_LOGIC_1164.TO_STDULOGICVECTOR(ww_HEX4);
HEX5 <= IEEE.STD_LOGIC_1164.TO_STDULOGICVECTOR(ww_HEX5);
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
\DUT|ALT_INV_R.BCDCounter[1][3]~DUPLICATE_q\ <= NOT \DUT|R.BCDCounter[1][3]~DUPLICATE_q\;
\DUT|ALT_INV_R.BCDCounter[1][1]~DUPLICATE_q\ <= NOT \DUT|R.BCDCounter[1][1]~DUPLICATE_q\;
\DUT|ALT_INV_R.BCDCounter[1][0]~DUPLICATE_q\ <= NOT \DUT|R.BCDCounter[1][0]~DUPLICATE_q\;
\DUT|ALT_INV_R.BCDCounter[0][3]~DUPLICATE_q\ <= NOT \DUT|R.BCDCounter[0][3]~DUPLICATE_q\;
\DUT|ALT_INV_R.BCDCounter[0][1]~DUPLICATE_q\ <= NOT \DUT|R.BCDCounter[0][1]~DUPLICATE_q\;
\DUT|ALT_INV_R.LEDStates.LightChaserOn~DUPLICATE_q\ <= NOT \DUT|R.LEDStates.LightChaserOn~DUPLICATE_q\;
\DUT|ALT_INV_R.StrobeCount[12]~DUPLICATE_q\ <= NOT \DUT|R.StrobeCount[12]~DUPLICATE_q\;
\DUT|ALT_INV_R.StrobeCount[2]~DUPLICATE_q\ <= NOT \DUT|R.StrobeCount[2]~DUPLICATE_q\;
\DUT|ALT_INV_R.StrobeCount[15]~DUPLICATE_q\ <= NOT \DUT|R.StrobeCount[15]~DUPLICATE_q\;
\DUT|ALT_INV_R.StrobeCount[0]~DUPLICATE_q\ <= NOT \DUT|R.StrobeCount[0]~DUPLICATE_q\;
\ALT_INV_SW[0]~input_o\ <= NOT \SW[0]~input_o\;
\ALT_INV_SW[3]~input_o\ <= NOT \SW[3]~input_o\;
\DUT|ALT_INV_R.IncCounterDelayed~q\ <= NOT \DUT|R.IncCounterDelayed~q\;
\DUT|ALT_INV_R.IncCounter~q\ <= NOT \DUT|R.IncCounter~q\;
\DUT|ALT_INV_Equal1~0_combout\ <= NOT \DUT|Equal1~0_combout\;
\DUT|ALT_INV_R.BCDCounter[0][3]~0_combout\ <= NOT \DUT|R.BCDCounter[0][3]~0_combout\;
\DUT|ALT_INV_R.Carries\(0) <= NOT \DUT|R.Carries\(0);
\DUT|ALT_INV_Equal0~4_combout\ <= NOT \DUT|Equal0~4_combout\;
\DUT|ALT_INV_Equal0~3_combout\ <= NOT \DUT|Equal0~3_combout\;
\DUT|ALT_INV_Equal0~2_combout\ <= NOT \DUT|Equal0~2_combout\;
\DUT|ALT_INV_Equal0~1_combout\ <= NOT \DUT|Equal0~1_combout\;
\DUT|ALT_INV_Equal0~0_combout\ <= NOT \DUT|Equal0~0_combout\;
\DUT|ALT_INV_R.LightChaserOnDelayed~q\ <= NOT \DUT|R.LightChaserOnDelayed~q\;
\DUT|ALT_INV_R.LightChaserOn~q\ <= NOT \DUT|R.LightChaserOn~q\;
\DUT|ALT_INV_NextR.LEDStates.LightChaserOn~0_combout\ <= NOT \DUT|NextR.LEDStates.LightChaserOn~0_combout\;
\DUT|ALT_INV_R.LedsOffDelayed~q\ <= NOT \DUT|R.LedsOffDelayed~q\;
\DUT|ALT_INV_R.LedsOff~q\ <= NOT \DUT|R.LedsOff~q\;
\DUT|ALT_INV_R.LedsOnDelayed~q\ <= NOT \DUT|R.LedsOnDelayed~q\;
\DUT|ALT_INV_R.LedsOn~q\ <= NOT \DUT|R.LedsOn~q\;
\DUT|ALT_INV_NextR.LEDStates.AllLedsOn~0_combout\ <= NOT \DUT|NextR.LEDStates.AllLedsOn~0_combout\;
\DUT|ALT_INV_R.BCDCounter[1][3]~q\ <= NOT \DUT|R.BCDCounter[1][3]~q\;
\DUT|ALT_INV_R.BCDCounter[1][2]~q\ <= NOT \DUT|R.BCDCounter[1][2]~q\;
\DUT|ALT_INV_R.BCDCounter[1][1]~q\ <= NOT \DUT|R.BCDCounter[1][1]~q\;
\DUT|ALT_INV_R.BCDCounter[1][0]~q\ <= NOT \DUT|R.BCDCounter[1][0]~q\;
\DUT|ALT_INV_R.BCDCounter[0][3]~q\ <= NOT \DUT|R.BCDCounter[0][3]~q\;
\DUT|ALT_INV_R.BCDCounter[0][2]~q\ <= NOT \DUT|R.BCDCounter[0][2]~q\;
\DUT|ALT_INV_R.BCDCounter[0][1]~q\ <= NOT \DUT|R.BCDCounter[0][1]~q\;
\DUT|ALT_INV_R.BCDCounter[0][0]~q\ <= NOT \DUT|R.BCDCounter[0][0]~q\;
\DUT|ALT_INV_R.LEDStates.LightChaserOn~q\ <= NOT \DUT|R.LEDStates.LightChaserOn~q\;
\DUT|ALT_INV_R.Strobe~q\ <= NOT \DUT|R.Strobe~q\;
\DUT|ALT_INV_R.LEDStates.AllLedsOn~q\ <= NOT \DUT|R.LEDStates.AllLedsOn~q\;
\DUT|ALT_INV_R.LEDStates.LightChaserRunning~q\ <= NOT \DUT|R.LEDStates.LightChaserRunning~q\;
\DUT|ALT_INV_R.Hex[5][5]~q\ <= NOT \DUT|R.Hex[5][5]~q\;
\DUT|ALT_INV_R.Hex[5][4]~q\ <= NOT \DUT|R.Hex[5][4]~q\;
\DUT|ALT_INV_R.Hex[5][3]~q\ <= NOT \DUT|R.Hex[5][3]~q\;
\DUT|ALT_INV_R.Hex[5][2]~q\ <= NOT \DUT|R.Hex[5][2]~q\;
\DUT|ALT_INV_R.Hex[5][1]~q\ <= NOT \DUT|R.Hex[5][1]~q\;
\DUT|ALT_INV_R.Hex[5][0]~q\ <= NOT \DUT|R.Hex[5][0]~q\;
\DUT|ALT_INV_R.Hex[4][5]~q\ <= NOT \DUT|R.Hex[4][5]~q\;
\DUT|ALT_INV_R.Hex[4][4]~q\ <= NOT \DUT|R.Hex[4][4]~q\;
\DUT|ALT_INV_R.Hex[4][3]~q\ <= NOT \DUT|R.Hex[4][3]~q\;
\DUT|ALT_INV_R.Hex[4][2]~q\ <= NOT \DUT|R.Hex[4][2]~q\;
\DUT|ALT_INV_R.Hex[4][1]~q\ <= NOT \DUT|R.Hex[4][1]~q\;
\DUT|ALT_INV_R.Hex[4][0]~q\ <= NOT \DUT|R.Hex[4][0]~q\;
\DUT|ALT_INV_R.Hex[3][5]~q\ <= NOT \DUT|R.Hex[3][5]~q\;
\DUT|ALT_INV_R.Hex[3][4]~q\ <= NOT \DUT|R.Hex[3][4]~q\;
\DUT|ALT_INV_R.Hex[3][3]~q\ <= NOT \DUT|R.Hex[3][3]~q\;
\DUT|ALT_INV_R.Hex[3][2]~q\ <= NOT \DUT|R.Hex[3][2]~q\;
\DUT|ALT_INV_R.Hex[3][1]~q\ <= NOT \DUT|R.Hex[3][1]~q\;
\DUT|ALT_INV_R.Hex[3][0]~q\ <= NOT \DUT|R.Hex[3][0]~q\;
\DUT|ALT_INV_R.Hex[2][5]~q\ <= NOT \DUT|R.Hex[2][5]~q\;
\DUT|ALT_INV_R.Hex[2][4]~q\ <= NOT \DUT|R.Hex[2][4]~q\;
\DUT|ALT_INV_R.Hex[2][3]~q\ <= NOT \DUT|R.Hex[2][3]~q\;
\DUT|ALT_INV_R.Hex[2][2]~q\ <= NOT \DUT|R.Hex[2][2]~q\;
\DUT|ALT_INV_R.Hex[2][1]~q\ <= NOT \DUT|R.Hex[2][1]~q\;
\DUT|ALT_INV_R.Hex[2][0]~q\ <= NOT \DUT|R.Hex[2][0]~q\;
\DUT|ALT_INV_R.Hex[1][6]~q\ <= NOT \DUT|R.Hex[1][6]~q\;
\DUT|ALT_INV_R.Hex[1][5]~q\ <= NOT \DUT|R.Hex[1][5]~q\;
\DUT|ALT_INV_R.Hex[1][4]~q\ <= NOT \DUT|R.Hex[1][4]~q\;
\DUT|ALT_INV_R.Hex[1][3]~q\ <= NOT \DUT|R.Hex[1][3]~q\;
\DUT|ALT_INV_R.Hex[1][2]~q\ <= NOT \DUT|R.Hex[1][2]~q\;
\DUT|ALT_INV_R.Hex[1][1]~q\ <= NOT \DUT|R.Hex[1][1]~q\;
\DUT|ALT_INV_R.Hex[1][0]~q\ <= NOT \DUT|R.Hex[1][0]~q\;
\DUT|ALT_INV_R.Hex[0][6]~q\ <= NOT \DUT|R.Hex[0][6]~q\;
\DUT|ALT_INV_R.Hex[0][5]~q\ <= NOT \DUT|R.Hex[0][5]~q\;
\DUT|ALT_INV_R.Hex[0][4]~q\ <= NOT \DUT|R.Hex[0][4]~q\;
\DUT|ALT_INV_R.Hex[0][3]~q\ <= NOT \DUT|R.Hex[0][3]~q\;
\DUT|ALT_INV_R.Hex[0][2]~q\ <= NOT \DUT|R.Hex[0][2]~q\;
\DUT|ALT_INV_R.Hex[0][1]~q\ <= NOT \DUT|R.Hex[0][1]~q\;
\DUT|ALT_INV_R.Hex[0][0]~q\ <= NOT \DUT|R.Hex[0][0]~q\;
\DUT|ALT_INV_R.Leds\(9) <= NOT \DUT|R.Leds\(9);
\DUT|ALT_INV_R.Leds\(8) <= NOT \DUT|R.Leds\(8);
\DUT|ALT_INV_R.Leds\(7) <= NOT \DUT|R.Leds\(7);
\DUT|ALT_INV_R.Leds\(6) <= NOT \DUT|R.Leds\(6);
\DUT|ALT_INV_R.Leds\(5) <= NOT \DUT|R.Leds\(5);
\DUT|ALT_INV_R.Leds\(4) <= NOT \DUT|R.Leds\(4);
\DUT|ALT_INV_R.Leds\(3) <= NOT \DUT|R.Leds\(3);
\DUT|ALT_INV_R.Leds\(2) <= NOT \DUT|R.Leds\(2);
\DUT|ALT_INV_R.Leds\(1) <= NOT \DUT|R.Leds\(1);
\DUT|ALT_INV_R.Leds\(0) <= NOT \DUT|R.Leds\(0);
\DUT|ALT_INV_Add0~21_sumout\ <= NOT \DUT|Add0~21_sumout\;
\DUT|ALT_INV_R.StrobeCount\(9) <= NOT \DUT|R.StrobeCount\(9);
\DUT|ALT_INV_R.StrobeCount\(10) <= NOT \DUT|R.StrobeCount\(10);
\DUT|ALT_INV_R.StrobeCount\(11) <= NOT \DUT|R.StrobeCount\(11);
\DUT|ALT_INV_R.StrobeCount\(12) <= NOT \DUT|R.StrobeCount\(12);
\DUT|ALT_INV_R.StrobeCount\(13) <= NOT \DUT|R.StrobeCount\(13);
\DUT|ALT_INV_R.StrobeCount\(18) <= NOT \DUT|R.StrobeCount\(18);
\DUT|ALT_INV_R.StrobeCount\(19) <= NOT \DUT|R.StrobeCount\(19);
\DUT|ALT_INV_R.StrobeCount\(20) <= NOT \DUT|R.StrobeCount\(20);
\DUT|ALT_INV_R.StrobeCount\(21) <= NOT \DUT|R.StrobeCount\(21);
\DUT|ALT_INV_R.StrobeCount\(22) <= NOT \DUT|R.StrobeCount\(22);
\DUT|ALT_INV_R.StrobeCount\(23) <= NOT \DUT|R.StrobeCount\(23);
\DUT|ALT_INV_R.StrobeCount\(24) <= NOT \DUT|R.StrobeCount\(24);
\DUT|ALT_INV_R.StrobeCount\(1) <= NOT \DUT|R.StrobeCount\(1);
\DUT|ALT_INV_R.StrobeCount\(2) <= NOT \DUT|R.StrobeCount\(2);
\DUT|ALT_INV_R.StrobeCount\(3) <= NOT \DUT|R.StrobeCount\(3);
\DUT|ALT_INV_R.StrobeCount\(4) <= NOT \DUT|R.StrobeCount\(4);
\DUT|ALT_INV_R.StrobeCount\(5) <= NOT \DUT|R.StrobeCount\(5);
\DUT|ALT_INV_R.StrobeCount\(6) <= NOT \DUT|R.StrobeCount\(6);
\DUT|ALT_INV_R.StrobeCount\(7) <= NOT \DUT|R.StrobeCount\(7);
\DUT|ALT_INV_R.StrobeCount\(16) <= NOT \DUT|R.StrobeCount\(16);
\DUT|ALT_INV_R.StrobeCount\(15) <= NOT \DUT|R.StrobeCount\(15);
\DUT|ALT_INV_R.StrobeCount\(14) <= NOT \DUT|R.StrobeCount\(14);
\DUT|ALT_INV_R.StrobeCount\(17) <= NOT \DUT|R.StrobeCount\(17);
\DUT|ALT_INV_R.StrobeCount\(8) <= NOT \DUT|R.StrobeCount\(8);
\DUT|ALT_INV_R.StrobeCount\(0) <= NOT \DUT|R.StrobeCount\(0);

-- Location: IOOBUF_X52_Y0_N2
\LEDR[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \DUT|R.Leds\(0),
	devoe => ww_devoe,
	o => ww_LEDR(0));

-- Location: IOOBUF_X52_Y0_N19
\LEDR[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \DUT|R.Leds\(1),
	devoe => ww_devoe,
	o => ww_LEDR(1));

-- Location: IOOBUF_X60_Y0_N2
\LEDR[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \DUT|R.Leds\(2),
	devoe => ww_devoe,
	o => ww_LEDR(2));

-- Location: IOOBUF_X80_Y0_N2
\LEDR[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \DUT|R.Leds\(3),
	devoe => ww_devoe,
	o => ww_LEDR(3));

-- Location: IOOBUF_X60_Y0_N19
\LEDR[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \DUT|R.Leds\(4),
	devoe => ww_devoe,
	o => ww_LEDR(4));

-- Location: IOOBUF_X80_Y0_N19
\LEDR[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \DUT|R.Leds\(5),
	devoe => ww_devoe,
	o => ww_LEDR(5));

-- Location: IOOBUF_X84_Y0_N2
\LEDR[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \DUT|R.Leds\(6),
	devoe => ww_devoe,
	o => ww_LEDR(6));

-- Location: IOOBUF_X89_Y6_N5
\LEDR[7]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \DUT|R.Leds\(7),
	devoe => ww_devoe,
	o => ww_LEDR(7));

-- Location: IOOBUF_X89_Y8_N5
\LEDR[8]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \DUT|R.Leds\(8),
	devoe => ww_devoe,
	o => ww_LEDR(8));

-- Location: IOOBUF_X89_Y6_N22
\LEDR[9]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \DUT|R.Leds\(9),
	devoe => ww_devoe,
	o => ww_LEDR(9));

-- Location: IOOBUF_X89_Y8_N39
\HEX0[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \DUT|ALT_INV_R.Hex[0][0]~q\,
	devoe => ww_devoe,
	o => ww_HEX0(0));

-- Location: IOOBUF_X89_Y11_N79
\HEX0[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \DUT|ALT_INV_R.Hex[0][1]~q\,
	devoe => ww_devoe,
	o => ww_HEX0(1));

-- Location: IOOBUF_X89_Y11_N96
\HEX0[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \DUT|ALT_INV_R.Hex[0][2]~q\,
	devoe => ww_devoe,
	o => ww_HEX0(2));

-- Location: IOOBUF_X89_Y4_N79
\HEX0[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \DUT|ALT_INV_R.Hex[0][3]~q\,
	devoe => ww_devoe,
	o => ww_HEX0(3));

-- Location: IOOBUF_X89_Y13_N56
\HEX0[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \DUT|ALT_INV_R.Hex[0][4]~q\,
	devoe => ww_devoe,
	o => ww_HEX0(4));

-- Location: IOOBUF_X89_Y13_N39
\HEX0[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \DUT|ALT_INV_R.Hex[0][5]~q\,
	devoe => ww_devoe,
	o => ww_HEX0(5));

-- Location: IOOBUF_X89_Y4_N96
\HEX0[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \DUT|ALT_INV_R.Hex[0][6]~q\,
	devoe => ww_devoe,
	o => ww_HEX0(6));

-- Location: IOOBUF_X89_Y6_N39
\HEX1[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \DUT|ALT_INV_R.Hex[1][0]~q\,
	devoe => ww_devoe,
	o => ww_HEX1(0));

-- Location: IOOBUF_X89_Y6_N56
\HEX1[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \DUT|ALT_INV_R.Hex[1][1]~q\,
	devoe => ww_devoe,
	o => ww_HEX1(1));

-- Location: IOOBUF_X89_Y16_N39
\HEX1[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \DUT|ALT_INV_R.Hex[1][2]~q\,
	devoe => ww_devoe,
	o => ww_HEX1(2));

-- Location: IOOBUF_X89_Y16_N56
\HEX1[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \DUT|ALT_INV_R.Hex[1][3]~q\,
	devoe => ww_devoe,
	o => ww_HEX1(3));

-- Location: IOOBUF_X89_Y15_N39
\HEX1[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \DUT|ALT_INV_R.Hex[1][4]~q\,
	devoe => ww_devoe,
	o => ww_HEX1(4));

-- Location: IOOBUF_X89_Y15_N56
\HEX1[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \DUT|ALT_INV_R.Hex[1][5]~q\,
	devoe => ww_devoe,
	o => ww_HEX1(5));

-- Location: IOOBUF_X89_Y8_N56
\HEX1[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \DUT|ALT_INV_R.Hex[1][6]~q\,
	devoe => ww_devoe,
	o => ww_HEX1(6));

-- Location: IOOBUF_X89_Y9_N22
\HEX2[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \DUT|ALT_INV_R.Hex[2][0]~q\,
	devoe => ww_devoe,
	o => ww_HEX2(0));

-- Location: IOOBUF_X89_Y23_N39
\HEX2[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \DUT|ALT_INV_R.Hex[2][1]~q\,
	devoe => ww_devoe,
	o => ww_HEX2(1));

-- Location: IOOBUF_X89_Y23_N56
\HEX2[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \DUT|ALT_INV_R.Hex[2][2]~q\,
	devoe => ww_devoe,
	o => ww_HEX2(2));

-- Location: IOOBUF_X89_Y20_N79
\HEX2[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \DUT|ALT_INV_R.Hex[2][3]~q\,
	devoe => ww_devoe,
	o => ww_HEX2(3));

-- Location: IOOBUF_X89_Y25_N39
\HEX2[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \DUT|ALT_INV_R.Hex[2][4]~q\,
	devoe => ww_devoe,
	o => ww_HEX2(4));

-- Location: IOOBUF_X89_Y20_N96
\HEX2[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \DUT|ALT_INV_R.Hex[2][5]~q\,
	devoe => ww_devoe,
	o => ww_HEX2(5));

-- Location: IOOBUF_X89_Y25_N56
\HEX2[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_HEX2(6));

-- Location: IOOBUF_X89_Y16_N5
\HEX3[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \DUT|ALT_INV_R.Hex[3][0]~q\,
	devoe => ww_devoe,
	o => ww_HEX3(0));

-- Location: IOOBUF_X89_Y16_N22
\HEX3[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \DUT|ALT_INV_R.Hex[3][1]~q\,
	devoe => ww_devoe,
	o => ww_HEX3(1));

-- Location: IOOBUF_X89_Y4_N45
\HEX3[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \DUT|ALT_INV_R.Hex[3][2]~q\,
	devoe => ww_devoe,
	o => ww_HEX3(2));

-- Location: IOOBUF_X89_Y4_N62
\HEX3[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \DUT|ALT_INV_R.Hex[3][3]~q\,
	devoe => ww_devoe,
	o => ww_HEX3(3));

-- Location: IOOBUF_X89_Y21_N39
\HEX3[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \DUT|ALT_INV_R.Hex[3][4]~q\,
	devoe => ww_devoe,
	o => ww_HEX3(4));

-- Location: IOOBUF_X89_Y11_N62
\HEX3[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \DUT|ALT_INV_R.Hex[3][5]~q\,
	devoe => ww_devoe,
	o => ww_HEX3(5));

-- Location: IOOBUF_X89_Y9_N5
\HEX3[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_HEX3(6));

-- Location: IOOBUF_X89_Y11_N45
\HEX4[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \DUT|ALT_INV_R.Hex[4][0]~q\,
	devoe => ww_devoe,
	o => ww_HEX4(0));

-- Location: IOOBUF_X89_Y13_N5
\HEX4[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \DUT|ALT_INV_R.Hex[4][1]~q\,
	devoe => ww_devoe,
	o => ww_HEX4(1));

-- Location: IOOBUF_X89_Y13_N22
\HEX4[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \DUT|ALT_INV_R.Hex[4][2]~q\,
	devoe => ww_devoe,
	o => ww_HEX4(2));

-- Location: IOOBUF_X89_Y8_N22
\HEX4[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \DUT|ALT_INV_R.Hex[4][3]~q\,
	devoe => ww_devoe,
	o => ww_HEX4(3));

-- Location: IOOBUF_X89_Y15_N22
\HEX4[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \DUT|ALT_INV_R.Hex[4][4]~q\,
	devoe => ww_devoe,
	o => ww_HEX4(4));

-- Location: IOOBUF_X89_Y15_N5
\HEX4[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \DUT|ALT_INV_R.Hex[4][5]~q\,
	devoe => ww_devoe,
	o => ww_HEX4(5));

-- Location: IOOBUF_X89_Y20_N45
\HEX4[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_HEX4(6));

-- Location: IOOBUF_X89_Y20_N62
\HEX5[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \DUT|ALT_INV_R.Hex[5][0]~q\,
	devoe => ww_devoe,
	o => ww_HEX5(0));

-- Location: IOOBUF_X89_Y21_N56
\HEX5[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \DUT|ALT_INV_R.Hex[5][1]~q\,
	devoe => ww_devoe,
	o => ww_HEX5(1));

-- Location: IOOBUF_X89_Y25_N22
\HEX5[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \DUT|ALT_INV_R.Hex[5][2]~q\,
	devoe => ww_devoe,
	o => ww_HEX5(2));

-- Location: IOOBUF_X89_Y23_N22
\HEX5[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \DUT|ALT_INV_R.Hex[5][3]~q\,
	devoe => ww_devoe,
	o => ww_HEX5(3));

-- Location: IOOBUF_X89_Y9_N56
\HEX5[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \DUT|ALT_INV_R.Hex[5][4]~q\,
	devoe => ww_devoe,
	o => ww_HEX5(4));

-- Location: IOOBUF_X89_Y23_N5
\HEX5[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \DUT|ALT_INV_R.Hex[5][5]~q\,
	devoe => ww_devoe,
	o => ww_HEX5(5));

-- Location: IOOBUF_X89_Y9_N39
\HEX5[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_HEX5(6));

-- Location: IOIBUF_X32_Y0_N1
\iClk~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_iClk,
	o => \iClk~input_o\);

-- Location: CLKCTRL_G6
\iClk~inputCLKENA0\ : cyclonev_clkena
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	disable_mode => "low",
	ena_register_mode => "always enabled",
	ena_register_power_up => "high",
	test_syn => "high")
-- pragma translate_on
PORT MAP (
	inclk => \iClk~input_o\,
	outclk => \iClk~inputCLKENA0_outclk\);

-- Location: LABCELL_X81_Y2_N30
\DUT|Add0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Add0~1_sumout\ = SUM(( \DUT|R.StrobeCount\(0) ) + ( VCC ) + ( !VCC ))
-- \DUT|Add0~2\ = CARRY(( \DUT|R.StrobeCount\(0) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \DUT|ALT_INV_R.StrobeCount\(0),
	cin => GND,
	sumout => \DUT|Add0~1_sumout\,
	cout => \DUT|Add0~2\);

-- Location: IOIBUF_X2_Y0_N58
\inResetAsync~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_inResetAsync,
	o => \inResetAsync~input_o\);

-- Location: FF_X81_Y2_N31
\DUT|R.StrobeCount[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Add0~1_sumout\,
	clrn => \inResetAsync~input_o\,
	sclr => \DUT|Equal0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.StrobeCount\(0));

-- Location: LABCELL_X81_Y2_N33
\DUT|Add0~49\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Add0~49_sumout\ = SUM(( \DUT|R.StrobeCount\(1) ) + ( GND ) + ( \DUT|Add0~2\ ))
-- \DUT|Add0~50\ = CARRY(( \DUT|R.StrobeCount\(1) ) + ( GND ) + ( \DUT|Add0~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \DUT|ALT_INV_R.StrobeCount\(1),
	cin => \DUT|Add0~2\,
	sumout => \DUT|Add0~49_sumout\,
	cout => \DUT|Add0~50\);

-- Location: FF_X81_Y2_N35
\DUT|R.StrobeCount[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Add0~49_sumout\,
	clrn => \inResetAsync~input_o\,
	sclr => \DUT|Equal0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.StrobeCount\(1));

-- Location: LABCELL_X81_Y2_N36
\DUT|Add0~45\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Add0~45_sumout\ = SUM(( \DUT|R.StrobeCount\(2) ) + ( GND ) + ( \DUT|Add0~50\ ))
-- \DUT|Add0~46\ = CARRY(( \DUT|R.StrobeCount\(2) ) + ( GND ) + ( \DUT|Add0~50\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \DUT|ALT_INV_R.StrobeCount\(2),
	cin => \DUT|Add0~50\,
	sumout => \DUT|Add0~45_sumout\,
	cout => \DUT|Add0~46\);

-- Location: FF_X81_Y2_N38
\DUT|R.StrobeCount[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Add0~45_sumout\,
	clrn => \inResetAsync~input_o\,
	sclr => \DUT|Equal0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.StrobeCount\(2));

-- Location: LABCELL_X81_Y2_N39
\DUT|Add0~41\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Add0~41_sumout\ = SUM(( \DUT|R.StrobeCount\(3) ) + ( GND ) + ( \DUT|Add0~46\ ))
-- \DUT|Add0~42\ = CARRY(( \DUT|R.StrobeCount\(3) ) + ( GND ) + ( \DUT|Add0~46\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \DUT|ALT_INV_R.StrobeCount\(3),
	cin => \DUT|Add0~46\,
	sumout => \DUT|Add0~41_sumout\,
	cout => \DUT|Add0~42\);

-- Location: FF_X81_Y2_N41
\DUT|R.StrobeCount[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Add0~41_sumout\,
	clrn => \inResetAsync~input_o\,
	sclr => \DUT|Equal0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.StrobeCount\(3));

-- Location: LABCELL_X81_Y2_N42
\DUT|Add0~37\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Add0~37_sumout\ = SUM(( \DUT|R.StrobeCount\(4) ) + ( GND ) + ( \DUT|Add0~42\ ))
-- \DUT|Add0~38\ = CARRY(( \DUT|R.StrobeCount\(4) ) + ( GND ) + ( \DUT|Add0~42\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \DUT|ALT_INV_R.StrobeCount\(4),
	cin => \DUT|Add0~42\,
	sumout => \DUT|Add0~37_sumout\,
	cout => \DUT|Add0~38\);

-- Location: FF_X81_Y2_N43
\DUT|R.StrobeCount[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Add0~37_sumout\,
	clrn => \inResetAsync~input_o\,
	sclr => \DUT|Equal0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.StrobeCount\(4));

-- Location: LABCELL_X81_Y2_N45
\DUT|Add0~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Add0~33_sumout\ = SUM(( \DUT|R.StrobeCount\(5) ) + ( GND ) + ( \DUT|Add0~38\ ))
-- \DUT|Add0~34\ = CARRY(( \DUT|R.StrobeCount\(5) ) + ( GND ) + ( \DUT|Add0~38\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \DUT|ALT_INV_R.StrobeCount\(5),
	cin => \DUT|Add0~38\,
	sumout => \DUT|Add0~33_sumout\,
	cout => \DUT|Add0~34\);

-- Location: FF_X81_Y2_N47
\DUT|R.StrobeCount[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Add0~33_sumout\,
	clrn => \inResetAsync~input_o\,
	sclr => \DUT|Equal0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.StrobeCount\(5));

-- Location: LABCELL_X81_Y2_N48
\DUT|Add0~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Add0~29_sumout\ = SUM(( \DUT|R.StrobeCount\(6) ) + ( GND ) + ( \DUT|Add0~34\ ))
-- \DUT|Add0~30\ = CARRY(( \DUT|R.StrobeCount\(6) ) + ( GND ) + ( \DUT|Add0~34\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \DUT|ALT_INV_R.StrobeCount\(6),
	cin => \DUT|Add0~34\,
	sumout => \DUT|Add0~29_sumout\,
	cout => \DUT|Add0~30\);

-- Location: FF_X81_Y2_N50
\DUT|R.StrobeCount[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Add0~29_sumout\,
	clrn => \inResetAsync~input_o\,
	sclr => \DUT|Equal0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.StrobeCount\(6));

-- Location: LABCELL_X81_Y2_N51
\DUT|Add0~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Add0~25_sumout\ = SUM(( \DUT|R.StrobeCount\(7) ) + ( GND ) + ( \DUT|Add0~30\ ))
-- \DUT|Add0~26\ = CARRY(( \DUT|R.StrobeCount\(7) ) + ( GND ) + ( \DUT|Add0~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \DUT|ALT_INV_R.StrobeCount\(7),
	cin => \DUT|Add0~30\,
	sumout => \DUT|Add0~25_sumout\,
	cout => \DUT|Add0~26\);

-- Location: FF_X81_Y2_N52
\DUT|R.StrobeCount[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Add0~25_sumout\,
	clrn => \inResetAsync~input_o\,
	sclr => \DUT|Equal0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.StrobeCount\(7));

-- Location: LABCELL_X81_Y2_N54
\DUT|Add0~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Add0~5_sumout\ = SUM(( \DUT|R.StrobeCount\(8) ) + ( GND ) + ( \DUT|Add0~26\ ))
-- \DUT|Add0~6\ = CARRY(( \DUT|R.StrobeCount\(8) ) + ( GND ) + ( \DUT|Add0~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \DUT|ALT_INV_R.StrobeCount\(8),
	cin => \DUT|Add0~26\,
	sumout => \DUT|Add0~5_sumout\,
	cout => \DUT|Add0~6\);

-- Location: FF_X81_Y2_N56
\DUT|R.StrobeCount[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Add0~5_sumout\,
	clrn => \inResetAsync~input_o\,
	sclr => \DUT|Equal0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.StrobeCount\(8));

-- Location: LABCELL_X81_Y2_N57
\DUT|Add0~97\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Add0~97_sumout\ = SUM(( \DUT|R.StrobeCount\(9) ) + ( GND ) + ( \DUT|Add0~6\ ))
-- \DUT|Add0~98\ = CARRY(( \DUT|R.StrobeCount\(9) ) + ( GND ) + ( \DUT|Add0~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \DUT|ALT_INV_R.StrobeCount\(9),
	cin => \DUT|Add0~6\,
	sumout => \DUT|Add0~97_sumout\,
	cout => \DUT|Add0~98\);

-- Location: FF_X81_Y2_N59
\DUT|R.StrobeCount[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Add0~97_sumout\,
	clrn => \inResetAsync~input_o\,
	sclr => \DUT|Equal0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.StrobeCount\(9));

-- Location: LABCELL_X81_Y1_N0
\DUT|Add0~93\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Add0~93_sumout\ = SUM(( \DUT|R.StrobeCount\(10) ) + ( GND ) + ( \DUT|Add0~98\ ))
-- \DUT|Add0~94\ = CARRY(( \DUT|R.StrobeCount\(10) ) + ( GND ) + ( \DUT|Add0~98\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \DUT|ALT_INV_R.StrobeCount\(10),
	cin => \DUT|Add0~98\,
	sumout => \DUT|Add0~93_sumout\,
	cout => \DUT|Add0~94\);

-- Location: FF_X81_Y1_N2
\DUT|R.StrobeCount[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Add0~93_sumout\,
	clrn => \inResetAsync~input_o\,
	sclr => \DUT|Equal0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.StrobeCount\(10));

-- Location: LABCELL_X81_Y1_N3
\DUT|Add0~89\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Add0~89_sumout\ = SUM(( \DUT|R.StrobeCount\(11) ) + ( GND ) + ( \DUT|Add0~94\ ))
-- \DUT|Add0~90\ = CARRY(( \DUT|R.StrobeCount\(11) ) + ( GND ) + ( \DUT|Add0~94\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \DUT|ALT_INV_R.StrobeCount\(11),
	cin => \DUT|Add0~94\,
	sumout => \DUT|Add0~89_sumout\,
	cout => \DUT|Add0~90\);

-- Location: FF_X81_Y1_N5
\DUT|R.StrobeCount[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Add0~89_sumout\,
	clrn => \inResetAsync~input_o\,
	sclr => \DUT|Equal0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.StrobeCount\(11));

-- Location: LABCELL_X81_Y1_N6
\DUT|Add0~85\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Add0~85_sumout\ = SUM(( \DUT|R.StrobeCount[12]~DUPLICATE_q\ ) + ( GND ) + ( \DUT|Add0~90\ ))
-- \DUT|Add0~86\ = CARRY(( \DUT|R.StrobeCount[12]~DUPLICATE_q\ ) + ( GND ) + ( \DUT|Add0~90\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \DUT|ALT_INV_R.StrobeCount[12]~DUPLICATE_q\,
	cin => \DUT|Add0~90\,
	sumout => \DUT|Add0~85_sumout\,
	cout => \DUT|Add0~86\);

-- Location: FF_X81_Y1_N7
\DUT|R.StrobeCount[12]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Add0~85_sumout\,
	clrn => \inResetAsync~input_o\,
	sclr => \DUT|Equal0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.StrobeCount[12]~DUPLICATE_q\);

-- Location: LABCELL_X81_Y1_N9
\DUT|Add0~81\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Add0~81_sumout\ = SUM(( \DUT|R.StrobeCount\(13) ) + ( GND ) + ( \DUT|Add0~86\ ))
-- \DUT|Add0~82\ = CARRY(( \DUT|R.StrobeCount\(13) ) + ( GND ) + ( \DUT|Add0~86\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \DUT|ALT_INV_R.StrobeCount\(13),
	cin => \DUT|Add0~86\,
	sumout => \DUT|Add0~81_sumout\,
	cout => \DUT|Add0~82\);

-- Location: FF_X81_Y1_N11
\DUT|R.StrobeCount[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Add0~81_sumout\,
	clrn => \inResetAsync~input_o\,
	sclr => \DUT|Equal0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.StrobeCount\(13));

-- Location: LABCELL_X81_Y1_N12
\DUT|Add0~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Add0~13_sumout\ = SUM(( \DUT|R.StrobeCount\(14) ) + ( GND ) + ( \DUT|Add0~82\ ))
-- \DUT|Add0~14\ = CARRY(( \DUT|R.StrobeCount\(14) ) + ( GND ) + ( \DUT|Add0~82\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \DUT|ALT_INV_R.StrobeCount\(14),
	cin => \DUT|Add0~82\,
	sumout => \DUT|Add0~13_sumout\,
	cout => \DUT|Add0~14\);

-- Location: FF_X81_Y1_N14
\DUT|R.StrobeCount[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Add0~13_sumout\,
	clrn => \inResetAsync~input_o\,
	sclr => \DUT|Equal0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.StrobeCount\(14));

-- Location: LABCELL_X81_Y1_N15
\DUT|Add0~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Add0~17_sumout\ = SUM(( \DUT|R.StrobeCount[15]~DUPLICATE_q\ ) + ( GND ) + ( \DUT|Add0~14\ ))
-- \DUT|Add0~18\ = CARRY(( \DUT|R.StrobeCount[15]~DUPLICATE_q\ ) + ( GND ) + ( \DUT|Add0~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \DUT|ALT_INV_R.StrobeCount[15]~DUPLICATE_q\,
	cin => \DUT|Add0~14\,
	sumout => \DUT|Add0~17_sumout\,
	cout => \DUT|Add0~18\);

-- Location: FF_X81_Y1_N17
\DUT|R.StrobeCount[15]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Add0~17_sumout\,
	clrn => \inResetAsync~input_o\,
	sclr => \DUT|Equal0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.StrobeCount[15]~DUPLICATE_q\);

-- Location: LABCELL_X81_Y1_N18
\DUT|Add0~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Add0~21_sumout\ = SUM(( \DUT|R.StrobeCount\(16) ) + ( GND ) + ( \DUT|Add0~18\ ))
-- \DUT|Add0~22\ = CARRY(( \DUT|R.StrobeCount\(16) ) + ( GND ) + ( \DUT|Add0~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \DUT|ALT_INV_R.StrobeCount\(16),
	cin => \DUT|Add0~18\,
	sumout => \DUT|Add0~21_sumout\,
	cout => \DUT|Add0~22\);

-- Location: LABCELL_X81_Y2_N3
\DUT|R.StrobeCount[16]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|R.StrobeCount[16]~feeder_combout\ = ( \DUT|Add0~21_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \DUT|ALT_INV_Add0~21_sumout\,
	combout => \DUT|R.StrobeCount[16]~feeder_combout\);

-- Location: FF_X81_Y2_N5
\DUT|R.StrobeCount[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|R.StrobeCount[16]~feeder_combout\,
	clrn => \inResetAsync~input_o\,
	sclr => \DUT|Equal0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.StrobeCount\(16));

-- Location: FF_X81_Y1_N16
\DUT|R.StrobeCount[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Add0~17_sumout\,
	clrn => \inResetAsync~input_o\,
	sclr => \DUT|Equal0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.StrobeCount\(15));

-- Location: LABCELL_X81_Y1_N21
\DUT|Add0~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Add0~9_sumout\ = SUM(( \DUT|R.StrobeCount\(17) ) + ( GND ) + ( \DUT|Add0~22\ ))
-- \DUT|Add0~10\ = CARRY(( \DUT|R.StrobeCount\(17) ) + ( GND ) + ( \DUT|Add0~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \DUT|ALT_INV_R.StrobeCount\(17),
	cin => \DUT|Add0~22\,
	sumout => \DUT|Add0~9_sumout\,
	cout => \DUT|Add0~10\);

-- Location: FF_X81_Y1_N23
\DUT|R.StrobeCount[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Add0~9_sumout\,
	clrn => \inResetAsync~input_o\,
	sclr => \DUT|Equal0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.StrobeCount\(17));

-- Location: LABCELL_X81_Y2_N24
\DUT|Equal0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Equal0~0_combout\ = ( \DUT|R.StrobeCount\(14) & ( !\DUT|R.StrobeCount\(17) & ( (\DUT|R.StrobeCount\(16) & (!\DUT|R.StrobeCount\(7) & (\DUT|R.StrobeCount\(6) & !\DUT|R.StrobeCount\(15)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000001000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \DUT|ALT_INV_R.StrobeCount\(16),
	datab => \DUT|ALT_INV_R.StrobeCount\(7),
	datac => \DUT|ALT_INV_R.StrobeCount\(6),
	datad => \DUT|ALT_INV_R.StrobeCount\(15),
	datae => \DUT|ALT_INV_R.StrobeCount\(14),
	dataf => \DUT|ALT_INV_R.StrobeCount\(17),
	combout => \DUT|Equal0~0_combout\);

-- Location: FF_X81_Y2_N37
\DUT|R.StrobeCount[2]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Add0~45_sumout\,
	clrn => \inResetAsync~input_o\,
	sclr => \DUT|Equal0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.StrobeCount[2]~DUPLICATE_q\);

-- Location: LABCELL_X81_Y2_N9
\DUT|Equal0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Equal0~1_combout\ = ( !\DUT|R.StrobeCount[2]~DUPLICATE_q\ & ( !\DUT|R.StrobeCount\(3) & ( (!\DUT|R.StrobeCount\(4) & (!\DUT|R.StrobeCount\(5) & !\DUT|R.StrobeCount\(1))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \DUT|ALT_INV_R.StrobeCount\(4),
	datac => \DUT|ALT_INV_R.StrobeCount\(5),
	datad => \DUT|ALT_INV_R.StrobeCount\(1),
	datae => \DUT|ALT_INV_R.StrobeCount[2]~DUPLICATE_q\,
	dataf => \DUT|ALT_INV_R.StrobeCount\(3),
	combout => \DUT|Equal0~1_combout\);

-- Location: FF_X81_Y2_N32
\DUT|R.StrobeCount[0]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Add0~1_sumout\,
	clrn => \inResetAsync~input_o\,
	sclr => \DUT|Equal0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.StrobeCount[0]~DUPLICATE_q\);

-- Location: LABCELL_X81_Y1_N24
\DUT|Add0~77\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Add0~77_sumout\ = SUM(( \DUT|R.StrobeCount\(18) ) + ( GND ) + ( \DUT|Add0~10\ ))
-- \DUT|Add0~78\ = CARRY(( \DUT|R.StrobeCount\(18) ) + ( GND ) + ( \DUT|Add0~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \DUT|ALT_INV_R.StrobeCount\(18),
	cin => \DUT|Add0~10\,
	sumout => \DUT|Add0~77_sumout\,
	cout => \DUT|Add0~78\);

-- Location: FF_X81_Y1_N26
\DUT|R.StrobeCount[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Add0~77_sumout\,
	clrn => \inResetAsync~input_o\,
	sclr => \DUT|Equal0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.StrobeCount\(18));

-- Location: LABCELL_X81_Y1_N27
\DUT|Add0~73\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Add0~73_sumout\ = SUM(( \DUT|R.StrobeCount\(19) ) + ( GND ) + ( \DUT|Add0~78\ ))
-- \DUT|Add0~74\ = CARRY(( \DUT|R.StrobeCount\(19) ) + ( GND ) + ( \DUT|Add0~78\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \DUT|ALT_INV_R.StrobeCount\(19),
	cin => \DUT|Add0~78\,
	sumout => \DUT|Add0~73_sumout\,
	cout => \DUT|Add0~74\);

-- Location: FF_X81_Y1_N29
\DUT|R.StrobeCount[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Add0~73_sumout\,
	clrn => \inResetAsync~input_o\,
	sclr => \DUT|Equal0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.StrobeCount\(19));

-- Location: LABCELL_X81_Y1_N30
\DUT|Add0~69\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Add0~69_sumout\ = SUM(( \DUT|R.StrobeCount\(20) ) + ( GND ) + ( \DUT|Add0~74\ ))
-- \DUT|Add0~70\ = CARRY(( \DUT|R.StrobeCount\(20) ) + ( GND ) + ( \DUT|Add0~74\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \DUT|ALT_INV_R.StrobeCount\(20),
	cin => \DUT|Add0~74\,
	sumout => \DUT|Add0~69_sumout\,
	cout => \DUT|Add0~70\);

-- Location: FF_X81_Y1_N32
\DUT|R.StrobeCount[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Add0~69_sumout\,
	clrn => \inResetAsync~input_o\,
	sclr => \DUT|Equal0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.StrobeCount\(20));

-- Location: LABCELL_X81_Y1_N33
\DUT|Add0~65\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Add0~65_sumout\ = SUM(( \DUT|R.StrobeCount\(21) ) + ( GND ) + ( \DUT|Add0~70\ ))
-- \DUT|Add0~66\ = CARRY(( \DUT|R.StrobeCount\(21) ) + ( GND ) + ( \DUT|Add0~70\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \DUT|ALT_INV_R.StrobeCount\(21),
	cin => \DUT|Add0~70\,
	sumout => \DUT|Add0~65_sumout\,
	cout => \DUT|Add0~66\);

-- Location: FF_X81_Y1_N35
\DUT|R.StrobeCount[21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Add0~65_sumout\,
	clrn => \inResetAsync~input_o\,
	sclr => \DUT|Equal0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.StrobeCount\(21));

-- Location: LABCELL_X81_Y1_N36
\DUT|Add0~61\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Add0~61_sumout\ = SUM(( \DUT|R.StrobeCount\(22) ) + ( GND ) + ( \DUT|Add0~66\ ))
-- \DUT|Add0~62\ = CARRY(( \DUT|R.StrobeCount\(22) ) + ( GND ) + ( \DUT|Add0~66\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \DUT|ALT_INV_R.StrobeCount\(22),
	cin => \DUT|Add0~66\,
	sumout => \DUT|Add0~61_sumout\,
	cout => \DUT|Add0~62\);

-- Location: FF_X81_Y1_N38
\DUT|R.StrobeCount[22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Add0~61_sumout\,
	clrn => \inResetAsync~input_o\,
	sclr => \DUT|Equal0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.StrobeCount\(22));

-- Location: LABCELL_X81_Y1_N39
\DUT|Add0~57\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Add0~57_sumout\ = SUM(( \DUT|R.StrobeCount\(23) ) + ( GND ) + ( \DUT|Add0~62\ ))
-- \DUT|Add0~58\ = CARRY(( \DUT|R.StrobeCount\(23) ) + ( GND ) + ( \DUT|Add0~62\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \DUT|ALT_INV_R.StrobeCount\(23),
	cin => \DUT|Add0~62\,
	sumout => \DUT|Add0~57_sumout\,
	cout => \DUT|Add0~58\);

-- Location: FF_X81_Y1_N41
\DUT|R.StrobeCount[23]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Add0~57_sumout\,
	clrn => \inResetAsync~input_o\,
	sclr => \DUT|Equal0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.StrobeCount\(23));

-- Location: LABCELL_X81_Y1_N42
\DUT|Add0~53\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Add0~53_sumout\ = SUM(( \DUT|R.StrobeCount\(24) ) + ( GND ) + ( \DUT|Add0~58\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \DUT|ALT_INV_R.StrobeCount\(24),
	cin => \DUT|Add0~58\,
	sumout => \DUT|Add0~53_sumout\);

-- Location: FF_X81_Y1_N44
\DUT|R.StrobeCount[24]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Add0~53_sumout\,
	clrn => \inResetAsync~input_o\,
	sclr => \DUT|Equal0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.StrobeCount\(24));

-- Location: LABCELL_X81_Y1_N48
\DUT|Equal0~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Equal0~2_combout\ = ( \DUT|R.StrobeCount\(19) & ( \DUT|R.StrobeCount\(20) & ( (\DUT|R.StrobeCount\(22) & (!\DUT|R.StrobeCount\(23) & (\DUT|R.StrobeCount\(21) & \DUT|R.StrobeCount\(24)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \DUT|ALT_INV_R.StrobeCount\(22),
	datab => \DUT|ALT_INV_R.StrobeCount\(23),
	datac => \DUT|ALT_INV_R.StrobeCount\(21),
	datad => \DUT|ALT_INV_R.StrobeCount\(24),
	datae => \DUT|ALT_INV_R.StrobeCount\(19),
	dataf => \DUT|ALT_INV_R.StrobeCount\(20),
	combout => \DUT|Equal0~2_combout\);

-- Location: FF_X81_Y1_N8
\DUT|R.StrobeCount[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Add0~85_sumout\,
	clrn => \inResetAsync~input_o\,
	sclr => \DUT|Equal0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.StrobeCount\(12));

-- Location: LABCELL_X81_Y1_N54
\DUT|Equal0~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Equal0~3_combout\ = ( !\DUT|R.StrobeCount\(10) & ( !\DUT|R.StrobeCount\(9) & ( (\DUT|R.StrobeCount\(11) & (\DUT|R.StrobeCount\(13) & (\DUT|R.StrobeCount\(18) & \DUT|R.StrobeCount\(12)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000001000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \DUT|ALT_INV_R.StrobeCount\(11),
	datab => \DUT|ALT_INV_R.StrobeCount\(13),
	datac => \DUT|ALT_INV_R.StrobeCount\(18),
	datad => \DUT|ALT_INV_R.StrobeCount\(12),
	datae => \DUT|ALT_INV_R.StrobeCount\(10),
	dataf => \DUT|ALT_INV_R.StrobeCount\(9),
	combout => \DUT|Equal0~3_combout\);

-- Location: LABCELL_X81_Y2_N15
\DUT|Equal0~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Equal0~4_combout\ = ( !\DUT|R.StrobeCount\(8) & ( \DUT|Equal0~3_combout\ & ( (\DUT|Equal0~0_combout\ & (\DUT|Equal0~1_combout\ & (!\DUT|R.StrobeCount[0]~DUPLICATE_q\ & \DUT|Equal0~2_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000100000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \DUT|ALT_INV_Equal0~0_combout\,
	datab => \DUT|ALT_INV_Equal0~1_combout\,
	datac => \DUT|ALT_INV_R.StrobeCount[0]~DUPLICATE_q\,
	datad => \DUT|ALT_INV_Equal0~2_combout\,
	datae => \DUT|ALT_INV_R.StrobeCount\(8),
	dataf => \DUT|ALT_INV_Equal0~3_combout\,
	combout => \DUT|Equal0~4_combout\);

-- Location: MLABCELL_X82_Y2_N42
\DUT|R.Strobe~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|R.Strobe~feeder_combout\ = ( \DUT|Equal0~4_combout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \DUT|ALT_INV_Equal0~4_combout\,
	combout => \DUT|R.Strobe~feeder_combout\);

-- Location: FF_X82_Y2_N44
\DUT|R.Strobe\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|R.Strobe~feeder_combout\,
	clrn => \inResetAsync~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.Strobe~q\);

-- Location: IOIBUF_X4_Y0_N52
\SW[3]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(3),
	o => \SW[3]~input_o\);

-- Location: LABCELL_X83_Y2_N36
\DUT|R.LightChaserOn~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|R.LightChaserOn~feeder_combout\ = ( \SW[3]~input_o\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \ALT_INV_SW[3]~input_o\,
	combout => \DUT|R.LightChaserOn~feeder_combout\);

-- Location: FF_X83_Y2_N38
\DUT|R.LightChaserOn\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|R.LightChaserOn~feeder_combout\,
	clrn => \inResetAsync~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.LightChaserOn~q\);

-- Location: FF_X83_Y2_N35
\DUT|R.LightChaserOnDelayed\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	asdata => \DUT|R.LightChaserOn~q\,
	clrn => \inResetAsync~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.LightChaserOnDelayed~q\);

-- Location: FF_X83_Y2_N17
\DUT|R.LEDStates.LightChaserOn\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|NextR.LEDStates.LightChaserOn~1_combout\,
	clrn => \inResetAsync~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.LEDStates.LightChaserOn~q\);

-- Location: IOIBUF_X16_Y0_N1
\SW[1]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(1),
	o => \SW[1]~input_o\);

-- Location: FF_X83_Y2_N56
\DUT|R.LedsOn\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	asdata => \SW[1]~input_o\,
	clrn => \inResetAsync~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.LedsOn~q\);

-- Location: IOIBUF_X8_Y0_N35
\SW[2]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(2),
	o => \SW[2]~input_o\);

-- Location: FF_X83_Y2_N59
\DUT|R.LedsOff\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	asdata => \SW[2]~input_o\,
	clrn => \inResetAsync~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.LedsOff~q\);

-- Location: FF_X83_Y2_N46
\DUT|R.LedsOffDelayed\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	asdata => \DUT|R.LedsOff~q\,
	clrn => \inResetAsync~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.LedsOffDelayed~q\);

-- Location: FF_X83_Y2_N28
\DUT|R.LedsOnDelayed\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	asdata => \DUT|R.LedsOn~q\,
	clrn => \inResetAsync~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.LedsOnDelayed~q\);

-- Location: LABCELL_X83_Y2_N57
\DUT|NextR.LEDStates.LightChaserOn~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|NextR.LEDStates.LightChaserOn~0_combout\ = ( \DUT|R.LedsOff~q\ & ( \DUT|R.LedsOnDelayed~q\ & ( !\DUT|R.LedsOffDelayed~q\ ) ) ) # ( \DUT|R.LedsOff~q\ & ( !\DUT|R.LedsOnDelayed~q\ & ( (!\DUT|R.LedsOffDelayed~q\) # (\DUT|R.LedsOn~q\) ) ) ) # ( 
-- !\DUT|R.LedsOff~q\ & ( !\DUT|R.LedsOnDelayed~q\ & ( \DUT|R.LedsOn~q\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101111111110101010100000000000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \DUT|ALT_INV_R.LedsOn~q\,
	datad => \DUT|ALT_INV_R.LedsOffDelayed~q\,
	datae => \DUT|ALT_INV_R.LedsOff~q\,
	dataf => \DUT|ALT_INV_R.LedsOnDelayed~q\,
	combout => \DUT|NextR.LEDStates.LightChaserOn~0_combout\);

-- Location: LABCELL_X83_Y2_N15
\DUT|NextR.LEDStates.LightChaserOn~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|NextR.LEDStates.LightChaserOn~1_combout\ = ( \DUT|R.LEDStates.LightChaserOn~q\ & ( !\DUT|NextR.LEDStates.LightChaserOn~0_combout\ & ( !\DUT|R.Strobe~q\ ) ) ) # ( !\DUT|R.LEDStates.LightChaserOn~q\ & ( !\DUT|NextR.LEDStates.LightChaserOn~0_combout\ & 
-- ( (!\DUT|R.LightChaserOnDelayed~q\ & \DUT|R.LightChaserOn~q\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011110000101010101010101000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \DUT|ALT_INV_R.Strobe~q\,
	datac => \DUT|ALT_INV_R.LightChaserOnDelayed~q\,
	datad => \DUT|ALT_INV_R.LightChaserOn~q\,
	datae => \DUT|ALT_INV_R.LEDStates.LightChaserOn~q\,
	dataf => \DUT|ALT_INV_NextR.LEDStates.LightChaserOn~0_combout\,
	combout => \DUT|NextR.LEDStates.LightChaserOn~1_combout\);

-- Location: FF_X83_Y2_N16
\DUT|R.LEDStates.LightChaserOn~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|NextR.LEDStates.LightChaserOn~1_combout\,
	clrn => \inResetAsync~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.LEDStates.LightChaserOn~DUPLICATE_q\);

-- Location: LABCELL_X83_Y2_N48
\DUT|NextR.LEDStates.AllLedsOn~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|NextR.LEDStates.AllLedsOn~0_combout\ = ( \DUT|R.LEDStates.LightChaserOn~DUPLICATE_q\ & ( \DUT|R.Strobe~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \DUT|ALT_INV_R.Strobe~q\,
	dataf => \DUT|ALT_INV_R.LEDStates.LightChaserOn~DUPLICATE_q\,
	combout => \DUT|NextR.LEDStates.AllLedsOn~0_combout\);

-- Location: LABCELL_X83_Y2_N33
\DUT|NextR.LEDStates.AllLedsOn~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|NextR.LEDStates.AllLedsOn~1_combout\ = ( \DUT|R.LightChaserOn~q\ & ( \DUT|NextR.LEDStates.AllLedsOn~0_combout\ ) ) # ( !\DUT|R.LightChaserOn~q\ & ( \DUT|NextR.LEDStates.AllLedsOn~0_combout\ ) ) # ( \DUT|R.LightChaserOn~q\ & ( 
-- !\DUT|NextR.LEDStates.AllLedsOn~0_combout\ & ( (!\DUT|R.LightChaserOnDelayed~q\) # (\DUT|NextR.LEDStates.LightChaserOn~0_combout\) ) ) ) # ( !\DUT|R.LightChaserOn~q\ & ( !\DUT|NextR.LEDStates.AllLedsOn~0_combout\ & ( 
-- \DUT|NextR.LEDStates.LightChaserOn~0_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101111101011111010111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \DUT|ALT_INV_NextR.LEDStates.LightChaserOn~0_combout\,
	datac => \DUT|ALT_INV_R.LightChaserOnDelayed~q\,
	datae => \DUT|ALT_INV_R.LightChaserOn~q\,
	dataf => \DUT|ALT_INV_NextR.LEDStates.AllLedsOn~0_combout\,
	combout => \DUT|NextR.LEDStates.AllLedsOn~1_combout\);

-- Location: FF_X83_Y2_N50
\DUT|R.LEDStates.LightChaserRunning\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|NextR.LEDStates.AllLedsOn~0_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|NextR.LEDStates.AllLedsOn~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.LEDStates.LightChaserRunning~q\);

-- Location: LABCELL_X83_Y2_N21
\DUT|NextR.LEDStates.AllLedsOn~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|NextR.LEDStates.AllLedsOn~2_combout\ = ( \DUT|R.LedsOn~q\ & ( !\DUT|R.LedsOnDelayed~q\ & ( (!\DUT|R.Strobe~q\ & ((!\DUT|R.LedsOff~q\) # ((\DUT|R.LedsOffDelayed~q\)))) # (\DUT|R.Strobe~q\ & (!\DUT|R.LEDStates.LightChaserOn~q\ & ((!\DUT|R.LedsOff~q\) # 
-- (\DUT|R.LedsOffDelayed~q\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110010001111101000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \DUT|ALT_INV_R.Strobe~q\,
	datab => \DUT|ALT_INV_R.LedsOff~q\,
	datac => \DUT|ALT_INV_R.LEDStates.LightChaserOn~q\,
	datad => \DUT|ALT_INV_R.LedsOffDelayed~q\,
	datae => \DUT|ALT_INV_R.LedsOn~q\,
	dataf => \DUT|ALT_INV_R.LedsOnDelayed~q\,
	combout => \DUT|NextR.LEDStates.AllLedsOn~2_combout\);

-- Location: FF_X83_Y2_N22
\DUT|R.LEDStates.AllLedsOn\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|NextR.LEDStates.AllLedsOn~2_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|NextR.LEDStates.AllLedsOn~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.LEDStates.AllLedsOn~q\);

-- Location: MLABCELL_X84_Y2_N48
\DUT|Selector0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Selector0~0_combout\ = ( \DUT|R.Leds\(0) & ( ((\DUT|R.LEDStates.AllLedsOn~q\) # (\DUT|R.LEDStates.LightChaserOn~DUPLICATE_q\)) # (\DUT|R.LEDStates.LightChaserRunning~q\) ) ) # ( !\DUT|R.Leds\(0) & ( (\DUT|R.LEDStates.AllLedsOn~q\) # 
-- (\DUT|R.LEDStates.LightChaserOn~DUPLICATE_q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111111111111000011111111111101011111111111110101111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \DUT|ALT_INV_R.LEDStates.LightChaserRunning~q\,
	datac => \DUT|ALT_INV_R.LEDStates.LightChaserOn~DUPLICATE_q\,
	datad => \DUT|ALT_INV_R.LEDStates.AllLedsOn~q\,
	dataf => \DUT|ALT_INV_R.Leds\(0),
	combout => \DUT|Selector0~0_combout\);

-- Location: FF_X84_Y2_N49
\DUT|R.Leds[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Selector0~0_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.Strobe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.Leds\(9));

-- Location: MLABCELL_X84_Y2_N24
\DUT|Selector1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Selector1~0_combout\ = ( \DUT|R.Leds\(9) & ( (\DUT|R.LEDStates.AllLedsOn~q\) # (\DUT|R.LEDStates.LightChaserRunning~q\) ) ) # ( !\DUT|R.Leds\(9) & ( \DUT|R.LEDStates.AllLedsOn~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111101010101111111110101010111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \DUT|ALT_INV_R.LEDStates.LightChaserRunning~q\,
	datad => \DUT|ALT_INV_R.LEDStates.AllLedsOn~q\,
	dataf => \DUT|ALT_INV_R.Leds\(9),
	combout => \DUT|Selector1~0_combout\);

-- Location: FF_X84_Y2_N25
\DUT|R.Leds[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Selector1~0_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.Strobe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.Leds\(8));

-- Location: MLABCELL_X84_Y2_N27
\DUT|Selector2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Selector2~0_combout\ = ( \DUT|R.Leds\(8) & ( (\DUT|R.LEDStates.AllLedsOn~q\) # (\DUT|R.LEDStates.LightChaserRunning~q\) ) ) # ( !\DUT|R.Leds\(8) & ( \DUT|R.LEDStates.AllLedsOn~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111101010101111111110101010111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \DUT|ALT_INV_R.LEDStates.LightChaserRunning~q\,
	datad => \DUT|ALT_INV_R.LEDStates.AllLedsOn~q\,
	dataf => \DUT|ALT_INV_R.Leds\(8),
	combout => \DUT|Selector2~0_combout\);

-- Location: FF_X84_Y2_N28
\DUT|R.Leds[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Selector2~0_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.Strobe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.Leds\(7));

-- Location: MLABCELL_X84_Y2_N30
\DUT|Selector3~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Selector3~0_combout\ = ( \DUT|R.Leds\(7) & ( (\DUT|R.LEDStates.AllLedsOn~q\) # (\DUT|R.LEDStates.LightChaserRunning~q\) ) ) # ( !\DUT|R.Leds\(7) & ( \DUT|R.LEDStates.AllLedsOn~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111101010101111111110101010111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \DUT|ALT_INV_R.LEDStates.LightChaserRunning~q\,
	datad => \DUT|ALT_INV_R.LEDStates.AllLedsOn~q\,
	dataf => \DUT|ALT_INV_R.Leds\(7),
	combout => \DUT|Selector3~0_combout\);

-- Location: FF_X84_Y2_N31
\DUT|R.Leds[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Selector3~0_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.Strobe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.Leds\(6));

-- Location: MLABCELL_X84_Y2_N33
\DUT|Selector4~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Selector4~0_combout\ = ( \DUT|R.Leds\(6) & ( (\DUT|R.LEDStates.AllLedsOn~q\) # (\DUT|R.LEDStates.LightChaserRunning~q\) ) ) # ( !\DUT|R.Leds\(6) & ( \DUT|R.LEDStates.AllLedsOn~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111101010101111111110101010111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \DUT|ALT_INV_R.LEDStates.LightChaserRunning~q\,
	datad => \DUT|ALT_INV_R.LEDStates.AllLedsOn~q\,
	dataf => \DUT|ALT_INV_R.Leds\(6),
	combout => \DUT|Selector4~0_combout\);

-- Location: FF_X84_Y2_N34
\DUT|R.Leds[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Selector4~0_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.Strobe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.Leds\(5));

-- Location: MLABCELL_X84_Y2_N15
\DUT|Selector5~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Selector5~0_combout\ = ( \DUT|R.Leds\(5) & ( (\DUT|R.LEDStates.AllLedsOn~q\) # (\DUT|R.LEDStates.LightChaserRunning~q\) ) ) # ( !\DUT|R.Leds\(5) & ( \DUT|R.LEDStates.AllLedsOn~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111101010101111111110101010111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \DUT|ALT_INV_R.LEDStates.LightChaserRunning~q\,
	datad => \DUT|ALT_INV_R.LEDStates.AllLedsOn~q\,
	dataf => \DUT|ALT_INV_R.Leds\(5),
	combout => \DUT|Selector5~0_combout\);

-- Location: FF_X84_Y2_N16
\DUT|R.Leds[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Selector5~0_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.Strobe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.Leds\(4));

-- Location: MLABCELL_X84_Y2_N12
\DUT|Selector6~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Selector6~0_combout\ = ( \DUT|R.Leds\(4) & ( (\DUT|R.LEDStates.AllLedsOn~q\) # (\DUT|R.LEDStates.LightChaserRunning~q\) ) ) # ( !\DUT|R.Leds\(4) & ( \DUT|R.LEDStates.AllLedsOn~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111101010101111111110101010111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \DUT|ALT_INV_R.LEDStates.LightChaserRunning~q\,
	datad => \DUT|ALT_INV_R.LEDStates.AllLedsOn~q\,
	dataf => \DUT|ALT_INV_R.Leds\(4),
	combout => \DUT|Selector6~0_combout\);

-- Location: FF_X84_Y2_N13
\DUT|R.Leds[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Selector6~0_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.Strobe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.Leds\(3));

-- Location: MLABCELL_X84_Y2_N9
\DUT|Selector7~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Selector7~0_combout\ = ( \DUT|R.Leds\(3) & ( (\DUT|R.LEDStates.AllLedsOn~q\) # (\DUT|R.LEDStates.LightChaserRunning~q\) ) ) # ( !\DUT|R.Leds\(3) & ( \DUT|R.LEDStates.AllLedsOn~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111101010101111111110101010111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \DUT|ALT_INV_R.LEDStates.LightChaserRunning~q\,
	datad => \DUT|ALT_INV_R.LEDStates.AllLedsOn~q\,
	dataf => \DUT|ALT_INV_R.Leds\(3),
	combout => \DUT|Selector7~0_combout\);

-- Location: FF_X84_Y2_N10
\DUT|R.Leds[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Selector7~0_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.Strobe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.Leds\(2));

-- Location: MLABCELL_X84_Y2_N6
\DUT|Selector8~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Selector8~0_combout\ = ( \DUT|R.Leds\(2) & ( (\DUT|R.LEDStates.AllLedsOn~q\) # (\DUT|R.LEDStates.LightChaserRunning~q\) ) ) # ( !\DUT|R.Leds\(2) & ( \DUT|R.LEDStates.AllLedsOn~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111101010101111111110101010111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \DUT|ALT_INV_R.LEDStates.LightChaserRunning~q\,
	datad => \DUT|ALT_INV_R.LEDStates.AllLedsOn~q\,
	dataf => \DUT|ALT_INV_R.Leds\(2),
	combout => \DUT|Selector8~0_combout\);

-- Location: FF_X84_Y2_N7
\DUT|R.Leds[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Selector8~0_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.Strobe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.Leds\(1));

-- Location: MLABCELL_X84_Y2_N51
\DUT|Selector9~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Selector9~0_combout\ = ( \DUT|R.Leds\(1) & ( (\DUT|R.LEDStates.AllLedsOn~q\) # (\DUT|R.LEDStates.LightChaserRunning~q\) ) ) # ( !\DUT|R.Leds\(1) & ( \DUT|R.LEDStates.AllLedsOn~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111101010101111111110101010111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \DUT|ALT_INV_R.LEDStates.LightChaserRunning~q\,
	datad => \DUT|ALT_INV_R.LEDStates.AllLedsOn~q\,
	dataf => \DUT|ALT_INV_R.Leds\(1),
	combout => \DUT|Selector9~0_combout\);

-- Location: FF_X84_Y2_N53
\DUT|R.Leds[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Selector9~0_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.Strobe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.Leds\(0));

-- Location: IOIBUF_X12_Y0_N18
\SW[0]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(0),
	o => \SW[0]~input_o\);

-- Location: MLABCELL_X84_Y2_N36
\DUT|R.IncCounter~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|R.IncCounter~feeder_combout\ = ( \SW[0]~input_o\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \ALT_INV_SW[0]~input_o\,
	combout => \DUT|R.IncCounter~feeder_combout\);

-- Location: FF_X84_Y2_N38
\DUT|R.IncCounter\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|R.IncCounter~feeder_combout\,
	clrn => \inResetAsync~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.IncCounter~q\);

-- Location: FF_X84_Y2_N58
\DUT|R.IncCounterDelayed\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	asdata => \DUT|R.IncCounter~q\,
	clrn => \inResetAsync~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.IncCounterDelayed~q\);

-- Location: MLABCELL_X84_Y2_N54
\DUT|NextR.Carries[0]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|NextR.Carries[0]~0_combout\ = ( \DUT|R.Carries\(0) & ( \DUT|R.IncCounterDelayed~q\ & ( !\DUT|R.Strobe~q\ ) ) ) # ( \DUT|R.Carries\(0) & ( !\DUT|R.IncCounterDelayed~q\ & ( !\DUT|R.Strobe~q\ ) ) ) # ( !\DUT|R.Carries\(0) & ( 
-- !\DUT|R.IncCounterDelayed~q\ & ( \DUT|R.IncCounter~q\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111101010101010101000000000000000001010101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \DUT|ALT_INV_R.Strobe~q\,
	datac => \DUT|ALT_INV_R.IncCounter~q\,
	datae => \DUT|ALT_INV_R.Carries\(0),
	dataf => \DUT|ALT_INV_R.IncCounterDelayed~q\,
	combout => \DUT|NextR.Carries[0]~0_combout\);

-- Location: FF_X84_Y2_N55
\DUT|R.Carries[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|NextR.Carries[0]~0_combout\,
	clrn => \inResetAsync~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.Carries\(0));

-- Location: MLABCELL_X84_Y2_N3
\DUT|R.BCDCounter[0][0]~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|R.BCDCounter[0][0]~6_combout\ = ( !\DUT|R.BCDCounter[0][0]~q\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111000000000000000011111111111111110000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \DUT|ALT_INV_R.BCDCounter[0][0]~q\,
	combout => \DUT|R.BCDCounter[0][0]~6_combout\);

-- Location: LABCELL_X85_Y2_N6
\DUT|R.BCDCounter[0][3]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|R.BCDCounter[0][3]~0_combout\ = ( \DUT|R.Carries\(0) & ( \DUT|R.Strobe~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \DUT|ALT_INV_R.Strobe~q\,
	dataf => \DUT|ALT_INV_R.Carries\(0),
	combout => \DUT|R.BCDCounter[0][3]~0_combout\);

-- Location: FF_X84_Y2_N4
\DUT|R.BCDCounter[0][0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|R.BCDCounter[0][0]~6_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.BCDCounter[0][3]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.BCDCounter[0][0]~q\);

-- Location: LABCELL_X85_Y2_N57
\DUT|NextR~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|NextR~4_combout\ = (!\DUT|R.BCDCounter[0][0]~q\ & (((\DUT|R.BCDCounter[0][1]~q\)))) # (\DUT|R.BCDCounter[0][0]~q\ & (!\DUT|R.BCDCounter[0][1]~q\ & ((!\DUT|R.BCDCounter[0][3]~q\) # (\DUT|R.BCDCounter[0][2]~q\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011000111001100001100011100110000110001110011000011000111001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \DUT|ALT_INV_R.BCDCounter[0][2]~q\,
	datab => \DUT|ALT_INV_R.BCDCounter[0][0]~q\,
	datac => \DUT|ALT_INV_R.BCDCounter[0][3]~q\,
	datad => \DUT|ALT_INV_R.BCDCounter[0][1]~q\,
	combout => \DUT|NextR~4_combout\);

-- Location: FF_X85_Y2_N58
\DUT|R.BCDCounter[0][1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|NextR~4_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.BCDCounter[0][3]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.BCDCounter[0][1]~q\);

-- Location: LABCELL_X85_Y2_N24
\DUT|R.BCDCounter[0][2]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|R.BCDCounter[0][2]~1_combout\ = ( \DUT|R.BCDCounter[0][2]~q\ & ( \DUT|R.BCDCounter[0][1]~q\ & ( (!\DUT|R.Carries\(0)) # ((!\DUT|R.Strobe~q\) # (!\DUT|R.BCDCounter[0][0]~q\)) ) ) ) # ( !\DUT|R.BCDCounter[0][2]~q\ & ( \DUT|R.BCDCounter[0][1]~q\ & ( 
-- (\DUT|R.Carries\(0) & (\DUT|R.Strobe~q\ & \DUT|R.BCDCounter[0][0]~q\)) ) ) ) # ( \DUT|R.BCDCounter[0][2]~q\ & ( !\DUT|R.BCDCounter[0][1]~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000111111111111111100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \DUT|ALT_INV_R.Carries\(0),
	datac => \DUT|ALT_INV_R.Strobe~q\,
	datad => \DUT|ALT_INV_R.BCDCounter[0][0]~q\,
	datae => \DUT|ALT_INV_R.BCDCounter[0][2]~q\,
	dataf => \DUT|ALT_INV_R.BCDCounter[0][1]~q\,
	combout => \DUT|R.BCDCounter[0][2]~1_combout\);

-- Location: FF_X85_Y2_N26
\DUT|R.BCDCounter[0][2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|R.BCDCounter[0][2]~1_combout\,
	clrn => \inResetAsync~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.BCDCounter[0][2]~q\);

-- Location: LABCELL_X85_Y2_N54
\DUT|NextR~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|NextR~5_combout\ = ( \DUT|R.BCDCounter[0][1]~q\ & ( !\DUT|R.BCDCounter[0][3]~q\ $ (((!\DUT|R.BCDCounter[0][2]~q\) # (!\DUT|R.BCDCounter[0][0]~q\))) ) ) # ( !\DUT|R.BCDCounter[0][1]~q\ & ( (\DUT|R.BCDCounter[0][3]~q\ & ((!\DUT|R.BCDCounter[0][0]~q\) # 
-- (\DUT|R.BCDCounter[0][2]~q\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011011101000000001101110100010001111011100001000111101110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \DUT|ALT_INV_R.BCDCounter[0][2]~q\,
	datab => \DUT|ALT_INV_R.BCDCounter[0][0]~q\,
	datad => \DUT|ALT_INV_R.BCDCounter[0][3]~q\,
	dataf => \DUT|ALT_INV_R.BCDCounter[0][1]~q\,
	combout => \DUT|NextR~5_combout\);

-- Location: FF_X85_Y2_N55
\DUT|R.BCDCounter[0][3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|NextR~5_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.BCDCounter[0][3]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.BCDCounter[0][3]~q\);

-- Location: FF_X85_Y2_N59
\DUT|R.BCDCounter[0][1]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|NextR~4_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.BCDCounter[0][3]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.BCDCounter[0][1]~DUPLICATE_q\);

-- Location: LABCELL_X85_Y2_N36
\DUT|Mux6~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Mux6~0_combout\ = ( \DUT|R.BCDCounter[0][0]~q\ & ( (!\DUT|R.BCDCounter[0][3]~q\ & ((\DUT|R.BCDCounter[0][1]~DUPLICATE_q\) # (\DUT|R.BCDCounter[0][2]~q\))) # (\DUT|R.BCDCounter[0][3]~q\ & (!\DUT|R.BCDCounter[0][2]~q\ $ 
-- (\DUT|R.BCDCounter[0][1]~DUPLICATE_q\))) ) ) # ( !\DUT|R.BCDCounter[0][0]~q\ & ( ((!\DUT|R.BCDCounter[0][2]~q\) # (\DUT|R.BCDCounter[0][1]~DUPLICATE_q\)) # (\DUT|R.BCDCounter[0][3]~q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111001111111111111100111111111100111100110011110011110011001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \DUT|ALT_INV_R.BCDCounter[0][3]~q\,
	datac => \DUT|ALT_INV_R.BCDCounter[0][2]~q\,
	datad => \DUT|ALT_INV_R.BCDCounter[0][1]~DUPLICATE_q\,
	dataf => \DUT|ALT_INV_R.BCDCounter[0][0]~q\,
	combout => \DUT|Mux6~0_combout\);

-- Location: FF_X85_Y2_N37
\DUT|R.Hex[0][0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Mux6~0_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.Strobe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.Hex[0][0]~q\);

-- Location: LABCELL_X85_Y2_N21
\DUT|Mux5~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Mux5~0_combout\ = (!\DUT|R.BCDCounter[0][3]~q\ & ((!\DUT|R.BCDCounter[0][2]~q\) # (!\DUT|R.BCDCounter[0][0]~q\ $ (\DUT|R.BCDCounter[0][1]~DUPLICATE_q\)))) # (\DUT|R.BCDCounter[0][3]~q\ & ((!\DUT|R.BCDCounter[0][0]~q\ & (!\DUT|R.BCDCounter[0][2]~q\)) 
-- # (\DUT|R.BCDCounter[0][0]~q\ & ((!\DUT|R.BCDCounter[0][1]~DUPLICATE_q\)))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1110101110101100111010111010110011101011101011001110101110101100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \DUT|ALT_INV_R.BCDCounter[0][2]~q\,
	datab => \DUT|ALT_INV_R.BCDCounter[0][3]~q\,
	datac => \DUT|ALT_INV_R.BCDCounter[0][0]~q\,
	datad => \DUT|ALT_INV_R.BCDCounter[0][1]~DUPLICATE_q\,
	combout => \DUT|Mux5~0_combout\);

-- Location: FF_X85_Y2_N22
\DUT|R.Hex[0][1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Mux5~0_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.Strobe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.Hex[0][1]~q\);

-- Location: LABCELL_X85_Y2_N18
\DUT|Mux4~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Mux4~0_combout\ = ( \DUT|R.BCDCounter[0][0]~q\ & ( (!\DUT|R.BCDCounter[0][3]~q\) # ((!\DUT|R.BCDCounter[0][2]~q\) # (!\DUT|R.BCDCounter[0][1]~DUPLICATE_q\)) ) ) # ( !\DUT|R.BCDCounter[0][0]~q\ & ( (!\DUT|R.BCDCounter[0][3]~q\ & 
-- ((!\DUT|R.BCDCounter[0][1]~DUPLICATE_q\) # (\DUT|R.BCDCounter[0][2]~q\))) # (\DUT|R.BCDCounter[0][3]~q\ & (!\DUT|R.BCDCounter[0][2]~q\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111110000111100111111000011110011111111111111001111111111111100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \DUT|ALT_INV_R.BCDCounter[0][3]~q\,
	datac => \DUT|ALT_INV_R.BCDCounter[0][2]~q\,
	datad => \DUT|ALT_INV_R.BCDCounter[0][1]~DUPLICATE_q\,
	dataf => \DUT|ALT_INV_R.BCDCounter[0][0]~q\,
	combout => \DUT|Mux4~0_combout\);

-- Location: FF_X85_Y2_N20
\DUT|R.Hex[0][2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Mux4~0_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.Strobe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.Hex[0][2]~q\);

-- Location: LABCELL_X85_Y2_N39
\DUT|Mux3~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Mux3~0_combout\ = (!\DUT|R.BCDCounter[0][1]~DUPLICATE_q\ & ((!\DUT|R.BCDCounter[0][2]~q\ $ (\DUT|R.BCDCounter[0][0]~q\)) # (\DUT|R.BCDCounter[0][3]~q\))) # (\DUT|R.BCDCounter[0][1]~DUPLICATE_q\ & ((!\DUT|R.BCDCounter[0][2]~q\ & 
-- ((!\DUT|R.BCDCounter[0][3]~q\) # (\DUT|R.BCDCounter[0][0]~q\))) # (\DUT|R.BCDCounter[0][2]~q\ & ((!\DUT|R.BCDCounter[0][0]~q\)))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1011011111011010101101111101101010110111110110101011011111011010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \DUT|ALT_INV_R.BCDCounter[0][2]~q\,
	datab => \DUT|ALT_INV_R.BCDCounter[0][3]~q\,
	datac => \DUT|ALT_INV_R.BCDCounter[0][0]~q\,
	datad => \DUT|ALT_INV_R.BCDCounter[0][1]~DUPLICATE_q\,
	combout => \DUT|Mux3~0_combout\);

-- Location: FF_X85_Y2_N40
\DUT|R.Hex[0][3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Mux3~0_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.Strobe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.Hex[0][3]~q\);

-- Location: LABCELL_X85_Y2_N51
\DUT|Mux2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Mux2~0_combout\ = ( \DUT|R.BCDCounter[0][0]~q\ & ( (\DUT|R.BCDCounter[0][3]~q\ & ((\DUT|R.BCDCounter[0][1]~DUPLICATE_q\) # (\DUT|R.BCDCounter[0][2]~q\))) ) ) # ( !\DUT|R.BCDCounter[0][0]~q\ & ( (!\DUT|R.BCDCounter[0][2]~q\) # 
-- ((\DUT|R.BCDCounter[0][1]~DUPLICATE_q\) # (\DUT|R.BCDCounter[0][3]~q\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010111111111111101011111111111100000101000011110000010100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \DUT|ALT_INV_R.BCDCounter[0][2]~q\,
	datac => \DUT|ALT_INV_R.BCDCounter[0][3]~q\,
	datad => \DUT|ALT_INV_R.BCDCounter[0][1]~DUPLICATE_q\,
	dataf => \DUT|ALT_INV_R.BCDCounter[0][0]~q\,
	combout => \DUT|Mux2~0_combout\);

-- Location: FF_X85_Y2_N53
\DUT|R.Hex[0][4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Mux2~0_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.Strobe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.Hex[0][4]~q\);

-- Location: FF_X85_Y2_N56
\DUT|R.BCDCounter[0][3]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|NextR~5_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.BCDCounter[0][3]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.BCDCounter[0][3]~DUPLICATE_q\);

-- Location: LABCELL_X85_Y2_N45
\DUT|Mux1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Mux1~0_combout\ = ( \DUT|R.BCDCounter[0][0]~q\ & ( !\DUT|R.BCDCounter[0][3]~DUPLICATE_q\ $ (((!\DUT|R.BCDCounter[0][2]~q\) # (\DUT|R.BCDCounter[0][1]~DUPLICATE_q\))) ) ) # ( !\DUT|R.BCDCounter[0][0]~q\ & ( ((!\DUT|R.BCDCounter[0][1]~DUPLICATE_q\) # 
-- (\DUT|R.BCDCounter[0][3]~DUPLICATE_q\)) # (\DUT|R.BCDCounter[0][2]~q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111010111111111111101011111111101010000101011110101000010101111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \DUT|ALT_INV_R.BCDCounter[0][2]~q\,
	datac => \DUT|ALT_INV_R.BCDCounter[0][1]~DUPLICATE_q\,
	datad => \DUT|ALT_INV_R.BCDCounter[0][3]~DUPLICATE_q\,
	dataf => \DUT|ALT_INV_R.BCDCounter[0][0]~q\,
	combout => \DUT|Mux1~0_combout\);

-- Location: FF_X85_Y2_N46
\DUT|R.Hex[0][5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Mux1~0_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.Strobe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.Hex[0][5]~q\);

-- Location: LABCELL_X85_Y2_N12
\DUT|Mux0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Mux0~0_combout\ = ( \DUT|R.BCDCounter[0][0]~q\ & ( (!\DUT|R.BCDCounter[0][2]~q\ $ (!\DUT|R.BCDCounter[0][1]~DUPLICATE_q\)) # (\DUT|R.BCDCounter[0][3]~DUPLICATE_q\) ) ) # ( !\DUT|R.BCDCounter[0][0]~q\ & ( (!\DUT|R.BCDCounter[0][3]~DUPLICATE_q\ $ 
-- (!\DUT|R.BCDCounter[0][2]~q\)) # (\DUT|R.BCDCounter[0][1]~DUPLICATE_q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101101011111111010110101111111101011111111101010101111111110101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \DUT|ALT_INV_R.BCDCounter[0][3]~DUPLICATE_q\,
	datac => \DUT|ALT_INV_R.BCDCounter[0][2]~q\,
	datad => \DUT|ALT_INV_R.BCDCounter[0][1]~DUPLICATE_q\,
	dataf => \DUT|ALT_INV_R.BCDCounter[0][0]~q\,
	combout => \DUT|Mux0~0_combout\);

-- Location: FF_X85_Y2_N13
\DUT|R.Hex[0][6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Mux0~0_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.Strobe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.Hex[0][6]~q\);

-- Location: FF_X85_Y2_N11
\DUT|R.BCDCounter[1][0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|R.BCDCounter[1][0]~2_combout\,
	clrn => \inResetAsync~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.BCDCounter[1][0]~q\);

-- Location: LABCELL_X85_Y2_N42
\DUT|Equal1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Equal1~0_combout\ = ( \DUT|R.BCDCounter[0][0]~q\ & ( (\DUT|R.BCDCounter[0][3]~q\ & (!\DUT|R.BCDCounter[0][2]~q\ & !\DUT|R.BCDCounter[0][1]~DUPLICATE_q\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000110000000000000011000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \DUT|ALT_INV_R.BCDCounter[0][3]~q\,
	datac => \DUT|ALT_INV_R.BCDCounter[0][2]~q\,
	datad => \DUT|ALT_INV_R.BCDCounter[0][1]~DUPLICATE_q\,
	dataf => \DUT|ALT_INV_R.BCDCounter[0][0]~q\,
	combout => \DUT|Equal1~0_combout\);

-- Location: LABCELL_X85_Y2_N9
\DUT|R.BCDCounter[1][0]~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|R.BCDCounter[1][0]~2_combout\ = ( \DUT|Equal1~0_combout\ & ( !\DUT|R.BCDCounter[0][3]~0_combout\ $ (!\DUT|R.BCDCounter[1][0]~q\) ) ) # ( !\DUT|Equal1~0_combout\ & ( \DUT|R.BCDCounter[1][0]~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111101010101101010100101010110101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \DUT|ALT_INV_R.BCDCounter[0][3]~0_combout\,
	datad => \DUT|ALT_INV_R.BCDCounter[1][0]~q\,
	dataf => \DUT|ALT_INV_Equal1~0_combout\,
	combout => \DUT|R.BCDCounter[1][0]~2_combout\);

-- Location: FF_X85_Y2_N10
\DUT|R.BCDCounter[1][0]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|R.BCDCounter[1][0]~2_combout\,
	clrn => \inResetAsync~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.BCDCounter[1][0]~DUPLICATE_q\);

-- Location: FF_X85_Y2_N5
\DUT|R.BCDCounter[1][1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|R.BCDCounter[1][1]~3_combout\,
	clrn => \inResetAsync~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.BCDCounter[1][1]~q\);

-- Location: LABCELL_X85_Y2_N3
\DUT|R.BCDCounter[1][1]~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|R.BCDCounter[1][1]~3_combout\ = ( \DUT|Equal1~0_combout\ & ( !\DUT|R.BCDCounter[1][1]~q\ $ (((!\DUT|R.BCDCounter[0][3]~0_combout\) # (!\DUT|R.BCDCounter[1][0]~q\))) ) ) # ( !\DUT|Equal1~0_combout\ & ( \DUT|R.BCDCounter[1][1]~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000101111110100000010111111010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \DUT|ALT_INV_R.BCDCounter[0][3]~0_combout\,
	datac => \DUT|ALT_INV_R.BCDCounter[1][0]~q\,
	datad => \DUT|ALT_INV_R.BCDCounter[1][1]~q\,
	dataf => \DUT|ALT_INV_Equal1~0_combout\,
	combout => \DUT|R.BCDCounter[1][1]~3_combout\);

-- Location: FF_X85_Y2_N4
\DUT|R.BCDCounter[1][1]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|R.BCDCounter[1][1]~3_combout\,
	clrn => \inResetAsync~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.BCDCounter[1][1]~DUPLICATE_q\);

-- Location: LABCELL_X85_Y2_N0
\DUT|R.BCDCounter[1][2]~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|R.BCDCounter[1][2]~4_combout\ = ( \DUT|R.BCDCounter[1][1]~DUPLICATE_q\ & ( !\DUT|R.BCDCounter[1][2]~q\ $ (((!\DUT|R.BCDCounter[0][3]~0_combout\) # ((!\DUT|R.BCDCounter[1][0]~q\) # (!\DUT|Equal1~0_combout\)))) ) ) # ( 
-- !\DUT|R.BCDCounter[1][1]~DUPLICATE_q\ & ( \DUT|R.BCDCounter[1][2]~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000001111111100000000111111110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \DUT|ALT_INV_R.BCDCounter[0][3]~0_combout\,
	datab => \DUT|ALT_INV_R.BCDCounter[1][0]~q\,
	datac => \DUT|ALT_INV_Equal1~0_combout\,
	datad => \DUT|ALT_INV_R.BCDCounter[1][2]~q\,
	dataf => \DUT|ALT_INV_R.BCDCounter[1][1]~DUPLICATE_q\,
	combout => \DUT|R.BCDCounter[1][2]~4_combout\);

-- Location: FF_X85_Y2_N2
\DUT|R.BCDCounter[1][2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|R.BCDCounter[1][2]~4_combout\,
	clrn => \inResetAsync~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.BCDCounter[1][2]~q\);

-- Location: FF_X85_Y2_N32
\DUT|R.BCDCounter[1][3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|R.BCDCounter[1][3]~5_combout\,
	clrn => \inResetAsync~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.BCDCounter[1][3]~q\);

-- Location: LABCELL_X85_Y2_N30
\DUT|R.BCDCounter[1][3]~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|R.BCDCounter[1][3]~5_combout\ = ( \DUT|R.BCDCounter[1][3]~q\ & ( \DUT|R.BCDCounter[1][1]~DUPLICATE_q\ & ( (!\DUT|R.BCDCounter[0][3]~0_combout\) # ((!\DUT|Equal1~0_combout\) # ((!\DUT|R.BCDCounter[1][2]~q\) # (!\DUT|R.BCDCounter[1][0]~q\))) ) ) ) # ( 
-- !\DUT|R.BCDCounter[1][3]~q\ & ( \DUT|R.BCDCounter[1][1]~DUPLICATE_q\ & ( (\DUT|R.BCDCounter[0][3]~0_combout\ & (\DUT|Equal1~0_combout\ & (\DUT|R.BCDCounter[1][2]~q\ & \DUT|R.BCDCounter[1][0]~q\))) ) ) ) # ( \DUT|R.BCDCounter[1][3]~q\ & ( 
-- !\DUT|R.BCDCounter[1][1]~DUPLICATE_q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000011111111111111110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \DUT|ALT_INV_R.BCDCounter[0][3]~0_combout\,
	datab => \DUT|ALT_INV_Equal1~0_combout\,
	datac => \DUT|ALT_INV_R.BCDCounter[1][2]~q\,
	datad => \DUT|ALT_INV_R.BCDCounter[1][0]~q\,
	datae => \DUT|ALT_INV_R.BCDCounter[1][3]~q\,
	dataf => \DUT|ALT_INV_R.BCDCounter[1][1]~DUPLICATE_q\,
	combout => \DUT|R.BCDCounter[1][3]~5_combout\);

-- Location: FF_X85_Y2_N31
\DUT|R.BCDCounter[1][3]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|R.BCDCounter[1][3]~5_combout\,
	clrn => \inResetAsync~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.BCDCounter[1][3]~DUPLICATE_q\);

-- Location: MLABCELL_X82_Y2_N12
\DUT|Mux13~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Mux13~0_combout\ = ( \DUT|R.BCDCounter[1][1]~DUPLICATE_q\ & ( \DUT|R.BCDCounter[1][3]~DUPLICATE_q\ & ( (!\DUT|R.BCDCounter[1][0]~DUPLICATE_q\) # (\DUT|R.BCDCounter[1][2]~q\) ) ) ) # ( !\DUT|R.BCDCounter[1][1]~DUPLICATE_q\ & ( 
-- \DUT|R.BCDCounter[1][3]~DUPLICATE_q\ & ( (!\DUT|R.BCDCounter[1][0]~DUPLICATE_q\) # (!\DUT|R.BCDCounter[1][2]~q\) ) ) ) # ( \DUT|R.BCDCounter[1][1]~DUPLICATE_q\ & ( !\DUT|R.BCDCounter[1][3]~DUPLICATE_q\ ) ) # ( !\DUT|R.BCDCounter[1][1]~DUPLICATE_q\ & ( 
-- !\DUT|R.BCDCounter[1][3]~DUPLICATE_q\ & ( !\DUT|R.BCDCounter[1][0]~DUPLICATE_q\ $ (\DUT|R.BCDCounter[1][2]~q\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100001111000011111111111111111111111100111111001100111111001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \DUT|ALT_INV_R.BCDCounter[1][0]~DUPLICATE_q\,
	datac => \DUT|ALT_INV_R.BCDCounter[1][2]~q\,
	datae => \DUT|ALT_INV_R.BCDCounter[1][1]~DUPLICATE_q\,
	dataf => \DUT|ALT_INV_R.BCDCounter[1][3]~DUPLICATE_q\,
	combout => \DUT|Mux13~0_combout\);

-- Location: FF_X82_Y2_N13
\DUT|R.Hex[1][0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Mux13~0_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.Strobe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.Hex[1][0]~q\);

-- Location: MLABCELL_X82_Y2_N18
\DUT|Mux12~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Mux12~0_combout\ = ( \DUT|R.BCDCounter[1][1]~DUPLICATE_q\ & ( \DUT|R.BCDCounter[1][3]~DUPLICATE_q\ & ( (!\DUT|R.BCDCounter[1][0]~DUPLICATE_q\ & !\DUT|R.BCDCounter[1][2]~q\) ) ) ) # ( !\DUT|R.BCDCounter[1][1]~DUPLICATE_q\ & ( 
-- \DUT|R.BCDCounter[1][3]~DUPLICATE_q\ & ( (!\DUT|R.BCDCounter[1][2]~q\) # (\DUT|R.BCDCounter[1][0]~DUPLICATE_q\) ) ) ) # ( \DUT|R.BCDCounter[1][1]~DUPLICATE_q\ & ( !\DUT|R.BCDCounter[1][3]~DUPLICATE_q\ & ( (!\DUT|R.BCDCounter[1][2]~q\) # 
-- (\DUT|R.BCDCounter[1][0]~DUPLICATE_q\) ) ) ) # ( !\DUT|R.BCDCounter[1][1]~DUPLICATE_q\ & ( !\DUT|R.BCDCounter[1][3]~DUPLICATE_q\ & ( (!\DUT|R.BCDCounter[1][0]~DUPLICATE_q\) # (!\DUT|R.BCDCounter[1][2]~q\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111110011111100111100111111001111110011111100111100000011000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \DUT|ALT_INV_R.BCDCounter[1][0]~DUPLICATE_q\,
	datac => \DUT|ALT_INV_R.BCDCounter[1][2]~q\,
	datae => \DUT|ALT_INV_R.BCDCounter[1][1]~DUPLICATE_q\,
	dataf => \DUT|ALT_INV_R.BCDCounter[1][3]~DUPLICATE_q\,
	combout => \DUT|Mux12~0_combout\);

-- Location: FF_X82_Y2_N19
\DUT|R.Hex[1][1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Mux12~0_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.Strobe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.Hex[1][1]~q\);

-- Location: LABCELL_X81_Y2_N21
\DUT|Mux11~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Mux11~0_combout\ = ( \DUT|R.BCDCounter[1][1]~DUPLICATE_q\ & ( (!\DUT|R.BCDCounter[1][3]~DUPLICATE_q\ & ((\DUT|R.BCDCounter[1][2]~q\) # (\DUT|R.BCDCounter[1][0]~DUPLICATE_q\))) # (\DUT|R.BCDCounter[1][3]~DUPLICATE_q\ & ((!\DUT|R.BCDCounter[1][2]~q\))) 
-- ) ) # ( !\DUT|R.BCDCounter[1][1]~DUPLICATE_q\ & ( (!\DUT|R.BCDCounter[1][3]~DUPLICATE_q\) # ((!\DUT|R.BCDCounter[1][2]~q\) # (\DUT|R.BCDCounter[1][0]~DUPLICATE_q\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111110101111111111111010111101011111101010100101111110101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \DUT|ALT_INV_R.BCDCounter[1][3]~DUPLICATE_q\,
	datac => \DUT|ALT_INV_R.BCDCounter[1][0]~DUPLICATE_q\,
	datad => \DUT|ALT_INV_R.BCDCounter[1][2]~q\,
	dataf => \DUT|ALT_INV_R.BCDCounter[1][1]~DUPLICATE_q\,
	combout => \DUT|Mux11~0_combout\);

-- Location: FF_X81_Y2_N22
\DUT|R.Hex[1][2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Mux11~0_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.Strobe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.Hex[1][2]~q\);

-- Location: MLABCELL_X82_Y2_N48
\DUT|Mux10~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Mux10~0_combout\ = ( \DUT|R.BCDCounter[1][1]~DUPLICATE_q\ & ( \DUT|R.BCDCounter[1][3]~DUPLICATE_q\ & ( !\DUT|R.BCDCounter[1][0]~DUPLICATE_q\ $ (!\DUT|R.BCDCounter[1][2]~q\) ) ) ) # ( !\DUT|R.BCDCounter[1][1]~DUPLICATE_q\ & ( 
-- \DUT|R.BCDCounter[1][3]~DUPLICATE_q\ ) ) # ( \DUT|R.BCDCounter[1][1]~DUPLICATE_q\ & ( !\DUT|R.BCDCounter[1][3]~DUPLICATE_q\ & ( (!\DUT|R.BCDCounter[1][0]~DUPLICATE_q\) # (!\DUT|R.BCDCounter[1][2]~q\) ) ) ) # ( !\DUT|R.BCDCounter[1][1]~DUPLICATE_q\ & ( 
-- !\DUT|R.BCDCounter[1][3]~DUPLICATE_q\ & ( !\DUT|R.BCDCounter[1][0]~DUPLICATE_q\ $ (\DUT|R.BCDCounter[1][2]~q\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100001111000011111111001111110011111111111111110011110000111100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \DUT|ALT_INV_R.BCDCounter[1][0]~DUPLICATE_q\,
	datac => \DUT|ALT_INV_R.BCDCounter[1][2]~q\,
	datae => \DUT|ALT_INV_R.BCDCounter[1][1]~DUPLICATE_q\,
	dataf => \DUT|ALT_INV_R.BCDCounter[1][3]~DUPLICATE_q\,
	combout => \DUT|Mux10~0_combout\);

-- Location: FF_X82_Y2_N49
\DUT|R.Hex[1][3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Mux10~0_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.Strobe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.Hex[1][3]~q\);

-- Location: LABCELL_X81_Y2_N18
\DUT|Mux9~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Mux9~0_combout\ = ( \DUT|R.BCDCounter[1][1]~DUPLICATE_q\ & ( (!\DUT|R.BCDCounter[1][0]~DUPLICATE_q\) # (\DUT|R.BCDCounter[1][3]~DUPLICATE_q\) ) ) # ( !\DUT|R.BCDCounter[1][1]~DUPLICATE_q\ & ( (!\DUT|R.BCDCounter[1][2]~q\ & 
-- ((!\DUT|R.BCDCounter[1][0]~DUPLICATE_q\))) # (\DUT|R.BCDCounter[1][2]~q\ & (\DUT|R.BCDCounter[1][3]~DUPLICATE_q\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100010111000101110001011100010111011101110111011101110111011101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \DUT|ALT_INV_R.BCDCounter[1][3]~DUPLICATE_q\,
	datab => \DUT|ALT_INV_R.BCDCounter[1][0]~DUPLICATE_q\,
	datac => \DUT|ALT_INV_R.BCDCounter[1][2]~q\,
	dataf => \DUT|ALT_INV_R.BCDCounter[1][1]~DUPLICATE_q\,
	combout => \DUT|Mux9~0_combout\);

-- Location: FF_X81_Y2_N19
\DUT|R.Hex[1][4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Mux9~0_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.Strobe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.Hex[1][4]~q\);

-- Location: LABCELL_X85_Y2_N48
\DUT|Mux8~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Mux8~0_combout\ = ( \DUT|R.BCDCounter[1][0]~DUPLICATE_q\ & ( !\DUT|R.BCDCounter[1][3]~q\ $ (((!\DUT|R.BCDCounter[1][2]~q\) # (\DUT|R.BCDCounter[1][1]~q\))) ) ) # ( !\DUT|R.BCDCounter[1][0]~DUPLICATE_q\ & ( ((!\DUT|R.BCDCounter[1][1]~q\) # 
-- (\DUT|R.BCDCounter[1][2]~q\)) # (\DUT|R.BCDCounter[1][3]~q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111001111111111111100111111111100110011110000110011001111000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \DUT|ALT_INV_R.BCDCounter[1][3]~q\,
	datac => \DUT|ALT_INV_R.BCDCounter[1][1]~q\,
	datad => \DUT|ALT_INV_R.BCDCounter[1][2]~q\,
	dataf => \DUT|ALT_INV_R.BCDCounter[1][0]~DUPLICATE_q\,
	combout => \DUT|Mux8~0_combout\);

-- Location: FF_X85_Y2_N49
\DUT|R.Hex[1][5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Mux8~0_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.Strobe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.Hex[1][5]~q\);

-- Location: LABCELL_X85_Y2_N15
\DUT|Mux7~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|Mux7~0_combout\ = ( \DUT|R.BCDCounter[1][1]~DUPLICATE_q\ & ( ((!\DUT|R.BCDCounter[1][0]~q\) # (!\DUT|R.BCDCounter[1][2]~q\)) # (\DUT|R.BCDCounter[1][3]~q\) ) ) # ( !\DUT|R.BCDCounter[1][1]~DUPLICATE_q\ & ( (!\DUT|R.BCDCounter[1][3]~q\ & 
-- ((\DUT|R.BCDCounter[1][2]~q\))) # (\DUT|R.BCDCounter[1][3]~q\ & ((!\DUT|R.BCDCounter[1][2]~q\) # (\DUT|R.BCDCounter[1][0]~q\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001111001111001100111100111111111111111100111111111111110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \DUT|ALT_INV_R.BCDCounter[1][3]~q\,
	datac => \DUT|ALT_INV_R.BCDCounter[1][0]~q\,
	datad => \DUT|ALT_INV_R.BCDCounter[1][2]~q\,
	dataf => \DUT|ALT_INV_R.BCDCounter[1][1]~DUPLICATE_q\,
	combout => \DUT|Mux7~0_combout\);

-- Location: FF_X85_Y2_N16
\DUT|R.Hex[1][6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|Mux7~0_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.Strobe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.Hex[1][6]~q\);

-- Location: LABCELL_X88_Y9_N51
\DUT|R.Hex[2][0]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|R.Hex[2][0]~feeder_combout\ = VCC

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \DUT|R.Hex[2][0]~feeder_combout\);

-- Location: FF_X88_Y9_N52
\DUT|R.Hex[2][0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|R.Hex[2][0]~feeder_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.Strobe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.Hex[2][0]~q\);

-- Location: LABCELL_X88_Y15_N15
\DUT|R.Hex[2][1]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|R.Hex[2][1]~feeder_combout\ = VCC

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \DUT|R.Hex[2][1]~feeder_combout\);

-- Location: FF_X88_Y15_N16
\DUT|R.Hex[2][1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|R.Hex[2][1]~feeder_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.Strobe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.Hex[2][1]~q\);

-- Location: LABCELL_X88_Y15_N18
\DUT|R.Hex[2][2]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|R.Hex[2][2]~feeder_combout\ = VCC

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \DUT|R.Hex[2][2]~feeder_combout\);

-- Location: FF_X88_Y15_N19
\DUT|R.Hex[2][2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|R.Hex[2][2]~feeder_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.Strobe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.Hex[2][2]~q\);

-- Location: LABCELL_X88_Y15_N27
\DUT|R.Hex[2][3]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|R.Hex[2][3]~feeder_combout\ = VCC

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \DUT|R.Hex[2][3]~feeder_combout\);

-- Location: FF_X88_Y15_N28
\DUT|R.Hex[2][3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|R.Hex[2][3]~feeder_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.Strobe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.Hex[2][3]~q\);

-- Location: LABCELL_X88_Y15_N33
\DUT|R.Hex[2][4]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|R.Hex[2][4]~feeder_combout\ = VCC

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \DUT|R.Hex[2][4]~feeder_combout\);

-- Location: FF_X88_Y15_N34
\DUT|R.Hex[2][4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|R.Hex[2][4]~feeder_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.Strobe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.Hex[2][4]~q\);

-- Location: LABCELL_X88_Y15_N30
\DUT|R.Hex[2][5]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|R.Hex[2][5]~feeder_combout\ = VCC

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \DUT|R.Hex[2][5]~feeder_combout\);

-- Location: FF_X88_Y15_N31
\DUT|R.Hex[2][5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|R.Hex[2][5]~feeder_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.Strobe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.Hex[2][5]~q\);

-- Location: LABCELL_X88_Y15_N3
\DUT|R.Hex[3][0]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|R.Hex[3][0]~feeder_combout\ = VCC

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \DUT|R.Hex[3][0]~feeder_combout\);

-- Location: FF_X88_Y15_N4
\DUT|R.Hex[3][0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|R.Hex[3][0]~feeder_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.Strobe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.Hex[3][0]~q\);

-- Location: LABCELL_X88_Y15_N6
\DUT|R.Hex[3][1]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|R.Hex[3][1]~feeder_combout\ = VCC

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \DUT|R.Hex[3][1]~feeder_combout\);

-- Location: FF_X88_Y15_N7
\DUT|R.Hex[3][1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|R.Hex[3][1]~feeder_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.Strobe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.Hex[3][1]~q\);

-- Location: MLABCELL_X84_Y2_N18
\DUT|R.Hex[3][2]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|R.Hex[3][2]~feeder_combout\ = VCC

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \DUT|R.Hex[3][2]~feeder_combout\);

-- Location: FF_X84_Y2_N19
\DUT|R.Hex[3][2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|R.Hex[3][2]~feeder_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.Strobe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.Hex[3][2]~q\);

-- Location: MLABCELL_X87_Y6_N12
\DUT|R.Hex[3][3]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|R.Hex[3][3]~feeder_combout\ = VCC

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \DUT|R.Hex[3][3]~feeder_combout\);

-- Location: FF_X87_Y6_N13
\DUT|R.Hex[3][3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|R.Hex[3][3]~feeder_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.Strobe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.Hex[3][3]~q\);

-- Location: LABCELL_X88_Y15_N21
\DUT|R.Hex[3][4]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|R.Hex[3][4]~feeder_combout\ = VCC

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \DUT|R.Hex[3][4]~feeder_combout\);

-- Location: FF_X88_Y15_N22
\DUT|R.Hex[3][4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|R.Hex[3][4]~feeder_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.Strobe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.Hex[3][4]~q\);

-- Location: LABCELL_X88_Y9_N6
\DUT|R.Hex[3][5]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|R.Hex[3][5]~feeder_combout\ = VCC

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \DUT|R.Hex[3][5]~feeder_combout\);

-- Location: FF_X88_Y9_N7
\DUT|R.Hex[3][5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|R.Hex[3][5]~feeder_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.Strobe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.Hex[3][5]~q\);

-- Location: LABCELL_X88_Y15_N39
\DUT|R.Hex[4][0]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|R.Hex[4][0]~feeder_combout\ = VCC

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \DUT|R.Hex[4][0]~feeder_combout\);

-- Location: FF_X88_Y15_N40
\DUT|R.Hex[4][0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|R.Hex[4][0]~feeder_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.Strobe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.Hex[4][0]~q\);

-- Location: LABCELL_X88_Y15_N42
\DUT|R.Hex[4][1]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|R.Hex[4][1]~feeder_combout\ = VCC

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \DUT|R.Hex[4][1]~feeder_combout\);

-- Location: FF_X88_Y15_N43
\DUT|R.Hex[4][1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|R.Hex[4][1]~feeder_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.Strobe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.Hex[4][1]~q\);

-- Location: LABCELL_X88_Y15_N45
\DUT|R.Hex[4][2]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|R.Hex[4][2]~feeder_combout\ = VCC

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \DUT|R.Hex[4][2]~feeder_combout\);

-- Location: FF_X88_Y15_N46
\DUT|R.Hex[4][2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|R.Hex[4][2]~feeder_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.Strobe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.Hex[4][2]~q\);

-- Location: MLABCELL_X87_Y6_N9
\DUT|R.Hex[4][3]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|R.Hex[4][3]~feeder_combout\ = VCC

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \DUT|R.Hex[4][3]~feeder_combout\);

-- Location: FF_X87_Y6_N10
\DUT|R.Hex[4][3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|R.Hex[4][3]~feeder_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.Strobe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.Hex[4][3]~q\);

-- Location: LABCELL_X88_Y15_N0
\DUT|R.Hex[4][4]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|R.Hex[4][4]~feeder_combout\ = VCC

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \DUT|R.Hex[4][4]~feeder_combout\);

-- Location: FF_X88_Y15_N1
\DUT|R.Hex[4][4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|R.Hex[4][4]~feeder_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.Strobe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.Hex[4][4]~q\);

-- Location: LABCELL_X88_Y15_N24
\DUT|R.Hex[4][5]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|R.Hex[4][5]~feeder_combout\ = VCC

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \DUT|R.Hex[4][5]~feeder_combout\);

-- Location: FF_X88_Y15_N25
\DUT|R.Hex[4][5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|R.Hex[4][5]~feeder_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.Strobe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.Hex[4][5]~q\);

-- Location: LABCELL_X88_Y15_N48
\DUT|R.Hex[5][0]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|R.Hex[5][0]~feeder_combout\ = VCC

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \DUT|R.Hex[5][0]~feeder_combout\);

-- Location: FF_X88_Y15_N49
\DUT|R.Hex[5][0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|R.Hex[5][0]~feeder_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.Strobe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.Hex[5][0]~q\);

-- Location: LABCELL_X88_Y15_N51
\DUT|R.Hex[5][1]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|R.Hex[5][1]~feeder_combout\ = VCC

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \DUT|R.Hex[5][1]~feeder_combout\);

-- Location: FF_X88_Y15_N53
\DUT|R.Hex[5][1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|R.Hex[5][1]~feeder_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.Strobe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.Hex[5][1]~q\);

-- Location: LABCELL_X88_Y15_N54
\DUT|R.Hex[5][2]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|R.Hex[5][2]~feeder_combout\ = VCC

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \DUT|R.Hex[5][2]~feeder_combout\);

-- Location: FF_X88_Y15_N55
\DUT|R.Hex[5][2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|R.Hex[5][2]~feeder_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.Strobe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.Hex[5][2]~q\);

-- Location: LABCELL_X88_Y15_N57
\DUT|R.Hex[5][3]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|R.Hex[5][3]~feeder_combout\ = VCC

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \DUT|R.Hex[5][3]~feeder_combout\);

-- Location: FF_X88_Y15_N58
\DUT|R.Hex[5][3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|R.Hex[5][3]~feeder_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.Strobe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.Hex[5][3]~q\);

-- Location: LABCELL_X88_Y9_N27
\DUT|R.Hex[5][4]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|R.Hex[5][4]~feeder_combout\ = VCC

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \DUT|R.Hex[5][4]~feeder_combout\);

-- Location: FF_X88_Y9_N28
\DUT|R.Hex[5][4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|R.Hex[5][4]~feeder_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.Strobe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.Hex[5][4]~q\);

-- Location: LABCELL_X88_Y15_N36
\DUT|R.Hex[5][5]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \DUT|R.Hex[5][5]~feeder_combout\ = VCC

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \DUT|R.Hex[5][5]~feeder_combout\);

-- Location: FF_X88_Y15_N37
\DUT|R.Hex[5][5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \iClk~inputCLKENA0_outclk\,
	d => \DUT|R.Hex[5][5]~feeder_combout\,
	clrn => \inResetAsync~input_o\,
	ena => \DUT|R.Strobe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DUT|R.Hex[5][5]~q\);

-- Location: LABCELL_X30_Y32_N0
\~QUARTUS_CREATED_GND~I\ : cyclonev_lcell_comb
-- Equation(s):

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
;
END structure;



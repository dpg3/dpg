----------------------------------------------------------------------------------------------------------------
-- Workfile:    databuffer-Rtl.vhd
-- Author:      Tobias Kerbl
----------------------------------------------------------------------------------------------------------------
-- Description: architecture for the databuffer entity
----------------------------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

architecture Rtl of tbd_visualizer is
    signal SevenSeg0 : std_ulogic_vector(6 downto 0);
    signal SevenSeg1 : std_ulogic_vector(6 downto 0);
    signal SevenSeg2 : std_ulogic_vector(6 downto 0);
    signal SevenSeg3 : std_ulogic_vector(6 downto 0);
    signal SevenSeg4 : std_ulogic_vector(6 downto 0);
    signal SevenSeg5 : std_ulogic_vector(6 downto 0);
begin
    DUT : entity work.visualizer
          port map(
              clk               => iClk,    
              reset_n           => inResetAsync,
              resetCounter      => SW(0),
              incCounterByOne   => SW(1),
              setAllLedsOn      => SW(2),
              setAllLedsOff     => SW(3),
              setLightChaserOn  => SW(4),
              oleds             => LEDR,
              ohex0             => SevenSeg0,
              ohex1             => SevenSeg1,
              ohex2             => SevenSeg2,
              ohex3             => SevenSeg3,
              ohex4             => SevenSeg4,
              ohex5             => SevenSeg5
        );

    -- Sevensegmentdisplays of DE1-SOC are low active
    HEX0 <= not SevenSeg0;
    HEX1 <= not SevenSeg1;
    HEX2 <= not SevenSeg2;
    HEX3 <= not SevenSeg3;
    HEX4 <= not SevenSeg4;
    HEX5 <= not SevenSeg5;

end architecture Rtl;
----------------------------------------------------------------------------------------------------------------
-- Workfile:    tbd_visualizer-e.vhd
-- Author:      Tobias Kerbl
----------------------------------------------------------------------------------------------------------------
-- Description: Tbd for visualizer unit
----------------------------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Databuffer library
library work; 
use work.pkg_visualizer.all;

entity tbd_visualizer is
    port (
        -- General
        iClk            : in  std_ulogic;
        inResetAsync    : in  std_ulogic;

        -- Control Inputs
        SW              : in std_ulogic_vector(4 downto 0);

        -- Visualization Outputs
        LEDR            : out std_ulogic_vector(9 downto 0);
        HEX0            : out std_ulogic_vector(6 downto 0);
        HEX1            : out std_ulogic_vector(6 downto 0);
        HEX2            : out std_ulogic_vector(6 downto 0);
        HEX3            : out std_ulogic_vector(6 downto 0);
        HEX4            : out std_ulogic_vector(6 downto 0);
        HEX5            : out std_ulogic_vector(6 downto 0)
    );
begin
end entity tbd_visualizer;
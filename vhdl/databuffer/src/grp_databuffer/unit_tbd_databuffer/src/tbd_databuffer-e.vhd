----------------------------------------------------------------------------------------------------------------
-- Workfile:    tbd_databuffer-e.vhd
-- Author:      Tobias Kerbl
----------------------------------------------------------------------------------------------------------------
-- Description: testbed for databuffer - entity
----------------------------------------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Databuffer library
library work; 
use work.pkg_databuffer.all;

entity tbd_databuffer is
    generic (
        gAddressWidth       : natural := cAddressWidth;
        gAvalonMMDataWidth  : natural := 16;
        gAvalonSTDataWidth  : natural := cAvalonSTDataWidth;
        gLengthOfRAM        : natural := cLengthOfRAM
    );
    port (
        -- General
        clk              : in  std_logic;
        reset_n          : in  std_logic;

        -- Avalon MM Slave
        address          : in  std_logic_vector(gAddressWidth-1 downto 0);
        write            : in  std_logic;
        writedata        : in  std_logic_vector(gAvalonMMDataWidth-1 downto 0);
        read             : in  std_logic;
        waitrequest      : out std_logic;
        readdata         : out std_logic_vector(gAvalonMMDataWidth-1 downto 0) := (others => '0');

        -- Avalon ST Source
        readySrc         : in  std_logic;
        dataSrc          : out std_logic_vector(gAvalonSTDataWidth-1 downto 0);
        validSrc         : out std_logic;
        startofpacketSrc : out std_logic;
        endofpacketSrc   : out std_logic;

        -- Avalon ST Sink
        dataSink         : in  std_logic_vector(gAvalonSTDataWidth-1 downto 0);
        validSink        : in  std_logic;
        readySink        : out std_logic
    );
begin

end entity tbd_databuffer;
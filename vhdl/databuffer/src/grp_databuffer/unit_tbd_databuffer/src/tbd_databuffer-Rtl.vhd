----------------------------------------------------------------------------------------------------------------
-- Workfile:    tbd_databuffer-Rtl.vhd
-- Author:      Tobias Kerbl
----------------------------------------------------------------------------------------------------------------
-- Description: testbed for databuffer - entity
----------------------------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Databuffer library
library work; 
use work.pkg_databuffer.all;

architecture Rtl of tbd_databuffer is
begin

    DUT : entity work.databuffer
        generic map (
            gAddressWidth       => gAddressWidth,
            gAvalonMMDataWidth  => gAvalonMMDataWidth,
            gAvalonSTDataWidth  => gAvalonSTDataWidth,
            gLengthOfRAM        => gLengthOfRAM
        )
        port map (
            clk                 => clk,          
            reset_n             => reset_n,         
            address             => address,         
            write               => write,          
            writedata           => writedata,        
            read                => read,
            waitrequest         => waitrequest,
            readdata            => readdata,         
            readySrc            => readySrc,       
            dataSrc             => dataSrc,         
            validSrc            => validSrc,        
            startofpacketSrc    => startofpacketSrc,
            endofpacketSrc      => endofpacketSrc,   
            dataSink            => dataSink,        
            validSink           => validSink,
            readySink           => readySink        
        );

end architecture Rtl;



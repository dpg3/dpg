----------------------------------------------------------------------------------------------------------------
-- Workfile:    pkg_databuffer.vhd
-- Author:      Tobias Kerbl
----------------------------------------------------------------------------------------------------------------
-- Description: This package contains all constants, functions and definitions for the 'Databuffer'. 
--              It also contains constants for the Simulation via UVVM (BFMs, etc.)
----------------------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------
-- Libraries
--------------------------------------------------------------------------
-- general IEEE libraries
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

--------------------------------------------------------------------------
-- Package
--------------------------------------------------------------------------

package pkg_gitrevision is

    -- Type constants
    constant cBitWidthWord      : natural := 32;
    constant cGitRevisionLength : natural := 6;

    -- Types
    type aGitRevision is array(cGitRevisionLength-1 downto 0) of unsigned(cBitWidthWord-1 downto 0);

    -- General constants
    constant cGitRevision : aGitRevision := (0 => x"DEADBEEF", 1 => x"FEEDC0DE", others => (others => '0'));

end pkg_gitrevision;

-- package body
package body pkg_gitrevision is
-- currently nothing to declare here
end pkg_gitrevision;

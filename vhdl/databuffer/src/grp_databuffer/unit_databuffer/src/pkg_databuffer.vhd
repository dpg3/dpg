----------------------------------------------------------------------------------------------------------------
-- Workfile:    pkg_databuffer.vhd
-- Author:      Tobias Kerbl
----------------------------------------------------------------------------------------------------------------
-- Description: This package contains all constants, functions and definitions for the 'Databuffer'. 
--              It also contains constants for the Simulation via UVVM (BFMs, etc.)
----------------------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------
-- Libraries
--------------------------------------------------------------------------
-- general IEEE libraries
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

--------------------------------------------------------------------------
-- Package
--------------------------------------------------------------------------

package pkg_databuffer is

    ----------------------------------------
    -- Constants
    ----------------------------------------

    -- Clock
    ------------------------------------
    constant cClkPeriod          : time    := 10 ns; -- 100 MHz
    constant cClkGen             : natural := 1;

    -- Avalon Interfaces
    ------------------------------------
    constant cAddressWidth       : natural := 12;
    constant cAvalonMMDataWidth  : natural := 32;
    constant cAvalonSTDataWidth  : natural := 8;

    -- Buffer elements
    ------------------------------------
    constant cNrOfLogicalBuffer  : natural := 2;

    -- Strobe generation - internal
    ------------------------------------
    constant cClkCycPerStrobeCyc : natural := 50E6 / (1 sec / 100 us);

    -- RAM-Block
    ------------------------------------
    constant cLengthOfRAM        : natural := 2560; -- 10KB RAM (Words)

    constant cTestLength : natural := 8;

    -- RAM:
    -- Array of values each reflecting the length of the pulse
    -- constant cRAMSize : natural := (2**16)/4;
    -- subtype aRAMAddress is natural range 0 to cRAMSize-1;

    -- only for ram-inference tests
    subtype aByteTest is unsigned(cAvalonSTDataWidth-1 downto 0);
    type aWordTest is array ((cAvalonMMDataWidth / cAvalonSTDataWidth)-1 downto 0) of aByteTest;
    type aRAMBlockTest is array (cLengthOfRAM-1 downto 0) of aWordTest;

    subtype aBufferCount is natural range 1 to 2;

    constant cNrBufferOne : aBufferCount := 1;
    constant cNrBufferTwo : aBufferCount := 2;
 
    subtype Range_LSB        is natural range 7 downto 0;
    subtype Range_SecondByte is natural range 15 downto 8;
    subtype Range_ThirdByte  is natural range 23 downto 16;
    subtype Range_MSB        is natural range 31 downto 24;

    constant LSB        : natural := 0;
    constant SecondByte : natural := 1;
    constant ThirdByte  : natural := 2;
    constant MSB        : natural := 3;

    -- Memory Mapping
    -----------------------------------
    -- GitRevision       : #0 --> r
    -- TxStart           : #8 --> rw
    -- TxTransfer        : #9 --> r
    -- TxData            : #10 --> rw
    -- TxCount           : #11 --> r
    -- RxData            : #12 --> r
    -- NrRemainingSpace  : #13 --> r
    -- IRQMask           : #14 --> rw
    -- IRQStatus         : #15 --> rw

    -- Transfer Control Register
    constant cNrGitRevision     : natural := 0;
    constant cNrRxCount         : natural := 7;
    constant cNrTxStart         : natural := 8;
    constant cNrTxTransfer      : natural := 9;
    constant cNrTxData          : natural := 10;
    constant cNrTxCount         : natural := 11;
    constant cNrRxData          : natural := 12;
    constant cNrRemainingSpace  : natural := 13;

    -- Interrupt Control Regsiter
    constant cNrIRQMask         : natural := 14;
    constant cNrIRQStatus       : natural := 15;

end pkg_databuffer;

-- package body
package body pkg_databuffer is
-- currently nothing to declare here
end pkg_databuffer;

----------------------------------------------------------------------------------------------------------------
-- Workfile:    databuffer-e.vhd
-- Author:      Tobias Kerbl
----------------------------------------------------------------------------------------------------------------
-- Description: Entity for the databuffer
----------------------------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Databuffer library
library work; 
use work.pkg_databuffer.all;
use work.pkg_visualizer.all;

entity databuffer is
    generic (
        gAddressWidth       : natural := cAddressWidth;
        gAvalonMMDataWidth  : natural := cAvalonMMDataWidth;
        gAvalonSTDataWidth  : natural := cAvalonSTDataWidth;
        gLengthOfRAM        : natural := cLengthOfRAM
    );
    port (
        -- General
        clk              : in  std_logic;
        reset_n          : in  std_logic;

        -- Avalon MM Slave
        address          : in  std_logic_vector(gAddressWidth-1 downto 0);
        write            : in  std_logic;
        writedata        : in  std_logic_vector(gAvalonMMDataWidth-1 downto 0);
        read             : in  std_logic;
        waitrequest      : out std_logic;
        readdata         : out std_logic_vector(gAvalonMMDataWidth-1 downto 0) := (others => '0');
        readdatavalid    : out std_logic;

        -- Avalon ST Source
        readySrc         : in  std_logic;
        dataSrc          : out std_logic_vector(gAvalonSTDataWidth-1 downto 0);
        validSrc         : out std_logic;
        startofpacketSrc : out std_logic;
        endofpacketSrc   : out std_logic;

        -- Avalon ST Sink
        dataSink         : in  std_logic_vector(gAvalonSTDataWidth-1 downto 0);
        validSink        : in  std_logic;
        readySink        : out std_logic;

        -- Avalon Interrupt
        dataAvailable    : out std_logic;

        -- Avalon Conduit
        leds             : out std_logic_vector(cNrOfLeds-1 downto 0);
        hex0             : out std_logic_vector(cNrOfHexSegments-1 downto 0);
        hex1             : out std_logic_vector(cNrOfHexSegments-1 downto 0);
        hex2             : out std_logic_vector(cNrOfHexSegments-1 downto 0);
        hex3             : out std_logic_vector(cNrOfHexSegments-1 downto 0);
        hex4             : out std_logic_vector(cNrOfHexSegments-1 downto 0);
        hex5             : out std_logic_vector(cNrOfHexSegments-1 downto 0)
    );
begin
    -- generic assertions
    assert(gAvalonMMDataWidth > gAvalonSTDataWidth)
    report("Cannot instantiate entity -> avalon memory mapped data width needs to be bigger than the avalon streaming data width!")
    severity failure;

    assert(gAvalonMMDataWidth mod gAvalonSTDataWidth = 0)
    report("Cannot instantiate entity -> avalon memory mapped data width needs to be multiple the avalon streaming data width!")
    severity failure;

end entity databuffer;

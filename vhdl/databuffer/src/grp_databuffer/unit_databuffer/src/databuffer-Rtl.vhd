----------------------------------------------------------------------------------------------------------------
-- Workfile:    databuffer-Rtl.vhd
-- Author:      Tobias Kerbl
----------------------------------------------------------------------------------------------------------------
-- Description: architecture for the databuffer entity
----------------------------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Databuffer library
library work; 
use work.pkg_databuffer.all;
use work.pkg_gitrevision.all;

architecture Rtl of databuffer is

    ----------------------------------------------------------
    -- Types
    ----------------------------------------------------------
    -- Buffer States:
    -- HPSTransfer: indicates that the buffer is can be read or written
    -- SPITransfer: indicates that the buffer is processed currently - no read/write possible
    type aBufferState   is (HPSTransfer, SPITransfer);
    
    type aHPSReadState  is (HpsRead);
    type aHPSWriteState is (HpsWrite, ReadyForSpiTransfer);

    type aSPIWriteState is (Idle, SpiRead, SpiSend, ReadyForHpsTransfer);
    type aSPIReadState  is (SpiReceive, ReadyForHpsTransfer);

    -- types for data-packets
    subtype aByte       is unsigned(gAvalonSTDataWidth-1 downto 0);
    type aWord          is array ((gAvalonMMDataWidth / gAvalonSTDataWidth)-1 downto 0) of aByte;
    type aRAMBlock      is array (gLengthOfRAM-1 downto 0) of aWord;

    subtype aBufferWordAddr is natural range 0 to gLengthOfRAM-1;
    subtype aBufferByteAddr is natural range 0 to (gLengthOfRAM*4)-1;

    type aBufferRegSet is record
        -- Buffer One: Tx
        TxWrAddr     : aBufferWordAddr;
        TxNextWrAddr : aBufferWordAddr;
        TxWr         : std_ulogic;
        TxRdAddr     : aBufferByteAddr;
        TxRdData     : aByte;

        -- Buffer One: Rx
        RxWrAddr     : aBufferByteAddr;
        RxNextWrAddr : aBufferByteAddr;
        RxWr         : std_ulogic;
        RxWrData     : aByte;
        RxRdAddr     : aBufferWordAddr;
        RxRdData     : aWord;
    end record;

    type aBufferSet is array (aBufferCount) of aBufferRegSet;

    -- Register Set
    type aRegSet is record
        -- Internal Buffer States
        BufferOneState        : aBufferState;      -- indicates the current state of the HPS buffer
        BufferTwoState        : aBufferState;      -- indicates the current state of the SPI buffer

        HPSReadState          : aHPSReadState;     -- indicates the current state of the HPS read transfer
        HPSWriteState         : aHPSWriteState;    -- indicates the current state of the HPS write transfer
        SPISendState          : aSPIWriteState;    -- indicates the current state of the SPI (master) write transfer
        SPIReceiveState       : aSPIReadState;     -- indicates the current state of the SPI (read) transfer

        WaitRequest           : std_ulogic;
        ReadySink             : std_ulogic;
        DataAvailableIRQ      : std_ulogic;

        Buffers               : aBufferSet;
        
        -- Register the output for avalon mm read
        ReadDataOut           : std_ulogic_vector(gAvalonMMDataWidth-1 downto 0);
        ReadDataValid         : std_ulogic;

        -- Interrupt Control Register
        InterruptMask         : std_ulogic;                                  -- rw - for enabling/disabling the interrupt
        InterruptStatus       : std_ulogic;                                  -- rw - for checking the irq state and resetting the interrupt

        -- Transfer Control Register
        TxStart               : std_ulogic;                                  -- rw - indicates, if the data transfer should start
        TxTransfer            : std_ulogic;                                  -- r  - indicates, if the data is currently processed by the fpga
        TxData                : aWord;                                       -- rw - a new word, which needs to be stored in the ram
        TxCount               : unsigned(gAvalonMMDataWidth-1 downto 0);     -- r  - indicates the current amount of words stored inside the ram
        -- TODO: return current readpointer as integer

        -- Visualizer Control Register (internal only)
        ResetCounter          : std_ulogic;
        IncCounterByOne       : std_ulogic;
        SetAllLedsOn          : std_ulogic;
        SetAllLedsOff         : std_ulogic;
        SetLightChaserOn      : std_ulogic;

        -- Debug Led Setting - Overview:
        -----------------------------------------------------------------
        -- #0: BufferOne - State: HPSTransfer
        -- #1: entered Avalon MM Interface procedure     - general
        -- #2: entered Avalon MM interface procedure     - read
        -- #3: entered Avalon MM interface procedure     - write
        -- #4: entered HPSWrite - when write bit is high - general
        -- #5: entered HPSWrite - when write bit is high - TxData found
        -- #6: entered HPSRead  - when read bit is high  - general
        -- #7: entered HPSRead  - when read bit is high  - RxData found
        -- #8: entered SPIWrite - when readySrc  = 1
        -- #9: entered SPIRead  - when validSink = 1
        LedsDebug : std_logic_vector(cNrOfLeds-1 downto 0);
        
    end record aRegSet;

    ----------------------------------------------------------
    -- constants
    ----------------------------------------------------------
    -- indentifies how many st-bytes are in the mm data width
    constant cNumberOfBytes : natural := gAvalonMMDataWidth / gAvalonSTDataWidth;

    constant cInitialBufferRegSet : aBufferRegSet := (
        TxWrAddr     => 0,
        TxNextWrAddr => 0,
        TxWr         => '0',
        TxRdAddr     => 0,
        TxRdData     => (others => '0'),

        RxWrAddr     => 0,
        RxNextWrAddr => 0,
        RxWr         => '0',
        RxWrData     => (others => '0'),
        RxRdAddr     => 0,
        RxRdData     => (others => (others => '0'))
    );

    constant cInitialRegSet : aRegSet := (
        -- Internal Buffer States
        BufferOneState        => HPSTransfer,
        BufferTwoState        => SPITransfer,
        HPSReadState          => HpsRead,
        HPSWriteState         => HpsWrite,
        SPISendState          => Idle,
        SPIReceiveState       => ReadyForHpsTransfer,

        WaitRequest           => '1',
        ReadySink             => '0',
        DataAvailableIRQ      => '0',

        Buffers               => (others => cInitialBufferRegSet),

        ReadDataOut           => (others => '0'),
        ReadDataValid         => '0',

        -- Interrupt Control Register
        InterruptMask         => '0',
        InterruptStatus       => '0',

        -- Transfer Control Register
        TxStart               => '0',
        TxTransfer            => '0',
        TxData                => (others => (others => '0')),
        TxCount               => (others => '0'),

        -- Visualizer Control Register (internal only)
        ResetCounter          => '0',
        IncCounterByOne       => '0',
        SetAllLedsOn          => '0',
        SetAllLedsOff         => '0',
        SetLightChaserOn      => '0', 

        LedsDebug => (others => '0')
    );

    ----------------------------------------------------------
    -- Signals
    ----------------------------------------------------------
    signal R, NextR : aRegSet;

    -- Buffer One
    signal ReadRAMTxOne : aByte;
    signal ReadRAMRxOne : aWord;

    -- Buffer Two
    signal ReadRAMTxTwo : aByte;
    signal ReadRAMRxTwo : aWord;

    ----------------------------------------------------------
    -- RAM
    ----------------------------------------------------------
    -- You want to read the old value when the same address is read as
    -- is written on a given clock cycle. To ensure this a RAM wait
    -- state may be used. It would be possible to interleave RAM reading
    -- and use of the read value to save a clock cycle, but this would
    -- need more area and doesn’t make much sense, 
    -- Possible values: MLAB, M10K, logic
    attribute ramstyle : string;

    signal RamTxOne : aRAMBlock;
    attribute ramstyle of RAMTxOne : signal is "M10K";
    
    signal RamRxOne : aRAMBlock;
    attribute ramstyle of RAMRxOne : signal is "M10K";

    signal RamTxTwo : aRAMBlock;
    attribute ramstyle of RAMTxTwo : signal is "M10K";

    signal RamRxTwo : aRAMBlock;
    attribute ramstyle of RAMRxTwo : signal is "M10K";

begin

    --**************************************************************************
    -- Registers
    --**************************************************************************
    Registers: process(clk, reset_n) is
    begin
        if(reset_n = not '1') then
            R <= cInitialRegSet;
        elsif rising_edge(clk) then
            R <= NextR;
        end if;
    end process Registers;

    --**************************************************************************
    -- RAM-Interfaces
    --**************************************************************************
    -- TX RAMs
    BufferOneTX: process(clk, R) is
    begin
        if(rising_edge(clk)) then
            if(R.Buffers(cNrBufferOne).TxWr = '1') then
                RamTxOne(R.Buffers(cNrBufferOne).TxWrAddr) <= R.TxData;
            end if;
            ReadRAMTxOne <= RamTxOne(R.Buffers(cNrBufferOne).TxRdAddr / 4)(R.Buffers(cNrBufferOne).TxRdAddr mod 4);
        end if;
    end process BufferOneTX;

    BufferTwoTX: process(clk, R) is
    begin
        if(rising_edge(clk)) then
            if(R.Buffers(cNrBufferTwo).TxWr = '1') then
                RamTxTwo(R.Buffers(cNrBufferTwo).TxWrAddr) <= R.TxData;
            end if;
            ReadRAMTxTwo <= RamTxTwo(R.Buffers(cNrBufferTwo).TxRdAddr / 4)(R.Buffers(cNrBufferTwo).TxRdAddr mod 4);
        end if;
    end process BufferTwoTX;

    -- RX RAMs
    BufferOneRX: process(clk, R) is
    begin
        if(rising_edge(clk)) then
            if(R.Buffers(cNrBufferOne).RxWr = '1') then
                RamRxOne(R.Buffers(cNrBufferOne).RxWrAddr / 4)(R.Buffers(cNrBufferOne).RxWrAddr mod 4) <= R.Buffers(cNrBufferOne).RxWrData;
            end if;
        end if;
        ReadRAMRxOne <= RamRxOne(R.Buffers(cNrBufferOne).RxRdAddr);
    end process BufferOneRX;

    BufferTwoRX: process(clk, R) is
    begin
        if(rising_edge(clk)) then
            if(R.Buffers(cNrBufferTwo).RxWr = '1') then
                RamRxTwo(R.Buffers(cNrBufferTwo).RxWrAddr / 4)(R.Buffers(cNrBufferTwo).RxWrAddr mod 4) <= R.Buffers(cNrBufferTwo).RxWrData;
            end if;
        end if;
        ReadRAMRxTwo <= RamRxTwo(R.Buffers(cNrBufferTwo).RxRdAddr);
    end process BufferTwoRX;

    --**************************************************************************
    -- Combinatorial
    --**************************************************************************
    Comb: process(R, address, write, writedata, read, readySrc, dataSink, validSink, ReadRAMRxOne, ReadRAMRxTwo, ReadRAMTxOne, ReadRAMTxTwo) is
        -----------------------------------------------
        -- Avalon MM Mapped
        -----------------------------------------------
        -- Note: Handles the read and write of the avalon memory mapped interface,
        --       which are not handeld inside the fsmds
        -- Memory Mapping
        -----------------------------------
        -- GitRevision       : #0 --> r
        -- RxCount           : #7 --> r
        -- TxStart           : #8 --> rw
        -- TxTransfer        : #9 --> r
        -- TxData            : #10 --> rw
        -- TxCount           : #11 --> r
        -- RxData            : #12 --> r
        -- NrRemainingSpace  : #13 --> r
        -- IRQMask           : #14 --> rw
        -- IRQStatus         : #15 --> rw

        procedure AvalonMMInterface is
            variable vAddress : natural := 0;
        begin
            vAddress := to_integer(unsigned(address));

            -- read
            -------------------------------------------------------------
            if(read = '1' and R.WaitRequest /= '1') then

                NextR.ReadDataValid <= '1';

                -- GitRevision
                if(vAddress >= cNrGitRevision and vAddress < cNrGitRevision + cGitRevisionLength) then
                    NextR.ReadDataOut <= std_ulogic_vector(cGitRevision(vAddress - cNrGitRevision));
                -- TxStart
                elsif(vAddress = cNrTxStart) then
                    NextR.ReadDataOut(readdata'low) <= R.TxStart;
                    NextR.ReadDataOut(readdata'high downto readdata'low+1) <=  (others => '0');
                -- TxTransfer
                elsif(vAddress = cNrTxTransfer) then
                    NextR.ReadDataOut(readdata'low) <= R.TxTransfer;
                    NextR.ReadDataOut(readdata'high downto readdata'low+1) <=  (others => '0');
                -- TxData
                elsif(vAddress = cNrTxData) then
                    for k in 1 to cNumberOfBytes loop
                        -- dynamically set/increment range in readdata
                        -- e.g. ST Width = 8 Bit
                        -- 1*8-1 = 7 & 1*8-8 = 0 -> 7 downto 0
                        NextR.ReadDataOut((k*gAvalonSTDataWidth)-1 downto (k*gAvalonSTDataWidth)-gAvalonSTDataWidth) <= std_ulogic_vector(R.TxData(k-1)); -- index starts with 0 but k with 1
                    end loop;
                -- TxCount
                elsif(vAddress = cNrTxCount) then
                    NextR.ReadDataOut <= std_ulogic_vector(R.TxCount);
                -- RxData: (done in BufferOneHPSTransfer or BufferTwoHPSTransfer)
                elsif(vAddress = cNrRemainingSpace) then
                    NextR.ReadDataOut <= std_ulogic_vector(gLengthOfRAM - R.TxCount);                -- InterruptMask:
                elsif(vAddress = cNrIRQMask) then
                    NextR.ReadDataOut(0) <= R.InterruptMask;
                    NextR.ReadDataOut(readdata'high downto readdata'low+1) <=  (others => '0');
                -- InterruptStatus:
                elsif(vAddress = cNrIRQStatus) then
                    NextR.ReadDataOut(0) <= R.InterruptStatus;
                    NextR.ReadDataOut(readdata'high downto readdata'low+1) <=  (others => '0');
                elsif(vAddress = cNrRxCount) then
                    if R.BufferOneState = HPSTransfer then
                        NextR.ReadDataOut <= std_ulogic_vector(to_signed(R.Buffers(cNrBufferOne).RxWrAddr/4 - R.Buffers(cNrBufferOne).RxRdAddr, gAvalonMMDataWidth));
                    else
                        NextR.ReadDataOut <= std_ulogic_vector(to_signed(R.Buffers(cNrBufferTwo).RxWrAddr/4 - R.Buffers(cNrBufferTwo).RxRdAddr, gAvalonMMDataWidth));
                    end if;
                end if;
            end if;
    
            -- write
            -------------------------------------------------------------
            if(write = '1') then

                -- TxStart: (done in BufferOneHPSTransfer or BufferTwoHPSTransfer)
                -- TxData:  (done in BufferOneHPSTransfer or BufferTwoHPSTransfer)
                -- InterruptMask:
                if(vAddress = cNrIRQMask) then
                    NextR.InterruptMask <= writedata(writedata'low);
                -- InterruptStatus:
                elsif(vAddress = cNrIRQStatus) then
                    NextR.InterruptStatus <= writedata(writedata'low);
                    NextR.DataAvailableIRQ <= writedata(writedata'low);
                end if;
            end if;
        end procedure AvalonMMInterface;

        -----------------------------------------------
        -- Buffer Orchestration
        -----------------------------------------------
        procedure SwapBuffers is
            variable vHpsIndex : aBufferCount;
            variable vSpiIndex : aBufferCount;
        begin
            -- swap/set states of the buffers
            --  BufferOne -> SPITransfer
            --  BufferTwo -> HPSTransfer
            if (R.BufferOneState = HPSTransfer) then
                -- set states
                NextR.BufferOneState <= SPITransfer;
                NextR.BufferTwoState <= HPSTransfer;

                vHpsIndex := cNrBufferTwo;
                vSpiIndex := cNrBufferOne;
            else
                NextR.BufferOneState <= HPSTransfer;
                NextR.BufferTwoState <= SPITransfer;

                vHpsIndex := cNrBufferOne;
                vSpiIndex := cNrBufferTwo;
            end if;
           
            -- set pointers
            -- Buffer One -> SPITransfer
            -- DO NOT reset TxWrAddr! (indicates how much bytes should be send)
            NextR.Buffers(vSpiIndex).RxWrAddr     <= 0;
            NextR.Buffers(vSpiIndex).RxNextWrAddr <= 0;
            NextR.Buffers(vSpiIndex).TxRdAddr     <= 0;

            -- Buffer Two -> HPSTransfer
            NextR.Buffers(vHpsIndex).TxWrAddr     <= 0;
            NextR.Buffers(vHpsIndex).TxNextWrAddr <= 0;
            NextR.Buffers(vHpsIndex).RxRdAddr     <= 0;

            -- set the states of the fsmds
            if R.TxCount = 0 then
                NextR.SPISendState    <= Idle;
                NextR.SPIReceiveState <= ReadyForHpsTransfer;
            else
                NextR.SPISendState    <= SpiRead;
                NextR.SPIReceiveState <= SpiReceive;
            end if;
            NextR.HpsWriteState   <= HpsWrite;
            NextR.HpsReadState    <= HpsRead;
            NextR.TxCount         <= (others => '0');
            NextR.TxStart         <= '0';
            NextR.Waitrequest     <= '1';
            
        end procedure SwapBuffers;
        
        -- FSMD: HPS Write
        -------------------------------------------------------------
        procedure HpsWrite (
            constant cBufferNumber : in aBufferCount
            ) is
        begin
            case R.HPSWriteState is
                --  HpsWrite
                -------------------------
                when HpsWrite =>
                    NextR.Buffers(cBufferNumber).TxWr <= '0'; -- reset write bit
                    if(write = '1') then
                        -- TxStart
                        if(to_integer(unsigned(address)) = cNrTxStart) then
                            NextR.TxStart <= writedata(0);
                            if(unsigned(writedata) = 1) then
                                -- change to state "StateChange" to prepare change to SPITransferState
                                NextR.HPSWriteState <= ReadyForSpiTransfer;
                                -- wait so the states can change
                                NextR.Waitrequest <= '1';
                            end if;
                        -- TxData
                        -- TODO: send error signal to user if buffer is full
                        elsif(to_integer(unsigned(address)) = cNrTxData and R.Buffers(cBufferNumber).TxWrAddr /= cLengthOfRAM-1) then
                            for k in 1 to cNumberOfBytes loop
                                -- dynamically set/increment range in writedata
                                -- e.g. ST Width = 8 Bit
                                -- 1*8-1 = 7 & 1*8-8 = 0 -> 7 downto 0
                                NextR.TxData(k-1) <= unsigned(writedata((k*gAvalonSTDataWidth)-1 downto (k*gAvalonSTDataWidth)-gAvalonSTDataWidth)); -- index starts with 0 but k with 1
                            end loop;

                            NextR.Buffers(cBufferNumber).TxWrAddr <= R.Buffers(cBufferNumber).TxNextWrAddr;
                            -- to avoid that the first word will be written twice
                            if(R.WaitRequest = '0') then
                                NextR.Buffers(cBufferNumber).TxNextWrAddr <= R.Buffers(cBufferNumber).TxNextWrAddr + 1;
                            end if;
                            NextR.TxCount <= R.TxCount + 1; -- TODO: Change this
                            NextR.Buffers(cBufferNumber).TxWr <= '1'; -- reset write bit
                        end if;
                    end if;
                    
                --  ReadyForHpsTransfer
                -------------------------
                when ReadyForSpiTransfer => 
                    -- nothing to do here, wait for buffer swap
            end case;
        end procedure HpsWrite;

        procedure HpsRead (
            constant cBufferNumber : in aBufferCount;
            signal   ReadRAM       : in aWord
        ) is
        begin
            -- FSMD: HPS Read
            -------------------------------------------------------------
            case R.HPSReadState is
                --  HpsRead
                -------------------------
                when HpsRead =>
                    if(read = '1' and R.WaitRequest /= '1') then
                        NextR.ReadDataValid <= '1';
                        -- RxData
                        if(to_integer(unsigned(address)) = cNrRxData) then
                            -- check if address pointer for reading is still not out of range
                            if(R.Buffers(cBufferNumber).RxRdAddr < cLengthOfRAM-1) then
                                -- return error if so
                                -- output the data
                                for k in 1 to cNumberOfBytes loop
                                    -- dynamically set/increment range in readdata
                                    -- e.g. ST Width = 8 Bit
                                    -- 1*8-1 = 7 & 1*8-8 = 0 -> 7 downto 0
                                    NextR.ReadDataOut((k*gAvalonSTDataWidth)-1 downto (k*gAvalonSTDataWidth)-gAvalonSTDataWidth) <= std_ulogic_vector(ReadRAM(k-1)); -- index starts with 0 but k with 1
                                end loop;
                                
                                -- Increase the addresspointer
                                -- the data is read all the time so no need for initiating a read
                                -- NextR.WaitRequest <= '1';
                                if(R.WaitRequest = '0') then
                                    NextR.Buffers(cBufferNumber).RxRdAddr <= R.Buffers(cBufferNumber).RxRdAddr + 1;
                                end if;
                            else
                                -- TODO: Think about better error signal
                                -- Indicate last value with own signal or just keep this signal
                                NextR.ReadDataOut <= (others => '0');
                            end if;
                        end if; 
                    end if;
            end case;
        end procedure HpsRead;

        -- FSMD: SPI Write
        -------------------------------------------------------------
        procedure SpiWrite (
            constant cBufferNumber : in aBufferCount;
            signal   ReadRAM       : in aByte
        ) is
        begin
            case R.SPISendState is
                --  Idle
                -------------------------
                when Idle =>
                    -- Do nothing --> just wait until the state gets changed
                    -- Note: This is needed, if you do not want to send anything
                    NextR.LedsDebug(2) <= '1';

                --  SpiRead
                -------------------------    
                when SpiRead =>
                    -- When entering this state, one clock-cycle should be over
                    -- This means that the sink should have been able to read the value.
                    -- Now we do not want to send data, so we have to pull the valid signal to low
                    validSrc <= '0';
                    NextR.SPISendState <= SpiSend;
                    NextR.LedsDebug(1) <= not R.LedsDebug(1);

                --  SpiSend
                -------------------------   
                when SpiSend =>
                    NextR.LedsDebug(4) <= '0';

                    -- We do not need to send something if the TxWrAddr = 0 (means no data to send).
                    -- If so, change to "ready-state"
                    -- if(R.Buffers(cBufferNumber).TxWrAddr = 0) then
                        -- NextR.SPISendState <= ReadyForHpsTransfer;
                    -- end if;

                    -- otherwise, wait for backpressure signal to be ready
                    if(readySrc = '1') then
                        
                        dataSrc <= std_logic_vector(ReadRAM);
                        validSrc <= '1';
                        NextR.TxTransfer <= '1';
                        NextR.SetAllLedsOn <= '1';
                        NextR.IncCounterByOne <= '1';

                        NextR.LedsDebug(8) <= not R.LedsDebug(8);
                        NextR.LedsDebug(3) <= '0';

                        if(R.Buffers(cBufferNumber).TxRdAddr = 0) then
                            startofpacketSrc <= '1'; -- if so, we have are sending the first byte (=> start of the packet)
                        else
                            startofpacketSrc <= '0';
                        end if;
                        
                        -- The TxWrAddr-pointer indicates, how many bytes needs to be sent
                        -- So we need to check if we have reached this pointer (which means to stop sending more bytes)
                        -- Due the fact, that TxRdAddr is 4 times bigger(due byte width) than TxWrAddr, we need to divide it by 4 for comparison
                        -- Note: We need to add the TxWrAddr by one, because otherwise the last 4 bytes will be lost
                        --       E.g. 40 bytes to send -> TxWrAddr = 9 -> TxRdAddr = 36 -> 36 / 4 = 9! TxRdAddr must be 39!
                        if(((R.Buffers(cBufferNumber).TxRdAddr + 1) / 4) = (R.Buffers(cBufferNumber).TxWrAddr + 1)) then
                            -- indicate the end of the packet
                            endofpacketSrc <= '1';
                            NextR.SPISendState <= ReadyForHpsTransfer;
                        elsif((R.Buffers(cBufferNumber).TxRdAddr / 4) /= (R.Buffers(cBufferNumber).TxWrAddr + 1)) then
                            -- After reading we can increment the pointer for the next read
                            NextR.Buffers(cBufferNumber).TxRdAddr <= R.Buffers(cBufferNumber).TxRdAddr + 1;
                            NextR.SPISendState <= SpiRead;                            
                        end if;
                    else
                        NextR.LedsDebug(3) <= '1';
                    end if;

                --  ReadyForHpsTransfer
                -------------------------
                when ReadyForHpsTransfer =>
                    -- endofpacketSrc was high for a cycle
                    -- we can pull it low and change state now
                    NextR.LedsDebug(4) <= '1';
                    endofpacketSrc <= '0';
            end case;
        end procedure SpiWrite;

        procedure SpiRead (
            constant cBufferNumber : in aBufferCount
        ) is
        begin
             -- FSMD: SPI Read
            -------------------------------------------------------------
            case R.SPIReceiveState is
                --  SpiReceive
                -------------------------
                when SpiReceive =>
                    -- check if write signal is already high
                    -- if so, the value is already written and we can change state
                    NextR.Buffers(cBufferNumber).RxWr <= '0';
                    NextR.ReadySink <= '1';

                    if(validSink = '1' and R.ReadySink = '1') then
                        -- initiate transfer
                        NextR.TxTransfer <= '1';
                        NextR.SetAllLedsOn <= '1';

                        NextR.Buffers(cBufferNumber).RxWrAddr <= R.Buffers(cBufferNumber).RxNextWrAddr;
                        NextR.Buffers(cBufferNumber).RxNextWrAddr <= R.Buffers(cBufferNumber).RxNextWrAddr + 1;
                        -- save the data, to ensure it's stable while writing to the ram
                        NextR.Buffers(cBufferNumber).RxWrData <= unsigned(dataSink);
                        NextR.Buffers(cBufferNumber).RxWr <= '1';

                        NextR.LedsDebug(9) <= not R.LedsDebug(9);
                    end if;

                    if(R.Buffers(cBufferNumber).RxWrAddr = R.Buffers(cBufferNumber).TxRdAddr and R.SpiSendState = ReadyForHpsTransfer) then
                        -- if so we, have received the same amount of bytes
                        -- now we can change the state
                        NextR.SPIReceiveState <= ReadyForHpsTransfer;
                        NextR.ReadySink <= '0';
                    end if;

                --  ReadyForHpsTransfer
                -------------------------
                when ReadyForHpsTransfer =>
                    -- nothing to do here, wait for buffer swap
            end case;
        end procedure SpiRead;
        
    --**************************************************************************
    -- Start of Comb-Process
    --**************************************************************************
    begin
        -- setting the defaults, to prevent latches
        ---------------------------------------------
        NextR <= R;
        if R.BufferOneState = HPSTransfer then
            NextR.LedsDebug(0) <= '1'; 
        else
            NextR.LedsDebug(0) <= '0'; 
        end if;
        NextR.LedsDebug(2) <= '0';


        -- deassert the waitrequest by default,
        -- assert it if needed
        NextR.WaitRequest      <= '0';

        -- pull all visualizer cmd lines to low by default
        NextR.ResetCounter     <= '0';
        NextR.IncCounterByOne  <= '0';
        NextR.SetAllLedsOff    <= '0';

        NextR.ReadDataValid    <= '0';
        
        -- pull all outputs to low by default
        NextR.ReadDataOut      <= (others => '0');
        validSrc               <= '0';
        startofpacketSrc       <= '0';
        endofpacketSrc         <= '0';
        dataSrc                <= (others => '0');

        -- Avalon MM interface handling
        ---------------------------------------------
        AvalonMMInterface;

        -- Buffer swap orchestration
        ---------------------------------------------
        -- Case 1: Ongoing operation
        if(R.HPSWriteState = ReadyForSpiTransfer and R.HPSReadState = HpsRead and
           R.SPISendState = ReadyForHpsTransfer and R.SPIReceiveState = ReadyForHpsTransfer) then

            NextR.TxTransfer       <= '0';
            --NextR.SetAllLedsOff    <= '1';

            if(R.InterruptMask = '1') then
                NextR.DataAvailableIRQ <= '1';
                NextR.InterruptStatus <= '1';
            end if;

            SwapBuffers;
        end if;
        
        -- Case 2: first hps transfer, no spi transfer yet
        if(R.HPSWriteState = ReadyForSpiTransfer and R.HPSReadState = HpsRead and
           R.SPISendState = Idle and R.SPIReceiveState = ReadyForHpsTransfer) then
            
            SwapBuffers;
        end if;

        -- Case 3: End/last transfer, no new hps transfer
        -- and think about how to determine state
        if(((R.Buffers(cNrBufferOne).TxWrAddr = 0 and R.BufferOneState = HPSTransfer) or (R.Buffers(cNrBufferTwo).TxWrAddr = 0 and R.BufferTwoState = HPSTransfer)) and
            R.HPSReadState = HpsRead and R.SPISendState = ReadyForHpsTransfer and R.SPIReceiveState = ReadyForHpsTransfer) then
            NextR.TxTransfer       <= '0';
            NextR.SetAllLedsOff    <= '1';

            if(R.InterruptMask = '1') then
                NextR.DataAvailableIRQ <= '1';
                NextR.InterruptStatus <= '1';
            end if;

            SwapBuffers;
        end if;

        -- Determine current buffer functionality
        ---------------------------------------------
        -- Buffer One
        if(R.BufferOneState = HPSTransfer) then
            HpsWrite(cNrBufferOne);
            HpsRead(cNrBufferOne, ReadRAMRxOne);
            SpiWrite(cNrBufferTwo, ReadRAMTxTwo);
            SpiRead(cNrBufferTwo);
        end if;

        if(R.BufferTwoState = HPSTransfer) then
            HpsWrite(cNrBufferTwo);
            HpsRead(cNrBufferTwo, ReadRAMRxTwo);
            SpiWrite(cNrBufferOne, ReadRAMTxOne);
            SpiRead(cNrBufferOne);
        end if;
    end process Comb;

    --**************************************************************************
    -- Entity Instantiation
    --**************************************************************************        

    Visualizer : entity work.visualizer
          port map(
              clk               => clk,
              reset_n           => reset_n,
              resetCounter      => R.ResetCounter,
              incCounterByOne   => R.IncCounterByOne,
              setAllLedsOn      => R.SetAllLedsOn,
              setAllLedsOff     => R.SetAllLedsOff,
              setLightChaserOn  => R.SetLightChaserOn,
              oleds             => open,
              ohex0             => Hex0,
              ohex1             => Hex1,
              ohex2             => Hex2,
              ohex3             => Hex3,
              ohex4             => Hex4,
              ohex5             => Hex5 
        );

    --**************************************************************************
    -- Outputs
    --**************************************************************************
    waitrequest   <= R.WaitRequest;
    readySink     <= R.ReadySink;
    dataAvailable <= R.DataAvailableIRQ;

    readdata      <= std_logic_vector(R.ReadDataOut) after 5 ns;
    readdatavalid <= R.ReadDataValid after 5 ns;

    leds <= R.LedsDebug;

end architecture Rtl;

----------------------------------------------------------------------------------------------------------------
-- Workfile:    databuffer-Rtl.vhd
-- Author:      Tobias Kerbl
----------------------------------------------------------------------------------------------------------------
-- Description: architecture for the databuffer entity
----------------------------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Visualizer library
library work; 
use work.pkg_visualizer.all;

architecture Rtl of visualizer is
    ----------------------------------------------------------
    -- Types
    ----------------------------------------------------------
    -- Strobe-Generator:
    subtype aStrobeCounter is natural range 0 to cClkCycPerStrobeCyc;

    -- BCD-Counter
    type a6DigitBCDCounter is array(cNrOfHex-1 downto 0) of aHexDigit;

    -- HEX Values:

    -- LED States:
    -- AllLedsOn:     turns on every led
    -- AllLedsOff:    turns off every led
    -- LightChaserOn: turns on a running light with the leds (sets the initial value: 1)
    -- LightChaserRunning: light chaser is running now (led-bits are rotating)
    type aLEDState is (AllLedsOn, AllLedsOff, LightChaserOn, LightChaserRunning);
    --type aShiftDirection is (Forward, Backwards); -- need for controlling the light-chaser

    -- Register Set
    type aRegSet is record
        -- Strobe Generator
        StrobeCount   : aStrobeCounter;
        Strobe        : std_ulogic;

        -- Edge Detectors
        ResetCounter         : std_ulogic;
        ResetCounterDelayed  : std_ulogic;
        IncCounter           : std_ulogic;
        IncCounterDelayed    : std_ulogic;
        LedsOn               : std_ulogic;
        LedsOnDelayed        : std_ulogic;
        LedsOff              : std_ulogic;
        LedsOffDelayed       : std_ulogic;
        LightChaserOn        : std_ulogic;
        LightChaserOnDelayed : std_ulogic;

        -- BCD-Counter 
        -- Carries       : unsigned(cNrOfHex downto 0); --one more bit for last carry bit
        BCDCounterStrobe : std_ulogic;
        BCDCounter    : a6DigitBCDCounter;

        -- Leds
        LEDStates       : aLEDState;
        --LightChaserDir  : aShiftDirection;

        -- Visualization  
        Leds          : unsigned(cNrOfLeds-1 downto 0);
        Hex           : aHexSegValues;
    end record aRegSet;

    -- constants
    constant cInitialRegSet : aRegSet := (
        -- Strobe Generator
        StrobeCount   => 0,
        Strobe        => '0',

        -- Edge Detectors
        ResetCounter         => '0',
        ResetCounterDelayed  => '0',
        IncCounter           => '0',
        IncCounterDelayed    => '0',
        LedsOn               => '0',
        LedsOnDelayed        => '0',
        LedsOff              => '0',
        LedsOffDelayed       => '0',
        LightChaserOn        => '0',
        LightChaserOnDelayed => '0',
        
        -- BCD-Counter 
        -- Carries    => (others => '0'), -- TODO: Check if Carries, are still needed!
        BCDCounterStrobe => '0',
        BCDCounter => (others => (others => '0')),

        -- Leds
        LEDStates => AllLedsOff,
        --LightChaserDir => Forward,

        -- Visualization
        Leds         => (others => '0'), --ready by default
        Hex          => (others => (others => '0'))
    );

    ----------------------------------------------------------
    -- Signals
    ----------------------------------------------------------
    signal R, NextR : aRegSet;
begin

    ----------------------------------------------------------
    -- Registers
    ----------------------------------------------------------
    Registers: process(clk, reset_n) is
    begin
        if(reset_n = not '1') then
            R <= cInitialRegSet;
        elsif rising_edge(clk) then
            R <= NextR;
        end if;
    end process Registers;

    ----------------------------------------------------------
    -- Combinatorial
    ----------------------------------------------------------
    Comb: process(R, incCounterByOne, setAllLedsOn, setAllLedsOff, setLightChaserOn, resetCounter) is
        -----------------------------------------------
        -- Procedures
        -----------------------------------------------
        -- Generating the strobe signal
        procedure StrobeGen is
        begin
            if(R.StrobeCount = cClkCycPerStrobeCyc) then
                NextR.StrobeCount <= 0;
                NextR.Strobe <= '1';
            else
                NextR.StrobeCount <= R.StrobeCount + 1;
                NextR.Strobe <= '0'; 
            end if;
        end procedure StrobeGen;

        -- Detect the rising edges of the control signals
        procedure DetectEdges is
        begin
            -- Set "drag-signals" for edge detection
            NextR.ResetCounter            <= resetCounter;
            NextR.ResetCounterDelayed     <= R.ResetCounter;

            NextR.IncCounter            <= incCounterByOne;
            NextR.IncCounterDelayed     <= R.IncCounter;

            NextR.LedsOn                <= setAllLedsOn;
            NextR.LedsOnDelayed         <= R.LedsOn;

            NextR.LedsOff               <= setAllLedsOff;
            NextR.LedsOffDelayed        <= R.LedsOff;

            NextR.LightChaserOn         <= setLightChaserOn;
            NextR.LightChaserOnDelayed  <= R.LightChaserOn;

            -- detect rising edges
            if (((not R.ResetCounterDelayed) and R.ResetCounter) = '1') then
                -- reset bcd-counter
                NextR.BCDCounter <= (others => (others => '0'));
            end if;

            if (((not R.IncCounterDelayed) and R.IncCounter) = '1') then
                -- increment bcd counter
                NextR.BCDCounterStrobe <= '1';
            else
                NextR.BCDCounterStrobe <= '0';
            end if;

            if(((not R.LightChaserOnDelayed) and R.LightChaserOn) = '1') then
                NextR.LedStates <= LightChaserOn;
            end if;

            if(((not R.LedsOnDelayed) and R.LedsOn) = '1') then
                NextR.LedStates <= AllLedsOn;
            end if;

            -- put "all leds off" at the end, so if all edges are detected
            -- at the same time, the leds will we turned off
            if(((not R.LedsOffDelayed) and R.LedsOff) = '1') then
                NextR.LedStates <= AllLedsOff;
            end if;
            
        end procedure;

        -- call this procedure, if you want to add one number
        -- to the 6 digit bcd counter chain
        procedure BCDCounter is
            constant cMaxValue : natural := 9;
            variable vCarry : std_ulogic;
        begin
            vCarry := R.BCDCounterStrobe;
            --if (R.Strobe = '1') then
                for vIndex in 0 to cNrOfHex-1 loop
                    -- use the carry as enable
                    if(vCarry = '1') then
                        if(R.BCDCounter(vIndex) = cMaxValue) then
                            NextR.BCDCounter(vIndex) <= (others => '0');
                            vCarry := '1';
                        else
                            NextR.BCDCounter(vIndex) <= R.BCDCounter(vIndex) + 1;
                            vCarry := '0';
                        end if;
                    end if;
                end loop;
            --end if;
        end procedure BCDCounter;
        
        -- procedure for decoding the bcd counter (4 bit)
        -- to 7 bits for the seven segment displays on the
        -- board
        procedure DecodeToSevenSegment is
        begin
            for vIndex in 0 to cNrOfHex-1 loop
                NextR.Hex(vIndex) <= num_to_7seg(R.BCDCounter(vIndex));
            end loop;
        end procedure DecodeToSevenSegment;
        
        -- procedure for the led visualization
        -- implements a led ladder, to indicate 'processing'
        -- turns on every led, to indicate 'ready'
        -- turns off every led, to indicate 'not ready'
        procedure SetLEDs is
        begin
            --if (R.Strobe = '1') then
                case R.LedStates is
                    when AllLedsOn          => NextR.Leds <= (others => '1');
                    when AllLedsOff         => NextR.Leds <= (others => '0');
                    when LightChaserOn      => NextR.Leds <= (NextR.Leds'high => '1', others => '0');
                                               NextR.LedStates <= LightChaserRunning;
                    when LightChaserRunning => NextR.Leds <= R.Leds(R.Leds'low) & R.Leds(R.Leds'high downto R.Leds'low+1);  -- rotate right with concatenation
                    when others             => NextR.Leds <= (others => 'X');
                end case;
            --end if;
        end procedure SetLEDs;

    -----------------------------------------------
    -- Comb-Process
    -----------------------------------------------
    begin
        --setting the defaults, to prevent latches
        NextR <= R;

        -- Control logic
        StrobeGen;

        -- Detect Edges (detect edges)
        DetectEdges;
        
        -- BCD Counter (handles the counter logic)
        BCDCounter;

        -- Visualization
        DecodeToSevenSegment;
        SetLEDs;

    end process Comb;

    -----------------------------------------------
    -- Outputs
    -----------------------------------------------
    OutputLed: for i in cNrOfLeds-1 downto 0 generate
        oleds(i) <= R.Leds(i);
    end generate;

    OutputHex: for i in cNrOfHexSegments-1 downto 0 generate
        ohex0(i) <= R.Hex(0)(i);
        ohex1(i) <= R.Hex(1)(i);
        ohex2(i) <= R.Hex(2)(i);
        ohex3(i) <= R.Hex(3)(i);
        ohex4(i) <= R.Hex(4)(i);
        ohex5(i) <= R.Hex(5)(i);
    end generate;
end architecture Rtl;

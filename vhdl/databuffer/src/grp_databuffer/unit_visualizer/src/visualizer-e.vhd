----------------------------------------------------------------------------------------------------------------
-- Workfile:    databuffer-e.vhd
-- Author:      Tobias Kerbl
----------------------------------------------------------------------------------------------------------------
-- Description: Entity for the databuffer
----------------------------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Visualizer library
library work; 
use work.pkg_visualizer.all;

entity visualizer is
    port (
        -- General
        clk              : in  std_ulogic;
        reset_n          : in  std_ulogic;

        -- -- Control Inputs - Avalon MM Slave
        -- address          : in std_logic_vector(2 downto 0);
        -- write            : in std_logic;
        -- writedata        : in std_logic;

        -- Control Inputs - old
        resetCounter     : in std_ulogic;
        incCounterByOne  : in std_ulogic;
        setAllLedsOn     : in std_ulogic;
        setAllLedsOff    : in std_ulogic;
        setLightChaserOn : in std_ulogic;

        -- Visualization Outputs
        oleds            : out std_logic_vector(cNrOfLeds-1 downto 0);
        ohex0            : out std_logic_vector(cNrOfHexSegments-1 downto 0);
        ohex1            : out std_logic_vector(cNrOfHexSegments-1 downto 0);
        ohex2            : out std_logic_vector(cNrOfHexSegments-1 downto 0);
        ohex3            : out std_logic_vector(cNrOfHexSegments-1 downto 0);
        ohex4            : out std_logic_vector(cNrOfHexSegments-1 downto 0);
        ohex5            : out std_logic_vector(cNrOfHexSegments-1 downto 0)
    );
begin
end entity visualizer;
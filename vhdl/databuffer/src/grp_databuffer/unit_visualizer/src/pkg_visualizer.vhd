----------------------------------------------------------------------------------------------------------------
-- Workfile:    pkg_databuffer.vhd
-- Author:      Tobias Kerbl
----------------------------------------------------------------------------------------------------------------
-- Description: This package contains all constants, functions and definitions for the 'Databuffer'. 
--              It also contains constants for the Simulation via UVVM (BFMs, etc.)
----------------------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------
-- Libraries
--------------------------------------------------------------------------
-- general IEEE libraries
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

-- -- UVVM librarys
-- library uvvm_util;
-- context uvvm_util.uvvm_util_context;
-- library uvvm_vvc_framework;
-- use uvvm_vvc_framework.ti_vvc_framework_support_pkg.all;

-- -- Clock Generator librarys
-- library bitvis_vip_clock_generator;
-- context bitvis_vip_clock_generator.vvc_context;

-- -- Avalon ST librarys
-- library bitvis_vip_avalon_st;
-- use bitvis_vip_avalon_st.avalon_st_bfm_pkg.all;

-- -- Avalon MM librarys
-- library bitvis_vip_avalon_mm;
-- use bitvis_vip_avalon_mm.avalon_mm_bfm_pkg.all;

--------------------------------------------------------------------------
-- Package
--------------------------------------------------------------------------

package pkg_visualizer is

    ----------------------------------------
    -- Constants
    ----------------------------------------

    -- Clock
    ------------------------------------
    constant cClkPeriod          : time    := 10 ns; -- 100 MHz
    constant cClkGen             : natural := 1;

    -- Avalon Interfaces
    ------------------------------------
    -- constant cAddressWidth       : natural := 12;
    -- constant cAvalonMMDataWidth  : natural := 32;
    -- constant cAvalonSTDataWidth  : natural := 8;

    -- "Visualization"
    ------------------------------------
    constant cNrOfLeds           : natural := 10;
    constant cNrOfHex            : natural := 6;
    constant cNrOfHexSegments    : natural := 7;

    -- strobe generation - internal
    ------------------------------------
    -- for simulation
    -- constant cClkCycPerStrobeCyc : natural := 50E6 / (1 sec / 100 us);
    constant cClkCycPerStrobeCyc : natural := 50E6 / (1 sec / 0.5 sec);
    --constant cClkCycPerStrobeCyc : natural := 2;

    -- Hex Display

    subtype a7SegDisplay is std_ulogic_vector(cNrOfHexSegments - 1 downto 0);
    type aHexSegValues is array(cNrOfHex-1 downto 0) of a7SegDisplay;

    subtype aHexDigit is unsigned(3 downto 0);

    function num_to_7seg(constant number: aHexDigit) return a7SegDisplay;

end pkg_visualizer;

-- package body
package body pkg_visualizer is
    function num_to_7seg(constant number: aHexDigit) return a7SegDisplay is
    begin
        case number is
            when "0000" => return "0111111";
            when "0001" => return "0000110";
            when "0010" => return "1011011";
            when "0011" => return "1001111";
            when "0100" => return "1100110";
            when "0101" => return "1101101";
            when "0110" => return "1111101";
            when "0111" => return "0000111";
            when "1000" => return "1111111";
            when "1001" => return "1101111";
            when "1010" => return "1110111";
            when "1011" => return "1111100";
            when "1100" => return "0111001";
            when "1101" => return "1011110";
            when "1110" => return "1111001";
            when "1111" => return "1110001";
            when others => return "XXXXXXX";
        end case;
    end function;
end pkg_visualizer;

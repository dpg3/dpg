----------------------------------------------------------------------------------------------------------------
-- Workfile:    ramtest-Rtl.vhd
-- Author:      Tobias Kerbl
----------------------------------------------------------------------------------------------------------------
-- Description: architecture for testing ram inference (mixed width port)
----------------------------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Databuffer library
library work; 
use work.pkg_databuffer.all;

architecture Rtl of ramtest is
  ----------------------------------------------------------
  -- Types
  ----------------------------------------------------------
  type aRegSet is record
    BufferOneTxWrAddr   : natural range 0 to cTestLength-1;
    BufferOneTxWr       : std_ulogic;
    BufferOneTxWrData   : aWordTest;
    BufferOneTxRdAddr   : natural range 0 to (cTestLength*4)-1;

    -- Buffer One: Rx
    BufferOneRxWrAddr   : natural range 0 to (cTestLength*4)-1;
    BufferOneRxWr       : std_ulogic;
    BufferOneRxWrData   : aByteTest;
    BufferOneRxRdAddr   : natural range 0 to cTestLength-1;
  end record aRegSet;

  ----------------------------------------------------------
  -- Constants
  ----------------------------------------------------------
  constant cInitialRegSet : aRegSet := (
    -- Buffer One: Tx
    BufferOneTxWrAddr   => 0,
    BufferOneTxWr       => '0',
    BufferOneTxWrData   => (others => (others => '0')),
    BufferOneTxRdAddr   => 0,

    -- Buffer One: Rx
    BufferOneRxWrAddr   => 0,
    BufferOneRxWr       => '0',
    BufferOneRxWrData   => (others => '0'),
    BufferOneRxRdAddr   => 0
  );

  ----------------------------------------------------------
  -- Signals
  ----------------------------------------------------------
  signal R, NextR : aRegSet;

  signal ReadFromBuffOneTx : aByteTest;
  signal ReadFromBuffOneRx : aWordTest;

  ----------------------------------------------------------
  -- RAM
  ----------------------------------------------------------
  attribute ramstyle : string;

  signal RamTxOne : aRAMBlockTest;
  attribute ramstyle of RAMTxOne : signal is "M10K";
  
  signal RamRxOne : aRAMBlockTest;
  attribute ramstyle of RAMRxOne : signal is "M10K";

begin

  ----------------------------------------------------------
  -- Registers
  ----------------------------------------------------------
  Registers: process(clk, reset_n) is
  begin
      if(reset_n = not '1') then
          R <= cInitialRegSet;
      elsif rising_edge(clk) then
          R <= NextR;
      end if;
  end process Registers;

  -----------------------------------------------
  -- RAM-Interfaces
  -----------------------------------------------
  -- TX RAMs
  BufferOneTX: process(clk, R) is
  begin
      if(rising_edge(clk)) then
          if(R.BufferOneTxWr = '1') then
              RamTxOne(R.BufferOneTxWrAddr) <= R.BufferOneTxWrData;
          end if;
          ReadFromBuffOneTx <= RamTxOne(R.BufferOneTxRdAddr / 4)(R.BufferOneTxRdAddr mod 4);
      end if;
  end process BufferOneTX;

  -- RX RAMs
  BufferOneRX: process(clk, R) is
  begin
      if(rising_edge(clk)) then
          if(R.BufferOneRxWr = '1') then
              RamRxOne(R.BufferOneRxWrAddr / 4)(R.BufferOneRxWrAddr mod 4) <= R.BufferOneRxWrData;
          end if;
          ReadFromBuffOneRx <= RamRxOne(R.BufferOneRxRdAddr);
      end if;
  end process BufferOneRX;

  ----------------------------------------------------------
  -- Combinatorial
  ----------------------------------------------------------
  Comb: process(R,  TBufferOneTxWrAddr,
                    TBufferOneTxWr,
                    TBufferOneTxWrData,
                    TBufferOneTxRdAddr,
                    TBufferOneRxWrAddr,
                    TBufferOneRxWr,
                    TBufferOneRxWrData,
                    TBufferOneRxRdAddr) is
  begin
    -- setting the defaults, to prevent latches
    NextR <= R;

    -- Buffer One: Tx
    NextR.BufferOneTxWrAddr   <= to_integer(unsigned(TBufferOneTxWrAddr));
    NextR.BufferOneTxWr       <= TBufferOneTxWr;
    NextR.BufferOneTxWrData   <= TBufferOneTxWrData;
    NextR.BufferOneTxRdAddr   <= to_integer(unsigned(TBufferOneTxRdAddr));

    -- Buffer One: Rx
    NextR.BufferOneRxWrAddr   <= to_integer(unsigned(TBufferOneRxWrAddr));
    NextR.BufferOneRxWr       <= TBufferOneRxWr;
    NextR.BufferOneRxWrData   <= TBufferOneRxWrData;
    NextR.BufferOneRxRdAddr   <= to_integer(unsigned(TBufferOneRxRdAddr));
  end process Comb;

  -----------------------------------------------
  -- Outputs
  -----------------------------------------------
  TBufferOneTxRdData <= ReadFromBuffOneTx;
  TBufferOneRxRdData <= ReadFromBuffOneRx;

end architecture Rtl;
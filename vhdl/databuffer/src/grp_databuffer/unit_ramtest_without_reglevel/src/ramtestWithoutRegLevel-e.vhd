----------------------------------------------------------------------------------------------------------------
-- Workfile:    ramtest-e.vhd
-- Author:      Tobias Kerbl
----------------------------------------------------------------------------------------------------------------
-- Description: Entity for testing ram inference (mixed width port)
----------------------------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Databuffer library
library work; 
use work.pkg_databuffer.all;

entity ramtestWithoutRegLevel is
  generic (
        gAddressWidth       : natural := cAddressWidth;
        gAvalonMMDataWidth  : natural := cAvalonMMDataWidth;
        gAvalonSTDataWidth  : natural := cAvalonSTDataWidth;
        gLengthOfRAM        : natural := cLengthOfRAM
    );
  port (
    -- General
    clk              : in  std_logic;
    reset_n          : in  std_logic;

    -- Test Pin Assignment
    TBufferOneTxWrAddr   : in std_ulogic_vector(0 to cTestLength-1);
    TBufferOneTxWr       : in std_ulogic;
    TBufferOneTxWrData   : in aWordTest;
    TBufferOneTxRdAddr   : in std_ulogic_vector(0 to (cTestLength*4)-1);
    TBufferOneTxRdData   : out aByteTest;

    -- Buffer One: Rx
    TBufferOneRxWrAddr   : in std_ulogic_vector(0 to (cTestLength*4)-1);
    TBufferOneRxWr       : in std_ulogic;
    TBufferOneRxWrData   : in aByteTest;
    TBufferOneRxRdAddr   : in std_ulogic_vector(0 to cTestLength-1);
    TBufferOneRxRdData   : out aWordTest
  );
begin

end entity ramtestWithoutRegLevel;
----------------------------------------------------------------------------------------------------------------
-- Workfile:    ramtest-Rtl.vhd
-- Author:      Tobias Kerbl
----------------------------------------------------------------------------------------------------------------
-- Description: architecture for testing ram inference (mixed width port)
----------------------------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Databuffer library
library work; 
use work.pkg_databuffer.all;

architecture Rtl of ramtestWithoutRegLevel is
  ----------------------------------------------------------
  -- Types
  ----------------------------------------------------------
  ----------------------------------------------------------
  -- Constants
  ----------------------------------------------------------
  ----------------------------------------------------------
  -- Signals
  ----------------------------------------------------------
  signal R, NextR : std_ulogic;

  ----------------------------------------------------------
  -- RAM
  ----------------------------------------------------------
  attribute ramstyle : string;

  signal RamTxOne : aRAMBlockTest;
  attribute ramstyle of RAMTxOne : signal is "M10K";
  
  signal RamRxOne : aRAMBlockTest;
  attribute ramstyle of RAMRxOne : signal is "M10K";

begin

  ----------------------------------------------------------
  -- Registers
  ----------------------------------------------------------
  Registers: process(clk, reset_n) is
  begin
      if(reset_n = not '1') then
          R <= '0';
      elsif rising_edge(clk) then
          R <= NextR;
      end if;
  end process Registers;

  -----------------------------------------------
  -- RAM-Interfaces
  -----------------------------------------------
  -- TX RAMs
  BufferOneTX: process(clk) is
  begin
      if(rising_edge(clk)) then
          if(TBufferOneTxWr = '1') then
              RamTxOne(to_integer(unsigned(TBufferOneTxWrAddr))) <= TBufferOneTxWrData;
          end if;
          TBufferOneTxRdData <= RamTxOne(to_integer(unsigned(TBufferOneTxRdAddr) / 4))(to_integer(unsigned(TBufferOneTxRdAddr) mod 4));
      end if;
  end process BufferOneTX;

  -- RX RAMs
  BufferOneRX: process(clk) is
  begin
      if(rising_edge(clk)) then
          if(TBufferOneRxWr = '1') then
              RamRxOne(to_integer(unsigned(TBufferOneRxWrAddr) / 4))(to_integer(unsigned(TBufferOneRxWrAddr) mod 4)) <= TBufferOneRxWrData;
          end if;
          TBufferOneRxRdData <= RamRxOne(to_integer(unsigned(TBufferOneRxRdAddr)));
      end if;
  end process BufferOneRX;

  ----------------------------------------------------------
  -- Combinatorial
  ----------------------------------------------------------
  Comb: process(R) is
  begin
    -- setting the defaults, to prevent latches
    NextR <= R;
  end process Comb;

  -----------------------------------------------
  -- Outputs
  -----------------------------------------------

end architecture Rtl;
----------------------------------------------------------------------------------------------------------------
-- Workfile:    tb_databuffer_vvc-func-ea.vhd
-- Author:      Tobias Kerbl
----------------------------------------------------------------------------------------------------------------
-- Description: This file contains the testcases of the UVVM tests.
----------------------------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library vunit_lib;
context vunit_lib.vunit_context;

library uvvm_util;
context uvvm_util.uvvm_util_context;

library uvvm_vvc_framework;
use uvvm_vvc_framework.ti_vvc_framework_support_pkg.all;

library bitvis_vip_clock_generator;
context bitvis_vip_clock_generator.vvc_context;

-- UVVM VIP Avalon MM
library bitvis_vip_avalon_mm;
use bitvis_vip_avalon_mm.vvc_methods_pkg.all;
use bitvis_vip_avalon_mm.td_vvc_framework_common_methods_pkg.all;

-- UVVM VIP Avalon ST 
library bitvis_vip_avalon_st;
use bitvis_vip_avalon_st.vvc_methods_pkg.all;
use bitvis_vip_avalon_st.td_vvc_framework_common_methods_pkg.all;

-- Databuffer libraries
library work; 
use work.pkg_databuffer.all;
use work.pkg_tb_databuffer.all;

-- Test bench entity
entity tb_DataBuffer is
    generic (runner_cfg : string); -- used by VUnit
end entity tb_DataBuffer;

architecture Bhv of tb_DataBuffer is

-----------------------------------------------------------------------------
-- General Types
-----------------------------------------------------------------------------
-- Avalon Memory Mapped Write
type MM_Write_Data is record
    w_address    : unsigned(cAddressWidth-1 downto 0);
    data         : std_logic_vector(cAvalonMMDataWidth-1 downto 0); -- = expected output
end record;
type MM_Write_Dataset is array(natural range <>) of MM_Write_Data;

-- Avalon Memory Mapped Read
type MM_Read_Data is record
    r_address    : unsigned(cAddressWidth-1 downto 0);
    data         : std_logic_vector(cAvalonMMDataWidth-1 downto 0);
end record;
type MM_Read_Dataset is array(natural range <>) of MM_Read_Data;

-----------------------------------------------------------------------------
-- Constants
-----------------------------------------------------------------------------
-- Dataset: A
constant cNumberOfRows_MMWrite_Dataset_A   : natural := 10;

-- Dataset: B
constant cNumberOfRows_MMWrite_Dataset_B   : natural := 9;

-- Dataset: C
constant cNumberOfRows_MMWrite_Dataset_C   : natural := 10;
constant cNumberOfRows_MMRead_Dataset_C    : natural := 10;
constant cNumberOfRows_STReceive_Dataset_C : natural := 40;

-- Dataset: D
constant cNumberOfRows_MMWrite_Dataset_D   : natural := 3;
constant cNumberOfRows_STSend_Dataset_D    : natural := 12;

-----------------------------------------------------------------------------
-- Specific Types
-----------------------------------------------------------------------------
-- Dataset A:
subtype MM_Write_Dataset_A is MM_Write_Dataset(1 to cNumberOfRows_MMWrite_Dataset_A);

-- Dataset B:
subtype MM_Write_Dataset_B  is MM_Write_Dataset(1 to cNumberOfRows_MMWrite_Dataset_B);
subtype MM_Read_Dataset_B   is MM_Read_Dataset(1 to cNumberOfRows_MMWrite_Dataset_B);

-- Dataset C:
subtype MM_Write_Dataset_C  is MM_Write_Dataset(1 to cNumberOfRows_MMWrite_Dataset_C);
subtype MM_Read_Dataset_C   is MM_Read_Dataset(1 to cNumberOfRows_MMRead_Dataset_C);
subtype ST_Receive_Dataset_C is t_slv_array(0 to cNumberOfRows_STReceive_Dataset_C-1)(cAvalonSTDataWidth-1 downto 0);

--type ST_Send_Dataset_C is array(0 to 39) of t_slv_array(0 to 0)(cAvalonSTDataWidth-1 downto 0);

-- Dataset D:
subtype MM_Write_Dataset_D   is MM_Write_Dataset(1 to cNumberOfRows_MMWrite_Dataset_D);
subtype MM_Read_Dataset_D   is MM_Read_Dataset(1 to cNumberOfRows_MMWrite_Dataset_D);
subtype ST_Send_Dataset_D    is t_slv_array(0 to cNumberOfRows_STSend_Dataset_D)(cAvalonSTDataWidth-1 downto 0);

-----------------------------------------------------------------------------
-- Signals
-----------------------------------------------------------------------------
signal ResetAsync    : std_ulogic := '0';
signal DataAvailable : std_ulogic := '0';

begin

  -----------------------------------------------------------------------------
  -- Instantiate DUT
  -----------------------------------------------------------------------------
    i_test_harness : entity work.th_databuffer_vvc
    generic map (
        gLengthOfRam => 15
    )
    port map (
        reset_n_i        => ResetAsync,
        data_available_o => DataAvailable
    );

  -----------------------------------------------------------------------------
  -- Main test process
  -----------------------------------------------------------------------------
    p_main : process

    -- Test datasets
    -----------------------------------------------------------------------------
        -- Dataset: 1
        -----------------
        variable vDataset1 : MM_Write_Dataset_A := (
            (w_address => to_unsigned(cNrTxData, cAddressWidth), data => x"00000000"), -- Testcase 1
            (w_address => to_unsigned(cNrTxData, cAddressWidth), data => x"FFFFFFFF"), -- Testcase 2
            (w_address => to_unsigned(cNrTxData, cAddressWidth), data => x"0000000F"), -- Testcase 3
            (w_address => to_unsigned(cNrTxData, cAddressWidth), data => x"F0000000"), -- Testcase 4
            (w_address => to_unsigned(cNrTxData, cAddressWidth), data => x"000000F0"), -- Testcase 5
            (w_address => to_unsigned(cNrTxData, cAddressWidth), data => x"0F000000"), -- Testcase 6
            (w_address => to_unsigned(cNrTxData, cAddressWidth), data => x"00000F00"), -- Testcase 7
            (w_address => to_unsigned(cNrTxData, cAddressWidth), data => x"00F00000"), -- Testcase 8
            (w_address => to_unsigned(cNrTxData, cAddressWidth), data => x"0000F000"), -- Testcase 9
            (w_address => to_unsigned(cNrTxData, cAddressWidth), data => x"000F0000")  -- Testcase 10
        );

        -- Dataset: 2
        -----------------
        variable vDataset2_Input : MM_Write_Dataset_B := (
            (w_address => to_unsigned(cNrTxStart, cAddressWidth), data => x"00000001"), -- Testcase 1
            (w_address => to_unsigned(cNrTxStart, cAddressWidth), data => x"00000000"), -- Testcase 2
            (w_address => to_unsigned(cNrTxStart, cAddressWidth), data => x"00000010"), -- Testcase 3
            (w_address => to_unsigned(cNrTxStart, cAddressWidth), data => x"10000001"), -- Testcase 4
            (w_address => to_unsigned(cNrTxStart, cAddressWidth), data => x"10000000"), -- Testcase 5
            (w_address => to_unsigned(cNrTxStart, cAddressWidth), data => x"0000000F"), -- Testcase 6
            (w_address => to_unsigned(cNrTxStart, cAddressWidth), data => x"000000F0"), -- Testcase 7
            (w_address => to_unsigned(cNrTxStart, cAddressWidth), data => x"FFFFFFFF"), -- Testcase 8
            (w_address => to_unsigned(cNrTxStart, cAddressWidth), data => x"F0000000")  -- Testcase 9
        );

        variable vDataset2_Expected : MM_Read_Dataset_B := (
            (r_address => to_unsigned(cNrTxStart, cAddressWidth), data => x"00000001"), -- Testcase 1
            (r_address => to_unsigned(cNrTxStart, cAddressWidth), data => x"00000000"), -- Testcase 2
            (r_address => to_unsigned(cNrTxStart, cAddressWidth), data => x"00000000"), -- Testcase 3
            (r_address => to_unsigned(cNrTxStart, cAddressWidth), data => x"00000001"), -- Testcase 4
            (r_address => to_unsigned(cNrTxStart, cAddressWidth), data => x"00000000"), -- Testcase 5
            (r_address => to_unsigned(cNrTxStart, cAddressWidth), data => x"00000001"), -- Testcase 6
            (r_address => to_unsigned(cNrTxStart, cAddressWidth), data => x"00000000"), -- Testcase 7
            (r_address => to_unsigned(cNrTxStart, cAddressWidth), data => x"00000001"), -- Testcase 8
            (r_address => to_unsigned(cNrTxStart, cAddressWidth), data => x"00000000")  -- Testcase 9
        );

        -- Dataset: 3
        -----------------
        variable vDataset3_Input_MM : MM_Write_Dataset_C := (
            (w_address => to_unsigned(cNrTxData, cAddressWidth), data => x"04030201"), -- Testcase 1
            (w_address => to_unsigned(cNrTxData, cAddressWidth), data => x"08070605"), -- Testcase 2
            (w_address => to_unsigned(cNrTxData, cAddressWidth), data => x"0C0B0A09"), -- Testcase 3
            (w_address => to_unsigned(cNrTxData, cAddressWidth), data => x"100F0E0D"), -- Testcase 4
            (w_address => to_unsigned(cNrTxData, cAddressWidth), data => x"14131211"), -- Testcase 5
            (w_address => to_unsigned(cNrTxData, cAddressWidth), data => x"18171615"), -- Testcase 6
            (w_address => to_unsigned(cNrTxData, cAddressWidth), data => x"1C1B1A19"), -- Testcase 7
            (w_address => to_unsigned(cNrTxData, cAddressWidth), data => x"201F1E1D"), -- Testcase 8
            (w_address => to_unsigned(cNrTxData, cAddressWidth), data => x"24232221"), -- Testcase 9
            (w_address => to_unsigned(cNrTxData, cAddressWidth), data => x"28272625")  -- Testcase 10
        );

        variable vDataset3_Expected_ST : ST_Receive_Dataset_C := (
            0  => x"01", 1  => x"02", 2  => x"03", 3  => x"04", -- Testcase 1
            4  => x"05", 5  => x"06", 6  => x"07", 7  => x"08", -- Testcase 2
            8  => x"09", 9  => x"0A", 10 => x"0B", 11 => x"0C", -- Testcase 3
            12 => x"0D", 13 => x"0E", 14 => x"0F", 15 => x"10", -- Testcase 4
            16 => x"11", 17 => x"12", 18 => x"13", 19 => x"14", -- Testcase 5
            20 => x"15", 21 => x"16", 22 => x"17", 23 => x"18", -- Testcase 6
            24 => x"19", 25 => x"1A", 26 => x"1B", 27 => x"1C", -- Testcase 7
            28 => x"1D", 29 => x"1E", 30 => x"1F", 31 => x"20", -- Testcase 8
            32 => x"21", 33 => x"22", 34 => x"23", 35 => x"24", -- Testcase 9
            36 => x"25", 37 => x"26", 38 => x"27", 39 => x"28"  -- Testcase 10
        );

        variable vDataset3_Expected_MM : MM_Read_Dataset_C := (
            (r_address => to_unsigned(cNrRxData, cAddressWidth), data => x"04030201"), -- Testcase 1
            (r_address => to_unsigned(cNrRxData, cAddressWidth), data => x"08070605"), -- Testcase 2
            (r_address => to_unsigned(cNrRxData, cAddressWidth), data => x"0C0B0A09"), -- Testcase 3
            (r_address => to_unsigned(cNrRxData, cAddressWidth), data => x"100F0E0D"), -- Testcase 4
            (r_address => to_unsigned(cNrRxData, cAddressWidth), data => x"14131211"), -- Testcase 5
            (r_address => to_unsigned(cNrRxData, cAddressWidth), data => x"18171615"), -- Testcase 6
            (r_address => to_unsigned(cNrRxData, cAddressWidth), data => x"1C1B1A19"), -- Testcase 7
            (r_address => to_unsigned(cNrRxData, cAddressWidth), data => x"201F1E1D"), -- Testcase 8
            (r_address => to_unsigned(cNrRxData, cAddressWidth), data => x"24232221"), -- Testcase 9
            (r_address => to_unsigned(cNrRxData, cAddressWidth), data => x"28272625")  -- Testcase 10
        );

        -- variable vDataset3_Input_MM_ST : ST_Send_Dataset_C := (
        --     (0  => x"01"), (0  => x"02"), (0  => x"03"), (0  => x"04"), -- Testcase 1
        --     (0  => x"05"), (0  => x"06"), (0  => x"07"), (0  => x"08"), -- Testcase 2
        --     (0  => x"09"), (0  => x"0A"), (0 => x"0B"),  (0 => x"0C"),  -- Testcase 3
        --     (0 => x"0D"),  (0 => x"0E"),  (0 => x"0F"),  (0 => x"10"),  -- Testcase 4
        --     (0 => x"11"),  (0 => x"12"),  (0 => x"13"),  (0 => x"14"),  -- Testcase 5
        --     (0 => x"15"),  (0 => x"16"),  (0 => x"17"),  (0 => x"18"),  -- Testcase 6
        --     (0 => x"19"),  (0 => x"1A"),  (0 => x"1B"),  (0 => x"1C"),  -- Testcase 7
        --     (0 => x"1D"),  (0 => x"1E"),  (0 => x"1F"),  (0 => x"20"),  -- Testcase 8
        --     (0 => x"21"),  (0 => x"22"),  (0 => x"23"),  (0 => x"24"),  -- Testcase 9
        --     (0 => x"25"),  (0 => x"26"),  (0 => x"27"),  (0 => x"28")   -- Testcase 10
        -- );

        -- Datasets: 4
        -----------------
        variable vDataset4_Input_MM : MM_Write_Dataset_C := (
            (w_address => to_unsigned(cNrTxData, cAddressWidth), data => x"25262728"), -- Testcase 1
            (w_address => to_unsigned(cNrTxData, cAddressWidth), data => x"21222324"), -- Testcase 2
            (w_address => to_unsigned(cNrTxData, cAddressWidth), data => x"1D1E1F20"), -- Testcase 3
            (w_address => to_unsigned(cNrTxData, cAddressWidth), data => x"191A1B1C"), -- Testcase 4
            (w_address => to_unsigned(cNrTxData, cAddressWidth), data => x"15161718"), -- Testcase 5
            (w_address => to_unsigned(cNrTxData, cAddressWidth), data => x"11121314"), -- Testcase 6
            (w_address => to_unsigned(cNrTxData, cAddressWidth), data => x"0D0E0F10"), -- Testcase 7
            (w_address => to_unsigned(cNrTxData, cAddressWidth), data => x"090A0B0C"), -- Testcase 8
            (w_address => to_unsigned(cNrTxData, cAddressWidth), data => x"05060708"), -- Testcase 9
            (w_address => to_unsigned(cNrTxData, cAddressWidth), data => x"01020304")  -- Testcase 10
        );

        variable vDataset4_Expected_ST : ST_Receive_Dataset_C := (
            0  => x"28", 1  => x"27", 2  => x"26", 3  => x"25", -- Testcase 1
            4  => x"24", 5  => x"23", 6  => x"22", 7  => x"21", -- Testcase 2
            8  => x"20", 9  => x"1F", 10 => x"1E", 11 => x"1D", -- Testcase 3
            12 => x"1C", 13 => x"1B", 14 => x"1A", 15 => x"19", -- Testcase 4
            16 => x"18", 17 => x"17", 18 => x"16", 19 => x"15", -- Testcase 5
            20 => x"14", 21 => x"13", 22 => x"12", 23 => x"11", -- Testcase 6
            24 => x"10", 25 => x"0F", 26 => x"0E", 27 => x"0D", -- Testcase 7
            28 => x"0C", 29 => x"0B", 30 => x"0A", 31 => x"09", -- Testcase 8
            32 => x"08", 33 => x"07", 34 => x"06", 35 => x"05", -- Testcase 9
            36 => x"04", 37 => x"03", 38 => x"02", 39 => x"01"  -- Testcase 10
        );

        variable vDataset4_Expected_MM : MM_Read_Dataset_C := (
            (r_address => to_unsigned(cNrRxData, cAddressWidth), data => x"25262728"), -- Testcase 1
            (r_address => to_unsigned(cNrRxData, cAddressWidth), data => x"21222324"), -- Testcase 2
            (r_address => to_unsigned(cNrRxData, cAddressWidth), data => x"1D1E1F20"), -- Testcase 3
            (r_address => to_unsigned(cNrRxData, cAddressWidth), data => x"191A1B1C"), -- Testcase 4
            (r_address => to_unsigned(cNrRxData, cAddressWidth), data => x"15161718"), -- Testcase 5
            (r_address => to_unsigned(cNrRxData, cAddressWidth), data => x"11121314"), -- Testcase 6
            (r_address => to_unsigned(cNrRxData, cAddressWidth), data => x"0D0E0F10"), -- Testcase 7
            (r_address => to_unsigned(cNrRxData, cAddressWidth), data => x"090A0B0C"), -- Testcase 8
            (r_address => to_unsigned(cNrRxData, cAddressWidth), data => x"05060708"), -- Testcase 9
            (r_address => to_unsigned(cNrRxData, cAddressWidth), data => x"01020304")  -- Testcase 10
        );

        -- Datasets: 5
        -----------------
        variable vDataset5_Input_ST : t_slv_array(0 to 11)(cAvalonSTDataWidth-1 downto 0) := (
            0  => x"28", 1  => x"27", 2  => x"26", 3  => x"25",
            4  => x"24", 5  => x"23", 6  => x"22", 7  => x"21",
            8  => x"20", 9  => x"1F", 10 => x"1E", 11 => x"1D"
        );

        variable vDataset5_Input_MM : MM_Write_Dataset_D := (
            (w_address => to_unsigned(cNrTxData, cAddressWidth), data => x"25262728"), -- Testcase 1
            (w_address => to_unsigned(cNrTxData, cAddressWidth), data => x"21222324"), -- Testcase 2
            (w_address => to_unsigned(cNrTxData, cAddressWidth), data => x"1D1E1F20")  -- Testcase 3
        );

        variable vDataset5_Expected : MM_Read_Dataset_D := (
            (r_address => to_unsigned(cNrRxData, cAddressWidth), data => x"25262728"), -- Testcase 1
            (r_address => to_unsigned(cNrRxData, cAddressWidth), data => x"21222324"), -- Testcase 2
            (r_address => to_unsigned(cNrRxData, cAddressWidth), data => x"1D1E1F20")  -- Testcase 3
        );

        -- Datasets: 6
        -----------------
        variable vDataset6_Input_ST : t_slv_array(0 to 11)(cAvalonSTDataWidth-1 downto 0) := (
            0  => x"0C", 1  => x"0B", 2  => x"0A", 3  => x"09",
            4  => x"08", 5  => x"07", 6  => x"06", 7  => x"05",
            8  => x"04", 9  => x"03", 10 => x"02", 11 => x"01"
        );

        variable vDataset6_Input_MM : MM_Write_Dataset_D := (
            (w_address => to_unsigned(cNrTxData, cAddressWidth), data => x"090A0B0C"), -- Testcase 1
            (w_address => to_unsigned(cNrTxData, cAddressWidth), data => x"05060708"), -- Testcase 2
            (w_address => to_unsigned(cNrTxData, cAddressWidth), data => x"01020304")  -- Testcase 3
        );

        variable vDataset6_Expected : MM_Read_Dataset_D := (
            (r_address => to_unsigned(cNrRxData, cAddressWidth), data => x"090A0B0C"), -- Testcase 1
            (r_address => to_unsigned(cNrRxData, cAddressWidth), data => x"05060708"), -- Testcase 2
            (r_address => to_unsigned(cNrRxData, cAddressWidth), data => x"01020304")  -- Testcase 3
        );

    -- Test helper functions/procedures
    -----------------------------------------------------------------------------
    procedure ReleaseReset is
    begin
        wait for 20 * cClkPeriod;
        ResetAsync <= not('0');
        wait for 20 * cClkPeriod;
    end procedure ReleaseReset;

    -- Activates the interrupt
    procedure ActivateIRQ is
    begin
        -- activate irq
        avalon_mm_write(AVALON_MM_VVCT, cAvalonMmMasterIdx, to_unsigned(cNrIRQMask, cAddressWidth), x"00000001", "Activate IRQ");
        await_completion(AVALON_MM_VVCT, cAvalonMmMasterIdx, cMaxTimeToWait, "");

        wait for 100 ns;

    end procedure ActivateIRQ;
    
    -- Releases the interrupt
    procedure ReleaseIRQ is
    begin
        -- release irq
        avalon_mm_write(AVALON_MM_VVCT, cAvalonMmMasterIdx, to_unsigned(cNrIRQStatus, cAddressWidth), x"00000000", "Release IRQ");
        await_completion(AVALON_MM_VVCT, cAvalonMmMasterIdx, cMaxTimeToWait, "");
    end procedure ReleaseIRQ;

    -- Start a transfer by setting the TxStart bit to 0
    -- Check if the transfer-bit ist set correctly (depends on the boolean-parameter)
    procedure StartTransferAndCheckTransferBit(
        CheckForTransferBit : in boolean
    ) is
    begin
        -- start buffer
        avalon_mm_write(AVALON_MM_VVCT, cAvalonMmMasterIdx, to_unsigned(cNrTxStart, cAddressWidth), x"00000001", "Set start bit");
        await_completion(AVALON_MM_VVCT, cAvalonMmMasterIdx, cMaxTimeToWait, "");

        -- Check transfer bit
        if(CheckForTransferBit = true) then
            avalon_mm_check(AVALON_MM_VVCT, cAvalonMmMasterIdx, to_unsigned(cNrTxTransfer, cAddressWidth), x"00000001", "Check if transfer bit is 1 ");
            await_completion(AVALON_MM_VVCT, cAvalonMmMasterIdx, cMaxTimeToWait, "");
        end if;
    end procedure StartTransferAndCheckTransferBit;

    -- Fills the buffer with avalon mm writes
    procedure FillBuffer (
        InputData : in MM_Write_Dataset;
        WaitForCompletion : in boolean
    ) is
    begin
        -- fill buffer
        for i in 1 to InputData'length loop
            -- write to avalon mm
            avalon_mm_write(AVALON_MM_VVCT, cAvalonMmMasterIdx, InputData(i).w_address, InputData(i).data, "Write to databuffer: " & integer'image(i));
            -- wait till write is finished
            if(WaitForCompletion = true) then
                await_completion(AVALON_MM_VVCT, cAvalonMmMasterIdx, cMaxTimeToWait, "");
            end if;
        end loop;
    end procedure;

    begin
        test_runner_setup(runner, runner_cfg);         -- VUnit setup

        report("###################################") severity note;

        -- Wait for UVVM to finish initialization
        await_uvvm_initialization(VOID);


        --***************************************************************************************************************  
        -- The following testbench will test the databuffer, behaving like a blackbox. This means, that specific data
        -- will be written into the buffer, via an "Avalon Memory Mapped" interface. After this, the transmitted/outputed 
        -- (via an "Avalon Streaming" interface) bytes will be checked against the input.
        -- While transmitting the bytes, addtional bytes will be send via an "Avalon Streaming" interface to the
        -- the databuffer. These bytes will be stored in the databuffer too and will be read via the "Avalon Memory Mapped"
        -- interface. The outputed bytes will be checked against the transmitted bytes.
        --
        -- General, there are three scenarios:
        -- -----------------------------------
        -- Case 1: A ongoing operation (hps and spi transfers): Transmits bytes, while new bytes gets stored
        -- Case 2: First hps transfer, no spi transfer        : Initates the first hps transfer and buffer swap
        -- Case 3: End/last spi transfer, no new hps transfer : Transmits the last bytes, no new bytes gets stored
        --***************************************************************************************************************
        while test_suite loop -- loop used to run several VUnit testcases
            --------------------------------------------------------------------------------------------------    
            -- Test 1:
            -- This test is for checking, that values are correctly written into the write-register
            -- The value of the write-register, will be written into the ram block afterwards
            --------------------------------------------------------------------------------------------------
            if run("Test 1: write and read into same register (write register)") then -- define one VUnit testcase
                
                start_clock(CLOCK_GENERATOR_VVCT, cClockGeneratorIdx, "Start clock generator");
                ReleaseReset;
                ActivateIRQ;

                for i in 1 to cNumberOfRows_MMWrite_Dataset_A loop
                    avalon_mm_write(AVALON_MM_VVCT, cAvalonMmMasterIdx, vDataset1(i).w_address, vDataset1(i).data, "Write to databuffer: " & integer'image(i));
                    await_completion(AVALON_MM_VVCT, cAvalonMmMasterIdx, cMaxTimeToWait, "Wait on Write");
                    avalon_mm_check(AVALON_MM_VVCT, cAvalonMmMasterIdx, vDataset1(i).w_address, vDataset1(i).data, "Compare read value with written value: "& integer'image(i));
                    await_completion(AVALON_MM_VVCT, cAvalonMmMasterIdx, cMaxTimeToWait, "Wait on Read");
                end loop;

                -- wait till all words are written
                await_completion(AVALON_MM_VVCT, cAvalonMmMasterIdx, cMaxTimeToWait, "");

            --------------------------------------------------------------------------------------------------    
            -- Test 2: - TBA
            -- This test is for checking, that the start-register is written correctly.
            -- The start-register initates a buffer-swap and the data-transfer to the spi-master unit
            --------------------------------------------------------------------------------------------------
            elsif run("Test 2: Check if start register is written correctly") then
                
                start_clock(CLOCK_GENERATOR_VVCT, cClockGeneratorIdx, "Start clock generator");
                ReleaseReset;
                ActivateIRQ;

                -- for i in 1 to cNumberOfRows_MMWrite_Dataset_B loop
                --     avalon_mm_write(AVALON_MM_VVCT, cAvalonMmMasterIdx, vDataset2_Input(i).w_address, vDataset2_Input(i).data, "Set start bit: " & integer'image(i));
                --     --await_completion(AVALON_MM_VVCT, cAvalonMmMasterIdx, cMaxTimeToWait, "");
                --     avalon_mm_check(AVALON_MM_VVCT, cAvalonMmMasterIdx, vDataset2_Expected(i).r_address, vDataset2_Expected(i).data, "Compare read value with written value: "& integer'image(i));
                -- end loop;

                -- -- wait till all words are written
                -- await_completion(AVALON_MM_VVCT, cAvalonMmMasterIdx, cMaxTimeToWait, "");

            --------------------------------------------------------------------------------------------------    
            -- Test 3:
            -- This test is for checking, that written values are correctly outputed via the avalon streaming
            -- interface (--> Input for SPI-Master)
            -- In this test, there will be no buffer swap. This means, that one wave of bytes will be inserted
            -- and transmitted
            --------------------------------------------------------------------------------------------------
            elsif run("Test 3: Write into Avalon MM, Check Avalon ST") then
                start_clock(CLOCK_GENERATOR_VVCT, cClockGeneratorIdx, "Start clock generator");
                ReleaseReset;
                ActivateIRQ;

                -- fill buffer
                FillBuffer(vDataset3_Input_MM, true);
                
                -- Start transfer
                StartTransferAndCheckTransferBit(false);

                -- compare avalon ss output with avalon mm input
                avalon_st_expect(AVALON_ST_VVCT, cAvalonStSlaveIdx, "Z", vDataset3_Expected_ST, "Compare LSB with expected LSB", ERROR, C_SCOPE);
                await_completion(AVALON_ST_VVCT, cAvalonStSlaveIdx, cMaxTimeToWait, "");

            --------------------------------------------------------------------------------------------------    
            -- Test 4:
            -- TBA
            --------------------------------------------------------------------------------------------------
            elsif run("Test 4 / Case 3 / Read Check: Initiate a new transfer after a short pause, st send delayed") then
                
                start_clock(CLOCK_GENERATOR_VVCT, cClockGeneratorIdx, "Start clock generator");
                ReleaseReset;
                ActivateIRQ;

                -- Wave 1:
                -------------------------------
                -- fill buffer
                FillBuffer(vDataset3_Input_MM, true);

                -- Start transfer
                StartTransferAndCheckTransferBit(false);

                -- Transmit receive bytes
                avalon_st_transmit(AVALON_ST_VVCT, cAvalonStMasterIdx, vDataset3_Expected_ST, "Send multiple bytes to databuffer");
                await_completion(AVALON_ST_VVCT, cAvalonStMasterIdx, cMaxTimeToWait, "");

                -- compare avalon ss output with avalon mm input
                avalon_st_expect(AVALON_ST_VVCT, cAvalonStSlaveIdx, "Z", vDataset3_Expected_ST, "Compare LSB with expected LSB : Wave 1", ERROR, C_SCOPE);
                await_completion(AVALON_ST_VVCT, cAvalonStSlaveIdx, cMaxTimeToWait, "");
                
                -- Check result
                -------------------------------
                await_value(DataAvailable, '1', 0 ns, 30 ns, "Waiting for Interrupt");
                ReleaseIRQ;

                for i in 1 to cNumberOfRows_MMRead_Dataset_C loop
                    avalon_mm_check(AVALON_MM_VVCT, cAvalonMmMasterIdx, vDataset3_Expected_MM(i).r_address, vDataset3_Expected_MM(i).data, "Check if transfer bits are read correctly");                    
                    await_completion(AVALON_MM_VVCT, cAvalonMmMasterIdx, cMaxTimeToWait, "");
                end loop;

                -- Wave 2:
                -------------------------------
                -- fill buffer
                FillBuffer(vDataset4_Input_MM, true);

                -- Start transfer
                StartTransferAndCheckTransferBit(false);

                -- await_value(DataAvailable, '1', 0 ns, 30 ns, "Waiting for Interrupt");
                -- ReleaseIRQ;
                wait for 2*cClkPeriod; -- Allow some time for waitrequest... Realistically SPI can only start responding after MM Write finished

                -- Transmit receive bytes
                avalon_st_transmit(AVALON_ST_VVCT, cAvalonStMasterIdx, vDataset4_Expected_ST, "Send multiple bytes to databuffer");
                await_completion(AVALON_ST_VVCT, cAvalonStMasterIdx, cMaxTimeToWait, "");

                -- compare avalon ss output with avalon mm input
                avalon_st_expect(AVALON_ST_VVCT, cAvalonStSlaveIdx, "Z", vDataset4_Expected_ST, "Compare LSB with expected LSB : Wave 2", ERROR, C_SCOPE);
                await_completion(AVALON_ST_VVCT, cAvalonStSlaveIdx, cMaxTimeToWait, "");

                -- Check result
                -------------------------------
                await_value(DataAvailable, '1', 0 ns, 30 ns, "Waiting for Interrupt");
                ReleaseIRQ;

                for i in 1 to cNumberOfRows_MMRead_Dataset_C loop
                    avalon_mm_check(AVALON_MM_VVCT, cAvalonMmMasterIdx, vDataset4_Expected_MM(i).r_address, vDataset4_Expected_MM(i).data, "Check if transfer bits are read correctly");                    
                    await_completion(AVALON_MM_VVCT, cAvalonMmMasterIdx, cMaxTimeToWait, "");
                end loop;

                wait for 500 ns;

            --------------------------------------------------------------------------------------------------    
            -- Test 5:
            -- TBA
            --------------------------------------------------------------------------------------------------
            elsif run("Test 5 / Case 3 / Read Check: Initiate a new transfer after a short pause, st send and receive simultaneously") then

                start_clock(CLOCK_GENERATOR_VVCT, cClockGeneratorIdx, "Start clock generator");
                ReleaseReset;
                ActivateIRQ;

                -- Wave 1:
                -------------------------------
                -- fill buffer
                FillBuffer(vDataset3_Input_MM, true);

                -- Start transfer
                StartTransferAndCheckTransferBit(false);

                -- Transmit receive bytes
                avalon_st_transmit(AVALON_ST_VVCT, cAvalonStMasterIdx, vDataset3_Expected_ST, "Send multiple bytes to databuffer");

                -- compare avalon ss output with avalon mm input
                avalon_st_expect(AVALON_ST_VVCT, cAvalonStSlaveIdx, "Z", vDataset3_Expected_ST, "Compare LSB with expected LSB : Wave 1", ERROR, C_SCOPE);
                await_completion(AVALON_ST_VVCT, cAvalonStSlaveIdx, cMaxTimeToWait, "");

                -- Check result
                -------------------------------
                await_value(DataAvailable, '1', 0 ns, 30 ns, "Waiting for Interrupt");
                ReleaseIRQ;

                for i in 1 to cNumberOfRows_MMRead_Dataset_C loop
                    avalon_mm_check(AVALON_MM_VVCT, cAvalonMmMasterIdx, vDataset3_Expected_MM(i).r_address, vDataset3_Expected_MM(i).data, "Check if transfer bits are read correctly");                    
                    await_completion(AVALON_MM_VVCT, cAvalonMmMasterIdx, cMaxTimeToWait, "");
                end loop;

                -- Wave 2:
                -------------------------------
                -- fill buffer
                FillBuffer(vDataset4_Input_MM, true);

                -- Start transfer
                StartTransferAndCheckTransferBit(false);

                -- Transmit receive bytes
                avalon_st_transmit(AVALON_ST_VVCT, cAvalonStMasterIdx, vDataset4_Expected_ST, "Send multiple bytes to databuffer");

                -- compare avalon ss output with avalon mm input
                avalon_st_expect(AVALON_ST_VVCT, cAvalonStSlaveIdx, "Z", vDataset4_Expected_ST, "Compare LSB with expected LSB : Wave 2", ERROR, C_SCOPE);
                await_completion(AVALON_ST_VVCT, cAvalonStSlaveIdx, cMaxTimeToWait, "");

                -- Check result
                -------------------------------
                await_value(DataAvailable, '1', 0 ns, 30 ns, "Waiting for Interrupt");
                ReleaseIRQ;

                for i in 1 to cNumberOfRows_MMRead_Dataset_C loop
                    avalon_mm_check(AVALON_MM_VVCT, cAvalonMmMasterIdx, vDataset4_Expected_MM(i).r_address, vDataset4_Expected_MM(i).data, "Check if transfer bits are read correctly");                    
                    await_completion(AVALON_MM_VVCT, cAvalonMmMasterIdx, cMaxTimeToWait, "");
                end loop;

                wait for 500 ns;

            --------------------------------------------------------------------------------------------------    
            -- Test 6:
            -- TBA
            --------------------------------------------------------------------------------------------------
            elsif run("Test 6 / Case 3 / Read Check: Initiate a new transfer after a short pause, st receive delayed") then
                
                start_clock(CLOCK_GENERATOR_VVCT, cClockGeneratorIdx, "Start clock generator");
                ReleaseReset;
                ActivateIRQ;

                -- Wave 1:
                -------------------------------
                -- fill buffer
                FillBuffer(vDataset3_Input_MM, true);

                -- Start transfer
                StartTransferAndCheckTransferBit(false);
               
                -- compare avalon ss output with avalon mm input
                avalon_st_expect(AVALON_ST_VVCT, cAvalonStSlaveIdx, "Z", vDataset3_Expected_ST, "Compare LSB with expected LSB : Wave 1", ERROR, C_SCOPE);
                await_completion(AVALON_ST_VVCT, cAvalonStSlaveIdx, cMaxTimeToWait, "");

                -- Transmit receive bytes
                avalon_st_transmit(AVALON_ST_VVCT, cAvalonStMasterIdx, vDataset3_Expected_ST, "Send multiple bytes to databuffer");

                -- Check result
                -------------------------------
                await_value(DataAvailable, '1', 0 ns, 430 ns, "Waiting for Interrupt");
                ReleaseIRQ;

                for i in 1 to cNumberOfRows_MMRead_Dataset_C loop
                    avalon_mm_check(AVALON_MM_VVCT, cAvalonMmMasterIdx, vDataset3_Expected_MM(i).r_address, vDataset3_Expected_MM(i).data, "Check if transfer bits are read correctly");                    
                    await_completion(AVALON_MM_VVCT, cAvalonMmMasterIdx, cMaxTimeToWait, "");
                end loop;

                -- Wave 2:
                -------------------------------
                -- fill buffer
                FillBuffer(vDataset4_Input_MM, true);

                -- Start transfer
                StartTransferAndCheckTransferBit(false);

                -- compare avalon ss output with avalon mm input
                avalon_st_expect(AVALON_ST_VVCT, cAvalonStSlaveIdx, "Z", vDataset4_Expected_ST, "Compare LSB with expected LSB : Wave 2", ERROR, C_SCOPE);
                await_completion(AVALON_ST_VVCT, cAvalonStSlaveIdx, cMaxTimeToWait, "");

                -- Transmit receive bytes
                avalon_st_transmit(AVALON_ST_VVCT, cAvalonStMasterIdx, vDataset4_Expected_ST, "Send multiple bytes to databuffer");
                await_completion(AVALON_ST_VVCT, cAvalonStMasterIdx, cMaxTimeToWait, "");

                -- Check result
                -------------------------------
                await_value(DataAvailable, '1', 0 ns, 30 ns, "Waiting for Interrupt");
                ReleaseIRQ;

                for i in 1 to cNumberOfRows_MMRead_Dataset_C loop
                    avalon_mm_check(AVALON_MM_VVCT, cAvalonMmMasterIdx, vDataset4_Expected_MM(i).r_address, vDataset4_Expected_MM(i).data, "Check if transfer bits are read correctly");                    
                    await_completion(AVALON_MM_VVCT, cAvalonMmMasterIdx, cMaxTimeToWait, "");
                end loop;

                wait for 500 ns;

            --------------------------------------------------------------------------------------------------    
            -- Test 7:
            -- TBA
            --------------------------------------------------------------------------------------------------
            elsif run("Test 7 / Case 3 / Write Only: Initiate a new transfer after a short pause, st send delayed") then
                
                start_clock(CLOCK_GENERATOR_VVCT, cClockGeneratorIdx, "Start clock generator");
                ReleaseReset;
                ActivateIRQ;

                -- Wave 1:
                -------------------------------
                -- fill buffer
                FillBuffer(vDataset3_Input_MM, true);

                -- Start transfer
                StartTransferAndCheckTransferBit(false);

                -- Transmit receive bytes
                avalon_st_transmit(AVALON_ST_VVCT, cAvalonStMasterIdx, vDataset3_Expected_ST, "Send multiple bytes to databuffer");
                await_completion(AVALON_ST_VVCT, cAvalonStMasterIdx, cMaxTimeToWait, "");

                -- compare avalon ss output with avalon mm input
                avalon_st_expect(AVALON_ST_VVCT, cAvalonStSlaveIdx, "Z", vDataset3_Expected_ST, "Compare LSB with expected LSB : Wave 1", ERROR, C_SCOPE);
                await_completion(AVALON_ST_VVCT, cAvalonStSlaveIdx, cMaxTimeToWait, "");
                
                await_value(DataAvailable, '1', 0 ns, 30 ns, "Waiting for Interrupt");
                ReleaseIRQ;

                -- Wave 2:
                -------------------------------
                -- fill buffer
                FillBuffer(vDataset4_Input_MM, true);

                -- Start transfer
                StartTransferAndCheckTransferBit(false);

                -- Transmit receive bytes
                avalon_st_transmit(AVALON_ST_VVCT, cAvalonStMasterIdx, vDataset4_Expected_ST, "Send multiple bytes to databuffer");
                await_completion(AVALON_ST_VVCT, cAvalonStMasterIdx, cMaxTimeToWait, "");

                -- compare avalon ss output with avalon mm input
                avalon_st_expect(AVALON_ST_VVCT, cAvalonStSlaveIdx, "Z", vDataset4_Expected_ST, "Compare LSB with expected LSB : Wave 2", ERROR, C_SCOPE);
                await_completion(AVALON_ST_VVCT, cAvalonStSlaveIdx, cMaxTimeToWait, "");

                wait for 500 ns;

            --------------------------------------------------------------------------------------------------    
            -- Test 8:
            -- TBA
            --------------------------------------------------------------------------------------------------
            elsif run("Test 8 / Case 3 / Write Only: Initiate a new transfer after a short pause, st send and receive simultaneously") then
                
                start_clock(CLOCK_GENERATOR_VVCT, cClockGeneratorIdx, "Start clock generator");
                ReleaseReset;
                ActivateIRQ;

                -- Wave 1:
                -------------------------------
                -- fill buffer
                FillBuffer(vDataset3_Input_MM, true);

                -- Start transfer
                StartTransferAndCheckTransferBit(false);

                -- Transmit receive bytes
                avalon_st_transmit(AVALON_ST_VVCT, cAvalonStMasterIdx, vDataset3_Expected_ST, "Send multiple bytes to databuffer");

                -- compare avalon ss output with avalon mm input
                avalon_st_expect(AVALON_ST_VVCT, cAvalonStSlaveIdx, "Z", vDataset3_Expected_ST, "Compare LSB with expected LSB : Wave 1", ERROR, C_SCOPE);
                await_completion(AVALON_ST_VVCT, cAvalonStSlaveIdx, cMaxTimeToWait, "");

                await_value(DataAvailable, '1', 0 ns, 30 ns, "Waiting for Interrupt");
                ReleaseIRQ;
            
                -- Wave 2:
                -------------------------------
                -- fill buffer
                FillBuffer(vDataset4_Input_MM, true);

                -- Start transfer
                StartTransferAndCheckTransferBit(false);

                -- Transmit receive bytes
                avalon_st_transmit(AVALON_ST_VVCT, cAvalonStMasterIdx, vDataset4_Expected_ST, "Send multiple bytes to databuffer");

                -- compare avalon ss output with avalon mm input
                avalon_st_expect(AVALON_ST_VVCT, cAvalonStSlaveIdx, "Z", vDataset4_Expected_ST, "Compare LSB with expected LSB : Wave 2", ERROR, C_SCOPE);
                await_completion(AVALON_ST_VVCT, cAvalonStSlaveIdx, cMaxTimeToWait, "");

                wait for 500 ns;

            --------------------------------------------------------------------------------------------------    
            -- Test 9:
            -- TBA
            --------------------------------------------------------------------------------------------------
            elsif run("Test 9 / Case 3 / Write Only: Initiate a new transfer after a short pause, st receive delayed") then
                
                start_clock(CLOCK_GENERATOR_VVCT, cClockGeneratorIdx, "Start clock generator");
                ReleaseReset;
                ActivateIRQ;

                -- Wave 1:
                -------------------------------
                -- fill buffer
                FillBuffer(vDataset3_Input_MM, true);

                -- Start transfer
                StartTransferAndCheckTransferBit(false);
               
                -- compare avalon ss output with avalon mm input
                avalon_st_expect(AVALON_ST_VVCT, cAvalonStSlaveIdx, "Z", vDataset3_Expected_ST, "Compare LSB with expected LSB : Wave 1", ERROR, C_SCOPE);
                await_completion(AVALON_ST_VVCT, cAvalonStSlaveIdx, cMaxTimeToWait, "");

                -- Transmit receive bytes
                avalon_st_transmit(AVALON_ST_VVCT, cAvalonStMasterIdx, vDataset3_Expected_ST, "Send multiple bytes to databuffer");

                await_value(DataAvailable, '1', 0 ns, 430 ns, "Waiting for Interrupt");
                ReleaseIRQ;

                -- Wave 2:
                -------------------------------
                -- fill buffer
                FillBuffer(vDataset4_Input_MM, true);

                -- Start transfer
                StartTransferAndCheckTransferBit(false);

                -- compare avalon ss output with avalon mm input
                avalon_st_expect(AVALON_ST_VVCT, cAvalonStSlaveIdx, "Z", vDataset4_Expected_ST, "Compare LSB with expected LSB : Wave 2", ERROR, C_SCOPE);
                await_completion(AVALON_ST_VVCT, cAvalonStSlaveIdx, cMaxTimeToWait, "");

                -- Transmit receive bytes
                avalon_st_transmit(AVALON_ST_VVCT, cAvalonStMasterIdx, vDataset4_Expected_ST, "Send multiple bytes to databuffer");
                await_completion(AVALON_ST_VVCT, cAvalonStMasterIdx, cMaxTimeToWait, "");

                wait for 500 ns;

            --------------------------------------------------------------------------------------------------    
            -- Test 10:
            -- TBA
            --------------------------------------------------------------------------------------------------
            elsif run("Test 10 / Case 2 / Read Check: Fill buffer directly after starting the transfer, st send delayed") then
                
                start_clock(CLOCK_GENERATOR_VVCT, cClockGeneratorIdx, "Start clock generator");
                ReleaseReset;
                ActivateIRQ;

                -- Wave 1:
                -------------------------------
                -- fill buffer
                FillBuffer(vDataset3_Input_MM, true);

                -- Start Transfer
                StartTransferAndCheckTransferBit(false);
                    
                -- fill second buffer
                FillBuffer(vDataset4_Input_MM, false);

                avalon_st_transmit(AVALON_ST_VVCT, cAvalonStMasterIdx, vDataset3_Expected_ST, "Send multiple bytes to databuffer");
                await_completion(AVALON_ST_VVCT, cAvalonStMasterIdx, cMaxTimeToWait, "");

                -- compare avalon ss output with avalon mm input
                avalon_st_expect(AVALON_ST_VVCT, cAvalonStSlaveIdx, "Z", vDataset3_Expected_ST, "Compare LSB with expected LSB : Wave 1", ERROR, C_SCOPE);
                await_completion(AVALON_ST_VVCT, cAvalonStSlaveIdx, cMaxTimeToWait, "");

                -- Wave 2:
                -------------------------------
                -- Start transfer
                StartTransferAndCheckTransferBit(false);

                -- Check result
                -------------------------------
                await_value(DataAvailable, '1', 0 ns, 30 ns, "Waiting for Interrupt");
                ReleaseIRQ;

                -- Note: (Check result after starting the transfer, to initate a swap, to get the received bytes)
                for i in 1 to cNumberOfRows_MMRead_Dataset_C loop
                    avalon_mm_check(AVALON_MM_VVCT, cAvalonMmMasterIdx, vDataset3_Expected_MM(i).r_address, vDataset3_Expected_MM(i).data, "Check if transfer bits are read correctly");                    
                    await_completion(AVALON_MM_VVCT, cAvalonMmMasterIdx, cMaxTimeToWait, "");
                end loop;

                avalon_st_transmit(AVALON_ST_VVCT, cAvalonStMasterIdx, vDataset4_Expected_ST, "Send multiple bytes to databuffer");
                await_completion(AVALON_ST_VVCT, cAvalonStMasterIdx, cMaxTimeToWait, "");

                -- compare avalon ss output with avalon mm input
                avalon_st_expect(AVALON_ST_VVCT, cAvalonStSlaveIdx, "Z", vDataset4_Expected_ST, "Compare LSB with expected LSB : Wave 2", ERROR, C_SCOPE);
                await_completion(AVALON_ST_VVCT, cAvalonStSlaveIdx, cMaxTimeToWait, "");

                -- Check result
                -------------------------------
                await_value(DataAvailable, '1', 0 ns, 30 ns, "Waiting for Interrupt");
                ReleaseIRQ;

                for i in 1 to cNumberOfRows_MMRead_Dataset_C loop
                    avalon_mm_check(AVALON_MM_VVCT, cAvalonMmMasterIdx, vDataset4_Expected_MM(i).r_address, vDataset4_Expected_MM(i).data, "Check if transfer bits are read correctly");                    
                    await_completion(AVALON_MM_VVCT, cAvalonMmMasterIdx, cMaxTimeToWait, "");
                end loop;

                wait for 500 ns;

            --------------------------------------------------------------------------------------------------    
            -- Test 11:
            -- TBA
            --------------------------------------------------------------------------------------------------
            elsif run("Test 11 / Case 2 / Read Check: Fill buffer directly after starting the transfer, st send and receive simultaneously") then
                
                start_clock(CLOCK_GENERATOR_VVCT, cClockGeneratorIdx, "Start clock generator");
                ReleaseReset;
                ActivateIRQ;

                -- Wave 1:
                -------------------------------
                -- fill buffer
                FillBuffer(vDataset3_Input_MM, true);

                -- Start Transfer
                StartTransferAndCheckTransferBit(false);
                    
                -- fill second buffer
                FillBuffer(vDataset4_Input_MM, false);

                avalon_st_transmit(AVALON_ST_VVCT, cAvalonStMasterIdx, vDataset3_Expected_ST, "Send multiple bytes to databuffer");

                -- compare avalon ss output with avalon mm input
                avalon_st_expect(AVALON_ST_VVCT, cAvalonStSlaveIdx, "Z", vDataset3_Expected_ST, "Compare LSB with expected LSB : Wave 1", ERROR, C_SCOPE);
                await_completion(AVALON_ST_VVCT, cAvalonStSlaveIdx, cMaxTimeToWait, "");

                -- Wave 2:
                -------------------------------
                -- Start transfer
                StartTransferAndCheckTransferBit(false);

                -- Check result
                -------------------------------
                await_value(DataAvailable, '1', 0 ns, 30 ns, "Waiting for Interrupt");
                ReleaseIRQ;

                -- Note: (Check result after starting the transfer, to initate a swap, to get the received bytes)
                for i in 1 to cNumberOfRows_MMRead_Dataset_C loop
                    avalon_mm_check(AVALON_MM_VVCT, cAvalonMmMasterIdx, vDataset3_Expected_MM(i).r_address, vDataset3_Expected_MM(i).data, "Check if transfer bits are read correctly");                    
                    await_completion(AVALON_MM_VVCT, cAvalonMmMasterIdx, cMaxTimeToWait, "");
                end loop;

                avalon_st_transmit(AVALON_ST_VVCT, cAvalonStMasterIdx, vDataset4_Expected_ST, "Send multiple bytes to databuffer");

                -- compare avalon ss output with avalon mm input
                avalon_st_expect(AVALON_ST_VVCT, cAvalonStSlaveIdx, "Z", vDataset4_Expected_ST, "Compare LSB with expected LSB : Wave 2", ERROR, C_SCOPE);
                await_completion(AVALON_ST_VVCT, cAvalonStSlaveIdx, cMaxTimeToWait, "");

                -- Check result
                -------------------------------
                await_value(DataAvailable, '1', 0 ns, 30 ns, "Waiting for Interrupt");
                ReleaseIRQ;

                for i in 1 to cNumberOfRows_MMRead_Dataset_C loop
                    avalon_mm_check(AVALON_MM_VVCT, cAvalonMmMasterIdx, vDataset4_Expected_MM(i).r_address, vDataset4_Expected_MM(i).data, "Check if transfer bits are read correctly");                    
                    await_completion(AVALON_MM_VVCT, cAvalonMmMasterIdx, cMaxTimeToWait, "");
                end loop;

                wait for 500 ns;

            --------------------------------------------------------------------------------------------------    
            -- Test 12:
            -- TBA
            --------------------------------------------------------------------------------------------------
            elsif run("Test 12 / Case 2 / Read Check: Fill buffer directly after starting the transfer, st receive delayed") then
                
                start_clock(CLOCK_GENERATOR_VVCT, cClockGeneratorIdx, "Start clock generator");
                ReleaseReset;
                ActivateIRQ;

                -- Wave 1:
                -------------------------------
                -- fill buffer
                FillBuffer(vDataset3_Input_MM, true);

                -- Start Transfer
                StartTransferAndCheckTransferBit(false);
                    
                -- fill second buffer
                FillBuffer(vDataset4_Input_MM, false);

                -- compare avalon ss output with avalon mm input
                avalon_st_expect(AVALON_ST_VVCT, cAvalonStSlaveIdx, "Z", vDataset3_Expected_ST, "Compare LSB with expected LSB : Wave 1", ERROR, C_SCOPE);
                await_completion(AVALON_ST_VVCT, cAvalonStSlaveIdx, cMaxTimeToWait, "");
                
                avalon_st_transmit(AVALON_ST_VVCT, cAvalonStMasterIdx, vDataset3_Expected_ST, "Send multiple bytes to databuffer");

                -- Wave 2:
                -------------------------------
                -- Start transfer
                StartTransferAndCheckTransferBit(false);

                await_value(DataAvailable, '1', 0 ns, 430 ns, "Waiting for Interrupt");
                ReleaseIRQ;

                -- Check result
                -------------------------------
                -- Note: (Check result after starting the transfer, to initate a swap, to get the received bytes)
                for i in 1 to cNumberOfRows_MMRead_Dataset_C loop
                    avalon_mm_check(AVALON_MM_VVCT, cAvalonMmMasterIdx, vDataset3_Expected_MM(i).r_address, vDataset3_Expected_MM(i).data, "Check if transfer bits are read correctly");                    
                    await_completion(AVALON_MM_VVCT, cAvalonMmMasterIdx, cMaxTimeToWait, "");
                end loop;

                -- compare avalon ss output with avalon mm input
                avalon_st_expect(AVALON_ST_VVCT, cAvalonStSlaveIdx, "Z", vDataset4_Expected_ST, "Compare LSB with expected LSB : Wave 2", ERROR, C_SCOPE);
                await_completion(AVALON_ST_VVCT, cAvalonStSlaveIdx, cMaxTimeToWait, "");

                avalon_st_transmit(AVALON_ST_VVCT, cAvalonStMasterIdx, vDataset4_Expected_ST, "Send multiple bytes to databuffer");

                -- Check result
                -------------------------------
                await_value(DataAvailable, '1', 0 ns, 430 ns, "Waiting for Interrupt");
                ReleaseIRQ;

                for i in 1 to cNumberOfRows_MMRead_Dataset_C loop
                    avalon_mm_check(AVALON_MM_VVCT, cAvalonMmMasterIdx, vDataset4_Expected_MM(i).r_address, vDataset4_Expected_MM(i).data, "Check if transfer bits are read correctly");                    
                    await_completion(AVALON_MM_VVCT, cAvalonMmMasterIdx, cMaxTimeToWait, "");
                end loop;

                wait for 500 ns;

            --------------------------------------------------------------------------------------------------    
            -- Test 13:
            -- TBA
            --------------------------------------------------------------------------------------------------
            elsif run("Test 13 / Case 2 / Read Check: Fill buffer directly after starting the transfer, st send delayed") then
                
                start_clock(CLOCK_GENERATOR_VVCT, cClockGeneratorIdx, "Start clock generator");
                ReleaseReset;
                ActivateIRQ;

                -- Wave 1:
                -------------------------------
                -- fill buffer
                FillBuffer(vDataset3_Input_MM, true);

                -- Start Transfer
                StartTransferAndCheckTransferBit(false);
                    
                -- fill second buffer
                FillBuffer(vDataset4_Input_MM, false);

                avalon_st_transmit(AVALON_ST_VVCT, cAvalonStMasterIdx, vDataset3_Expected_ST, "Send multiple bytes to databuffer");
                await_completion(AVALON_ST_VVCT, cAvalonStMasterIdx, cMaxTimeToWait, "");

                -- compare avalon ss output with avalon mm input
                avalon_st_expect(AVALON_ST_VVCT, cAvalonStSlaveIdx, "Z", vDataset3_Expected_ST, "Compare LSB with expected LSB : Wave 1", ERROR, C_SCOPE);
                await_completion(AVALON_ST_VVCT, cAvalonStSlaveIdx, cMaxTimeToWait, "");

                -- Wave 2:
                -------------------------------
                -- Start transfer
                StartTransferAndCheckTransferBit(false);

                -- Check result
                -------------------------------
                await_value(DataAvailable, '1', 0 ns, 30 ns, "Waiting for Interrupt");
                ReleaseIRQ;

                -- Note: (Check result after starting the transfer, to initate a swap, to get the received bytes)
                for i in 1 to cNumberOfRows_MMRead_Dataset_C loop
                    avalon_mm_check(AVALON_MM_VVCT, cAvalonMmMasterIdx, vDataset3_Expected_MM(i).r_address, vDataset3_Expected_MM(i).data, "Check if transfer bits are read correctly");                    
                    await_completion(AVALON_MM_VVCT, cAvalonMmMasterIdx, cMaxTimeToWait, "");
                end loop;

                avalon_st_transmit(AVALON_ST_VVCT, cAvalonStMasterIdx, vDataset4_Expected_ST, "Send multiple bytes to databuffer");
                await_completion(AVALON_ST_VVCT, cAvalonStMasterIdx, cMaxTimeToWait, "");

                -- compare avalon ss output with avalon mm input
                avalon_st_expect(AVALON_ST_VVCT, cAvalonStSlaveIdx, "Z", vDataset4_Expected_ST, "Compare LSB with expected LSB : Wave 2", ERROR, C_SCOPE);
                await_completion(AVALON_ST_VVCT, cAvalonStSlaveIdx, cMaxTimeToWait, "");

                -- Check result
                -------------------------------
                await_value(DataAvailable, '1', 0 ns, 30 ns, "Waiting for Interrupt");
                ReleaseIRQ;

                for i in 1 to cNumberOfRows_MMRead_Dataset_C loop
                    avalon_mm_check(AVALON_MM_VVCT, cAvalonMmMasterIdx, vDataset4_Expected_MM(i).r_address, vDataset4_Expected_MM(i).data, "Check if transfer bits are read correctly");                    
                    await_completion(AVALON_MM_VVCT, cAvalonMmMasterIdx, cMaxTimeToWait, "");
                end loop;

                wait for 500 ns;

            --------------------------------------------------------------------------------------------------    
            -- Test 14:
            -- TBA
            --------------------------------------------------------------------------------------------------
            elsif run("Test 14 / Case 2 / Read Check: Fill buffer directly after starting the transfer, st send and receive simultaneously") then
                
                start_clock(CLOCK_GENERATOR_VVCT, cClockGeneratorIdx, "Start clock generator");
                ReleaseReset;
                ActivateIRQ;

                -- Wave 1:
                -------------------------------
                -- fill buffer
                FillBuffer(vDataset3_Input_MM, true);

                -- Start Transfer
                StartTransferAndCheckTransferBit(false);
                    
                -- fill second buffer
                FillBuffer(vDataset4_Input_MM, false);

                avalon_st_transmit(AVALON_ST_VVCT, cAvalonStMasterIdx, vDataset3_Expected_ST, "Send multiple bytes to databuffer");

                -- compare avalon ss output with avalon mm input
                avalon_st_expect(AVALON_ST_VVCT, cAvalonStSlaveIdx, "Z", vDataset3_Expected_ST, "Compare LSB with expected LSB : Wave 1", ERROR, C_SCOPE);
                await_completion(AVALON_ST_VVCT, cAvalonStSlaveIdx, cMaxTimeToWait, "");

                -- Wave 2:
                -------------------------------
                -- Start transfer
                StartTransferAndCheckTransferBit(false);

                -- Check result
                -------------------------------
                await_value(DataAvailable, '1', 0 ns, 30 ns, "Waiting for Interrupt");
                ReleaseIRQ;

                -- Note: (Check result after starting the transfer, to initate a swap, to get the received bytes)
                for i in 1 to cNumberOfRows_MMRead_Dataset_C loop
                    avalon_mm_check(AVALON_MM_VVCT, cAvalonMmMasterIdx, vDataset3_Expected_MM(i).r_address, vDataset3_Expected_MM(i).data, "Check if transfer bits are read correctly");                    
                    await_completion(AVALON_MM_VVCT, cAvalonMmMasterIdx, cMaxTimeToWait, "");
                end loop;

                avalon_st_transmit(AVALON_ST_VVCT, cAvalonStMasterIdx, vDataset4_Expected_ST, "Send multiple bytes to databuffer");

                -- compare avalon ss output with avalon mm input
                avalon_st_expect(AVALON_ST_VVCT, cAvalonStSlaveIdx, "Z", vDataset4_Expected_ST, "Compare LSB with expected LSB : Wave 2", ERROR, C_SCOPE);
                await_completion(AVALON_ST_VVCT, cAvalonStSlaveIdx, cMaxTimeToWait, "");

                -- Check result
                -------------------------------
                await_value(DataAvailable, '1', 0 ns, 30 ns, "Waiting for Interrupt");
                ReleaseIRQ;

                for i in 1 to cNumberOfRows_MMRead_Dataset_C loop
                    avalon_mm_check(AVALON_MM_VVCT, cAvalonMmMasterIdx, vDataset4_Expected_MM(i).r_address, vDataset4_Expected_MM(i).data, "Check if transfer bits are read correctly");                    
                    await_completion(AVALON_MM_VVCT, cAvalonMmMasterIdx, cMaxTimeToWait, "");
                end loop;

                wait for 500 ns;

            --------------------------------------------------------------------------------------------------    
            -- Test 15:
            -- TBA
            --------------------------------------------------------------------------------------------------
            elsif run("Test 15 / Case 2 / Read Check: Fill buffer directly after starting the transfer, st receive delayed") then
                
                start_clock(CLOCK_GENERATOR_VVCT, cClockGeneratorIdx, "Start clock generator");
                ReleaseReset;
                ActivateIRQ;

                -- Wave 1:
                -------------------------------
                -- fill buffer
                FillBuffer(vDataset3_Input_MM, true);

                -- Start Transfer
                StartTransferAndCheckTransferBit(false);
                    
                -- fill second buffer
                FillBuffer(vDataset4_Input_MM, false);

                -- compare avalon ss output with avalon mm input
                avalon_st_expect(AVALON_ST_VVCT, cAvalonStSlaveIdx, "Z", vDataset3_Expected_ST, "Compare LSB with expected LSB : Wave 1", ERROR, C_SCOPE);
                await_completion(AVALON_ST_VVCT, cAvalonStSlaveIdx, cMaxTimeToWait, "");
                
                avalon_st_transmit(AVALON_ST_VVCT, cAvalonStMasterIdx, vDataset3_Expected_ST, "Send multiple bytes to databuffer");

                -- Wave 2:
                -------------------------------
                -- Start transfer
                StartTransferAndCheckTransferBit(false);

                await_value(DataAvailable, '1', 0 ns, 430 ns, "Waiting for Interrupt");
                ReleaseIRQ;

                -- Check result
                -------------------------------
                -- Note: (Check result after starting the transfer, to initate a swap, to get the received bytes)
                for i in 1 to cNumberOfRows_MMRead_Dataset_C loop
                    avalon_mm_check(AVALON_MM_VVCT, cAvalonMmMasterIdx, vDataset3_Expected_MM(i).r_address, vDataset3_Expected_MM(i).data, "Check if transfer bits are read correctly");                    
                    await_completion(AVALON_MM_VVCT, cAvalonMmMasterIdx, cMaxTimeToWait, "");
                end loop;

                -- compare avalon ss output with avalon mm input
                avalon_st_expect(AVALON_ST_VVCT, cAvalonStSlaveIdx, "Z", vDataset4_Expected_ST, "Compare LSB with expected LSB : Wave 2", ERROR, C_SCOPE);
                await_completion(AVALON_ST_VVCT, cAvalonStSlaveIdx, cMaxTimeToWait, "");

                avalon_st_transmit(AVALON_ST_VVCT, cAvalonStMasterIdx, vDataset4_Expected_ST, "Send multiple bytes to databuffer");

                -- Check result
                -------------------------------
                await_value(DataAvailable, '1', 0 ns, 430 ns, "Waiting for Interrupt");
                ReleaseIRQ;

                for i in 1 to cNumberOfRows_MMRead_Dataset_C loop
                    avalon_mm_check(AVALON_MM_VVCT, cAvalonMmMasterIdx, vDataset4_Expected_MM(i).r_address, vDataset4_Expected_MM(i).data, "Check if transfer bits are read correctly");                    
                    await_completion(AVALON_MM_VVCT, cAvalonMmMasterIdx, cMaxTimeToWait, "");
                end loop;

                wait for 500 ns;

            --------------------------------------------------------------------------------------------------    
            -- Test 16:
            -- TBA
            --------------------------------------------------------------------------------------------------
            elsif run("Test 16 / Case 2 / Write Only: Fill buffer directly after starting the transfer, st send delayed") then
                
                start_clock(CLOCK_GENERATOR_VVCT, cClockGeneratorIdx, "Start clock generator");
                ReleaseReset;
                ActivateIRQ;

                -- Wave 1:
                -------------------------------
                -- fill buffer
                FillBuffer(vDataset3_Input_MM, true);

                -- Start Transfer
                StartTransferAndCheckTransferBit(false);

                -- fill second buffer
                FillBuffer(vDataset4_Input_MM, false);
                
                avalon_st_transmit(AVALON_ST_VVCT, cAvalonStMasterIdx, vDataset3_Expected_ST, "Send multiple bytes to databuffer");
                await_completion(AVALON_ST_VVCT, cAvalonStMasterIdx, cMaxTimeToWait, "");
                
                -- compare avalon ss output with avalon mm input
                avalon_st_expect(AVALON_ST_VVCT, cAvalonStSlaveIdx, "Z", vDataset3_Expected_ST, "Compare LSB with expected LSB : Wave 1", ERROR, C_SCOPE);
                await_completion(AVALON_ST_VVCT, cAvalonStSlaveIdx, cMaxTimeToWait, "");

                -- Wave 2:
                -------------------------------
                -- Start transfer
                StartTransferAndCheckTransferBit(false);

                avalon_st_transmit(AVALON_ST_VVCT, cAvalonStMasterIdx, vDataset3_Expected_ST, "Send multiple bytes to databuffer");
                await_completion(AVALON_ST_VVCT, cAvalonStMasterIdx, cMaxTimeToWait, "");

                -- compare avalon ss output with avalon mm input
                avalon_st_expect(AVALON_ST_VVCT, cAvalonStSlaveIdx, "Z", vDataset4_Expected_ST, "Compare LSB with expected LSB : Wave 2", ERROR, C_SCOPE);
                await_completion(AVALON_ST_VVCT, cAvalonStSlaveIdx, cMaxTimeToWait, "");

                wait for 500 ns;

            --------------------------------------------------------------------------------------------------    
            -- Test 17:
            -- TBA
            --------------------------------------------------------------------------------------------------
            elsif run("Test 17 / Case 2 / Read Check: 4 Waves") then
                
                start_clock(CLOCK_GENERATOR_VVCT, cClockGeneratorIdx, "Start clock generator");
                ReleaseReset;
                ActivateIRQ;

                for j in 1 to 2 loop
                    -- Wave 1:
                    -------------------------------
                    -- fill buffer
                    FillBuffer(vDataset3_Input_MM, true);

                    -- Start Transfer
                    StartTransferAndCheckTransferBit(false);
                        
                    -- fill second buffer
                    FillBuffer(vDataset4_Input_MM, false);

                    avalon_st_transmit(AVALON_ST_VVCT, cAvalonStMasterIdx, vDataset3_Expected_ST, "Send multiple bytes to databuffer");

                    -- compare avalon ss output with avalon mm input
                    avalon_st_expect(AVALON_ST_VVCT, cAvalonStSlaveIdx, "Z", vDataset3_Expected_ST, "Compare LSB with expected LSB : Wave 1", ERROR, C_SCOPE);
                    await_completion(AVALON_ST_VVCT, cAvalonStSlaveIdx, cMaxTimeToWait, "");

                    -- Wave 2:
                    -------------------------------
                    -- Start transfer
                    StartTransferAndCheckTransferBit(false);

                    -- Check result
                    -------------------------------
                    await_value(DataAvailable, '1', 0 ns, 30 ns, "Waiting for Interrupt");
                    ReleaseIRQ;

                    -- Note: (Check result after starting the transfer, to initate a swap, to get the received bytes)
                    for i in 1 to cNumberOfRows_MMRead_Dataset_C loop
                        avalon_mm_check(AVALON_MM_VVCT, cAvalonMmMasterIdx, vDataset3_Expected_MM(i).r_address, vDataset3_Expected_MM(i).data, "Check if transfer bits are read correctly");                    
                        await_completion(AVALON_MM_VVCT, cAvalonMmMasterIdx, cMaxTimeToWait, "");
                    end loop;

                    avalon_st_transmit(AVALON_ST_VVCT, cAvalonStMasterIdx, vDataset4_Expected_ST, "Send multiple bytes to databuffer");

                    -- compare avalon ss output with avalon mm input
                    avalon_st_expect(AVALON_ST_VVCT, cAvalonStSlaveIdx, "Z", vDataset4_Expected_ST, "Compare LSB with expected LSB : Wave 2", ERROR, C_SCOPE);
                    await_completion(AVALON_ST_VVCT, cAvalonStSlaveIdx, cMaxTimeToWait, "");

                    -- Check result
                    -------------------------------
                    await_value(DataAvailable, '1', 0 ns, 30 ns, "Waiting for Interrupt");
                    ReleaseIRQ;

                    for i in 1 to cNumberOfRows_MMRead_Dataset_C loop
                        avalon_mm_check(AVALON_MM_VVCT, cAvalonMmMasterIdx, vDataset4_Expected_MM(i).r_address, vDataset4_Expected_MM(i).data, "Check if transfer bits are read correctly");                    
                        await_completion(AVALON_MM_VVCT, cAvalonMmMasterIdx, cMaxTimeToWait, "");
                    end loop;
                end loop;

            --------------------------------------------------------------------------------------------------    
            -- Test 18
            -- TBA
            --------------------------------------------------------------------------------------------------
            elsif run("Test 18 / Case 2 / Write Only: 4 Waves") then
                
                start_clock(CLOCK_GENERATOR_VVCT, cClockGeneratorIdx, "Start clock generator");
                ReleaseReset;
                ActivateIRQ;

                -- Wave 1:
                -------------------------------
                -- fill buffer
                FillBuffer(vDataset3_Input_MM, true);

                -- Start Transfer
                StartTransferAndCheckTransferBit(false);
                    
                -- fill second buffer
                FillBuffer(vDataset4_Input_MM, false);

                avalon_st_transmit(AVALON_ST_VVCT, cAvalonStMasterIdx, vDataset3_Expected_ST, "Send multiple bytes to databuffer");

                -- compare avalon ss output with avalon mm input
                avalon_st_expect(AVALON_ST_VVCT, cAvalonStSlaveIdx, "Z", vDataset3_Expected_ST, "Compare LSB with expected LSB : Wave 1", ERROR, C_SCOPE);
                await_completion(AVALON_ST_VVCT, cAvalonStSlaveIdx, cMaxTimeToWait, "");

                -- Wave 2:
                -------------------------------                 
                -- Start transfer
                StartTransferAndCheckTransferBit(false);

                await_value(DataAvailable, '1', 0 ns, 30 ns, "Waiting for Interrupt");
                ReleaseIRQ;

                -- fill buffer
                FillBuffer(vDataset3_Input_MM, false);

                avalon_st_transmit(AVALON_ST_VVCT, cAvalonStMasterIdx, vDataset4_Expected_ST, "Send multiple bytes to databuffer");

                -- compare avalon ss output with avalon mm input
                avalon_st_expect(AVALON_ST_VVCT, cAvalonStSlaveIdx, "Z", vDataset4_Expected_ST, "Compare LSB with expected LSB : Wave 2", ERROR, C_SCOPE);
                await_completion(AVALON_ST_VVCT, cAvalonStSlaveIdx, cMaxTimeToWait, "");

                -- Wave 3:
                -------------------------------             
                -- Start transfer
                StartTransferAndCheckTransferBit(false);

                await_value(DataAvailable, '1', 0 ns, 30 ns, "Waiting for Interrupt");
                ReleaseIRQ;

                -- fill buffer
                FillBuffer(vDataset4_Input_MM, false);

                avalon_st_transmit(AVALON_ST_VVCT, cAvalonStMasterIdx, vDataset3_Expected_ST, "Send multiple bytes to databuffer");

                -- compare avalon ss output with avalon mm input
                avalon_st_expect(AVALON_ST_VVCT, cAvalonStSlaveIdx, "Z", vDataset3_Expected_ST, "Compare LSB with expected LSB : Wave 2", ERROR, C_SCOPE);
                await_completion(AVALON_ST_VVCT, cAvalonStSlaveIdx, cMaxTimeToWait, "");

                -- Wave 4:
                -------------------------------
                StartTransferAndCheckTransferBit(false);

                -- await_value(DataAvailable, '1', 0 ns, 30 ns, "Waiting for Interrupt");
                -- ReleaseIRQ;

                avalon_st_transmit(AVALON_ST_VVCT, cAvalonStMasterIdx, vDataset4_Expected_ST, "Send multiple bytes to databuffer");

                -- compare avalon ss output with avalon mm input
                avalon_st_expect(AVALON_ST_VVCT, cAvalonStSlaveIdx, "Z", vDataset4_Expected_ST, "Compare LSB with expected LSB : Wave 2", ERROR, C_SCOPE);
                await_completion(AVALON_ST_VVCT, cAvalonStSlaveIdx, cMaxTimeToWait, "");

                wait for 500 ns;

            -- --------------------------------------------------------------------------------------------------    
            -- -- Test 8:
            -- -- This test is for checking, that transmitted values are correctly written by the avalon master
            -- --------------------------------------------------------------------------------------------------
            -- elsif run("Test 8: Transmit to Databuffer, check data with Avalon MM") then
                
            --     start_clock(CLOCK_GENERATOR_VVCT, cClockGeneratorIdx, "Start clock generator");
            --     ReleaseReset;
            --     ActivateIRQ;

            --     -- Transmit data
            --     -- (so bytes can be stored)
            --     -------------------------------
            --     -- fill buffer
            --     FillBuffer(vDataset5_Input_MM, true);

            --     -- Start transfer
            --     StartTransferAndCheckTransferBit(false);

            --     -- compare avalon ss output with avalon mm input
            --     avalon_st_expect(AVALON_ST_VVCT, cAvalonStSlaveIdx, "Z", vDataset5_Input_ST, "Compare LSB with expected LSB : Wave 2", ERROR, C_SCOPE);
            --     await_completion(AVALON_ST_VVCT, cAvalonStSlaveIdx, cMaxTimeToWait, "");

            --     -- Receive and store data
            --     -- (so bytes can be stored)
            --     -------------------------------
            --     avalon_st_transmit(AVALON_ST_VVCT, cAvalonStMasterIdx, vDataset5_Input_ST, "Send multiple bytes to databuffer");
            --     await_completion(AVALON_ST_VVCT, cAvalonStMasterIdx, cMaxTimeToWait, "");

            --     -- wait for a second, so the transfer signal gets pulled to low
            --     wait for 100 ns;

            --     -- Check result
            --     -------------------------------
            --     for i in 1 to cNumberOfRows_MMWrite_Dataset_D loop
            --         avalon_mm_check(AVALON_MM_VVCT, cAvalonMmMasterIdx, vDataset5_Expected(i).r_address, vDataset5_Expected(i).data, "Check if transfer bits are read correctly");                    
            --         await_completion(AVALON_MM_VVCT, cAvalonMmMasterIdx, cMaxTimeToWait, "");
            --     end loop;

            -- --------------------------------------------------------------------------------------------------    
            -- -- Test 22
            -- -- TBA
            -- --------------------------------------------------------------------------------------------------
            -- elsif run("Test 22 / Case 2 / Write Only: 4 Waves ") then
                
            --     start_clock(CLOCK_GENERATOR_VVCT, cClockGeneratorIdx, "Start clock generator");
            --     ReleaseReset;
            --     ActivateIRQ;

            --     -- Transmit data
            --     -- (so bytes can be stored)
            --     -------------------------------
            --     -- fill buffer
            --     FillBuffer(vDataset5_Input_MM, true);

            --     -- Start transfer
            --     StartTransferAndCheckTransferBit(false);

            --     -- compare avalon ss output with avalon mm input
            --     avalon_st_expect(AVALON_ST_VVCT, cAvalonStSlaveIdx, "Z", vDataset5_Input_ST, "Compare LSB with expected LSB : Wave 2", ERROR, C_SCOPE);
            --     await_completion(AVALON_ST_VVCT, cAvalonStSlaveIdx, cMaxTimeToWait, "");

            --     FillBuffer(vDataset6_Input_MM, true);

            --     -- Receive and store data
            --     -- (so bytes can be stored)
            --     -------------------------------
            --     avalon_st_transmit(AVALON_ST_VVCT, cAvalonStMasterIdx, vDataset5_Input_ST, "Send multiple bytes to databuffer");
            --     await_completion(AVALON_ST_VVCT, cAvalonStMasterIdx, cMaxTimeToWait, "");

            --     -- wait for a second, so the transfer signal gets pulled to low
            --     wait for 100 ns;

            --     -- Start transfer (and initiate buffer swap)
            --     StartTransferAndCheckTransferBit(false);

            --     -- Check result
            --     -------------------------------
            --     for i in 1 to cNumberOfRows_MMWrite_Dataset_D loop
            --         avalon_mm_check(AVALON_MM_VVCT, cAvalonMmMasterIdx, vDataset5_Expected(i).r_address, vDataset5_Expected(i).data, "Check if transfer bits are read correctly");                    
            --         await_completion(AVALON_MM_VVCT, cAvalonMmMasterIdx, cMaxTimeToWait, "");
            --     end loop;

            --     -- compare avalon ss output with avalon mm input
            --     avalon_st_expect(AVALON_ST_VVCT, cAvalonStSlaveIdx, "Z", vDataset6_Input_ST, "Compare LSB with expected LSB : Wave 2", ERROR, C_SCOPE);
            --     await_completion(AVALON_ST_VVCT, cAvalonStSlaveIdx, cMaxTimeToWait, "");

            --     -- Receive and store data
            --     -- (so bytes can be stored)
            --     -------------------------------
            --     avalon_st_transmit(AVALON_ST_VVCT, cAvalonStMasterIdx, vDataset6_Input_ST, "Send multiple bytes to databuffer");
            --     await_completion(AVALON_ST_VVCT, cAvalonStMasterIdx, cMaxTimeToWait, "");

            --     -- wait for a second, so the transfer signal gets pulled to low
            --     wait for 100 ns;

            --     -- Check result
            --     -------------------------------
            --     for i in 1 to cNumberOfRows_MMWrite_Dataset_D loop
            --         avalon_mm_check(AVALON_MM_VVCT, cAvalonMmMasterIdx, vDataset6_Expected(i).r_address, vDataset6_Expected(i).data, "Check if transfer bits are read correctly");                    
            --         await_completion(AVALON_MM_VVCT, cAvalonMmMasterIdx, cMaxTimeToWait, "");
            --     end loop;

            -- --------------------------------------------------------------------------------------------------    
            -- -- Test 9: TODO:
            -- -- This test is for checking, that transmitted values are correctly written by the avalon master
            -- --------------------------------------------------------------------------------------------------
            -- elsif run("Test 9: Transmit to Databuffer, check data with Avalon MM: 2x waves") then
                
            --     start_clock(CLOCK_GENERATOR_VVCT, cClockGeneratorIdx, "Start clock generator");
            --     ReleaseReset;
            --     ActivateIRQ;

            --     -- Transmit data
            --     -- (so bytes can be stored)
            --     -------------------------------
            --     -- fill buffer
            --     FillBuffer(vDataset5_Input_MM, true);

            --     -- Start transfer
            --     StartTransferAndCheckTransferBit(false);

            --     -- compare avalon ss output with avalon mm input
            --     avalon_st_expect(AVALON_ST_VVCT, cAvalonStSlaveIdx, "Z", vDataset5_Input_ST, "Compare LSB with expected LSB : Wave 2", ERROR, C_SCOPE);
            --     await_completion(AVALON_ST_VVCT, cAvalonStSlaveIdx, cMaxTimeToWait, "");

            --     FillBuffer(vDataset6_Input_MM, true);

            --     -- Receive and store data
            --     -- (so bytes can be stored)
            --     -------------------------------
            --     avalon_st_transmit(AVALON_ST_VVCT, cAvalonStMasterIdx, vDataset5_Input_ST, "Send multiple bytes to databuffer");
            --     await_completion(AVALON_ST_VVCT, cAvalonStMasterIdx, cMaxTimeToWait, "");

            --     -- wait for a second, so the transfer signal gets pulled to low
            --     wait for 100 ns;

            --     -- Start transfer (and initiate buffer swap)
            --     StartTransferAndCheckTransferBit(false);

            --     -- Check result
            --     -------------------------------
            --     for i in 1 to cNumberOfRows_MMWrite_Dataset_D loop
            --         avalon_mm_check(AVALON_MM_VVCT, cAvalonMmMasterIdx, vDataset5_Expected(i).r_address, vDataset5_Expected(i).data, "Check if transfer bits are read correctly");                    
            --         await_completion(AVALON_MM_VVCT, cAvalonMmMasterIdx, cMaxTimeToWait, "");
            --     end loop;

            --     -- compare avalon ss output with avalon mm input
            --     avalon_st_expect(AVALON_ST_VVCT, cAvalonStSlaveIdx, "Z", vDataset6_Input_ST, "Compare LSB with expected LSB : Wave 2", ERROR, C_SCOPE);
            --     await_completion(AVALON_ST_VVCT, cAvalonStSlaveIdx, cMaxTimeToWait, "");

            --     -- Receive and store data
            --     -- (so bytes can be stored)
            --     -------------------------------
            --     avalon_st_transmit(AVALON_ST_VVCT, cAvalonStMasterIdx, vDataset6_Input_ST, "Send multiple bytes to databuffer");
            --     await_completion(AVALON_ST_VVCT, cAvalonStMasterIdx, cMaxTimeToWait, "");

            --     -- wait for a second, so the transfer signal gets pulled to low
            --     wait for 100 ns;

            --     -- Check result
            --     -------------------------------
            --     for i in 1 to cNumberOfRows_MMWrite_Dataset_D loop
            --         avalon_mm_check(AVALON_MM_VVCT, cAvalonMmMasterIdx, vDataset6_Expected(i).r_address, vDataset6_Expected(i).data, "Check if transfer bits are read correctly");                    
            --         await_completion(AVALON_MM_VVCT, cAvalonMmMasterIdx, cMaxTimeToWait, "");
            --     end loop;
 
            end if;
        end loop;

        test_runner_cleanup(runner);

    end process; 
end architecture;

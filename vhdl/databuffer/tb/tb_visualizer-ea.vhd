-------------------------------------------------------------------------------
-- Title      : Temporary testbench for Visualizer unit
-- Description: tests the bcd counter, the 7segmentdecoder and the LED visualization
--              as well the control functionaliy of all of the above
-- CVS: $Id: $
-------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library vunit_lib;
context vunit_lib.vunit_context;

use work.pkg_visualizer.all;

entity tb_Visualizer is
    generic (runner_cfg : string); -- used by VUnit
end entity tb_Visualizer;

architecture Bhv of tb_Visualizer is

    type aExpectedHexValues is array(cNrOfHex - 1 downto 0) of natural range 0 to 15;

    -- General Control
    signal Clk   : std_ulogic := '0';
    signal Reset : std_ulogic;

    -- Control Inputs
    signal ResetCounter     : std_ulogic;
    signal IncCounterByOne  : std_ulogic;
    signal SetAllLedsOn     : std_ulogic;
    signal SetAllLedsOff    : std_ulogic;
    signal SetLightChaserOn : std_ulogic;

    -- Visualization Outputs
    signal Leds : std_ulogic_vector(cNrOfLeds-1 downto 0);
    signal Hexes : aHexSegValues;

    constant cClkFrequency : natural := 50E6;

begin
    DUT : entity work.visualizer
    port map(
                clk               => Clk,    
                reset_n           => Reset,
                resetCounter      => ResetCounter,
                incCounterByOne   => IncCounterByOne,
                setAllLedsOn      => SetAllLedsOn,
                setAllLedsOff     => SetAllLedsOff,
                setLightChaserOn  => SetLightChaserOn,
                oleds             => Leds,
                ohex0             => Hexes(0),
                ohex1             => Hexes(1),
                ohex2             => Hexes(2),
                ohex3             => Hexes(3),
                ohex4             => Hexes(4),
                ohex5             => Hexes(5)
            );

    -- Clock Generator
    Clk <= not(Clk) after 1 sec / cClkFrequency / 2; 

    Stimuli : process is

        procedure check_hexes(constant expected: in natural) is
            variable still_left : natural;
            variable remainder : natural;
        begin
            still_left := expected;
            for i in Hexes'low to Hexes'high loop
                remainder := still_left mod 10;
                still_left := still_left / 10;
                check_equal(Hexes(i), num_to_7seg(to_unsigned(remainder, aHexDigit'length)), "Comparing 7Segment Display " & integer'image(i) & " with expected value");
            end loop;
        end procedure;

    begin
        test_runner_setup(runner, runner_cfg); -- VUnit setup

        while test_suite loop
            if run("Testcase 1: Testing edge detection and led states") then
                Reset <= '0';
                IncCounterByOne  <= '0';
                SetAllLedsOn     <= '0';
                SetAllLedsOff    <= '0';
                SetLightChaserOn <= '0';
                    -- wait for 100 ns;
                wait for 120000 ns;

                Reset <= '1';
                    -- wait for 100 ns;
                wait for 120000 ns;

                    -- Check if edge detection works
                IncCounterByOne <= '1';
                    -- wait for 100 ns;
                wait for 120000 ns;

                    -- Check if edge detection works
                SetAllLedsOn <= '1';
                    -- wait for 100 ns;
                wait for 120000 ns;

                    -- Check if edge detection works
                SetAllLedsOff <= '1';
                    -- wait for 100 ns;
                wait for 120000 ns;

                    -- Check if edge detection works
                SetLightChaserOn <= '1';
                SetAllLedsOn <= '0';      -- prepare for next test
                SetAllLedsOff <= '0';     -- prepare for next test
                                          -- wait for 200 ns;
                wait for 240000 ns;

                    -- Check if edge detection works, if two control inputs are given
                SetAllLedsOn <= '1';
                SetAllLedsOff <= '1';
                    -- wait for 100 ns;
                wait for 120000 ns;

                SetAllLedsOn <= '0';      -- prepare for next test
                SetAllLedsOff <= '0';     -- prepare for next test
                SetLightChaserOn <= '0';  -- prepare for next test
                                          -- wait for 100 ns;
                wait for 120000 ns;

                SetAllLedsOn <= '1';
                SetLightChaserOn <= '1';
                    -- wait for 100 ns;
                wait for 120000 ns;

                SetAllLedsOn <= '0';      -- prepare for next test
                SetLightChaserOn <= '0';  -- prepare for next test
                                          -- wait for 100 ns;
                wait for 120000 ns;

                SetAllLedsOn <= '1';
                SetAllLedsOff <= '1';
                SetLightChaserOn <= '1';
                    -- wait for 100 ns;
                wait for 120000 ns;

            elsif run("Testcase 2: BCD Counter Increment") then
                Reset <= '0';
                IncCounterByOne  <= '0';
                SetAllLedsOn     <= '0';
                SetAllLedsOff    <= '0';
                SetLightChaserOn <= '0';
                    --wait for 100 ns;
                wait for 1200 ns;


                Reset <= '1';
                    --wait for 50 ns;
                wait for 1200 ns;

                check_hexes(0);
                    -- Check if edge detection works
                for i in 1 to 33 loop
                    IncCounterByOne <= '1';
                        --wait for 100 ns;
                    wait for 1200 ns;
                    check_hexes(i);

                    IncCounterByOne <= '0';
                        --wait for 100 ns;
                    wait for 1200 ns;
                end loop;
            end if;
        end loop;

        test_runner_cleanup(runner);
    end process Stimuli;
end architecture;

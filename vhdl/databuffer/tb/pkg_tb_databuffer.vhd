----------------------------------------------------------------------------------------------------------------
-- Workfile:    pkg_tb_databuffer.vhd
-- Author:      Tobias Kerbl
----------------------------------------------------------------------------------------------------------------
-- Description: This package contains all constants, functions and definitions for the testbench of the'Databuffer'. 
--              It also contains constants for the Simulation via UVVM (BFMs, etc.)
----------------------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------
-- Libraries
--------------------------------------------------------------------------

-- Databuffer library
library work; 
use work.pkg_databuffer.all;

-- UVVM librarys
library uvvm_util;
context uvvm_util.uvvm_util_context;
library uvvm_vvc_framework;
use uvvm_vvc_framework.ti_vvc_framework_support_pkg.all;

-- -- Clock Generator librarys
library bitvis_vip_clock_generator;
context bitvis_vip_clock_generator.vvc_context;

-- -- Avalon ST librarys
library bitvis_vip_avalon_st;
use bitvis_vip_avalon_st.avalon_st_bfm_pkg.all;

-- -- Avalon MM librarys
library bitvis_vip_avalon_mm;
use bitvis_vip_avalon_mm.avalon_mm_bfm_pkg.all;

--------------------------------------------------------------------------
-- Package
--------------------------------------------------------------------------
package pkg_tb_databuffer is

    ---------------------------------------
    -- Constants
    ---------------------------------------
    constant cAvalonStMasterIdx    : natural := 0;
    constant cAvalonStSlaveIdx     : natural := 1;
    constant cAvalonMmMasterIdx    : natural := 0;
    constant cClockGeneratorIdx    : natural := 1;

    constant cAvalonStChannelWidth : natural := 1;
    constant cAvalonStErrorWidth   : natural := 1;
    constant cAvalonStEmptyWidth   : natural := 1;

    constant cMaxTimeToWait        : time := 1 ms;

    ---------------------------------------
    -- Avalon Streaming Slave - BFM Config 
    ---------------------------------------
    constant C_AVALON_ST_SLAVE_BFM_CONFIG : t_avalon_st_bfm_config := (
        max_wait_cycles          => 1000,
        max_wait_cycles_severity => ERROR,
        clock_period             => cClkPeriod,
        clock_period_margin      => 0 ns,
        clock_margin_severity    => TB_ERROR,
        setup_time               => cClkPeriod/4,
        hold_time                => cClkPeriod/4,
        bfm_sync                 => SYNC_ON_CLOCK_ONLY,
        match_strictness         => MATCH_EXACT,
        symbol_width             => 8,
        first_symbol_in_msb      => true,
        max_channel              => 0,
        use_packet_transfer      => true,
        id_for_bfm               => ID_BFM
    );
 
    ---------------------------------------
    -- Avalon Streaming Master - BFM Config 
    ---------------------------------------
    constant C_AVALON_ST_MASTER_BFM_CONFIG : t_avalon_st_bfm_config := (
        max_wait_cycles          => 1000,
        max_wait_cycles_severity => ERROR,
        clock_period             => cClkPeriod,
        clock_period_margin      => 0 ns,
        clock_margin_severity    => TB_ERROR,
        setup_time               => cClkPeriod/4,
        hold_time                => cClkPeriod/4,
        bfm_sync                 => SYNC_ON_CLOCK_ONLY,
        match_strictness         => MATCH_EXACT,
        symbol_width             => 8,
        first_symbol_in_msb      => true,
        max_channel              => 0,
        use_packet_transfer      => true,
        id_for_bfm               => ID_BFM
    );
 
    ---------------------------------------
    -- Avalon MM Master - BFM Config
    ---------------------------------------
    constant C_AVALON_MM_BFM_CONFIG : t_avalon_mm_bfm_config := (
        max_wait_cycles          => 10,
        max_wait_cycles_severity => TB_FAILURE,
        clock_period             => cClkPeriod,
        clock_period_margin      => 0 ns,
        clock_margin_severity    => TB_ERROR,
        setup_time               => 0 ns,
        hold_time                => 0 ns,
        bfm_sync                 => SYNC_ON_CLOCK_ONLY,
        match_strictness         => MATCH_EXACT,
        num_wait_states_read     => 0,
        num_wait_states_write    => 0,
        use_waitrequest          => true,
        use_readdatavalid        => true,
        use_response_signal      => false,
        use_begintransfer        => false,
        id_for_bfm               => ID_BFM,
        id_for_bfm_wait          => ID_BFM_WAIT,
        id_for_bfm_poll          => ID_BFM_POLL
    );
end pkg_tb_databuffer;

-- package body
package body pkg_tb_databuffer is
-- currently nothing to declare here
end pkg_tb_databuffer;
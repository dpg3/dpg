----------------------------------------------------------------------------------------------------------------
-- Workfile:    th_databuffer_vvc-struct-ea.vhd
-- Author:      Tobias Kerbl
----------------------------------------------------------------------------------------------------------------
-- Description: This file contains the test-harness for the UVVM tests.
----------------------------------------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

-- Databuffer library
library work; 
use work.pkg_databuffer.all;
use work.pkg_tb_databuffer.all;

-- Avalon ST librarys
library bitvis_vip_avalon_st;
use bitvis_vip_avalon_st.avalon_st_bfm_pkg.all;

-- Avalon MM librarys
library bitvis_vip_avalon_mm;
use bitvis_vip_avalon_mm.avalon_mm_bfm_pkg.all;

-- UVVM librarys
library uvvm_util;
context uvvm_util.uvvm_util_context;
library uvvm_vvc_framework;
use uvvm_vvc_framework.ti_vvc_framework_support_pkg.all;

-- Clock Generator librarys
library bitvis_vip_clock_generator;
context bitvis_vip_clock_generator.vvc_context;

-- SPI-Slave librarys
library bitvis_vip_spi;
use bitvis_vip_spi.td_vvc_framework_common_methods_pkg.all;
use bitvis_vip_spi.spi_bfm_pkg.all;

-- Test harness entity
entity th_databuffer_vvc is
  generic (
    gLengthOfRam : natural := cLengthOfRAM
  );
  port (
    reset_n_i        : in std_ulogic;
    data_available_o : out std_ulogic 
  );
begin
end entity th_databuffer_vvc;

----------------------------------------------------------------------
-- Test harness architecture
----------------------------------------------------------------------
architecture Struct of th_databuffer_vvc is

  -- DSP interface and general control signals
  signal clk            : std_logic := '0';
  signal data_available : std_logic := '0';
  --signal rst_async : std_logic := '0';

  ----------------------------------------
  -- Avalon Streaming Slave - Interface
  ----------------------------------------
  signal avalon_st_if_sink : t_avalon_st_if(
    channel(cAvalonStChannelWidth-1 downto 0),
    data(cAvalonSTDataWidth-1 downto 0),
    data_error(cAvalonStErrorWidth-1 downto 0),
    empty(cAvalonStEmptyWidth-1 downto 0)
  ) := init_avalon_st_if_signals(false, cAvalonStChannelWidth, cAvalonSTDataWidth, cAvalonStErrorWidth, cAvalonStEmptyWidth);

  ----------------------------------------
  -- Avalon Streaming Master - Interface
  ----------------------------------------
  signal avalon_st_if_source : t_avalon_st_if(
    channel(cAvalonStChannelWidth-1 downto 0),
    data(cAvalonSTDataWidth-1 downto 0),
    data_error(cAvalonStErrorWidth-1 downto 0),
    empty(cAvalonStEmptyWidth-1 downto 0)
  ) := init_avalon_st_if_signals(true, cAvalonStChannelWidth, cAvalonSTDataWidth, cAvalonStErrorWidth, cAvalonStEmptyWidth);

  ----------------------------------------
  -- Avalon MM Slave - Interface
  ----------------------------------------
  signal avalon_mm_if : t_avalon_mm_if(
    address(cAddressWidth - 1 downto 0),
    writedata(cAvalonMMDataWidth - 1 downto 0),
    readdata(cAvalonMMDataWidth - 1 downto 0),
    byte_enable((cAvalonMMDataWidth / 8) - 1 downto 0)
  ) := init_avalon_mm_if_signals(cAddressWidth, cAvalonMMDataWidth);

begin
  -----------------------------------------------------------------------------
  -- Instantiate the concurrent procedure that initializes UVVM
  -----------------------------------------------------------------------------
  i_ti_uvvm_engine : entity uvvm_vvc_framework.ti_uvvm_engine;

  -----------------------------------------------------------------------------
  -- Instantiate DUT
  -----------------------------------------------------------------------------
  i_databuffer : entity work.databuffer
  generic map (
    gAddressWidth       => cAddressWidth,
    gAvalonMMDataWidth  => cAvalonMMDataWidth,
    gAvalonSTDataWidth  => cAvalonSTDataWidth,
    gLengthOfRAM        => gLengthOfRam
  )
  port map (
    -- General
    clk              => clk,
    reset_n          => reset_n_i, --not rst_async,
 
    -- Avalon MM Slave
    address          => avalon_mm_if.address,
    write            => avalon_mm_if.write,
    writedata        => avalon_mm_if.writedata,
    read             => avalon_mm_if.read,
    waitrequest      => avalon_mm_if.waitrequest,
    readdata         => avalon_mm_if.readdata,
    readdatavalid    => avalon_mm_if.readdatavalid,

    -- Avalon ST Source
    readySrc         => avalon_st_if_sink.ready, --'0',
    dataSrc          => avalon_st_if_sink.data,
    validSrc         => avalon_st_if_sink.valid,
    startofpacketSrc => avalon_st_if_sink.start_of_packet,
    endofpacketSrc   => avalon_st_if_sink.end_of_packet,

    -- Avalon ST Sink
    dataSink         => avalon_st_if_source.data,
    validSink        => avalon_st_if_source.valid,
    readySink        => avalon_st_if_source.ready,

    -- Avalon Interrupt
    dataAvailable     => data_available
  );

  -----------------------------------------------------------------------------
  -- Avalon ST master VVC
  -----------------------------------------------------------------------------
  avalon_st_vvc_master : entity bitvis_vip_avalon_st.avalon_st_vvc
    generic map(
      GC_VVC_IS_MASTER        => true,
      GC_DATA_WIDTH           => avalon_st_if_source.data'length,
      GC_DATA_ERROR_WIDTH     => avalon_st_if_source.data_error'length,
      GC_EMPTY_WIDTH          => avalon_st_if_source.empty'length,
      GC_INSTANCE_IDX         => cAvalonStMasterIdx,
      GC_AVALON_ST_BFM_CONFIG => C_AVALON_ST_MASTER_BFM_CONFIG)
    port map(
      clk              => Clk,
      avalon_st_vvc_if => avalon_st_if_source
    );
  -----------------------------------------------------------------------------
  -- Avalon ST slave VVC
  -----------------------------------------------------------------------------
  avalon_st_vvc_slave : entity bitvis_vip_avalon_st.avalon_st_vvc
    generic map(
      GC_VVC_IS_MASTER        => false,
      GC_DATA_WIDTH           => avalon_st_if_sink.data'length,
      GC_DATA_ERROR_WIDTH     => avalon_st_if_sink.data_error'length,
      GC_EMPTY_WIDTH          => avalon_st_if_sink.empty'length,
      GC_INSTANCE_IDX         => cAvalonStSlaveIdx,
      GC_AVALON_ST_BFM_CONFIG => C_AVALON_ST_SLAVE_BFM_CONFIG)

    port map(
      clk              => Clk,
      avalon_st_vvc_if => avalon_st_if_sink
    );
  -----------------------------------------------------------------------------
  -- Avalon MM Master
  -----------------------------------------------------------------------------
  i_avalon_mm_master : entity bitvis_vip_avalon_mm.avalon_mm_vvc
    generic map(
      GC_ADDR_WIDTH       => cAddressWidth,
      GC_DATA_WIDTH       => cAvalonMMDataWidth,
      GC_INSTANCE_IDX     => cAvalonMmMasterIdx,
      GC_AVALON_MM_CONFIG => C_AVALON_MM_BFM_CONFIG
    )
    port map(
      clk                     => clk,
      avalon_mm_vvc_master_if => avalon_mm_if
    );

  -----------------------------------------------------------------------------
  -- Async Reset (low active)
  -----------------------------------------------------------------------------
  -- Toggle the reset after 5 clock periods
  --p_RstAsync : rst_async <= '1', '0' after 5 * cClkPeriod;

  -----------------------------------------------------------------------------
  -- Clock Generator VVC
  -----------------------------------------------------------------------------
  i_clock_generator_vvc : entity bitvis_vip_clock_generator.clock_generator_vvc
    generic map(
      GC_INSTANCE_IDX    => cClkGen,
      GC_CLOCK_NAME      => "Databuffer-Clock",
      GC_CLOCK_PERIOD    => cClkPeriod,
      GC_CLOCK_HIGH_TIME => cClkPeriod / 2
    )
    port map(
      clk => clk
    );

  -----------------------------------------------------------------------------
  -- IRQ Output
  -----------------------------------------------------------------------------
  data_available_o <= data_available;
    
end Struct;
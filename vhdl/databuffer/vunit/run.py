#!/usr/bin/env python3

from pathlib import Path
from vunit import VUnit

VU = VUnit.from_argv()


#############################################
# add UVVM
#
# to add a VIP library simply add it to the uvvm_libraries list.
#############################################
uvvm_libraries = [
    "uvvm_util",
    "uvvm_vvc_framework",
    "bitvis_vip_scoreboard",
    "bitvis_vip_clock_generator",
    "bitvis_vip_spi",
    "bitvis_vip_avalon_st",
    "bitvis_vip_avalon_mm",
]

# uvvm_root_path = git_repo_path / "verification" / "uvvm"
UNITS_PATH = Path(__file__).resolve().parents[1]
VHDL_BASE_PATH = Path(__file__).resolve().parents[2]

uvvm_root_path = VHDL_BASE_PATH / "uvvm"

for libname in uvvm_libraries:
    scriptpath = uvvm_root_path / libname / "script"
    deps = scriptpath / "compile_order.txt"
    dep_file = open(deps, "r")
    print('adding library: ', libname)
    lib = VU.add_library(libname)
    for l in dep_file.readlines():
        if l.strip() and not l.startswith("#"):
            srcfile = scriptpath / l.strip()
            lib.add_source_files(srcfile)

lib = VU.add_library("lib")

#############################################
# Adding source files
#############################################

# rtl files
lib.add_source_files(UNITS_PATH / "src" / "grp_databuffer" / "*" / "src" / "*.vhd")

# testbench(es)
lib.add_source_files(UNITS_PATH / "tb" / "*.vhd")

VU.add_osvvm()

#############################################
# automatically load wave.do and execute run -all
#############################################
for tb in lib.get_test_benches():
    gui_do_path = Path(__file__).resolve().parent / "gui.do"
    tb.set_sim_option("modelsim.init_file.gui", str(gui_do_path))

############################################
# GHDL Options
#
# some must be set to compile UVVM correcly
#############################################

VU.set_compile_option("ghdl.a_flags", ["-frelaxed"])
VU.set_sim_option("ghdl.elab_flags", ["-frelaxed"])

VU.main()

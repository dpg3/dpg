onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider Controls
add wave -noupdate /tb_visualizer/Clk
add wave -noupdate /tb_visualizer/Reset
add wave -noupdate /tb_visualizer/IncCounterByOne
add wave -noupdate /tb_visualizer/SetAllLedsOn
add wave -noupdate /tb_visualizer/SetAllLedsOff
add wave -noupdate /tb_visualizer/SetLightChaserOn
add wave -noupdate -divider {Internal Registers}
add wave -noupdate -expand /tb_visualizer/DUT/R
add wave -noupdate -divider Leds
add wave -noupdate /tb_visualizer/Leds
add wave -noupdate -divider Hex0
add wave -noupdate /tb_visualizer/Hex0
add wave -noupdate -divider Hex1
add wave -noupdate /tb_visualizer/Hex1
add wave -noupdate -divider Hex2
add wave -noupdate /tb_visualizer/Hex2
add wave -noupdate -divider Hex3
add wave -noupdate /tb_visualizer/Hex3
add wave -noupdate -divider Hex4
add wave -noupdate /tb_visualizer/Hex4
add wave -noupdate -divider Hex5
add wave -noupdate /tb_visualizer/Hex5
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {240522271 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {2466250752 ps}

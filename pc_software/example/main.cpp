
#include <string>
#include "DPG_Api.h"
#include "SpiConfiguration.h"
#include "SpiData.h"
#include "json.hpp"
using namespace std;
using namespace dpg;

int main(int argc, char **argv) {
	try
	{
		// ---------------------------------------------------------------
		// 1) initialize the communication
		// ---------------------------------------------------------------
		// object for communication between PC and FPGA
        if (argc != 2) {
            cout << "Usage: " << argv[0] << " IP-Address\n";
            return -1;
        }
        std::string ip = argv[1];
        if (ip.find(":") == std::string::npos) {
            ip = ip + ":8080";
        }
		DpgInterface dpg(ip);
		//DpgInterface dpg("localhost:8080");
		if (!dpg.Init(IfTypes::SPI)) {
			cout << "ERROR in main.c: 'dpg.Init()' was unable to initialize DPG-member." << endl;
			return -1;
		}

		// ---------------------------------------------------------------
		// 2) send configuration to FPGA
		// ---------------------------------------------------------------
		// object for configuration of the wanted interface
		unique_ptr<SpiConfiguration> SpiConfig = make_unique<SpiConfiguration>(1000000, SpiConfiguration::SpiMode::cpolLow_shiftedRising);
		if (!dpg.SendConfig(move(SpiConfig))) {
			cout << "ERROR in main.c: 'dpg.SendConfig()' was unable to send configuration to server." << endl;
			return -2;
		}

		while(1){
			// ---------------------------------------------------------------
			// 3) send and then receive data to/from FPGA
			// ---------------------------------------------------------------
			// object for data collection of the wanted interface
			// unique_ptr<SpiData> SpiDataColl = make_unique<SpiData>();
			// for (int i = 0; i < 50; i++) {
			//     SpiDataColl->AddData(char(i));
			// }
			// // send data to FPGA
			// shared_ptr<TransferHdl> SpiHdl = dpg.SendData(move(SpiDataColl));
			// if (SpiHdl == nullptr) {
			//     cout << "ERROR in main.c: 'dpg.SendData()' was unable to send data to server." << endl;
			//     return -3;
			// }
			// // receive data from FPGA
			// Transfer recvData = { TransferState::TRANSFER_ONGOING, nullptr };
			// recvData = dpg.ReceiveData(SpiHdl);
			// if (recvData.state == TransferState::TRANSFER_UNDEF) {
			//     cout << "ERROR in main.c: 'dpg.ReceiveData()' was unable to receive data from server." << endl;
			//     return -3;
			// }

			// ---------------------------------------------------------------
			// 4) transfer data to FPGA
			// ---------------------------------------------------------------
			// object for data collection of the wanted interface
			vector<Data::UPtr> SpiDataCollVec;
			for (int i = 0; i < 10; i++) {
				unique_ptr<SpiData> SpiDataColl = make_unique<SpiData>();
				for (int j = 0; j < 50; j++) {
					SpiDataColl->AddData(char(j));
				}
				SpiDataCollVec.push_back(move(SpiDataColl));
			}
			// transfer data to FPGA
			vector<Data::UPtr> recvDataVec = dpg.TransferData(SpiDataCollVec);

			// ---------------------------------------------------------------
			// 5) evaluate of the data
			// ---------------------------------------------------------------
			// --> e.g.: print on the console
			// unique_ptr<SpiData> SpiDataCollExp = make_unique<SpiData>();
			// for (int i = 0; i < 50; i++) {
				// SpiDataCollExp->AddData(char(255-i));
			// }
			// dpg.AnalyzeData(move(recvData.data), move(SpiDataCollExp), OutSel::CONSOLE);

			// --> e.g.: compare received and expected data in the programm
			vector<Data::UPtr> SpiDataCollVecExp;
			for (int i = 0; i < 10; i++) {
				unique_ptr<SpiData> SpiDataCollExp = make_unique<SpiData>();
				for (int j = 0; j < 50; j++) {
					SpiDataCollExp->AddData(char(255-j));
				}
				SpiDataCollVecExp.push_back(move(SpiDataCollExp));
			}
			for (int i = 9; i >= 0; i--) {
				int result = dpg.AnalyzeData(move(recvDataVec[i]), move(SpiDataCollVecExp[i]), OutSel::NONE);
				if (result != 0) {
					cout << "ERROR in main.c: transfered data don't match expected data!" << endl;
					return -5;
				}
				else {
					cout << "main.c: transfered data at vector index [" << i << "] match expected data!" << endl;
				}
			}
		}
		
		cout << endl << "main.c: EXITING!" << endl;
	}
	// catch error messages from DPG-Api
	catch (string const& error){
		cerr << error << endl;
	}
	// catch error messages from JSON-lib.
	catch (exception const& errJSON) {
		cerr << errJSON.what() << endl;
	}
	// catch every other messages
	catch (...) {
		cerr << "Unexpected error!" << endl;
	}

	return 0;
}

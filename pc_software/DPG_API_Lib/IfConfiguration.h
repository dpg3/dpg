////////////////////////////////////////////////////////////////////////////////
// Workfile : IfConfiguration.h
// Author   : Stefan Untner
// Date     : 10.05.2022
// Description :  
// Remarks  : -
// Revision : 0
////////////////////////////////////////////////////////////////////////////////
#ifndef IfConfiguration_INCLUDE
#define IfConfiguration_INCLUDE

#include <string>
#include <memory>

class Configuration {
public:
	using UPtr = std::unique_ptr<Configuration>;
	virtual ~Configuration() = default;
	virtual std::string GetConfig() = 0;
};

#endif // !IfConfiguration_INCLUDE

////////////////////////////////////////////////////////////////////////////////
// Workfile : SpiConfiguration.h
// Author   : Stefan Untner
// Date     : 10.05.2022
// Description :  
// Remarks  : -
// Revision : 0
////////////////////////////////////////////////////////////////////////////////
#ifndef SpiConfiguration_INCLUDE
#define SpiConfiguration_INCLUDE

#include "IfConfiguration.h"
#include "json.hpp"

////////////////////////////////////////////////////////////////////////////////
// Class Declaration - SpiConfiguration
////////////////////////////////////////////////////////////////////////////////
class SpiConfiguration : public Configuration {
public:
	// -------------------- typdefinitions --------------------
	enum class SpiMode {
		cpolLow_shiftedFalling = 0,
		cpolLow_shiftedRising = 1,
		cpolHigh_shiftedFalling = 2,
		cpolHigh_shiftedRising = 3
	};
	// --------------------- class methods --------------------
	SpiConfiguration(int bitRate = 0, SpiMode mode = SpiMode::cpolLow_shiftedFalling) {
		mBitrate = bitRate;
		mMode = mode;
		mJSON["interface_type"] = "SPI";
		mJSON["bitrate"] = bitRate;
		switch (mode)
		{
		case SpiMode::cpolLow_shiftedFalling:
			mJSON["cpha"] = "SmplFalling";
			mJSON["cpol"] = "Low";
			break;
		case SpiMode::cpolLow_shiftedRising:
			mJSON["cpha"] = "SmplRising";
			mJSON["cpol"] = "Low";
			break;
		case SpiMode::cpolHigh_shiftedFalling:
			mJSON["cpha"] = "SmplFalling";
			mJSON["cpol"] = "High";
			break;
		case SpiMode::cpolHigh_shiftedRising:
			mJSON["cpha"] = "SmplRising";
			mJSON["cpol"] = "High";
			break;
		default:
			break;
		}
	}
	// --------------------- user methods ---------------------
	std::string GetConfig() override {
		return mJSON.dump();
	}

private:
	// ------------------------ member ------------------------
	int mBitrate;
	SpiMode mMode;
	nlohmann::json mJSON;		// json member
};

#endif // !SpiConfiguration_INCLUDE

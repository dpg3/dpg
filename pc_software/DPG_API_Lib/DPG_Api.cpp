////////////////////////////////////////////////////////////////////////////////
// Workfile : DpgInterface.cpp
// Author   : Stefan Untner
// Date     : 10.05.2022
// Description :  
// Remarks  : -
// Revision : 0
////////////////////////////////////////////////////////////////////////////////

#include "DPG_Api.h"
#include "SpiConfiguration.h"
#include "SpiData.h"

#include <thread>
#include <chrono>

using namespace dpg;

// --------------------------------------------------------
// ---------------------- constants -----------------------
// --------------------------------------------------------
static std::string const TRANSFER_STATUS_ERROR = "Error";
static std::string const TRANSFER_STATUS_ONGOING = "InProgress";
static std::string const TRANSFER_STATUS_FINISHED = "Finished";
static std::chrono::nanoseconds TRANSFER_TIME_OUT = std::chrono::nanoseconds(5000000000); // 5 seconds
// --------------------------------------------------------
// ------------------- error constants --------------------
// --------------------------------------------------------
static std::string const ERROR_DPG_NOT_INITIALIZED = "DPG-Api: 'Init()'-method has not yet been executed. Always run 'Init()' first!";
static std::string const ERROR_DPG_BODY_EMPTY = "DPG-Api: empty response-body found in function ";
static std::string const ERROR_DPG_RESULT_EMPTY = "DPG-Api: empty response-result found in function ";
static std::string const ERROR_DPG_HTTP_INIT = "DPG-Api: error during HTTP-Initialization.";
static std::string const ERROR_DPG_HTTP_GET_IFS = "DPG-Api: unable to get interface-list or no interfaces saved in DPG-device.";
static std::string const ERROR_DPG_HTTP_BAD_REQ = "DPG-Api: given body is not formatted correctly or the submitted config has errors.";
static std::string const ERROR_DPG_HTTP_NOT_FOUND = "DPG-Api: given interface type was not found or is not a supported type.";
static std::string const ERROR_DPG_HDL_NULLPTR = "DPG-Api: given transfer-hdl-pointer is a nullpointer! Function ";
static std::string const ERROR_DPG_TRANSFER_TIMEOUT = "Dpg-Api: Time-Out-error in function 'TransferData(). No response from transfer-handle '";


////////////////////////////////////////////////////////////////////////////////
// Class Methods - TransferHdl
////////////////////////////////////////////////////////////////////////////////
// --------------------------------------------------------
// ------------------------- CTor -------------------------
// --------------------------------------------------------
dpg::TransferHdl::TransferHdl(std::string const uri, httplib::Client* pHttpClient)
{
	mURI = uri;
	mPHttpClient = pHttpClient;
}
// --------------------------------------------------------
// ------------------------- DTor -------------------------
// --------------------------------------------------------
dpg::TransferHdl::~TransferHdl()
{
	if (mPHttpClient != nullptr) {
		// delete transfer by using "Delete()"-command of the HTTP-lib
		//		- httplib::Result Delete(const char* path);
		auto res = mPHttpClient->Delete(mURI.c_str());
		// check response status and execute error handling
		if (res->status != HTTP_STATUS_OK) {
			std::cerr << ERROR_DPG_HTTP_NOT_FOUND << " HTTP-Result = ERROR 404" << std::endl;
		}
		else {
			std::cout << "-----> Transfer with handle '"
				<< mURI << "' deleted successfully." << std::endl;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
// User Methods (public) - TransferHdl
////////////////////////////////////////////////////////////////////////////////
// --------------------------------------------------------
// ------------------------ GetURI ------------------------
// --------------------------------------------------------
std::string dpg::TransferHdl::GetURI()
{
	return mURI;
}


////////////////////////////////////////////////////////////////////////////////
// Class Methods - DpgInterface
////////////////////////////////////////////////////////////////////////////////
// --------------------------------------------------------
// ------------------------- CTor -------------------------
// --------------------------------------------------------
DpgInterface::DpgInterface(std::string const URL):
	mIsInitialised{ false },
	mType{IfTypes::SPI},
	mHttpClient{URL}
{
}
// --------------------------------------------------------
// ------------------------- DTor -------------------------
// --------------------------------------------------------
DpgInterface::~DpgInterface()
{
	//mHttpClient.CleanupSession();
}

////////////////////////////////////////////////////////////////////////////////
// User Methods (public) - DpgInterface
////////////////////////////////////////////////////////////////////////////////
// --------------------------------------------------------
// ------------------------- Init -------------------------
// --------------------------------------------------------
bool DpgInterface::Init(IfTypes const& type)
{
	PrintHeader("Init()");
	// check if wanted if-type is available
	std::string ifType = "/interfaces?if_type=" + EnumToString(type);
	// get informations, wrapped in a string, by using "Get()"-command of the HTTP-lib
	//		- httplib::Result Get(const char* path);
	auto res = mHttpClient.Get(ifType.c_str());
	// check response status and execute error handling
	std::string resultMsg = CheckHttpResult(res);
	if (resultMsg != "") {
		std::cerr << resultMsg << std::endl;
		PrintDivider(70);
		return false;
	}
	if (res->body.empty()) {
		std::cerr << ERROR_DPG_HTTP_GET_IFS << std::endl;
		PrintDivider(70);
		return false;
	}
	std::cout << "-----> initialising..." << std::endl;
	// convert result-body from STRING to JSON
	nlohmann::json resJSON = nlohmann::json::parse(res->body);
	// check the interface posibilities of the server
	bool wantedIfTypeFound = false;
	std::string ifLink = "";
	nlohmann::json::iterator it = resJSON.begin();
	std::cout << "-----> Possible interface types:" << std::endl;
	while (it != resJSON.end()) {
		ifType = (*it)["interface_type"].get<std::string>();
		ifLink = (*it)["link"].get<std::string>();
		if (ifType == EnumToString(type)) {
			wantedIfTypeFound = true;
		}
		std::cout << std::setw(15) << std::right << std::setfill('-') << "> "
			<< ifType << "; URI: " << ifLink << std::endl;
		it++;
	}
	PrintDivider(70);
	if (not wantedIfTypeFound) {
		std::cerr << "-----> Initialisation FAILED!" << std::endl;
		return false;
	}
	std::cout << "-----> Initialisation was successful." << std::endl;
	mType = type;
	mIsInitialised = true;
	return true;
}
// --------------------------------------------------------
// ---------------------- SendConfig ----------------------
// --------------------------------------------------------
bool DpgInterface::SendConfig(Configuration::UPtr ifConfig)
{
	PrintHeader("SendConfig()");
	// check if Init()-method has been executed
	if (!mIsInitialised) {
		std::cerr << ERROR_DPG_NOT_INITIALIZED << std::endl;
		PrintDivider(70);
		return false;
	}
	// use "GetConfig()" to create a JSON-string
	std::string config = ifConfig->GetConfig();
	std::cout << "-----> Send new configuration " << config << std::endl;
	// send the JSON-string by using "Put()"-command of the HTTP-lib ("Put()" overrides existing data)
	//    - httplib::Result Put(const char* path,
	//							const char* body,
	//							size_t content_length,
	//							const char* content_type);
	auto res = mHttpClient.Put("/interfaces/SPI0/config", config.c_str(), config.size(), "application/json");
	// check response status and execute error handling
	std::string resultMsg = CheckHttpResult(res);
	if (resultMsg != "") {
		std::cerr << resultMsg << std::endl;
		PrintDivider(70);
		return false;
	}
	// final status message
	std::cout << "-----> Configuration was sent successfully to " << EnumToString(mType) << "-Master" << std::endl;
	PrintDivider(70);
	return true;
}
// --------------------------------------------------------
// -------------------- DeleteTransfer --------------------
// --------------------------------------------------------
bool dpg::DpgInterface::DeleteTransfer(std::string const& ifURI)
{
	PrintHeader("DeleteTransfer()");
	// check if Init()-method has been executed
	if (!mIsInitialised) {
		std::cerr << ERROR_DPG_NOT_INITIALIZED << std::endl;
		PrintDivider(70);
		return false;
	}
	std::cout << "-----> deleting..." << std::endl;
	// delete transfer by using "Delete()"-command of the HTTP-lib
	//		- httplib::Result Delete(const char* path);
	auto res = mHttpClient.Delete(ifURI.c_str());
	// check response status and execute error handling
	std::string resultMsg = CheckHttpResult(res);
	if (!resultMsg.empty()) {
		std::cerr << resultMsg << std::endl;
		PrintDivider(70);
		return false;
	}
	std::cout << "-----> Transfer with handle '" << ifURI << "' deleted successfully." << std::endl;
	PrintDivider(70);
	return true;
}
// --------------------------------------------------------
// ----------------------- SendData -----------------------
// --------------------------------------------------------
TransferHdl::SPtr DpgInterface::SendData(Data::UPtr ifData)
{
	PrintHeader("SendData()");
	std::string data = ifData->GetDataFromJSON();
	// check if Init()-method has been executed
	if (!mIsInitialised) {
		std::cerr << ERROR_DPG_NOT_INITIALIZED << std::endl;
		PrintDivider(70);
		return nullptr;
	}
	std::cout << std::hex << std::showbase << "-----> Send new data " << data << std::endl;
	std::cout << std::dec << std::noshowbase;
	std::cout << "-----> sending..." << std::endl;
	// use "GetData()" to create a JSON-string and send it to the server
	TransferHdl::SPtr pResHdl = SendDataInternal(data);
	// final status message
	if (pResHdl != nullptr) {
		std::cout << "-----> Data was sent successfully to " << EnumToString(mType) << "-Master" << std::endl;
	}
	else {
		std::cerr << "-----> Sending data FAILED!" << std::endl;
	}
	PrintDivider(70);
	return pResHdl;
}
// --------------------------------------------------------
// ---------------------- ReceiveData ---------------------
// --------------------------------------------------------
Transfer DpgInterface::ReceiveData(TransferHdl::SPtr ifHdl)
{
	PrintHeader("ReceiveData()");
	std::string actURI = ifHdl->GetURI();
	// initialize returnData-structure
	Transfer received = { TransferState::TRANSFER_UNDEF, nullptr };
	// check if Init()-method has been executed
	if (!mIsInitialised) {
		std::cerr << ERROR_DPG_NOT_INITIALIZED << std::endl;
		PrintDivider(70);
		return received;
	}
	std::cout << "-----> Receiving data from " << actURI << std::endl;
	// use "GetURI()" to get the path of the transfer-handle
	received = ReceiveDataInternal(actURI);
	std::cout << "-----> receiving...";
	// check if transfer is still ongoing and try to receive again
	while (received.state == TransferState::TRANSFER_ONGOING) {
		std::cout << ".";
		received = ReceiveDataInternal(ifHdl->GetURI());
	}
	std::cout << std::endl;
	PrintDivider(70);
	return received;
}
// --------------------------------------------------------
// --------------------- TransferData ---------------------
// --------------------------------------------------------
std::vector<Data::UPtr> DpgInterface::TransferData(std::vector<Data::UPtr> const& inputVec)
{
	PrintHeader("TransferData()");
	std::vector<Data::UPtr> receivedData;
	std::string data = "";
	// check if Init()-method has been executed
	if (!mIsInitialised) {
		std::cerr << ERROR_DPG_NOT_INITIALIZED << std::endl;
		PrintDivider(70);
		return receivedData;
	}
	// send the data for the whole vector and generate the transfer-handles
	for (int i = 0; i < inputVec.size(); i++) {
		data = inputVec[i]->GetDataFromJSON();
		std::cout << std::hex << std::showbase << "-----> Send new data " << data << std::endl;
		std::cout << std::dec << std::noshowbase;
		std::cout << "-----> sending..." << std::endl;
		TransferHdl::SPtr pResHdl = SendDataInternal(data);
		// add transfer-hdl to queue if it's not a "nullptr"
		if (pResHdl != nullptr) {
			mTransferHdls.push(move(pResHdl));
			// final status message
			std::cout << "-----> Data was sent successfully to " << EnumToString(mType) << "-Master" << std::endl;
			PrintDivider(70);
		}
	}

	// initialize Transfer-structure
	Transfer actTransfer = { TransferState::TRANSFER_UNDEF, nullptr };
	auto start = std::chrono::time_point<std::chrono::system_clock>();
	// working through the transfer-handle-queue
	while (!mTransferHdls.empty()) {
		std::string actURI = mTransferHdls.front()->GetURI();
		std::cout << "-----> Receiving data from " << actURI << std::endl;
		std::cout << "-----> receiving...";
		// reset Transfer-structure
		actTransfer.state = TransferState::TRANSFER_ONGOING;
		actTransfer.data = nullptr;
		// save actual time
		start = std::chrono::system_clock::now();
		// check actual transfer-handle
		while (actTransfer.state == TransferState::TRANSFER_ONGOING) {
			std::cout << ".";
			// sleep for 10 msec to avoid server-overloading
            std::this_thread::sleep_for(std::chrono::milliseconds(1));
			// get transfer-handle from the next (first stored) element in queue
			actTransfer = ReceiveDataInternal(actURI);
			// check transfer state
			if (actTransfer.state == TransferState::TRANSFER_ONGOING) {
				// check time-out
				std::chrono::nanoseconds duration = std::chrono::system_clock::now() - start;
				if (duration >= TRANSFER_TIME_OUT) {
					std::cerr << ERROR_DPG_TRANSFER_TIMEOUT << actURI << "'!" << std::endl;
					PrintDivider(70);
					mTransferHdls.pop();
				}
			}
			// transfer is finished or has error
			else {
				if (actTransfer.state == TransferState::TRANSFER_FINISHED) {
					// save the received data into "receivedData"
					receivedData.push_back(move(actTransfer.data));
				}
				// delete the first stored element of queue
				mTransferHdls.pop();
			}
		}
		PrintDivider(70);
	}
	return receivedData;
}
// --------------------------------------------------------
// --------------------- AnalyzeData ----------------------
// --------------------------------------------------------
int dpg::DpgInterface::AnalyzeData(Data::UPtr recvData, Data::UPtr expData, OutSel const& sel)
{
	size_t i = 1;	// index for received data (is one ahead, because the first received data is rubbish)

	PrintHeader("AnalyzeData()");

	switch (sel)
	{
	case OutSel::NONE:
		while (i < recvData->GetBufferSize()) {
			if (recvData->GetDataFromBuffer(i) != expData->GetDataFromBuffer(i - 1)) {
				return (int)i;
			}
			i++;
		}
		return 0;

	case OutSel::CONSOLE:
		std::cout << std::hex << std::showbase
			<< std::left << std::setfill(' ') << std::setw(15) << "Received data:"
			<< std::right << std::setfill(' ') << std::setw(15) << "Expected data:" << std::endl;
		for (size_t i = 1; i < recvData->GetBufferSize(); i++) {
			std::cout << std::left << std::setfill('.') << std::setw(15) << int(recvData->GetDataFromBuffer(i))
				<< std::right << std::setfill('.') << std::setw(15) << int(expData->GetDataFromBuffer(i - 1));
			std::cout << std::setw(5) << std::setfill(' ');
			if (recvData->GetDataFromBuffer(i) == expData->GetDataFromBuffer(i - 1)) {
				std::cout << "OK";
			}
			else {
				std::cout << "NOK";
			}
			std::cout << std::endl;
		}
		PrintDivider(70);
		std::cout << std::left << std::dec << std::noshowbase;
		return 0;

	case OutSel::FILE:
		std::cout << "Dpg-Api: Error in function 'AnalyzeData()' --> 'write result into file' is not implemented yet!" << std::endl;
		return -1;

	default:
		return -1;
	}
}

////////////////////////////////////////////////////////////////////////////////
// User Methods (private) - DpgInterface
////////////////////////////////////////////////////////////////////////////////
// --------------------------------------------------------
// ------------------- SendDataInternal -------------------
// --------------------------------------------------------
TransferHdl::SPtr DpgInterface::SendDataInternal(std::string const& data)
{
	// send the JSON-string by using "Post()"-command of the HTTP-lib ("Post()" creates new data)
	//    - httplib::Result Post(const char* path,
	//							 const char* body,
	//							 size_t content_length,
	//							 const char* content_type);
	auto res = mHttpClient.Post("/interfaces/SPI0/transfers", data.c_str(), data.size(), "application/json");
	// check response status and execute error handling
	std::string resultMsg = CheckHttpResult(res);
	if (!resultMsg.empty()) {
		std::cerr << resultMsg << std::endl;
		PrintDivider(70);
		return nullptr;
	}
	if (res->body.empty()) {
		std::cerr << ERROR_DPG_BODY_EMPTY + "'SendDataInternal()'!" << std::endl;
		PrintDivider(70);
		return nullptr;
	}
	// convert result-body from STRING to JSON
	auto resJSON = nlohmann::json::parse(res->body);
	// make Transfer-Handle and return it
	TransferHdl::SPtr pResHdl = std::make_shared<TransferHdl>(resJSON["transfer"].get<std::string>(), &mHttpClient);
	return pResHdl;
}
// --------------------------------------------------------
// ------------------ ReceiveDataInternal -----------------
// --------------------------------------------------------
Transfer DpgInterface::ReceiveDataInternal(std::string const& uri)
{
	// initialize returnData-structure
	Transfer received = { TransferState::TRANSFER_UNDEF, nullptr };
	// get informations, wrapped in a string, by using "Get()"-command of the HTTP-lib
	//		- httplib::Result Get(const char* path);
	auto res = mHttpClient.Get(uri.c_str());
	// check response status and execute error handling
	std::string resultMsg = CheckHttpResult(res);
	if (!resultMsg.empty()) {
		std::cerr << resultMsg << std::endl;
		PrintDivider(70);
		return received;
	}
	if (res->body.empty()) {
		std::cerr << ERROR_DPG_BODY_EMPTY + "'ReceiveDataInternal()'!" << std::endl;
		PrintDivider(70);
		return received;
	}
	// convert result-body from STRING to JSON
	auto resJSON = nlohmann::json::parse(res->body);
	// get/parse the status from the transfer
	std::string resStatus = resJSON["status"].get<std::string>();
	// check status of transmition
	if (resStatus == TRANSFER_STATUS_FINISHED) {
		std::cout << std::endl << "-----> Transfer with handle '" << uri << "' is finished." << std::endl;
		received.state = TransferState::TRANSFER_FINISHED;
		received.data = std::make_unique<SpiData>();	// generate a SpiData-object
		received.data->SetData(resJSON);				// save the received data into SpiData
	}
	// set TransferState to "TRANSFER_ERROR" and print "error_msg"
	else if (resStatus == TRANSFER_STATUS_ERROR) {
		received.state = TransferState::TRANSFER_ERROR;
		std::cerr << resJSON["error_msg"].get<std::string>() << std::endl;
		PrintDivider(70);
	}
	else {
		received.state = TransferState::TRANSFER_ONGOING;
	}
	// Every "TransferState" other than "TRANSFER_FINISHED" returns a "nullptr" at "received.data"!
	return received;
}
// --------------------------------------------------------
// --------------------- EnumToString ---------------------
// --------------------------------------------------------
std::string DpgInterface::EnumToString(IfTypes const& type)
{
	switch (type)
	{
	case IfTypes::SPI:	return "SPI";
	case IfTypes::I2C:	return "I2C";
	case IfTypes::UART:	return "UART";
	case IfTypes::CAN:	return "CAN";
	default:			return "";
	}
}
// --------------------------------------------------------
// -------------------- CheckHttpResult -------------------
// --------------------------------------------------------
std::string DpgInterface::CheckHttpResult(httplib::Result const& httpResult)
{
	std::string result = "";
	if (httpResult == nullptr) {
		result = ERROR_DPG_RESULT_EMPTY + "'CheckHttpResult()'!" ;
		return result;
	}
	// ----- check http-result -> status -----
	// result = OK
	if (httpResult->status == HTTP_STATUS_OK || httpResult->status == HTTP_STATUS_ACCEPTED) {
		return "";
	}
	// result = NIO
	if (httpResult->status == HTTP_STATUS_BAD_REQ) {
		result = ERROR_DPG_HTTP_BAD_REQ + " HTTP-Result = ERROR 400 --> ";
		if (httpResult->body.empty()) {
			result += "no further infos available!";
		}
		else {
			result += httpResult->body;
		}
	}
	// result = NOT_FOUND
	else if (httpResult->status == HTTP_STATUS_NOT_FOUND) {
		result = ERROR_DPG_HTTP_NOT_FOUND + " HTTP-Result = ERROR 404";
	}
	// result = UNDEF
	else {
		result = "-----> Internal-error --> function 'CheckHttpResult()' couldn't find a suitable status!\n";
		auto err = httpResult.error();
		result += "HTTP-Lib-Error: ";
		result += httplib::to_string(err);
	}
	return result;
}
// --------------------------------------------------------
// ---------------------- PrintHeader ---------------------
// --------------------------------------------------------
void dpg::DpgInterface::PrintHeader(std::string const& txt)
{
	std::cout << std::endl;
	PrintDivider(50);
	std::cout << "***** DPG-Api: " << txt << std::endl;
	PrintDivider(50);
}

void dpg::DpgInterface::PrintDivider(int const& width)
{
	std::cout << std::setw(width) << std::setfill('*') << "" << std::endl;
}

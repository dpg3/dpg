////////////////////////////////////////////////////////////////////////////////
// Workfile : IfData.h
// Author   : Stefan Untner
// Date     : 10.05.2022
// Description :  
// Remarks  : -
// Revision : 0
////////////////////////////////////////////////////////////////////////////////
#ifndef IfData_INCLUDE
#define IfData_INCLUDE

#include <string>
#include <memory>

// Data:
//		- containes the data to be send to the server
//		- has to be created by the user of the api-lib.
class Data {
public:
	using UPtr = std::unique_ptr<Data>;
	virtual ~Data() = default;
	virtual size_t GetBufferSize() = 0;
	virtual std::string GetDataFromJSON() = 0;
	virtual unsigned char GetDataFromBuffer(size_t const& idx) = 0;
	virtual void SetData(nlohmann::json const& actJSON) = 0;
};
#endif // !IfData_INCLUDE

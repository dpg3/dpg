////////////////////////////////////////////////////////////////////////////////
// Workfile : SpiData.h
// Author   : Stefan Untner
// Date     : 10.05.2022
// Description :  
// Remarks  : -
// Revision : 0
////////////////////////////////////////////////////////////////////////////////
#ifndef SpiData_INCLUDE
#define SpiData_INCLUDE

#include <vector>
#include "IfData.h"
#include "json.hpp"

////////////////////////////////////////////////////////////////////////////////
// Class Declaration - SpiData
////////////////////////////////////////////////////////////////////////////////
class SpiData : public Data {
public:
	// --------------------- class methods --------------------
	SpiData(){}
	// --------------------- user methods ---------------------
	size_t GetBufferSize() override{
		return mDataBuffer.size();
	}

	// GetDataFromJSON(): transfers the informations stored in "mJSON" into a string
	std::string GetDataFromJSON() override {
		// check if container is empty
		if (mDataBuffer.size() == 0) {
			return "";
		}
		// convert saved data into JSON-format
		ConvertDataToJson();
		return mJSON.dump();
	}

	// GetDataFromBuffer(): returns an unsigned char value stored at idx in the vector
	unsigned char GetDataFromBuffer(size_t const& idx) override {
		if (idx >= mDataBuffer.size()) {
			std::cout << "DPG-Api --> SPI-Data: given index is greater then data buffer size" << std::endl;
			return 0;
		}
		return mDataBuffer[idx];
	}

	// SetDataToJson(): saves a JSON-variable into "mJSON" and parses from JSON into vector<int>
	void SetData(nlohmann::json const& actJSON) override {
		mJSON = actJSON;
		mJSON["data"].get_to(mDataBuffer);
	}

	// AddData(): adds single unsigned char values to "mDataBuffer"
	void AddData(unsigned char const& data) {
		// adds single value --> can be used in a loop
		mDataBuffer.push_back(data);
	}
	// AddContainer(): adds a whole container to "mDataBuffer"
	template <typename itor>
	void AddContainer(itor begin, itor end) {
		// adds a whole container --> shouldn't be used in a loop
		while (begin != end) {
			mDataBuffer.push_back(char(*begin));
			begin++;
		}
	}

private:
	// --------------------- user methods ---------------------
	void ConvertDataToJson() {
		// wordsize is fixed because of vector<int>
		mJSON["wordsize"] = 8;
		// vector with the data is automatically translated into
		// a JSON array by nlohmann::json
		mJSON["data"] = mDataBuffer;
		// keep_received is fixed to 'true', to ensure that data are received
		mJSON["keep_received"] = true;
	}
	// ------------------------ member ------------------------
	nlohmann::json mJSON;			// json member
	std::vector<unsigned char> mDataBuffer;	// vector with data for the SPI
};

#endif // !SpiData_INCLUDE

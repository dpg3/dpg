#!/bin/sh
#
# Synthesize a VHDL Module with qsys and quartus.
# Is supposed to run from within the quartus docker container
# Arguments: $1 = Path to quartus directory
#            $2 = (optional) path to copy the result to

QUARTUS=/opt/intelFPGA_lite/18.1/quartus
QUARTUS_SH=$QUARTUS/bin/quartus_sh
QSYS_GENERATE=$QUARTUS/sopc_builder/bin/qsys-generate

BASEDIR=$(pwd)
cd $1

QSYS_FILE=$(find . -name '*.qsys' | head -n 1)
$QSYS_GENERATE $QSYS_FILE --part=5CSEMA5F31C6 --synthesis=VHDL
$QUARTUS_SH -t Compile.tcl

cd $BASEDIR

if [ ! -z $2 ]; then
    RBF_FILE=$(find $1/output_files -name '*.rbf' | head -n 1)
    cp $RBF_FILE $2
fi


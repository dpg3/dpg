#!/bin/bash
BASEDIR=$(pwd)
mkdir toolchain && cd toolchain
make -C /buildroot-2022.08.3/ O=$(pwd) BR2_EXTERNAL=$BASEDIR/buildroot/external dpg_toolchain_defconfig
make -s sdk
echo "Copying toolchain to $BASEDIR/$1"
cp images/arm-dpg-linux-gnueabihf_sdk-buildroot.tar.gz $BASEDIR/$1
cd ..

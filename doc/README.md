# Structure

## Basics

One subdirectory per document, that contains all relevant files.

## Latex Documents

Larger Latex Documents should be split into multiple files, very large documents can even have directory structures.
To include Latex-Files from subdirectories, use the [import](https:://www.ctan.org/pkg/import) Latex package, because
this allows to use relative paths in the included file.

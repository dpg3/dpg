\section{Benutzerhandbuch}
\label{sec:api_user_guide}
In diesem Abschnitt wird Schritt für Schritt ein Beispielprogramm erstellt, um die Verwendung der
\textit{Dpg-Api} zu veranschaulichen.

\subsection{Das \textit{main.cpp}:}
Nachdem das Projekt erstellt wurde, muss unter \textbf{Source Files} eine neue \textit{C++} Datei hinzugefügt
werden. Hier wurde die Datei \textit{main.cpp} genannt. Wurde die Datei erstellt können folgende Codezeilen eingefügt werden:
\begin{lstlisting}[language=C++]
#include "DPG_Api.h"
#include "SpiConfiguration.h"
#include "SpiData.h"
#include "json.hpp"
using namespace std;
using namespace dpg;

int main() {
    try
    {
        // enter your code here
    }
    // catch error messages from DPG-Api
    catch (string const& error){
        cerr << error << endl;
    }
    // catch error messages from JSON-lib.
    catch (exception const& errJSON) {
        cerr << errJSON.what() << endl;
    }
    // catch every other messages
    catch (...) {
        cerr << "Unexpected error!" << endl;
    }

    return 0;
}
\end{lstlisting}
Innerhalb des \textit{try}-Blocks werden die Codesegmente von \autoref{sec:dpg_init}, \autoref{sec:dpg_send_config},
\autoref{sec:dpg_send_rec_data}, \autoref{sec:dpg_transfer_data} und \autoref{sec:dpg_analyze_data} eingefügt.

\pagebreak

% ---------------------------------------------------------------
% 1) initialize the communication
% ---------------------------------------------------------------
\subsection{Instanzierung und Initialisierung:}
\label{sec:dpg_init}
Um die \textit{Dpg-Api} verwenden zu können, muss zuerst ein \textbf{DpgInterface}-Objekt erzeugt werden. Dem Objekt
muss ein \textit{Uniform Resource Identifier (URI)} übergeben werden, der aus der IP-Adresse des Servers und einem
passenden Port besteht.\\
Anschließend muss das \textit{DpgInterface}-Objekt initialisiert werden. Hierfür wird die \textit{Init()}-Funktion
aufgerufen und der gewünschte Kommunikationstyp übergeben. Für eine genauere Beschreibung der Funktion siehe
\autoref{sec:api_funcDescr_DpgInterface}.
\begin{lstlisting}[language=C++]
// object for communication between PC and FPGA
DpgInterface dpg("10.24.99.105:8080");
//DpgInterface dpg("localhost:8080");
if (!dpg.Init(IfTypes::SPI)) {
    cout << "ERROR in main.c: 'dpg.Init()' was unable to initialize DPG-member." << endl;
    return -1;
}
\end{lstlisting}

% ---------------------------------------------------------------
% 2) send configuration to FPGA
% ---------------------------------------------------------------
\subsection{Senden einer Konfiguration:}
\label{sec:dpg_send_config}
Nachdem die Initialisierung erfolgreich war, kann eine Konfiguration erstellt und an den Server
gesendet werden. Hierfür wird die Funktion \textit{SendConfig()} verwendet. Eine genaue Beschreibung finden
sie unter \autoref{sec:api_funcDescr_DpgInterface}.\\
\textbf{!!ACHTUNG!!} da \textit{SendConfig()} einen \textit{unique\_ptr} benötigt, muss das erzeugte Konfigurationsobjekt
mittels \textit{move} an die Funktion übergeben werden und steht für eine weitere Verwendung danach nicht mehr zur Verfügung.
\begin{lstlisting}[language=C++]
// object for configuration of the wanted interface
unique_ptr<SpiConfiguration> SpiConfig = make_unique<SpiConfiguration>(1000000, SpiConfiguration::SpiMode::cpolLow_shiftedRising);
if (!dpg.SendConfig(move(SpiConfig))) {
    cout << "ERROR in main.c: 'dpg.SendConfig()' was unable to send configuration to server." << endl;
    return -2;
}
\end{lstlisting}

% ---------------------------------------------------------------
% 3) send and receive data to/from FPGA
% ---------------------------------------------------------------
\subsection{Senden und Empfangen von Daten:}
\label{sec:dpg_send_rec_data}
Zum Senden und Empfangen von Daten stehen die zwei Funktionen \textit{SendData()} und \textit{ReceiveData()} zur Verfügung. Um Daten
senden zu können, müssen sie vorher erstellt (\textbf{!!ACHTUNG!!} wieder \textit{unique\_ptr}) und befüllt werden. Anschließend
können die Daten mittels Funktion \textit{SendData()} gesendet werden. Zurückgegeben wird ein \textit{TransferHdl}-Objekt (siehe Punkt
\autoref{sec:api_description}). Dieses Objekt muss der \textit{ReceiveData()}-Funktion mitgegeben werden.\\
\textbf{!!ACHTUNG - diese Funktion arbeitet blockierend ohne Time-Out-Handling!!}\\
Mit beiden Funktionen kann selbst gesteuert werden wie und wann die Daten gesendet bzw. empfangen werden. Sie können beliebig oft
aufgerufen und verwendet werden, auch unabhängig von der Funktion \textit{TransferData()}.
\pagebreak
\begin{lstlisting}[language=C++]
// object for data collection of the wanted interface
unique_ptr<SpiData> SpiDataColl = make_unique<SpiData>();
for (int i = 0; i < 50; i++) {
    SpiDataColl->AddData(char(i));
}
// send data to FPGA
shared_ptr<TransferHdl> SpiHdl = dpg.SendData(move(SpiDataColl));
if (SpiHdl == nullptr) {
    cout << "ERROR in main.c: 'dpg.SendData()' was unable to send data to server." << endl;
    return -3;
}
// receive data from FPGA
Transfer recvData = { TransferState::TRANSFER_ONGOING, nullptr };
recvData = dpg.ReceiveData(SpiHdl);
if (recvData.state == TransferState::TRANSFER_UNDEF) {
    cout << "ERROR in main.c: 'dpg.ReceiveData()' was unable to receive data from server." << endl;
    return -3;
}
\end{lstlisting}

% ---------------------------------------------------------------
% 4) transfer data to FPGA
% ---------------------------------------------------------------
\subsection{Transferieren von Daten:}
\label{sec:dpg_transfer_data}
Für eine sichere und einfache Übertragung kann die Funktion \textit{TransferData()} verwendet werden. Dieser
Funktion wird ein ganzer \textit{STL-Container (Vector)} übergeben. Der \textit{Vector} muss vorher mit Daten befüllt
werden. Anschließend übernimmt die Funktion das Senden und Empfangen. Zuerst werden alle Daten, die
im \textit{Vector} gespeichert sind, gesendet. Anschließend wird versucht das erste gesendete Datenpacket vom
Server abzurufen. Sollte es noch nicht wieder vom Master empfangen worden sein, wird das Empfangen
erneut versucht. Wichtig bei der \textit{TransferData()}-Funktion ist, dass diese ein \textit{Time-Out-Handling} besitzt!
Für eine genaue Funktionsbeschreibung siehe bitte \autoref{sec:api_funcDescr_DpgInterface}.
\begin{lstlisting}[language=C++]
// object for data collection of the wanted interface
vector<Data::UPtr> SpiDataCollVec;
for (int i = 0; i < 10; i++) {
    unique_ptr<SpiData> SpiDataColl = make_unique<SpiData>();
    for (int j = 0; j < 50; j++) {
        SpiDataColl->AddData(char(j));
    }
    SpiDataCollVec.push_back(move(SpiDataColl));
}
// transfer data to FPGA
vector<Data::UPtr> recvDataVec = dpg.TransferData(SpiDataCollVec);
\end{lstlisting}

% ---------------------------------------------------------------
% 5) evaluate of the data
% ---------------------------------------------------------------
\subsection{Auswerten der Daten:}
\label{sec:dpg_analyze_data}
Um die empfangenen Daten auch auswerten zu können, steht eine einfache Funktion zur Verfügung. Mit
\textit{AnalyzeData()} können die empfangenen Daten mit erwarteten Daten abgeglichen und auf unterschiedliche
Art und Weiße angezeigt werden. Erstens gibt es eine Konsolenausgabe, die die Daten gegen überstellt
und zusätzlich eine visuelle Bestätigung (Textausgabe mit \quotes{OK} bzw. \quotes{NOK}) ausgibt.\\
Zweitens gibt es eine reine Kontrolle, ob die Daten identisch sind. Sollte eine Abweichung enthalten
sein, wird an dieser Stelle abgebrochen und die Stelle über den Rückgabewert zurückgegeben.
\pagebreak
\begin{lstlisting}[language=C++]
// --> e.g.: print on the console
unique_ptr<SpiData> SpiDataCollExp = make_unique<SpiData>();
for (int i = 0; i < 50; i++) {
    SpiDataCollExp->AddData(char(255-i));
}
dpg.AnalyzeData(move(recvData.data), move(SpiDataCollExp), OutSel::CONSOLE);

// --> e.g.: compare received and expected data in the programm
vector<Data::UPtr> SpiDataCollVecExp;
for (int i = 0; i < 10; i++) {
    unique_ptr<SpiData> SpiDataCollExp = make_unique<SpiData>();
    for (int j = 0; j < 50; j++) {
        SpiDataCollExp->AddData(char(255-j));
    }
    SpiDataCollVecExp.push_back(move(SpiDataCollExp));
}
for (int i = 9; i >= 0; i--) {
    int result = dpg.AnalyzeData(move(recvDataVec[i]), move(SpiDataCollVecExp[i]), OutSel::NONE);
    if (result != 0) {
        cout << "ERROR in main.c: transfered data don't match expected data!" << endl;
        return -5;
    }
    else {
        cout << "main.c: transfered data at vector index [" << i << "] match expected data!" << endl;
    }
}
cout << endl << "main.c: EXITING!" << endl;
\end{lstlisting}

\pagebreak

\subsection{Konsolenausgabe:}
\lstinputlisting[caption=Start- und Init-Phase]{../../../pc\_software/literature/ConsolOutput_Start.txt}
\lstinputlisting[caption=Sende- und Empfangsausgabe]{../../../pc\_software/literature/ConsolOutput_Transfer.txt}
\lstinputlisting[caption=Konsolenausgabe]{../../../pc\_software/literature/ConsolOutput_AnalyzeData.txt}
\chapter{FPGA Datenpuffer}


\section{Übersicht}
Der sog. "Databuffer" ist ein Teil des FPGA-Designs, welcher für die
Pufferung der Daten zuständig ist. Hierbei handelt es sich um eine
bidirektionale und doppelte Pufferung.

Der Buffer bekommt blockweise Daten vom HPS, welcher er puffert und
byteweise zum SPI-Master sendet. Zeitgleich bekommt er Daten vom
SPI-Master und speichert diese ab, damit der HPS diese auslesen kann.

\section{Das Konzept}
\subsection{Die Architektur}
\begin{fancyfigure}{FPGA-Architektur}
    \includegraphics[width=0.935\textwidth]{figures/fpga_architecture.pdf}
\end{fancyfigure}

\begin{fancyfigure}{Datenpuffer im Detail}
    \includegraphics[width=0.935\textwidth]{figures/databuffer_details.pdf}
\end{fancyfigure}

\newpage

Die FPGA-Architektur setzt sich aus mehreren Teilen zusammen. Hierbei gibt es
den HPS und die Lightweight HPS-to-FPGA Bridge, welche bereits vorhanden sind,
sowie den Datenpuffer und SPI-Master (eigene IP-Cores).

Die Kommunikation mit dem Datenpuffer erfolgt über zwei Avalon Schnittstellen.

Eine Avalon Memory Mapping Schnittstelle dient hierbei zum Schreiben von (Konfigurations-) Daten
und kann direkt über den HPS angesteuert werden.

Zwei Avalon Streaming Schnittstellen dienen zur Kommunikation zwischen dem Datenpuffer und
dem SPI-Master. Hierbei besitzt der Puffer eine Quelle und eine Senke. Die Quelle dient
zum Senden, die Senke dient zum Empfangen von Daten.

Innerhalb des Datenpuffers befindet sich eine FSMD, sowie RAM-Blöcke zu Pufferung.

\newpage
\subsection{Memory Mapping}
\subsubsection{Software Memory Mapping}
Diese Speicherabbildung berücksichtigt die \quotes{Lightweight-HPS-to-FPGA-Bridge} und stellt die Adressierung
dar, wie sie nach außen sichtbar ist und somit im Treiber berücksichtigt werden muss.
\vspace{5mm}

\begin{bytefield}{32}
    \memsection{FF3F FFFF}{FF20 C000}{20}{-- reserved --}\\
    \begin{rightwordgroup}{Data Buffer}
        \memsection{FF20 BFFF}{FF20 8000}{6}{Rx Data}\\
        \memsection{FF20 7FFF}{FF20 4000}{6}{Tx Data}   
    \end{rightwordgroup}\\
    \memsection{FF20 3FFF}{FF20 3FE8}{4}{Transfer Control Register}\\
    \memsection{FF20 3Fe7}{FF20 0020}{4}{-- reserved --}\\
    \memsection{FF20 001F}{FF20 0000}{2}{Git Revision}\\
\end{bytefield}
%FF20 0000 Lightweight Bridge

\newpage
\subsubsection{Hardware Memory Mapping}
Diese Speicherabbildung stellt die Addressierung innerhalb des FPGAs dar. Diese Adressierung ist nicht
nach außen sichtbar!
\vspace{5mm}

\begin{bytefield}{32}
    \memsection{001F FFFF}{0000 C000}{24}{-- reserved --}\\
    \begin{rightwordgroup}{Data Buffer}
        \memsection{0000 BFFF}{0000 8000}{6}{Rx Data}\\
        \memsection{0000 7FFF}{0000 4000}{6}{Tx Data}   
    \end{rightwordgroup}\\
    \memsection{0000 3FFF}{0000 3FE8}{4}{Transfer Control Register}\\
    \memsection{0000 3Fe7}{0000 0020}{4}{-- reserved --}\\
    \memsection{0000 001F}{0000 0000}{2}{Git Revision}\\
\end{bytefield}

Hinweis: Für eine einfachere Darstellung wurden Byte-Adressen verwendet. Bei der
tatsächlichen Implementation werden jedoch Word-Adressen genutzt.

\begin{designnote}
    \begin{tcolorbox}[title=Designentscheidungen]
        Die \quotes{Transfer-Control-Register} beinhalten alle Register, welche für die
        Orchestrierung des Datentransfers benötigt werden. Um ein schönes \quotes{Alignment}
        zu bekommen, sowie eine Reserve für zukünftig benötigte Register zu haben, wurde
        noch ein reservierter Bereich zwischen den Registern und der \quotes{Git-Revision}
        angelegt.\\
        \\
        Die Bereiche \quotes{RXData} und \quotes{TXData} spiegeln die jeweiligen Bereiche
        wider, in denen die Daten für den SPI-Transfer zur Verfügung gestellt (TX) und
        entgegen genommen werden können (RX).\\
        \\
        Der obige reservierter Bereich ist für den zweiten Puffer im Hintergrund. Dieser wird
        vor der Software \quotes{versteckt}.\\
        \\
        Das Memory-Mapping wurde in Hinblick auf einen möglichen DMA-Zugriff entworfen.
        Daher sind die \quotes{Transfer-Control-Register} auch auf den TX-Daten Block
        ausgerichtet, damit der DMA nicht durch einen reservierten Bereich schreiben muss.\\
        \\
        Als Empfehlung gilt, dass der DMA Addressen dekrementieren kann und der Speicher auch nur
        so beschrieben wird. Werden die \quotes{Transfer-Control-Register} als Erstes vom DMA beschrieben,
        könnte dies einen falschen Zustand signalisieren, z.B. wenn das \quotes{Valid-Flag} gesetzt wird,
        obwohl noch keine gültigen Daten geschrieben worden sind.\\
        \\
        Die \quotes{Transfer-Control-Register} wurden absichtlich nicht über dem TX-Block gesetzt, da es während
        der Entwicklungphase noch zu einer Verschiebung der Bereiche kommen kann (Adress- und Ausrichtungsänderung). Dies könnte
        mit strikter Handhabung mittels Konstanten innerhalb der Projekte gelöst werden. Auf dies wurde aber verzichtet.
    \end{tcolorbox}
\end{designnote}

\subsubsection{Git Revision}
Die Git-Revision ist ein 32-Byte großer Wert, welche die momentane Version des FPGA-Designs
widerspiegelt. Mittels dieser kann geprüft werden, ob die Version des Webservers, des Treibes,
des FPGA-Designs, sowie der PC-Applikation zusammenpassen.

Die Git-Revision kann sowohl gelesen, wie auch beschrieben werden (rw).
\vspace{5mm}

\begin{bytefield}{32}
    \bitheader{0-31} \\
    \bitbox{32}{Git-Revision - rw}
\end{bytefield}
    
\subsubsection{Transfer Control Register}
Die Transfer Control Register dienen zur Steuerung des Datentransfer und können zur Statusüberwachung
herangezogen werden.
%TODO: add valid Register values, 
% adapt bytefield
% show writeable bits (bit0 is writable, etc.)
% show values --> TX STart = 1 oder 0 
\subsubsubsection{TX NexFrame}
Dieses Register gibt an, ob bei der SPI-Master einen neuen Frame starten soll. Das heißt, dass der SPI-Master
die neuen Daten gleich nach den alten Daten transferiert und keine Pause dazwischen einlegt.
Dies ist möglich, da während eines Datentransfers der andere Puffer beschrieben werden kann und nahtlos
während des Transfers ausgetauscht werden kann.

Dieses Register kann von der Software beschrieben werden.

Address-Offset: 
\vspace{5mm}

\begin{bytefield}{32}
    \bitheader{0-31} \\
    \bitbox{32}{TX NewFrame - w}
\end{bytefield}

\subsubsubsection{TX Length}
Dieses Register enthält die Länge an Bytes die vom SPI-Master transferiert werden soll. Die Daten selbst, werden
aus dem \quotes{TX-Data} Speicherbereich gelesen. Diese Länge wird von der Software beschrieben, nachdem sie alle Daten in
den \quotes{TX-Data} Bereich geschrieben hat.

Wird dieses Register nicht beschrieben, kann es zu einem falschen Datentransfer kommen, da die letzte Länge herangezogen wird!

Address-Offset: 

Dieses Register kann von der Software beschrieben werden.
\vspace{5mm}

\begin{bytefield}{32}
    \bitheader{0-31} \\
    \bitbox{32}{TX Length - w}
\end{bytefield}

\subsubsubsection{TX Start}
Dieses Register kündigt den Start des Datentransfers an. Wird dieses beschrieben, werden die Daten nacheinander vom 
\quotes{TX-Data} Bereich ausgelesen und zum SPI-Master gesendet.

Dieses Register darf erst nach dem Setzen der Länge beschrieben werden, da ansonsten eine falscher Wert für die \quotes{TXLength}
genommen wird!

Address-Offset:

Dieses Register kann von der Software beschrieben werden.
\vspace{5mm}

\begin{bytefield}{32}
    \bitheader{0-31} \\
    \bitbox{32}{TX Start - w}
\end{bytefield}

\subsubsubsection{RX Finish}
Dieses Register gibt an, ob ein Datentransfer abgeschlossen ist und die empfangenen Daten vom \quotes{RX Data} Speicherbereich ausgelesen
werden können.

Address-Offset:

Dieses Register kann Nicht von der Software beschrieben werden.
\vspace{5mm}

\begin{bytefield}{32}
    \bitheader{0-31} \\
    \bitbox{32}{RX Finish - r}
\end{bytefield}


\subsubsubsection{TX Transfer}
Dieses Register gibt den momentanen Status des Datentransfers an. Es kann zur Statusüberwachung genutzt werden, um zu prüfen,
ob momentan Daten transferiert werden.

Address-Offset:

Dieses Register kann Nicht von der Software beschrieben werden.
\vspace{5mm}

\begin{bytefield}{32}
    \bitheader{0-31} \\
    \bitbox{32}{TX Transfer - r}
\end{bytefield}

\subsubsubsection{RX Length}
Dieses Register gibt die Länge der empfangenen Daten an. Diese kann genutzt werden um alle gültigen Daten aus dem
\quotes{RX Data} Bereich auszulesen.

Address-Offset:

Dieses Register kann Nicht von der Software beschrieben werden.
\vspace{5mm}

\begin{bytefield}{32}
    \bitheader{0-31} \\
    \bitbox{32}{RX Length - r}
\end{bytefield}

\subsection{Die Kommunikationschnittstellen}
\subsubsection{Avalon Memory Mapped Slave}
Die Avalon Memory Mapped Schnittstelle stellt einen Slave bzw. Host dar und kann von
Avalon Memory Mapped Master bzw. Hosts (wie z.B. der Lightweight HPS-to-FPGA Bridge)
angesteuert werden.

Da vom HPS Daten geschrieben und gelesen werden müssen, ermöglicht diese Schnittstelle
Lese- und Schreibzugriffe. Entsprechend setzt sich die Schnittstelle wie folgt zusammen:

\begin{center}
    \begin{tabular}{|c|c|l|}
        \hline
        Signal Rolle & Bit-Weite & Beschreibung\\
        \hline
        \verb+address+ & 12 Bit & Zum Anlegen der Adressen \\
        \hline
        \verb+read+ & 1 Bit & Gibt einen Lesezugriff an. \\
        \hline
        \verb+readdata+ & 32 Bit & Ergebnis eines Lesezugriffes als "Word" \\
        \hline
        \verb+write+ & 1 Bit & Gibt einen Schreibzugriff an. \\
        \hline
        \verb+writedata+ & 32 Bit & Zu schreibende Daten als "Word"\\
        \hline
    \end{tabular}
\end{center}

\begin{designnote}
    \begin{tcolorbox}[title=Designentscheidungen]
        Die Avalon Schnittstelle wurde zu Beginn absichtlich so schlank wie möglich gehalten. Weitere Signale,
        welche aufgrund der Avalon Spezifikation möglich wären, werden derzeit nicht umgesetzt. Die Idee dahinter
        ist, die Komplexität nur soweit, wie es nötig ist zu erhöhen und dies auch nur Schritt für Schritt zu machen.
        Hiermit soll eine qualitative Umsetzung des Design ermöglicht werden.\\
        Auch Punkte wie Waitstates, etc. werden derzeit nicht beachtet. Wenn das Timing es später benötigen sollte, wird dies
        entsprechend erst dann umgesetzt.\\
        Auch "Avalon MM Properties" werden momentan nicht explizit beschreiben.
    \end{tcolorbox}
\end{designnote}

\begin{designnote}
    \begin{tcolorbox}[title=Gleiche Bitweiten]
        In diesem Design, macht eine gleiche Lese- und Schreibweite natürlich durch aus Sinn. Pro
        Adresse kann ein Wort gelesen und geschrieben werden. Hierbei muss dann nicht auf Bitebene
        gearbeitet werden, was eine einfachere Kommunikation zwischen dem HPS und FPGA ermöglicht.
        Jedoch muss dazu gesagt werden, dass die Avalon Spezifikation, sowieso eine gleiche Bitweite
        zwingend erfordert!
    \end{tcolorbox}
\end{designnote}

\subsubsection{Avalon Streaming Source}
Die Avalon Streaming Quelle dient zum Senden der Pufferdaten zum SPI-Master.
Dies erfolgt byteweise. Die Daten werden von der FSMD aus einem der RAM-Blöcke
gelesen und über die Schnitstelle gesendet. Die Daten kommen hierbei vom HPS (über die Avalon MM Schnittstelle). \\

Die Schnittstelle setzt sich aus folgenden Signalen zusammen:

\begin{center}
    \begin{tabular}{|c|c|l|}
        \hline
        Signal Rolle & Bit-Weite & Beschreibung\\
        \hline
        \verb+readySrc+ & 1 Bit & Backpressure-Signal vom SPI-Master \\
        \hline
        \verb+dataSrc+ & 8 Bit & Daten vom Puffer\\
        \hline
        \verb+validSrc+ & 1 Bit & Gibt an ob die momentanen Daten gültig sind. \\
        \hline
        \verb+startofpacketSrc+ & 1 Bit & Markiert den Anfang eines Packetes \\
        \hline
        \verb+endofpacketSrc+ & 1 Bit & Markiert das Ende eines Packetes\\
        \hline
    \end{tabular}
\end{center}

\begin{designnote}
    \begin{tcolorbox}[title=Backpressure]
        Das \quotes{readySrc}-Signal dient als sog. "Backpressure"-Signal, welches vom SPI-Master angelegt werden kann.
        Dies wird umgesetzt, um dem SPI-Master die Möglichkeit zu geben, das Senden der Daten für eine Zeit zu
        stoppen. Der Vorteil davon ist, dass es zu keinem Datenverlust kommen wird, da der Puffer, bei aktivem Signal
        in eine Art Leerlauf gehen wird und die Daten nicht einfach weitersendet.\\
        Man könnte ''Backpressuring'' auch weglassen. Wenn jedoch der SPI-Master aus diversen Gründen die Daten nicht schnell
        genug bzw. überhaupt verarbeiten kann, würde der Puffer weiterhin senden und es würde zu einem Datenverlust kommen.
    \end{tcolorbox}
\end{designnote}

\begin{designnote}
    \begin{tcolorbox}[title=Datenweite und Valid-Bit]
        Die Datenweite wird mit acht Bits (bytweises Senden der Daten) definiert. Das Valid-Bit dient hierbei um den SPI-Master
        mitteilen zu können, dass die Daten, welche gerade auf der Datenleitung liegen gültig sind und
        er diese hinausschicken kann. Ansonsten könnte der SPI-Master nicht unterscheiden, ob die Daten
        gültig sind und würde permament die Daten schicken.
    \end{tcolorbox}
\end{designnote}

\begin{designnote}
    \begin{tcolorbox}[title=Packets]
        Der Datenverkehr wird mittels einer Form von Paketen umgesetzt. Da auch kontinuierlich Daten gesendet werden
        müssen, dessen Menge sogar die Größe des Puffers übersteigt, wird eine gewisse Information für den SPI-Master
        notwendig sein, damit er weiß wann er seine Signale wieder hinunterziehen kann.\\
        Die alleinige Verwendung des Valid-Signals als Information würde bedeuten, dass der Puffer zwingend immer
        schneller arbeiten muss als der SPI-Master. Wenn dies nicht der Fall ist, besteht die Gefahr, dass auch ungültige
        Daten vom SPI-Master gesendet werden, da das Valid-Signal auf "High" wäre, um den kontinuierlichen Datenstrom zu
        beschreiben.\\
        Um am Anfang zu gewährleisten, dass nur richtige Daten gesendet werden, werden die zwei Paketsignale hinzufügt, um auch
        während eines Paketes, die Valid-Leitung auf "Low" zu ziehen, falls die Daten kurzfristig ungültig werden sollten.
    \end{tcolorbox}
\end{designnote}

\subsubsection{Avalon Streaming Sink}
Die Avalon Streaming Senke dient zum Empfangen von Daten vom SPI-Master.
Dies erfolgt byteweise und wird gemacht, da eine SPI-Kommunikation immer
bidirektional erfolgt und man die empfangenen Daten, während eines Testzyklus
auswerten möchte. Diese Daten werden hierbei wieder in einen der RAM-Blöcke
geschrieben und können danach vom HPS ausgelesen werden (über die Avalon MM Schnittstelle).

Die Schnittstelle setzt sich aus folgenden Signalen zusammen:

\begin{center}
    \begin{tabular}{|c|c|l|}
        \hline
        Signal Rolle & Bit-Weite & Beschreibung\\
        \hline
        \verb+dataSink+ & 8 Bit & Daten vom SPI-Master \\
        \hline
        \verb+validSink+ & 1 Bit & Gibt an, ob die Daten gültig sind\\
        \hline
    \end{tabular}
\end{center}

\begin{designnote}
    \begin{tcolorbox}[title=Designentscheidungen]
        Momentan gibt es nur die Daten und das Valid-Bit. Bei einem Datentransfer werden die erhaltenen Bytes mit den
        gesendeten Bytes abgeglichen und einen RAM-Block geschrieben. Das heißt, dass während dem Senden auf das Valid Bit
        des SPI-Masters geprüft wird und entsprechende Bytes gespeichert werden. Ansonsten nicht.\\
        Da der Datenpuffer um mehr Kapazität verfügt (RAM), wird auch kein "Backpressure"-Mechanismus nötig sein.\\
        Das Markieren der Pakete passiert hierbei auch durch die interne Logik des Datenpuffers. Der SPI-Master muss dies
        nicht nochmal angeben.
    \end{tcolorbox}
\end{designnote}

\subsection{Doppelpufferung}
Der Datenpuffer wird als Doppelpuffer konzeptioniert. Dies bringt den Vorteil, dass ein nahtloser Übergang zwischen
dem Entleeren und Befüllen des Puffers möglich ist und somit größere Datenmengen fließend gesendet werden können.\\

Dies wird mittels zwei logischen Puffern implementiert, welche jeweils eigene RAM-Blöcke in der Umsetzung sind.
Da bei einem Transfer zum SPI-Master auch Daten empfangen werden können, teilt sich ein logischer Puffer in zwei
eigene RAM-Blöcke auf. Ein RAM-Block zum Entleeren und einer zum Befüllen.

Das heißt auch wiederrum, dass es insgesamt vier RAM-Blöcke innerhalb des Puffers gibt.

\begin{fancyfigure}{Doppelpufferung}
    \includegraphics[width=0.935\textwidth]{figures/double_buffer_concept.pdf}
\end{fancyfigure}

\begin{fancyfigure}{RAM-Block-Aufteilung}
    \includegraphics[width=0.935\textwidth]{figures/ram_concept.pdf}
\end{fancyfigure}

\begin{designnote}
    \begin{tcolorbox}[title=Designentscheidungen]
        Es werden insgesamt vier RAM-Blöcke genommen, da das der Datentransfer deutlich leichter zum implementieren wird und
        sich das Timing als einfacher gestaltet.\\
        Es gäbe noch die Option, dass nur ein RAM-Block für das Schreiben und Lesen zum/vom SPI-Master hergenommen wird. Hierbei
        müsste zunächst das erste Register hinausgeschrieben werden, danach kann der erste gelesene Wert abgespeichert werden, usw.
        Zur Hilfe kann ein \quotes{Schleppindex} hergenommen werden.\\
        \\
        Ein möglicher Algorithmus:
        \begin{lstlisting}
            WriteIndex = 0;
            StoreIndex = 0;
            while TransferNotCompleted do
                WriteIndex(WriteIndex);
                StoreIndex = WriteIndex;
                Store(StoreIndex);
                WriteIndex++;
            end
        \end{lstlisting}
    \end{tcolorbox}

\end{designnote}

\newpage
\subsection{Bidirektionale Pufferung}


\subsection{Visualisierung}

HEX-Indicators und LED-Leiter.....
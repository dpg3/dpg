# Structure of the LaTeX Files

I have split the document into a folder per section with files per subsection in them.

Main files of sections are prefixed with _ to mark them (and show them at the top sorted alphabetically).

If a subsection has many subsubsections, I moved it to its own folder.

Each section is imported with the import command, so it can use paths relative to the actual file, rather than to the main file, 
files in deeper nested folders are imported with subimport.

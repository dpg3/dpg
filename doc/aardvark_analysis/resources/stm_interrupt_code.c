if (LL_SPI_ReadReg(SPI2, SR) & SPI_SR_RXNE) {
  //Invert the received SPI byte
  uint8_t dataRead = LL_SPI_ReceiveData8(SPI2);
  LL_SPI_TransmitData8(SPI2, ~dataRead);
}

from aardvark_py import *

# Devs will be an array [len, [devices...]]
# So devs[0] is the number of devices found, devs[1] is an array of found devices
# Find at most 1 device
devs = aa_find_devices(1)

# If no devices were found, error
if devs[0] == 0:
    raise ValueError('No Aardvark found')

#Connect to first device found
aardvark = aa_open(devs[1][0])

# Default SPI Configuration
aa_spi_configure(aardvark, AA_SPI_POL_FALLING_RISING, AA_SPI_PHASE_SAMPLE_SETUP, AA_SPI_BITORDER_MSB)

#Configure the SPI Speed
aa_spi_bitrate(aardvark, 8000)

transmit = array('B', [0, 0xFF, 0xAA, 0xC2, 0xFF])
receive = array('B', [0, 0, 0, 0, 0])

aa_spi_write(aardvark, transmit, receive)

print("Sent")
for i in transmit:
    print(f"\t0x{i:02x}")
print("Received")
for i in receive:
    print(f"\t0x{i:02x}")

aa_close(aardvark)

Important Python API Functions to get started with Aardvark
==========================================

## Connecting

`aa_find_devices(max_devices)`

Parameter max_devices: How many devices are returned at most

Returns: Tuple len 2 with content [len, devices], where devices is an array of length len of device descriptors

`aa_open(device)`

Parameter device: Takes a device descriptor, that was returned by `aa_find_devices`

Returns: An aardvark Handle, needed for all further communications

## Configuring

`aa_spi_configure(handle, polarity, phase, bitorder)`

Configures the SPI, Parameters have constants defined, that start with `AA_SPI_POL`, `AA_SPI_PHASE` and `AA_SPI_BITORDER` respectively.

`aa_spi_bitrate(handle, bitrate)`

Configures the Frequency of the SPI in kHz

## Transmitting

`aa_spi_write(handle, transmit, receive)`

Transmits some Data.

transmit and receive have to be byte arrays (unsigned character).

Byte Arrays can be created with: `array('B', [...])`, where the second argument is a normal python array.

For more info about array, see the [Documentation](https://docs.python.org/3/library/array.html)


## Cleanup / Close

`aa_close(handle)`

Closes the connection to the aardvark

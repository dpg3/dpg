# HPS Web API Documentation

## General concept:

The API is modelled in a REST approach, Http requests are used to create, manipulate, read and delete Objects.
Everything exposed to the client is modelled as an Object.

If data is required, generally Json modies are used.
For details of the json formats, please see the schemas [here](schemas)

- Get requests are used to read objects
- Post request are used to create new objects (e.g. sending data creates a new transfer)
- Put requests are used to modify existing objects (e.g. change configuration)
- Delete requests are used to delete existing objects (e.g. delete transfer when you are done)

## Provided Objects

### List of Interfaces

**Path:** /interfaces

**Accepted Methods:**

- GET
  - Type can be filtered with query parameter ?type={DesiredType}
  - Respnses:
    - 200 OK:
      - Body: [interface list](schemas/interface_list.json)
      - [Example](schemas/interface_list_example.json**
	  
### Interface

**Path:** /interfaces/{IFName}

**Accepted Methods:**

- GET
  - Responses
     - 200 OK:
	   - Body: [interface description](schemas/interface.json)

### Interface capabilities

**Path:** /interfaces/{IFName}/capabilities

**Accepted methods:**

- GET: query Capabilities
  - Responses:
    - 200 OK: 
      - Body: any of [interface capabilities](schemas/capabilities)
    - 404 Not Found: If no Interface IFName exists

Which capabilities schema is used, depends on the type as returned by the Interface List.

### Transfer

**Path:** /interfaces/{IFName}/transfer

**Accepted methods:**

- POST: start a new Transfer
  - Body: [send data](schemas/send_data.json)
  - Responses:
    - 202 Accepted: Success
      - Body: {"transfer": "{TRANSFER_URI}"}
      - TRANSFER_URI is a full URI to a valid Transfer Object
    - 400 Bad Request: If the body is not formatted correctly
      - Body: [error](schemas/error.json)
    - 404 Not Found: If no Interface IFName exists

### Configuration

**Path:** /interfaces/{IFName}/config

**Accepted methods:**

- GET: query current configuration
  - Responses:
    - 200 OK:
      - Body: one of [interface configuration](schemas/configuration)
      - [Example](schemas/configuration/spi_master_configuration_example.json)
    - 404 Not Found: If no Interface IFName exists
- PUT: modify configuration
  - Responses:
    - 202 Accepted: Success
    - 400 Bad Request: If the submitted config has errors
      - Body: [error](schemas/error.json)
    - 404 Not Found: If no Interface IFName exists
   
Exactly which json schema is used is defined by the type of th eInterface as returned by the Interface List

### List of Transfers

**Path:** /interfaces/{IFName}/transfers

**Accepted methods:**

- GET: query all transfers of the Interface
  - Responses
    - 200 OK:
	  - Body: [transfer list](schemas/transfer_list.json)

### Single Transfer

**Path:** /interfaces/{IFName}/transfers/{TransferName}

**Accepted methods:**

- GET: query the transfer
  - Reponses:
    - 200 OK:
      - Body: [transfer status](schemas/transfer_status.json)
    - 404 Not Found: If no Interface IFName exists

- DELETE: delete the transfer
  - Does not abort any communication, that is already in progress.
    A transfer will also time-out if it has not been queried or deleted for 1h
  - Respnses:
    - 404 Not Found: If no Interface IFName exists
	
### Communication

A sample communication can be seen in [this document](sequenzdiagramm_spi_transfer.pdf).

It shows, which communications are required to configure an interface, as well as send and receive data.

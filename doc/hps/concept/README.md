# Disclaimer

This is an early plan, of how the HPS Application will be structured.
This structure might still change significantly.

# Overview

The HPS Application can be structured into three main Blocks:

- The Webservice
- Interface Handling for each Interface (only SPI Master at the Beginning)
- The Driver Interface to the Kernel Driver

Each block will be described in its own Paragraph.

![Block diagram](block_diagram_hps_application.jpg)

## Webservice

This Block will mainly provide the Endpoints for the REST API, with which the PC Application communicates to control the DPG.

It also contains some Logic to convert from the Format used in the web request (probably Json) to an internal Format (Rust structs and enums).

Ideally the Web service should not own any data.

A more detailed Idea of the interface can be seen [here](WebAPI.md)

## Interface Handling

This Block takes Data from the Webservice and manages it.

To support multiple different Interfaces, this Block should be implemented in a way, that makes it easy to create multiple Instances and also have different Instances at the same time (polymorphism).

The Interface Handling will also need to buffer data, that should be sent, if the Hardware cannot accept new Data.
Also it has to buffer received Data until the web client requests it.

## Driver Interface

This Block represents an Interface to the Linux Kernel Driver for the DPG Hardware.

It should encapsulate the Driver API into a safe Rust API.
If unsafe code is needed anywhere, it should be here.

It migh also need to do some more data transformations to bring the data into a form, that the Kernel Driver can understand.

# Additional Things to keep in mind

The Target Platform DE1-SoC only has 1GB of memory, so everything should be programmed with some thought put to saving memory.
Also the CPU is not very powerful (dual core ARM Cortex A-9 @ under 1GHz), so efficient programming is helpful everywhere.

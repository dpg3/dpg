# Chosen Framework

I have chosen Rust with actix_web, mainly because:

- Cargo package management
- Compile time guarantees for thread safety
- I have a little experience with it

# Possible Choices

# Language Choice 1: Rust

Pros:
- Low overhead
- Type safety
- Thread Safe
- Good package management (cargo)
- Pretty easy to cross compile for ARM

Cons:
- More training required
- Pretty new, frameworks not as well proven as other languages

## Framework choices:

### actix_web: https://docs.rs/actix-web/latest/actix_web/index.html

Pros:
- Fast
- Multithreading with Tokio
- Supports Mocking

Cons:
- Can require lots of boilerplate code

### warp: https://docs.rs/warp/0.3.1/warp/

Pros:
- Modern feel, not much boilerplate code

Cons:
- Is pretty new, still in 0.x version
- Does not seem to have mocking support

# Language Choice 2: Java

Pros:
- Very good Frameworks
- Type Safety
- Good package management (maven, gradle)
- No need to cross compile for ARM

cons:
- High Memory usage
- Not as fast as native compiled languages

## Frameworks

### Spring Boot

Pros:
- Very user friendly
- Easy to set up
- Well proven

Cons:
- High memory usage

# Language Choice 3: C++

Pros:
- Type Safety
- Good performance
- Well known by us

Cons:
- No/Bad package management
- Harder to cross compile
- Multithreading support is not as good
- No experience with any framework

